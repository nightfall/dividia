package it.nightfall.dividia.api.ai;

import it.nightfall.dividia.bl.characters.Monster;

public interface IDecision {

	public void applyNextStep(Monster monster, int tpf);

	public void terminate(Monster monster);

	
}
