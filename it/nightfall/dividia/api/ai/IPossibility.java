package it.nightfall.dividia.api.ai;

import it.nightfall.dividia.api.utility.IDecisionScore;
import it.nightfall.dividia.bl.characters.Monster;


public interface IPossibility {

	public IDecisionScore evaluate(Monster monster);
	
}
