package it.nightfall.dividia.api.ai;

import it.nightfall.dividia.bl.characters.Monster;

public interface IThinker {

	void think();
	Monster getMonster();
}
