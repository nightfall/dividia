package it.nightfall.dividia.api.battle;

import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import java.util.List;

public interface IAction{
	IDamageSource getDamageSource();

	IDamageType getActionType();

	List<IAlteredStatusDefinition> getAlteredStatuses();

	int getDamage();

	IElement getElement();

	String getSourceId();
}
