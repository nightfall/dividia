package it.nightfall.dividia.api.battle;

import it.nightfall.dividia.api.battle.executions.IAreaSpellExecution;
import it.nightfall.dividia.api.battle.executions.IBasicAttackExecution;
import it.nightfall.dividia.api.battle.executions.IExecution;
import it.nightfall.dividia.api.battle.executions.IFocusedSpellExecution;
import it.nightfall.dividia.api.battle.executions.ISpellExecution;
import it.nightfall.dividia.api.battle.executions.ITargetAreaSpellExecution;
import it.nightfall.dividia.api.battle.executions.ITargetSpellExecution;

public interface IBattleManager {
	/**
	 * Checks the list of monsters returned by Map for calling its receive
	 * action method.
	 *
	 * @param areaSpellExc The execution to be applied.
	 * @return : False if there are no enemies in the area, true otherwise.
	 */
	boolean checkExecutionInteraction(IAreaSpellExecution areaSpellExc);

	boolean checkExecutionInteraction(IFocusedSpellExecution focusedSpellExc);

	boolean checkExecutionInteraction(ITargetSpellExecution targetSpellExc);

	/**
	 * Checks the list of monsters returned by Map for calling its receive
	 * action method.
	 *
	 * @param basicalAttackExc
	 *                            : The execution to be applied.
	 * @return : false if there are no enemies in the area, true otherwise.
	 */
	boolean checkExecutionInteraction(IBasicAttackExecution basicalAttackExc);

	/**
	 * Checks whether a TargetAreaSpellExecution collides other playing
	 * characters.
	 * A TargetAreaSpellExecution has 2 ways of dealing damage, and this is managed by the exploded argument.
	 *
	 * @param targetAreaSpellExc
	 *                              the SpellExecution
	 * @param exploded
	 *                              It's set to true if the spell just exploded and we need to
	 *                              check which players are caught in the area of the explosion.
	 *                              This is done by treating the execution like a
	 *                              TargetSpellExecution and passing it to the right
	 *                              checkExecutionInteraction method.<br>
	 *                              It's set to false if the spell is still in the air and we need
	 *                              to check whatever it collides any PlayingCharacter.
	 * @return <b>true</b> if at least one player is affected by the explosion, <b>false</b> otherwise.
	 */
	boolean checkExecutionInteraction(ITargetAreaSpellExecution targetAreaSpellExc, boolean exploded);

	void executionSpreading(IExecution spellExc);
}
