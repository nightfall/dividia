package it.nightfall.dividia.api.battle;

import java.io.Serializable;
import java.util.Set;

public interface IDamageSummary extends Serializable {
	Set<String> getAlteredStatus();
	
	int getDamage();

	public IDamageSource getSource();

	public String getSourceId();

	public IDamageType getDamageType();
}
