package it.nightfall.dividia.api.battle;

public interface IDamageType {
	String getName();
}
