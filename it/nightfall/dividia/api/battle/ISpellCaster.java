package it.nightfall.dividia.api.battle;

public interface ISpellCaster {
	void castAreaSpell(String spellName);
	void castFocusedSpell(String spellName);
	void castTargetAreaSpell(String spellName);
	void castTargetSpell(String spellName);
}
