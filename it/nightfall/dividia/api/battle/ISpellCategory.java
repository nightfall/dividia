package it.nightfall.dividia.api.battle;

import java.util.List;

public interface ISpellCategory {
	String getDisplayName();

	String getName();

	List<String> getSpellEvolution();
}
