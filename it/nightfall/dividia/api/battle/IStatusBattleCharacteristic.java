package it.nightfall.dividia.api.battle;

public interface IStatusBattleCharacteristic {
	String getInstanceName();

	String getName();

	boolean doesAllowNegativeValues();
}
