package it.nightfall.dividia.api.battle;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.utility.ISummary;

public interface ISummon extends INetworkExportable{
	void gainExperience(int gainedExperience);

	/**
	 * returns the CastFactor basing on Summon's level.
	 * if summon's level is close to get the next level at witch spell gets improved,
	 * the CastFactor returned is close to the minimum.
	 * When a summon has just jumped from a spell to another the factor is close to 1
	 *
	 */
	double getCastFactor();

	int getCurrentExperience();

	int getTotalLevelExperience();
	
	int getCurrentSpellIndex();
	
	int getExperienceNeededAtNextLevel();
			
	ISpellDefinition getCurrentSpell();

	int getLevel();
	
	@Override
	ISummary getSummary();
}
