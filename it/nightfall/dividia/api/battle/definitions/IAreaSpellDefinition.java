package it.nightfall.dividia.api.battle.definitions;

import it.nightfall.dividia.api.utility.IGameArea;

public interface IAreaSpellDefinition extends ISpellDefinition {
	
	IGameArea getAreaOfEffect();
	
	public float getTravelDuration();

	public float getMaxApproachingDistance();
	
	public float getMinApproachingDistance();
}
