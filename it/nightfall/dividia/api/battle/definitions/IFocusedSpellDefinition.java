package it.nightfall.dividia.api.battle.definitions;

import it.nightfall.dividia.api.utility.IGameArea;

public interface IFocusedSpellDefinition extends ISpellDefinition {
	IGameArea getAreaOfEffect();

	double getSelectionRadius();
	
}
