package it.nightfall.dividia.api.battle.definitions;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.bl.characters.Player;
import java.util.Map;

public interface ISkillDefinition {
	int getCastSpeedAlteration();

	int getCooldownAlteration();

	String getDisplayName();

	Map<IElement, Integer> getElementsResisteceAlteration();

	int getElusionAlteration();

	int getMagicalAttackPowerAlteration();

	int getMagicalDefenceAlteration();

	int getMaxHPAlteration();

	int getMaxMPAlteration();

	int getMaxSP();

	int getMovementSpeedAlteration();

	String getName();

	int getPhisicalAttackPowerAlteration();

	int getPhisicalAttackSpeedAlteration();

	int getPhisicalDefenceAlteration();

	int getSusceptibilityAlteration();

	void modifyStatus(Player player, boolean bool);
}
