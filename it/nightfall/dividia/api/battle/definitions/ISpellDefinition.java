package it.nightfall.dividia.api.battle.definitions;

import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.ISpellCaster;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.characters.Status;
import it.nightfall.dividia.bl.utility.AlteredStatusSuccessRate;
import java.util.List;

public interface ISpellDefinition {
	int getActionDamage(Status playingCharacterStatus);

	List<AlteredStatusSuccessRate> getAlteredStatusesWithRates();

	double getCastDuration();

	float getCooldown();

	String getDisplayName();

	int getLevelRequirement();

	int getMpCost();
	
	boolean isRageSpell();

	String getName();

	double getPlayingCharacterCastDuration(PlayingCharacter character);

	int getSpellDamage();

	int getPowerRatio();
	
	IElement getSpellElement();

	IDamageType getSpellType();

	List<IAlteredStatusDefinition> getSuccededAlteredStatuses();

	boolean isForEnemiesOnly();

	boolean isSummoningSpell();
	
	void cast(ISpellCaster caster);
	
	float getHoldOnDuration();

	public IPossibility getPossibility();
}
