package it.nightfall.dividia.api.battle.definitions;

import it.nightfall.dividia.api.utility.IGameArea;

public interface ITargetAreaSpellDefinition extends ITargetSpellDefinition {
	IGameArea getAreaOfEffect();
}
