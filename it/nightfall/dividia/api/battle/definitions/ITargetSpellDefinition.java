package it.nightfall.dividia.api.battle.definitions;

public interface ITargetSpellDefinition extends ISpellDefinition {
	double getDamageRadius();

	float getDuration();

	int getMovementSpeed();

	double getSelectionRadius();

	boolean isMultiTarget();
}
