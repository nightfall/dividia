package it.nightfall.dividia.api.battle.executions;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;


public interface IAreaExecution {
	public IGameArea getAreaOfEffect();

	public IGamePoint getReferencePoint();

	public IDirection getDirection();
}
