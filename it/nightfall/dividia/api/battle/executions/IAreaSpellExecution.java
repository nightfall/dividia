package it.nightfall.dividia.api.battle.executions;

public interface IAreaSpellExecution extends ISpellExecution, IAreaExecution {
}
