package it.nightfall.dividia.api.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public interface IExecution extends Runnable {
	IAction getAction();

	PlayingCharacter getExecutor();
}
