package it.nightfall.dividia.api.battle.executions;

import it.nightfall.dividia.api.utility.IGamePoint;

public interface IFocusedSpellExecution extends ISpellExecution, IAreaExecution {
	IGamePoint getFocus();
}
