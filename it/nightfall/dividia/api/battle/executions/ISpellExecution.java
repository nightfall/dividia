package it.nightfall.dividia.api.battle.executions;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;

public interface ISpellExecution extends IExecution {
	ISpellDefinition getSpell();
}
