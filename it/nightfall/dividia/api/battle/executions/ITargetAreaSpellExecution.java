package it.nightfall.dividia.api.battle.executions;

import it.nightfall.dividia.api.utility.IGameArea;

public interface ITargetAreaSpellExecution extends ITargetSpellExecution, IAreaExecution {
	IGameArea getInterestedArea();
}
