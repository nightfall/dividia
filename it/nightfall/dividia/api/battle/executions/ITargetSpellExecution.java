package it.nightfall.dividia.api.battle.executions;

import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
public interface ITargetSpellExecution extends ISpellExecution {
	float getSpellDuration();

	IGamePoint getSpellPosition();

	PlayingCharacter getTargetCharacter();
}
