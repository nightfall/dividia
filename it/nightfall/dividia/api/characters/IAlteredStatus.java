package it.nightfall.dividia.api.characters;

import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public interface IAlteredStatus extends Runnable, INetworkExportable{
	IAlteredStatusDefinition getAlteredStatusInfo();

	float getDurationRemaining();

	PlayingCharacter getPlayerAffected();

	PlayingCharacter getPlayerCaster();

	int getRemainingTicks();
	
	void stopAlterations();
}
