package it.nightfall.dividia.api.characters;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.world.IGameMap;
import java.util.Collection;

/**
 * Character Interface.
 *
 * @author Nightfall
 *
 */
public interface ICharacter extends INetworkExportable {
	IGamePoint getPosition();

	IDirection getDirection();

	IGameMap getCurrentMap();

	String getDisplayName();

	void setCurrentMap(IGameMap currentMap);
	
	void setDirection(IDirection direction);
	/**
	 * @return the sectors list which contain the character.
	 */
	Collection<Integer> getSectors();
	
	void initPosition(IGamePoint position);
}
