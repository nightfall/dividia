package it.nightfall.dividia.api.characters;

public interface IInProgressState {
	
	String getName();
}
