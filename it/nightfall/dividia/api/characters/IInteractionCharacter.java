package it.nightfall.dividia.api.characters;

import it.nightfall.dividia.bl.characters.Player;

/**
 *
 * @author Nightfall
 */
public interface IInteractionCharacter extends ICharacter {
	public void speakToPlayer(Player p);
}
