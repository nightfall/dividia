package it.nightfall.dividia.api.characters.definitions;

import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;

public interface IAlteredStatusDefinition {
	int getAlteration(IStatusBattleCharacteristic characteristic);
	
	int getDamage();

	IDamageType getDamageType();

	String getDisplayName();

	long getDuration();

	IElement getElement();

	String getName();

	int getTotalTicks();

	boolean hasRemoveAlterations();

	float getTickInterval();
}
