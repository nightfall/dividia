package it.nightfall.dividia.api.characters.definitions;

import java.util.Map;

public interface IMonsterRaceDefinition extends IRaceDefinition {
	
	int getIntelligenceQuotience();

	float getDefaultPatrolRadius();

	float getDefaultTiredDistance();

	float getDefaultPatrolMaxPause();
	
	float getDefaultPatrolMinPause();
	
	Map<String, Float> getDropRates();
	
	int getDefaultManaReward();
	
	String getWeaponDefinition();
	
	boolean isRecoveringStatus();
	
	int getDefaultSecondsToRespawn();
	
}
