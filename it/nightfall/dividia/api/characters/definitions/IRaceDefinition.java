package it.nightfall.dividia.api.characters.definitions;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import it.nightfall.dividia.api.utility.IQuadraticFunction;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.Collection;

public interface IRaceDefinition {

	String getDisplayName();

	String getName();
	
	IQuadraticFunction getGrowingFunction(IStatusBattleCharacteristic c);
	
	IModifiableInteger getNonGrowingValue(IStatusBattleCharacteristic c);
	
	Collection<String> getStartingSpells();
	
	float getManaSelfRewardFactor();
	
	ICollisionShape getBoundingShape(GuiStatus guiStatus);
	
}
