package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.network.IEventDispatcher;

public interface IEventManager extends IEvents {
	void addListener(IGameMapEvents listener);

	void addListener(IMonsterEvents listener);

	void addListener(INetworkEvents listener);

	void addListener(IPlayerEvents listener);

	void addListener(IPlayingCharacterEvents listener);

	void addListener(IQuestEvents listener);

	void addListener(ISpellEvents listener);

	void addListener(IStatusEvents listener);

	void addListener(IEventDispatcher listener);

	void removeListener(IGameMapEvents listener);

	void removeListener(IMonsterEvents listener);

	void removeListener(INetworkEvents listener);

	void removeListener(IPlayerEvents listener);

	void removeListener(IPlayingCharacterEvents listener);

	void removeListener(IQuestEvents listener);

	void removeListener(ISpellEvents listener);

	void removeListener(IStatusEvents listener);

	void removeListener(IEventDispatcher listener);
	
	void addSingleCallListener(Object listener);
}
