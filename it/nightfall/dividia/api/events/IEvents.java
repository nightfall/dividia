package it.nightfall.dividia.api.events;

public interface IEvents extends IGameMapEvents, IMonsterEvents, INetworkEvents, IPlayerEvents, IPlayingCharacterEvents, IQuestEvents, ISpellEvents, IStatusEvents {
}
