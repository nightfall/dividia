package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.item.IBagOfItems;
import it.nightfall.dividia.api.world.IGameMap;

public interface IGameMapEvents {
	
	public void bagOfItemsDropped(IBagOfItems droppingBag, IGameMap map);

	public void bagOfItemsRemoved(IBagOfItems removingBag, IGameMap map);
	
}
