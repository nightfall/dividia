package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;

public interface IMonsterEvents {
	void monsterDied(Monster monster, Player killer);
	
	void monsterEnteredMap(ISummary summary, IGameMap currentMap);
}
