package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.summaries.IGeneralGameMapSummary;
import it.nightfall.dividia.bl.characters.Player;

public interface INetworkEvents {
	void userDisconnected(final Player player, final String username);

	void mapSummaryReady(final String player, final ISummary gameMapSummary);
	
	void playerSummaryReady(final Player player, final ISummary summary);
	
	void launchAreaSpell(final ISpellDefinition spell, final IDirection castingDirection, final IGamePoint castingPoint, final IGameMap map, final String casterId);
			
	void spellOnCooldown(final Player player, final ISpellDefinition spell);
	
	void ping(final String username);
}
