package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.world.Teleport;
import java.util.Collection;

public interface IPlayerEvents {
	void playerDied(Player player, PlayingCharacter killer);

	void itemPicked(IItem item, Player player);

	void itemAdded(IItem item, Player player);

	void itemRemoved(IItem item, Player player);

	void dialogSuccessfullyCompleted(String dialogID, Player player);

	void teleportUsed(IGameMap previousMap, Player player, Teleport teleport);

	void playerEnteredMap(ISummary playerSummary, IGameMap currentMap);

	void knapsackFull(Player player);

	void focusMoved(String playerId, double x, double y, String focusStatus);

	void focusReset(Player player, double x, double y);

	void spellCastingStart(ISpellDefinition spell, PlayingCharacter executor);

	void armorEquipped(IArmor armor, Player player);

	void bootsEquipped(IBoots boots, Player player);

	void helmetEquipped(IHelmet helmet, Player player);

	void glovesEquipped(IGloves gloves, Player player);

	void necklaceEquipped(INecklace necklace, Player player);

	void ringEquipped(IRing ring, Player player);

	void shieldEquipped(IShield shield, Player player);

	void weaponEquipped(IWeapon weapon, Player player);

	void mysticalSphereEquipped(IMysticalSphere mysticalSphere, Player player);

	void armorUnequipped(Player player);

	void bootsUnequipped(Player player);

	void helmetUnequipped(Player player);

	void glovesUnequipped(Player player);

	void necklaceUnequipped(Player player);

	void ringUnequipped(Player player);

	void shieldUnequipped(Player player);

	void weaponUnequipped(Player player);

	void mysticalSphereUnequipped(Player player);

	void dialogStarted(String id, Player player);

	void dialogsMenuOpened(Collection<IDialog> dialogs, final Player player);
}
