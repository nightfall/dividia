package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.characters.IAlteredStatus;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public interface IPlayingCharacterEvents{
	void characterDied(PlayingCharacter character, PlayingCharacter killer);

	void characterMoved(PlayingCharacter character);
	
	void characterStopped(PlayingCharacter character);

	void damageReceived(PlayingCharacter affected, PlayingCharacter attacker, IDamageSummary summary);
	//TODO: Evento per cambio elemento di status
	//TODO move spell cast here, from playerEvents
	void performBasicAttack(PlayingCharacter executor);

	void alteredStatusApplied(PlayingCharacter playingCharacter, PlayingCharacter executor, IAlteredStatus tempAlteredStatus);
}
