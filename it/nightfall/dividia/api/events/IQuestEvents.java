package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.IGoal;

public interface IQuestEvents {
	void questCompleted(IQuest quest);

	void questActivated(IQuest quest);

	void goalCompleted(IGoal goal);

	void goalActivated(IGoal goal);
	
	void goalUpdated(IGoal goal);
}
