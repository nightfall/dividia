package it.nightfall.dividia.api.events;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public interface IStatusEvents {
	void playingCharacterCharacteristicChanged(PlayingCharacter playingCharacter, IStatusBattleCharacteristic characteristic, int newValue, int delta);
	
	void playerCharacteristicChanged(Player player, IStatusBattleCharacteristic characteristic, int newValue, int delta);
	
	void playerLevelUp(Player player, int newLevel, int maxExperience);
	
	void playerLevelUpByExperience(Player player, int newLevel, int maxExperience);
	
	void playerGainExperience(Player player, int newValue, int delta);
}