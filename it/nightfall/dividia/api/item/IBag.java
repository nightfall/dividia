package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.utility.INetworkExportable;

/**
 * This interface represents a game bag of quantifable items.
 * @author Andrea
 *
 */
public interface IBag extends ISortableItem, IQuantifiable, INetworkExportable{

	/**
	 * Increments quantity by 1.
	 */
	public void incrementQuantity();
	
	/**
	 * Decrements quantity by 1.
	 */
	public void decrementQuantity();
	
	public void incrementBy (int num);
	
	public void decrementBy (int num);
	
	public void addItem(IItem item);
	
	public void removeItem(String id);
	
	public IItem getItem(String id);
}
