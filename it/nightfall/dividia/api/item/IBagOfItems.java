package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.utility.GamePoint;
import java.util.Collection;

public interface IBagOfItems {
	void addItem(IItem item);

	//TODO interfaccia che apre gli oggetti presenti nella bag
	void bagPickUp(Player player);

	int getBagWeight();

	String getID();

	Collection<IItem> getItemList();

	IGamePoint getPosition();
	
	void setPosition(IGamePoint position);

	boolean isEmpty();
}
