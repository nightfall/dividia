package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IDevaOrbDefinition;

/**
 * This interface describes a deva orb.
 *
 * @author Nightfall
 *
 */
public interface IDevaOrb extends ISellableItem {
	@Override
	public IDevaOrbDefinition getItemDefinition();
}
