package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IDevaOrbDefinition;

/**
 * This interface represent a bag containing deva orbs.
 *
 * @author Andrea
 *
 */
public interface IDevaOrbsBag extends IBag {
	public IDevaOrbDefinition getDevaOrb();
}
