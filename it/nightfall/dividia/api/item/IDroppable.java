package it.nightfall.dividia.api.item;

import it.nightfall.dividia.bl.item.BagOfItems;

public interface IDroppable extends IItem {
	public void drop(BagOfItems bag, IInventory inventory);
}
