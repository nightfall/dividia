package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.utility.INetworkImportable;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.ISaveState;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public interface IEquippedItems extends INetworkExportable, INetworkImportable, ISaveState {
	IArmor getArmor();

	IBoots getBoots();

	IGloves getGloves();

	IHelmet getHelmet();

	IMysticalSphere getMysticalSphere();

	INecklace getNecklace();

	IRing getRing();

	IShield getShield();

	IWeapon getWeapon();

	@Override
	ISummary getSummary();

	boolean hasArmorEquipped();

	boolean hasBootsEquipped();

	boolean hasGlovesEquipped();

	boolean hasHelmetEquipped();

	// EQUIPMENT INFO
	boolean hasMysticalSphereEquipped();

	boolean hasNecklaceEquipped();

	boolean hasRingEquipped();

	boolean hasShieldEquipped();

	void setArmor(IArmor armor);

	void setBoots(IBoots boots);

	void setGloves(IGloves gloves);

	void setHelmet(IHelmet helmet);

	void setMysticalSphere(IMysticalSphere mysticalSphere);

	void setNecklace(INecklace necklace);

	void setRing(IRing ring);

	void setShield(IShield shield);

	void setWeapon(IWeapon weapon);

	void equipAfterLoad(PlayingCharacter playingCharater);
}
