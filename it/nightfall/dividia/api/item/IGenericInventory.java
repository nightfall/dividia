package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;

public interface IGenericInventory {
	/*
	 * INVENTORY INTERACTION METHODS
	 */
	/**
	 * Adds an item to the inventory.
	 *
	 * @param item
	 */
	void registerToInventory(IItem item);

	void addItem(IEquipmentItem item);

	void addItem(IKeyItem item);

	void addItem(IUtilityItem item);

	void addItem(IMysticalSphere item);

	void addItem(IVial item);

	void addItem(ITropoGem item);

	void addItem(IDevaOrb item);

	/**
	 * Removes an item from the inventory.
	 *
	 * @param item
	 */
	void removeFromInventory(IItem item);

	void removeItem(IEquipmentItem item);

	void removeItem(IKeyItem item);

	void removeItem(IUtilityItem item);

	void removeItem(IMysticalSphere item);

	void removeItem(IVial item);

	void removeItem(ITropoGem item);

	void removeItem(IDevaOrb item);
}
