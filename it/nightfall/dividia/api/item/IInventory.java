package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.item.definitions.IKnapsackDefinition;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.bl.item.BagOfItems;
import it.nightfall.dividia.bl.item.storages.DevaOrbStorage;
import it.nightfall.dividia.bl.item.storages.DroppableStorage;
import it.nightfall.dividia.bl.item.storages.EquipmentStorage;
import it.nightfall.dividia.bl.item.storages.IDStorage;
import it.nightfall.dividia.bl.item.storages.KeyStorage;
import it.nightfall.dividia.bl.item.storages.MysticalSphereStorage;
import it.nightfall.dividia.bl.item.storages.TropoGemStorage;
import it.nightfall.dividia.bl.item.storages.UtilityStorage;
import it.nightfall.dividia.bl.item.storages.VialStorage;
import java.util.Collection;

/**
 * This interface represent an inventory, composed of item storages.
 *
 * @author Nightfall
 *
 */
public interface IInventory extends IGenericInventory, INetworkExportable {
	/*
	 * STORAGE GET METHODS
	 */
	/**
	 * Returns inventory's equipment storage.
	 *
	 * @return EquipmentStorage
	 */
	EquipmentStorage getEquipmentStorage();

	/**
	 * Returns inventory's utility storage.
	 *
	 * @return UtilityStorage
	 */
	UtilityStorage getUtilityStorage();

	/**
	 * Returns inventory's key storage.
	 *
	 * @return KeyStorage
	 */
	KeyStorage getKeyStorage();

	/**
	 * Returns inventory's mystical sphere storage.
	 *
	 * @return MysticalSphereStorage
	 */
	MysticalSphereStorage getMysticalSphereStorage();

	/**
	 * Returns inventory's deva orb storage.
	 *
	 * @return DevaOrbStorage
	 */
	DevaOrbStorage getDevaOrbStorage();

	/**
	 * Returns inventory's tropo gem storage.
	 *
	 * @return TropoGemStorage
	 */
	TropoGemStorage getTropoGemStorage();

	/**
	 * Returns inventory's id storage.
	 *
	 * @return IDStorage
	 */
	IDStorage getIDStorage();

	/**
	 * Returns inventory's vial storage.
	 *
	 * @return VialStorage
	 */
	VialStorage getVialStorage();

	/**
	 * Returns inventory's droppable storage.
	 *
	 * @return DroppableStorage
	 */
	DroppableStorage getDroppableStorage();

	/*
	 * GET METHODS 
	 */
	/**
	 * Returns inventory's knapsack.
	 *
	 * @return IKnapsack
	 */
	IKnapsackDefinition getKnapsack();

	/**
	 * Returns current weight of the inventory.
	 *
	 * @return
	 */
	int getCurrentWeight();

	/**
	 * Drops all the items contained in the inventory's collection of items to drop, in a bag of items.
	 *
	 * @return BagOfItems
	 */
	BagOfItems dropItems(Collection<String> items);

	/*
	 * UTILITY METHODS
	 */
	/**
	 * Returns true if the specified item is contained in inventory, false otherwise.
	 *
	 * @param name : Name of the item to be searched.
	 * @return
	 */
	boolean containsItem(String name);

	/**
	 * Checks if an item can be switched with an equippedItem respecting bag capacity
	 *
	 * @param equippedItem : item that will beequipped
	 * @param equippingItem : item that will be moved from equip to inventory
	 * @return true if bag capacity is respected
	 */
	boolean isCapacityEnoughToEquipItem(IItem equippedItem, IItem equippingItem);

	/**
	 * Changes the inventory's knapsack from the current to the passed one.
	 *
	 * @param knapsack
	 */
	void switchKnapsack(IKnapsackDefinition knapsack);

	/**
	 * Check if an item can be picked.
	 *
	 * @param itemWeight
	 * @return true if the item can be picked, false otherwise.
	 */
	boolean canPickUpItem(int itemWeight);
	
	public void removeKeyItemFromDefinition(IItemDefinition item);
}
