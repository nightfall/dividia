package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.utility.INetworkImportable;
import it.nightfall.dividia.api.utility.io.ISaveState;
import it.nightfall.dividia.bl.characters.Player;

/**
 * This interface describes a generic game item.
 *
 * @author Nightfall
 *
 */
public interface IItem extends ISortableItem, ISaveState, INetworkExportable, INetworkImportable {
	/**
	 * This method allows the player to use a game item.
	 *
	 * @param player
	 */
	void use(Player player);

	/**
	 * Returning the ID of an item in the current game.
	 *
	 * @return int
	 */
	@Override
	String getId();

	/**
	 * With this method item is picked up by the player.
	 *
	 * @param player
	 */
	void pickUp(Player player);

	/**
	 * Returns the pick up date of the item.
	 *
	 * @return GregorianCalendar
	 */
	long getPickUpDate();

	/**
	 * Returns the item definition.
	 *
	 * @return IItemDefinition
	 */
	@Override
	IItemDefinition getItemDefinition();
	
	void addToInventory(IGenericInventory intentory);
	
	void removeFromInventory(IGenericInventory inventory);
}
