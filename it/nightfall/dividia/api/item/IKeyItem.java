package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IKeyItemDefinition;

/**
 * This interface describes a key item.
 *
 * @author Andrea
 *
 */
public interface IKeyItem extends IItem {
	@Override
	public IKeyItemDefinition getItemDefinition();
}
