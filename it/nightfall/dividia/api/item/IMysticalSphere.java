package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.equipment_items.definitions.IMysticalSphereDefinition;

/**
 * This interface describe a mystical sphere.
 * @author Nightfall
 *
 */
public interface IMysticalSphere extends IItem {

	/**
	 * Increments the number of vials that the mystical sphere can cointains.
	 */
	public void incrementMaxVials();

	/**
	 * Does a full recharge of the mystical sphere : current vials returns to
	 * their maximum value.
	 */
	public void fullRecharge();

	/**
	 * Does a recharge of a part of the total vials.
	 *
	 * @param vialsNum : Quantity of vials recharged.
	 */
	public void rechargeOf(int vialsNum);

	/**
	 * Returns current maximum vials capacity of the mystical sphere.
	 * @return int
	 */
	int getCurrentMaxVialsCapacity();

	/**
	 * Returns the maximum vials capacity of the mystical sphere.
	 * @return int
	 */
	int getTotalVialsCapacity();

	/**
	 * Returns the delay time needed to equip a specific mystical sphere.
	 * @return
	 */
	int getEquipDelayTime();

	/**
	 * Returns the remaning vials of a mystical sphere.
	 * @return
	 */
	int getCurrentVials();

	@Override
	IMysticalSphereDefinition getItemDefinition();
}
