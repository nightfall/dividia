package it.nightfall.dividia.api.item;

/**
 * This interface represent all objects that are quantifiable through numbers.
 * All the classes that implement this interface can be grouped by the number of
 * equal objects.
 *
 * @author falko91
 *
 */
public interface IQuantifiable {
	/**
	 * Returns the quantity of an object.
	 *
	 * @return int
	 */
	public int getQuantity();
}
