package it.nightfall.dividia.api.item;

public interface IRemovable extends ISellableItem, IDroppable {
}
