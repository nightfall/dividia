package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.ISellableItemDefinition;

/**
 * This interface describes an item that can be sold which has a selling price
 * obtained by devaluating its original price.
 *
 * @author Andrea
 *
 */
public interface ISellableItem extends IItem {
	@Override
	ISellableItemDefinition getItemDefinition();
}
