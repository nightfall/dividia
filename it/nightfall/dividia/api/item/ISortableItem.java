package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;

public interface ISortableItem {
	String getId();
	String getName();
	String getType();
	int getPrice();
	int getWeight();
	int getQuantity();
	IItemDefinition getItemDefinition();
}
