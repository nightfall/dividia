package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.utility.INetworkExportable;

public interface IStorage<Type> extends INetworkExportable {
	public void add(Type obj);

	public void remove(Type obj);

	public boolean contains(Type obj);
	
	public boolean isEmpty();
}
