package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.ITropoGemDefinition;

public interface ITropoGem extends ISellableItem {
	@Override
	public ITropoGemDefinition getItemDefinition();
}
