package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.ITropoGemDefinition;

/**
 * This interface represents a tropo gems bag.
 *
 * @author Andrea
 *
 */
public interface ITropoGemsBag extends IBag {
	public ITropoGemDefinition getTropoGem();
}
