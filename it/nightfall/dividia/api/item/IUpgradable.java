package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.equipment_items.definitions.IEquipmentItemDefinition;

/**
 * This interface describes an upgradable item. An upgrade is an evolution of a
 * specific item. When an item is upgraded only its definition changes.
 *
 * @author Andrea
 *
 */
public interface IUpgradable {
	/**
	 * Upgrade an item by changing its definition.
	 *
	 * @param name
	 */
	void upgrade(IEquipmentItemDefinition newItemDefinition);
}
