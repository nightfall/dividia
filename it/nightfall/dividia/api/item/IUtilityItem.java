package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IUtilityItemDefinition;

/**
 * This interface represent all the utility item.
 *
 * @author Nightfall
 *
 */
public interface IUtilityItem extends IItem {
	@Override
	public IUtilityItemDefinition getItemDefinition();
}
