package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IVialDefinition;

/**
 * This interface describe a vial.
 *
 * @author falko91
 *
 */
public interface IVial extends ISellableItem {
	/**
	 * Returns the specific Vial Definition of a vial.
	 *
	 * @return
	 */
	@Override
	public IVialDefinition getItemDefinition();

	/**
	 * Returns the associated mystical sphere of a specific vial.
	 *
	 * @return
	 */
	public String getAssociatedMysticalSphere();
}
