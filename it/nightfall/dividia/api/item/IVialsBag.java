package it.nightfall.dividia.api.item;

import it.nightfall.dividia.api.item.definitions.IVialDefinition;

/**
 * This interface describes a vials bag.
 *
 * @author Andrea
 *
 */
public interface IVialsBag extends IBag {
	/**
	 * Returns the vial contained in a bag.
	 *
	 * @return IVial
	 */
	public IVialDefinition getVial();
}
