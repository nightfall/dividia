package it.nightfall.dividia.api.item.definitions;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.item.IDevaOrb;

/**
 * This interface describes a Deva Orb. A deva orb is a special item that can be associated with a weapon, giving a
 * specific element to it.
 * @author Nightfall
 *
 */
public interface IDevaOrbDefinition extends ISellableItemDefinition {

	/**
	 * Returns the deva orb's element.
	 * @return IElement
	 */
	public IElement getElement();
	
	@Override
	public IDevaOrb getItemFromDefinition();
}
