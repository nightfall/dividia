package it.nightfall.dividia.api.item.definitions;

import it.nightfall.dividia.api.item.IItem;

/**
 * This class is a generic item definition.
 * 
 * @author Nightfall
 * 
 */
public interface IItemDefinition {

	/**
	 * Returns the id name of an item.
	 * 
	 * @return String
	 */
	public String getName();

	/**
	 * Returns the display name of an item.
	 * 
	 * @return String
	 */
	public String getDisplayName();

	/**
	 * Returns the weight of an item.
	 * 
	 * @return int
	 */
	public int getWeight();

	/**
	 * Returns a short description of an item.
	 * 
	 * @return String
	 */
	public String getDescription();
	
	/**
	 * Creates and returns a specific item from its definition.
	 * @return IItem
	 */
	public IItem getItemFromDefinition();
	
}
