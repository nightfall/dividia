package it.nightfall.dividia.api.item.definitions;

import it.nightfall.dividia.api.item.IKeyItem;

/**
 * Interface representing a definition of a key item.  A Key Item is an item that can be used only in the right situations and when required.
 * @author Nightfall
 *
 */
public interface IKeyItemDefinition extends IItemDefinition {

	/**
	 * Creates and returns a new key item from its definition.
	 * @return IKeyItem
	 */
	@Override
	public IKeyItem getItemFromDefinition();
}
