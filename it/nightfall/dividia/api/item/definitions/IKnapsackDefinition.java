package it.nightfall.dividia.api.item.definitions;

/**
 * This interface describe a knapsack. A knapsack indicates the max weight
 * player can have in his own inventory.
 *
 * @author Nightfall
 *
 */
public interface IKnapsackDefinition {
	/**
	 * Returns knapstack's name.
	 *
	 * @return String
	 */
	public String getName();

	public String getDisplayName();

	/**
	 * Returns an integer that represent knapsack's max weight.
	 *
	 * @return
	 */
	public int getMaxWeight();
}
