package it.nightfall.dividia.api.item.definitions;

/**
 * This interface represents all sellable item, by introducing their price and
 * their devalue rate.
 * 
 * @author Nightfall
 * 
 */
public interface ISellableItemDefinition extends IItemDefinition{

	/**
	 * Returns item's price.
	 * 
	 * @return int
	 */
	int getPrice();

	/**
	 * Returns item's price devaluated.
	 */
	int devaluePrice();

	/**
	 * Returns the devalue rate of an item.
	 * 
	 * @return
	 */
	double getDevalueRate();

}
