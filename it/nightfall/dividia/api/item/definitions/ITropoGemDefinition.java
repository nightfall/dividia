package it.nightfall.dividia.api.item.definitions;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.bl.item.equipment_items.EquipmentItemType;
import java.util.Map;

/**
 * This interface describes a generic tropo gem. A tropo gem is a special item that can be associated with an
 * equipment item and can alterates player's stats.
 * @author Nightfall
 *
 */
public interface ITropoGemDefinition extends ISellableItemDefinition {

	/**
	 * Returns the tropo gem type (the equipment item that it can be associated to).
	 * @return
	 */
	public EquipmentItemType getType();
	
	public Map <IStatusBattleCharacteristic, Integer> getStatsAlterationsMap();
	
	@Override
	public ITropoGem getItemFromDefinition();
}
