package it.nightfall.dividia.api.item.definitions;

import it.nightfall.dividia.api.item.IUtilityItem;

/**
 * This interface represent the definition of a generic utility item. A utility
 * item is a special game item that helps the player in solving enigmas and
 * continue the game adventure.
 *
 * @author falko91
 *
 */
public interface IUtilityItemDefinition extends IItemDefinition {
	@Override
	IUtilityItem getItemFromDefinition();
}
