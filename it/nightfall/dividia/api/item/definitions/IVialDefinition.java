package it.nightfall.dividia.api.item.definitions;

import it.nightfall.dividia.api.item.IVial;

/**
 * This interface represent a vial definition, item containing energy for mystical sphere usage.
 * @author falko91
 *
 */
public interface IVialDefinition extends ISellableItemDefinition {
	
	/**
	 * Creates and returns a vial from its definition.
	 * @return
	 */
	@Override
	public IVial getItemFromDefinition();
	
	/**
	 * Returns the name of the vial's associated mystical sphere.
	 * @return
	 */
	public String getAssociatedMysticalSphere();
}
