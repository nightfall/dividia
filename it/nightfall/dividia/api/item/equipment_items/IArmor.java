package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.item.equipment_items.definitions.IArmorDefinition;

public interface IArmor extends IEquipmentItem {
	@Override
	public IArmorDefinition getItemDefinition();
}
