package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.item.equipment_items.definitions.IBootsDefinition;

public interface IBoots extends IEquipmentItem {

	@Override
	public IBootsDefinition getItemDefinition();
	
}
