package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.item.ISellableItem;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.IUpgradable;
import it.nightfall.dividia.api.item.equipment_items.definitions.IEquipmentItemDefinition;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import java.util.Map;

/**
 * Interface representing an equipment item.
 *
 * @author Andrea
 *
 */
public interface IEquipmentItem extends ISellableItem, IUpgradable {
	/**
	 * Returns equipment item's level requirement.
	 *
	 * @return int
	 */
	public int getLevelRequirement();

	/**
	 * Returns the tropo gem associated to an equipment item.
	 *
	 * @return
	 */
	public ITropoGem getTropoGem();

	/**
	 * Assign a tropo gem to an equipment item.
	 *
	 * @param tropoGem
	 */
	public void setTropoGem(ITropoGem tropoGem);
	
	public Map<IStatusBattleCharacteristic, IModifiableInteger> getFinalStatsAlterations();
	
	@Override
	public IEquipmentItemDefinition getItemDefinition();
}
