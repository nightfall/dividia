package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.item.equipment_items.definitions.IGlovesDefinition;

public interface IGloves extends IEquipmentItem {

	@Override
	public IGlovesDefinition getItemDefinition();
	
}
