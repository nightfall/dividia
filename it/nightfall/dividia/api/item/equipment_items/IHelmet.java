package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.item.equipment_items.definitions.IHelmetDefinition;

public interface IHelmet extends IEquipmentItem {

	@Override
	public IHelmetDefinition getItemDefinition();
	
}
