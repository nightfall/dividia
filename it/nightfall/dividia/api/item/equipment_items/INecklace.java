package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.item.equipment_items.definitions.INecklaceDefinition;

public interface INecklace extends IEquipmentItem {

	@Override
	public INecklaceDefinition getItemDefinition();

}
