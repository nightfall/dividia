package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.item.equipment_items.definitions.IRingDefinition;

public interface IRing extends IEquipmentItem {

	@Override
	public IRingDefinition getItemDefinition();

}
