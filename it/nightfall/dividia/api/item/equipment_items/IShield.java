package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.item.equipment_items.definitions.IShieldDefinition;

public interface IShield extends IEquipmentItem {

	@Override
	public IShieldDefinition getItemDefinition();
	
}
