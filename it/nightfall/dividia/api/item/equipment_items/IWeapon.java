package it.nightfall.dividia.api.item.equipment_items;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.equipment_items.definitions.IWeaponDefinition;
import it.nightfall.dividia.bl.utility.AlteredStatusSuccessRate;
import it.nightfall.dividia.bl.utility.GameArea;
import java.util.List;


public interface IWeapon extends IEquipmentItem {

	@Override
	public IWeaponDefinition getItemDefinition();

	
	/**
	 * Returns the weapon's attack range.
	 * @return GameArea
	 */
	public GameArea getAttackRange();

	/**
	 * Returns a list of weapon's altered statuses with their succes rate.
	 * @return List<AlteredStatusSuccessRate>
	 */
	public List<AlteredStatusSuccessRate> getWeaponAlteredStatuses();

	/**
	 * Returns weapon's element name.
	 * @return String
	 */
	public IElement getWeaponElement();

	/**
	 * Returns a list of altered statuses that had effect (based on altered statuses success rate).
	 * @return List<AlteredStatusSuccessRate>
	 */
	public List<IAlteredStatusDefinition> getSuccededAlteredStatuses();

	/**
	 * Sets the weapon's deva orb with the one passed. This method can be called only once per weapon.
	 * @param devaOrb
	 */
	void devaOrbFusion(IDevaOrb devaOrb);
}

