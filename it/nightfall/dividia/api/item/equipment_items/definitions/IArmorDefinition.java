package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IArmor;

/**
 * This interface represents an armor. It describes all the stats alterations the armor can do.
 * @author Nightfall
 *
 */
public interface IArmorDefinition extends IEquipmentItemDefinition {

	@Override
	public IArmor getItemFromDefinition();
}
