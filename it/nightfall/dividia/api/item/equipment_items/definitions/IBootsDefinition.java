package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IBoots;

/**
 * This interface represents a pair of boots. It describes all the stats alterations the boots can do.
 * @author Nightfall
 *
 */
public interface IBootsDefinition extends IEquipmentItemDefinition {

	@Override
	public IBoots getItemFromDefinition();
}
