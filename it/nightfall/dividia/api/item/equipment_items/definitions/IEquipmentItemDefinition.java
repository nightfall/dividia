package it.nightfall.dividia.api.item.equipment_items.definitions;
/**
 * Interface representing an equipment item definition.
 */
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.item.definitions.ISellableItemDefinition;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public interface IEquipmentItemDefinition extends IItemDefinition,ISellableItemDefinition {
	
	/**
	 * Returns a set of user races, which represents the races that can use the equipment item.
	 * @return Set<String>
	 */
	public Set<String> getUserRaces();

	/**
	 * Returns the level required for using the equipment item.
	 * @return int
	 */
	public int getLevelRequirement();

	/**
	 * Returns a map having element name as key and resistences to a specific element (expressed with an integer) as values.
	 * @return
	 */
	public Map<IElement, IModifiableInteger> getElementResistancesAlteration();
	
	public HashMap<IStatusBattleCharacteristic, IModifiableInteger> getStatsAlterations();
	
	@Override
	public IEquipmentItem getItemFromDefinition();
}
