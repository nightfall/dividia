package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IGloves;

/**
 * This interface represents a pair of gloves. It describes all the stats alterations the gloves can do.
 * @author Nightfall
 *
 */
public interface IGlovesDefinition extends IEquipmentItemDefinition {
	
	@Override
	public IGloves getItemFromDefinition();
}
