package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IHelmet;

/**
 * This interface represents a helmet. It describes all the stats alterations the helmet can do.
 * @author Nightfall
 *
 */
public interface IHelmetDefinition extends IEquipmentItemDefinition {

	@Override
	public IHelmet getItemFromDefinition();

}
