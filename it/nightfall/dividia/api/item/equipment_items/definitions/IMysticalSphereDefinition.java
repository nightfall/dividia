package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;

/**
 * This interface represent the definition of a mystical sphere.A mystical
 * sphere is a special item that allows the player to cast a particular spell
 * without decreasing his mp, but using mystical sphere's vials.
 *
 * @author Nightfall
 *
 */
public interface IMysticalSphereDefinition extends IItemDefinition {
	/**
	 * Creates and returns a mystical sphere from its definition.
	 *
	 * @return IMysticalSphere
	 */
	@Override
	IMysticalSphere getItemFromDefinition();
	
	int getTotalVialsCapacity();
	
	int getEquipDelayTime();
}
