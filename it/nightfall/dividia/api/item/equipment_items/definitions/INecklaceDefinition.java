package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.INecklace;

/**
 * This interface represents a necklace. It describes all the stats alterations the necklace can do.
 * @author Nightfall
 *
 */
public interface INecklaceDefinition extends IEquipmentItemDefinition {

	@Override
	public INecklace getItemFromDefinition();
}
