package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IRing;

/**
 * This interface represents a ring. It describes all the stats alterations the ring can do.
 * @author Nightfall
 *
 */
public interface IRingDefinition extends IEquipmentItemDefinition {
	
	@Override
	public IRing getItemFromDefinition();
}
