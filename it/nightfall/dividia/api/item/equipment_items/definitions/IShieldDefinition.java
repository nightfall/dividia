package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IShield;


/**
 * This interface represents a shield. It describes all the stats alterations the shield can do.
 * @author Nightfall
 *
 */
public interface IShieldDefinition extends IEquipmentItemDefinition {

	@Override
	public IShield getItemFromDefinition();

}
