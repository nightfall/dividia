package it.nightfall.dividia.api.item.equipment_items.definitions;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.bl.utility.AlteredStatusSuccessRate;
import it.nightfall.dividia.bl.utility.GameArea;
import java.util.List;
/**
 * This interface represents a weapon. It describes all the stats alterations the weapon can do.
 * @author Nightfall
 *
 */
public interface IWeaponDefinition extends IEquipmentItemDefinition {
	
		
	/**
	 * Returns the weapon's attack range.
	 * @return GameArea
	 */
	public GameArea getAttackRange();

	/**
	 * Returns a list of weapon's altered statuses with their succes rate.
	 * @return List<AlteredStatusSuccessRate>
	 */
	public List<AlteredStatusSuccessRate> getWeaponAlteredStatuses();

	/**
	 * Returns weapon's element name.
	 * @return String
	 */
	public IElement getWeaponElement();

	/**
	 * Returns a list of altered statuses that had effect (based on altered statuses success rate).
	 * @return List<AlteredStatusSuccessRate>
	 */
	public List<IAlteredStatusDefinition> getSuccededAlteredStatuses();
	
	@Override
	public IWeapon getItemFromDefinition();
}
