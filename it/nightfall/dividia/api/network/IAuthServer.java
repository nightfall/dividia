package it.nightfall.dividia.api.network;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IAuthServer extends Remote {
	void authorize(String username, String oneTimeKey, IClientEventListener clientEventsManager) throws RemoteException;
}
