package it.nightfall.dividia.api.network;

public interface IBLDispatcher extends IMessagesDispatcher {
	void addMessage(INetworkMessage message);
}
