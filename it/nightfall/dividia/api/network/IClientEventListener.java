package it.nightfall.dividia.api.network;

import it.nightfall.dividia.api.battle.IDamageSource;
import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;

public interface IClientEventListener extends Remote {
	String getUsername() throws RemoteException;

	void userDisconnected(final String username) throws RemoteException;

	void sessionAuthenticated(IIntentionManager intentionManager, ISummary savegames) throws RemoteException;

	void sessionRefused() throws RemoteException;

	void monsterDied(final String monster, final String killer) throws RemoteException;

	void playerDied(final String player, final String killer) throws RemoteException;

	void itemPicked(final ISummary item) throws RemoteException;

	void teleportUsed(final String player, final String nextMap) throws RemoteException;

	void playerEnteredMap(final ISummary playerSummary) throws RemoteException;

	void characterMoved(final String character, final double x, final double y, final double direction) throws RemoteException;

	void focusMoved(final String character, final double x, final double y, final String focusStatus) throws RemoteException;

	void characterStopped(final String character) throws RemoteException;

	void damageReceived(final String affected, final String attacker, final int damage, final IDamageSource source, final String attackId, final IDamageType damageType) throws RemoteException;

	void questCompleted(final String quest) throws RemoteException;

	void questActivated(final String quest) throws RemoteException;

	void goalCompleted(final String goal) throws RemoteException;

	void goalActivated(final String goal) throws RemoteException;

	void goalUpdated(final String goal) throws RemoteException;

	void playingCharacterCharacteristicChanged(final String playingCharacter, final String characteristic, final int newValue, final int delta) throws RemoteException;

	void playerCharacteristicChanged(final String player, final String characteristic, final int newValue, final int delta) throws RemoteException;

	void playerLevelUp(final String player, final int newLevel, final int maxExperience) throws RemoteException;

	void playerGainExperience(final String player, final int newValue, final int delta) throws RemoteException;

	void receiveMapSummary(final String player, final ISummary mapSummary) throws RemoteException;

	void playerSummaryReceived(final String player, final ISummary playerSummary) throws RemoteException;

	void spellOnCooldown(final String spell) throws RemoteException;

	void launchAreaSpell(final String spell, final double directionX, final double directionY, final double castingX, final double castingY, final String casterId) throws RemoteException;

	void ping() throws RemoteException;

	void focusReset(final String playerIdM, final double x, final double y) throws RemoteException;

	void spellCastingStart(final String spellName, final String characterId, final String raceId) throws RemoteException;

	void performBasicAttack(String id) throws RemoteException;

	void armorEquipped(ISummary item) throws RemoteException;

	void bootsEquipped(ISummary item) throws RemoteException;

	void helmetEquipped(ISummary item) throws RemoteException;

	void glovesEquipped(ISummary item) throws RemoteException;

	void necklaceEquipped(ISummary item) throws RemoteException;

	void ringEquipped(ISummary item) throws RemoteException;

	void shieldEquipped(ISummary item) throws RemoteException;

	void weaponEquipped(ISummary item) throws RemoteException;

	void mysticalSphereEquipped(ISummary item) throws RemoteException;

	void armorUnequipped() throws RemoteException;

	void bootsUnequipped() throws RemoteException;

	void helmetUnequipped() throws RemoteException;

	void glovesUnequipped() throws RemoteException;

	void necklaceUnequipped() throws RemoteException;

	void ringUnequipped() throws RemoteException;

	void shieldUnequipped() throws RemoteException;

	void weaponUnequipped() throws RemoteException;

	void mysticalSphereUnequipped() throws RemoteException;

	void alteredStatusApplied(final String affected, final String executor, final String name) throws RemoteException;

	void playerLevelUpByExperience(String id, int newLevel, int maxExperience) throws RemoteException;

	void monsterEnteredMap(ISummary summary, String name) throws RemoteException;

	void bagOfItemsDropped(float positionX, float positionY, String bagId) throws RemoteException;

	void bagOfItemsRremoved(String id) throws RemoteException;

	void itemAdded(ISummary item) throws RemoteException;

	void itemRemoved(String itemId) throws RemoteException;

	void dialogStarted(String id) throws RemoteException;

	void dialogsMenuOpened(Collection<String> dialogIds) throws RemoteException;
}
