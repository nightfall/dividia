package it.nightfall.dividia.api.network;

public interface IConnectionState {
	String[] getRequirements();
	int getParamLength();
}
