package it.nightfall.dividia.api.network;

public interface IDCPServer extends Runnable{
	void authorizeSession(String username, String uuid);

	boolean autheticateSession(String username, String uuid);

	String hashPassword(String password);

	boolean nickExists(String nickname);

	boolean passwordValid(String username, String password);

	void registerUser(String username, String password, String nickname);

	void sessionTerminated(String identifier);

	boolean userExists(String username);
	
	void flushUsersDatabase();
}
