package it.nightfall.dividia.api.network;

public interface IDCPSession extends Runnable {
	void disconnect();
}
