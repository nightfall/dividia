package it.nightfall.dividia.api.network;

import it.nightfall.dividia.api.events.IEvents;


public interface IEventDispatcher extends IEvents{
	void addClient(String username, IClientEventListener listener);
	IBLDispatcher getClientDispatcher(String username);
}
