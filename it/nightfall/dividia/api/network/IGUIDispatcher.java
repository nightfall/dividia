package it.nightfall.dividia.api.network;

public interface IGUIDispatcher extends IMessagesDispatcher {
	void addMessage(IReceivedMessage message);
}
