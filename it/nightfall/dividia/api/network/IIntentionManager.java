package it.nightfall.dividia.api.network;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IIntentionManager extends Remote {
	void loadGameSave(String gameSaveId) throws RemoteException;

	void movePlayer(String direction) throws RemoteException;

	void basicAttack() throws RemoteException;

	void castAreaSpell(String spellName) throws RemoteException;

	void castFocusedSpell(String spellName) throws RemoteException;

	void castTargetSpell(String spellName) throws RemoteException;

	void castTargetAreaSpell(String spellName) throws RemoteException;

	void detonateTargetAreaSpell(String spellName) throws RemoteException;

	void interact() throws RemoteException;

	void disconnect() throws RemoteException;

	void finishLoadingMapScene(String mapName) throws RemoteException;

	void loadingMapCompleted(String mapName) throws RemoteException;

	void pauseEvents() throws RemoteException;

	void resumeEvents() throws RemoteException;

	void ping() throws RemoteException;

	void resetFocus() throws RemoteException;

	void equipItem(String itemId) throws RemoteException;
	
	void unequipArmor() throws RemoteException;

	void unequipBoots() throws RemoteException;

	void unequipHelmet() throws RemoteException;

	void unequipGloves() throws RemoteException;

	void unequipNecklace() throws RemoteException;

	void unequipRing() throws RemoteException;
	
	void unequipShield() throws RemoteException;

	void unequipMysticalSphere() throws RemoteException;
	
	void dialogChoosen(String dialogId) throws RemoteException;
	
	void dialogCompleted(String dialogId) throws RemoteException;
}
