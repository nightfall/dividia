package it.nightfall.dividia.api.network;

public interface IMessagesDispatcher extends Runnable {
	void stop();

	void pause();

	void resume();
}
