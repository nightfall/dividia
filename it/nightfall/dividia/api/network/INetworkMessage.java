package it.nightfall.dividia.api.network;

import java.rmi.RemoteException;

public interface INetworkMessage {
	void dispatch(IClientEventListener client) throws RemoteException;
}
