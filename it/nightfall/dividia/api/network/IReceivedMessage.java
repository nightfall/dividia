package it.nightfall.dividia.api.network;

import java.rmi.RemoteException;

public interface IReceivedMessage {
	void perform() throws RemoteException;
}
