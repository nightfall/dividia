package it.nightfall.dividia.api.quests;

import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.ISaveState;
import it.nightfall.dividia.bl.characters.Player;
import java.util.Map;
import java.util.Set;

/**
 * Representation of an active Quest
 */
public interface IQuest extends ISaveState, INetworkExportable {
	/**
	 * Starts procedures to activate the quest
	 *
	 * @param player Owner of the quest
	 * @return void
	 */
	void activateQuest(Player player);

	/**
	 * Returns a map Identifier - Active goal
	 *
	 * @return Map<String, IGoal>
	 */
	Map<String, IGoal> getActiveGoals();

	/**
	 * Returns the owner (player) of the quest
	 *
	 * @return Player
	 */
	Player getOwner();

	/**
	 * Returns the definition of the quest
	 *
	 * @return IQuestDefinition
	 */
	IQuestDefinition getQuestInfo();

	/**
	 * Returns a Set of identifiers of remaining goals
	 *
	 * @return Set<String>
	 */
	Set<String> getRemainingGoals();

	/**
	 * Returns a Set of identifiers of solved goals
	 *
	 * @return Set<String>
	 */
	Set<String> getSolvedGoals();

	/**
	 * Inform the quest of the completion of a goal
	 *
	 * @param goal Goal completed
	 * @return void
	 */
	void targetSolved(IGoal goal);

	/**
	 * Returns true if the specified goal is currently active, false otherwise
	 *
	 * @param goalName
	 * @return boolean
	 */
	boolean isGoalActive(String goalName);
	
	@Override
	ISummary getSummary();
}
