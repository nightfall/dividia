package it.nightfall.dividia.api.quests;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import java.util.List;
import java.util.Set;

/**
 * Definition of a generic Quest, it has functions to manage goals and store data
 * about the quest
 */
public interface IQuestDefinition {
	/**
	 * Returns the Quest identifier
	 *
	 * @return String
	 */
	String getName();

	/**
	 * Return quest description
	 *
	 * @return String
	 */
	String getDescription();

	/**
	 * Returns the name that will be shown to the player
	 *
	 * @return String
	 */
	String getDisplayName();

	/**
	 * Returns a Set of goal identifiers that the quest need to get solved
	 *
	 * @return Set<String>
	 */
	Set<String> getGoalDefinitions();

	/**
	 * Returns the level required in order to get the quest active
	 *
	 * @return int
	 */
	int getLevelRequirement();

	/**
	 * Returns an instance of a quest from this definition
	 *
	 * @return IQuest
	 */
	IQuest getQuestFromDefinition();

	/**
	 * Returns a Set of identifiers of quests required in order to get the quest active
	 *
	 * @return Set<String>
	 */
	Set<String> getRequirements();

	/**
	 * Returns a List of identifiers of items that the player will earn from the solving of this quest
	 *
	 * @return List<String>
	 */
	List<IItemDefinition> getRewards();

	/**
	 * Returns true if the quest will be visible to the player, false otherwise
	 *
	 * @return boolean
	 */
	boolean isVisibleToPlayer();

	/**
	 * Returns true if quest requirements are a sub-set of the completed goals
	 *
	 * @param completedQuests Set of identifiers of quests (Strings) completed by the player
	 * @return boolean
	 */
	boolean meetsRequirements(Set<String> completedQuests);
	
	IDialog getActivationDialog();
}
