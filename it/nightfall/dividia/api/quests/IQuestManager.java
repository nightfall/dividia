package it.nightfall.dividia.api.quests;

import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.ISaveState;
import it.nightfall.dividia.bl.characters.Player;
import java.util.Map;
import java.util.Set;

/**
 * Representation of a Quest Manager, it manages currently active quests
 */
public interface IQuestManager extends ISaveState, INetworkExportable {
	/**
	 * Starts procedures to activate the quest
	 *
	 * @return void
	 */
	void activateQuestManager();

	/**
	 * Returns a map Identifier - Active quest
	 *
	 * @return Map<String, IGoal>
	 */
	Map<String, IQuest> getActiveQuests();

	/**
	 * Returns the owner (player) of the Quest Manager
	 *
	 * @return Player
	 */
	Player getOwner();

	/**
	 * Returns a Set of identifiers of solved quests
	 *
	 * @return Set<String>
	 */
	Set<String> getSolvedQuests();

	/**
	 * Returns true if the specified quest is currently active, false otherwise
	 *
	 * @param goalName Identifier of the goal
	 * @return boolean
	 */
	boolean isGoalActive(String goalName);
	
	@Override
	ISummary getSummary();
}
