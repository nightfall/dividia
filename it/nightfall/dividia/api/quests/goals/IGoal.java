package it.nightfall.dividia.api.quests.goals;

import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.definitions.IGoalDefinition;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.ISaveState;

/**
 * Representation of an active goal
 */
public interface IGoal extends ISaveState, INetworkExportable {
	/**
	 * Starts procedures to activate the goal
	 *
	 * @param quest Quest linked to the activating goal
	 * @return void
	 */
	public void activateGoal(IQuest quest);

	/**
	 * Returns the linked quest
	 *
	 * @return IQuest
	 */
	public IQuest getQuest();

	/**
	 * Returns the definition of the current goal
	 *
	 * @return IGoalDefinition
	 */
	public IGoalDefinition getGoalInfo();
	
	@Override
	ISummary getSummary();
}
