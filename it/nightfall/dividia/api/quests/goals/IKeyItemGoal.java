package it.nightfall.dividia.api.quests.goals;

import it.nightfall.dividia.api.quests.goals.definitions.IKeyItemGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;

/**
 * Representation of an active KeyItem goal
 */
public interface IKeyItemGoal extends IGoal {
	@Override
	IKeyItemGoalDefinition getGoalInfo();

	int getItemsPicked();
}
