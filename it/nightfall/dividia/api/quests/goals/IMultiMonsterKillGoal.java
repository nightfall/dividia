package it.nightfall.dividia.api.quests.goals;

import it.nightfall.dividia.api.quests.goals.definitions.IMultiMonsterKillGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;

/**
 * Representation of an active MultiMonsterKill goal
 */
public interface IMultiMonsterKillGoal extends IGoal {
	@Override
	IMultiMonsterKillGoalDefinition getGoalInfo();

	int getMonsterKills();
}
