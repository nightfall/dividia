package it.nightfall.dividia.api.quests.goals;

import it.nightfall.dividia.api.quests.goals.definitions.IMultiMonsterRaceKillGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;

/**
 * Representation of an active MultiMonsterRaceKill goal
 */
public interface IMultiMonsterRaceKillGoal extends IGoal {
	@Override
	IMultiMonsterRaceKillGoalDefinition getGoalInfo();

	int getMonsterKills();
}
