package it.nightfall.dividia.api.quests.goals;

import it.nightfall.dividia.api.quests.goals.definitions.ITalkToNPCGoalDefinition;

/**
 * Representation of an active TalkToNPC goal
 */
public interface ITalkToNPCGoal extends IGoal {
	@Override
	ITalkToNPCGoalDefinition getGoalInfo();
}
