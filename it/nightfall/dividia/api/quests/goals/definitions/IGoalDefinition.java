package it.nightfall.dividia.api.quests.goals.definitions;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.bl.world.Teleport;
import java.util.List;
import java.util.Set;

/**
 * Definition of a generic Goal, containing basic info about it
 */
public interface IGoalDefinition {
	/**
	 * Returns the identifier of the Goal
	 *
	 * @return String
	 */
	String getName();

	/**
	 * Returns the goal name that will be visualized by the player
	 *
	 * @return String
	 */
	String getDisplayName();

	/**
	 * Returns the description of the Goal
	 *
	 * @return String
	 */
	String getDescription();

	/**
	 * Returns a Set of identifiers (Strings) of goal requirements
	 *
	 * @return Set<String>
	 */
	Set<String> getRequirements();

	/**
	 * Returns true if the goal will be visible to the player, false otherwise
	 *
	 * @return boolean
	 */
	boolean isVisibleToPlayer();

	/**
	 * Returns true if goal requirements are a sub-set of the completed goals
	 *
	 * @param completedGoals Set of identifiers of goals (Strings) completed by the player
	 * @return boolean
	 */
	boolean meetsRequirements(Set<String> completedGoals);

	/**
	 * Returns a new instance of IGoal
	 *
	 * @return IGoal
	 */
	abstract IGoal getGoalFromDefinition();
	
	/**
	 * Returns a list of identifiers of Items, completing the goal will grant those items
	 *
	 * @return List<String>
	 */
	List<IItemDefinition> getRewards();
	
	Teleport getTeleportAfterGoal();
}
