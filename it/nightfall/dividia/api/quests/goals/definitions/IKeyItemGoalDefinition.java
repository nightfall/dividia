package it.nightfall.dividia.api.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.IKeyItemGoal;

/**
 * Extension of IGoalDefinition, contains information about a KeyItemGoal definition
 */
public interface IKeyItemGoalDefinition extends IGoalDefinition {
	/**
	 * Returns an identifier (String) of an Item definition
	 *
	 * @return String
	 */
	String getRequiredItem();

	/**
	 * Returns the total items that is required to pick to complete the goal
	 *
	 * @return int
	 */
	int getTotalItemsRequired();

	@Override
	IKeyItemGoal getGoalFromDefinition();
}
