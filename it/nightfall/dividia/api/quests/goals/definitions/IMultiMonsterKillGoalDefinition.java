package it.nightfall.dividia.api.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.IMultiMonsterKillGoal;

/**
 * Extension of IGoalDefinition, contains information about a MultiMonsterKillGoal definition
 */
public interface IMultiMonsterKillGoalDefinition extends IGoalDefinition {
	/**
	 * Returns the identifier (String) of the monsters to kill
	 *
	 * @return String
	 */
	String getMonsterName();

	/**
	 * Return the number of monsters to kill to complete the goal
	 *
	 * @return int
	 */
	int getTotalKillsRequired();

	@Override
	IMultiMonsterKillGoal getGoalFromDefinition();
}
