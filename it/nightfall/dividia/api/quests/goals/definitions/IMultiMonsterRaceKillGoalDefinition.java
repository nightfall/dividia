package it.nightfall.dividia.api.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.IMultiMonsterRaceKillGoal;

/**
 * Extension of IGoalDefinition, contains information about a MultiMonsterRaceKillGoal definition
 */
public interface IMultiMonsterRaceKillGoalDefinition extends IGoalDefinition {
	/**
	 * Returns the Race identifier of the monsters to kill
	 *
	 * @return String
	 */
	String getMonsterRaceName();

	/**
	 * Returns the total number of monster to kill in order to complete the goal
	 *
	 * @return int
	 */
	int getTotalKillsRequired();

	@Override
	IMultiMonsterRaceKillGoal getGoalFromDefinition();
}
