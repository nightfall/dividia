package it.nightfall.dividia.api.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.ISingleMonsterKillGoal;
import java.util.List;

/**
 * Extension of IGoalDefinition, contains information about a SingleMonsterKillGoal definition
 */
public interface ISingleMonsterKillGoalDefinition extends IGoalDefinition {
	/**
	 * Returns the identifier of the monster to kill in order to complete the goal
	 *
	 * @return String
	 */
	String getMonsterName();

	
	
	@Override
	ISingleMonsterKillGoal getGoalFromDefinition();
}
