package it.nightfall.dividia.api.quests.goals.definitions;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.goals.ITalkToNPCGoal;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import java.util.Collection;

/**
 * Extension of IGoalDefinition, contains information about a TalkToNPCGoal definition
 */
public interface ITalkToNPCGoalDefinition extends IGoalDefinition {
	@Override
	ITalkToNPCGoal getGoalFromDefinition();
	
	IDialog getLinkedDialog();
	
	Collection<IItemDefinition> getRequiredItems();
	
	String getNpc();
}
