package it.nightfall.dividia.api.utility;

public interface ICallbackWaiting {
	boolean execute();
}
