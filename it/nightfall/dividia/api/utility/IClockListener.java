package it.nightfall.dividia.api.utility;

public interface IClockListener {
	void tick(int tpf);
}
