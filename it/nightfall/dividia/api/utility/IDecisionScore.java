package it.nightfall.dividia.api.utility;

import it.nightfall.dividia.api.ai.IDecision;
import java.util.List;


public interface IDecisionScore {

	List<IDecision> getDecisionList();

	double getScore();
	
	void appendDecisions(IDecisionScore followingDecision);
}
