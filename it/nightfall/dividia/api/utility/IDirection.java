package it.nightfall.dividia.api.utility;

/**
 * Representation of a direction in two dimensions
 * 
 * 
 * @author Nightfall
 */
public interface IDirection {

	/**
	 * Calculates and returns the spanning angle from an IDirection to the method parameter direction
	 * rotating anti-clockwise	 
	 *
	 * @return double : angle returned in radians
	 */
	public double angleBetweenDirections(IDirection d2);

	/**
	 * Returns the decimalDegreeAngle of an IDirection object
	 *
	 * @return double 
	 */
	public double getDecimalDegreeAngle();

	/**
	 * Returns the angle of a direction expressed in radians
	 *
	 * @return double 
	 */
	public double getDirectionAngle();

	/**
	 * Returns the x compoment of the direction versor
	 *
	 * @return double 
	 */
	public double getVersorX();

	/**
	 * Returns the y compoment of the direction versor
	 *
	 * @return double 
	 */
	public double getVersorY();

	/**
	 * Rotate the direction by the amount indicated by method's paameter
	 * 
	 * @param rotationAngle angle expressed in radians wich is added to current direction
	 * giving new direction
	 */
	public void rotateDirection(double rotationAngle);

	/**
	 * Switches current direction to one another indicated by a directionPoint
	 * 
	 * @param directionPoint identifies a new direction
	 */
	public void switchDirection(IGamePoint directionPoint);
	
	/**
	 * Returns a Point along or close to this direction starting from the referencePoint
	 * and with a fixed distance from the referencePoint
	 * 
	 * @param referencePoint 
	 * @param distance 
	 * 
	 * @return IGamePoint: a Point along or close to this direction starting from the referencePoint
	 * and with a fixed distance from the referencePoint
	 */
	public IGamePoint getDistancedPointAlongDirection(IGamePoint referencePoint, double distance);

	/**
	 * Returns an IDirection opposite to this
	 * 
	 * @return IDirection : a direction opposite to this
	 */
	public IDirection oppositeDirection();

	public IDirection clone();
}
