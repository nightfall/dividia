package it.nightfall.dividia.api.utility;

public interface IFutureCallback extends Runnable{
	void runAndWait(int time);
	void resetCycle();
	void updateNextWaitCycle(int time);
}
