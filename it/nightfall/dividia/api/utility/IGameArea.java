package it.nightfall.dividia.api.utility;

import java.awt.geom.Area;
import java.util.ArrayList;

/**
 * Representation af a geometric area
 * 
 */
public interface IGameArea {

	/**
	 * Cheks if an IGamePoint is contained in the area
	 * 
	 * @param point : IGamePoint processed
	 *
	 * @return boolean : true if the area contains the point
	 */
	boolean contains(IGamePoint point);

	/**
	 * Returns the Area of an IGameArea instance
	 *
	 * @return Area
	 */
	Area getArea();

	/**
	 * Returns a new IGameArea obtained as a translation
	 * 
	 * @param translation vectorPoint of translation
	 * @return IGameArea
	 */
	IGameArea getGameAreaByTranslation(IGamePoint translation);

	/**
	 * Makes a rotation of the area
	 * 
	 * @param directionAngle, it's the angle of the clockwise rotation
	 */
	void rotate(IDirection directionAngle);

	/**
	 * Returns a new IGameArea obtained as a rotation 
	 * 
	 * @param directionAngle, it's the angle of the clockwise rotation
	 */
	IGameArea getGameAreaByRotation(IDirection directionAngle);
	/**
	 * Makes a translation of the area
	 * 
	 * @param vectorPoint, it's the translation vector
	 */
	void translate(IGamePoint vectorPoint);
	
	/**
	 * Create and returns a copy of this IGameArea
	 * 
	 * @param vectorPoint, it's the translation vector
	 */
	IGameArea clone();	
	
	ArrayList<IGamePoint> getPerimeter();
	
	boolean intersect(IGameArea otherArea);
	
	
}
