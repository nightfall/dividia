package it.nightfall.dividia.api.utility;

import it.nightfall.dividia.bl.utility.SharedFunctions;

public interface IGameConstants {
	/*
	 * Dividia Version
	 */
	public static final String DIVIDIA_VERSION = "0.0.0";
	
    public static final double MOVEMENT_SPEED_CONSTANT = 2;
   
	public static final double SPELL_RADIUS = 2;
	public static final double ROTATION_ANGLE_PER_FRAME = Math.PI/2;

	/**
	 * Damage and battle.
	 */
	public static final double MIN_MULTIPLIER_DAMAGE = 1.000;
	public static final double MAX_MULTIPLIER_DAMAGE = 1.000;
	public static final int RAGE_INCREMENT = 20;
	
	/**
	 * Thread constants
	 */
	public static final int BL_UPDATE_MS = 40;
	public static final int AI_UPDATE_MS = 200;
	public static final double BL_UPDATE_S = BL_UPDATE_MS/1000.0d;
	public static final int GUI_UPDATE_MS = 16;
	public static final double GUI_UPDATE_S = 0.016F;
	public static final int BASIC_ATTACK_SLEEP_MS = 1000;
	
	/**
	 * Summon constants
	 */	
	public static final double SUMMON_EPX_LINEAR_COEFFICIENT = 1;
	public static final double SUMMON_EPX_QUADRATIC_COEFFICIENT = 1;
	public static final double SUMMON_EPX_KNOWN_TERM = 100;
	public static final double MIN_SUMMON_CAST_REDUCTION_FACTOR = 0.6;
	public static final int NUMBER_OF_SUMMONS_EVOLUTIONS = 4; 
	public static final int SUMMON_MAXIMUM_LEVEL = 100;
	
	/**
	 * Player exp constants
	 */	
	public static final double PLAYER_EPX_LINEAR_COEFFICIENT = 2;
	public static final double PLAYER_EPX_QUADRATIC_COEFFICIENT = 1;
	public static final double PLAYER_EPX_KNOWN_TERM = 500;
	public static final int PLAYER_MAXIMUM_LEVEL = 100;
	public static int PLAYERS_RESPAWN_TIME = 4;
	/**
	 * Interaction constants
	 */
	public static final double PLAYER_HALF_INTERACTION_ANGLE = SharedFunctions.division(Math.PI, 4);
	public static final float INTERACTION_RADIUS = 1.5f;
	
	/**
	 * Monster constants
	 */	
	
	public static final int MONSTER_STANDARD_VISIBILITY_LENGHT = 6;
	public static final double MONSTER_STANDARD_VISIBILITY_HALF_ANGLE = SharedFunctions.division(Math.PI, 4);
	public static final int MONSTER_STANDARD_UDIBILITY_RADIUS = 2;
	public static final double MONSTER_EPX_REWARD_LINEAR_COEFFICIENT = 2;
	public static final double MONSTER_EPX_REWARD_QUADRATIC_COEFFICIENT = 0.2;
	public static final double MONSTER_EPX_REWARD_KNOWN_TERM = 100;
	public static final double MONSTER_FOLLOW_DISTANCE = 20;
	public static int MONSTER_MAXIMUM_LEVEL = 100;
	public static double MONSTER_IQ_STANDARD_DEV = 10;
	
	/**
	 * System path constants
	 */
	public static final String SEPARATOR = "/";
	public static final String SERVER_SAVEGAMES_DIRECTORY = SharedFunctions.getPath(".|savegames|");
	public static final String CONFIG_PATH = SharedFunctions.getPath(".|config|");
	/**
	 * Server constants
	 */
	public static final int SOCKET_PORT = 15000;
	public static final int RMI_PORT = 15001;
	
	/**
	 * Gui constants
	 */
	public static double PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION = 5;
	public static final float CAMERA_DEFAULT_HEIGHT = 4f;
	public static float DISTANCE_TO_FLOOR = 0.1f;
	
	/**
	 * Math constants
	 */
	public static float SQRT_OF_2f = (float) Math.sqrt(2);
}
