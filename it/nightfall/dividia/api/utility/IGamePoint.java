package it.nightfall.dividia.api.utility;

/**
 * Representation of a geometric point
 */
public interface IGamePoint extends Cloneable {

	/**
	 * Calculate the distance to another point
	 * 
	 * @return double : the distance between current point and another point
	 */
	public double getDistanceFromAnotherPoint(IGamePoint p2);

	/**
	 * Returns the X coordinate
	 * 
	 * @return double : the X coordinate
	 */
	public double getX();

	/**
	 * Returns the Y coordinate
	 * 
	 * @return double : the Y coordinate
	 */
	public double getY();

	/**
	 * Translates the game point with x and y deltas
	 * 
	 * @param  deltaX : x translation component
	 * @param  deltaY : y translation component
	 */	
	public void translate(double deltaX, double deltaY);
	
	/**
	 * Returns an IGamePoint instantiated with current point coordinates
	 */	
	public IGamePoint clone(); 
	
	/**
	 * Translates the game point with a vector identified by another IGamePoint
	 * 
	 * @param  vector : the translation vector
	 */	
	public void translate(IGamePoint vector);
	
	/**
	 * Rotate the game point with across the axes by a given rotational direction
	 * 
	 * @param  direction : the rotation direction
	 */	
	public void rotatePoint(IDirection rotateDirection);
	/**
	 * Creates a new translated point.
	 * @param offset Quantity of translation
	 * @return A new GamePoint translated from the original
	 */
	public IGamePoint getPointFromTranslation (double offset);
	
}
