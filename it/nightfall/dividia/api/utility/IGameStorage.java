package it.nightfall.dividia.api.utility;

import it.nightfall.dividia.api.battle.definitions.ISkillDefinition;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.item.definitions.IKnapsackDefinition;
import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.api.quests.goals.definitions.IGoalDefinition;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterRaceDefinition;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;

public interface IGameStorage {
	IAlteredStatusDefinition getAlteredStatusDefinition(String alteredStatusName);

	IGoalDefinition getGoalDefinition(String goalName);

	IItemDefinition getItemDefinition(String itemName);

	IGameMapDefinition getMapDefinition(String mapName);

	IQuestDefinition getQuestDefinition(String questName);

	IRaceDefinition getRaceDefinition(String raceName);

	ISkillDefinition getSkillDefinition(String skillName);

	ISpellDefinition getSpellDefinition(String spellName);

	IKnapsackDefinition getKnapsackDefinition(String knapsackName);

	void loadStorage();

	IDialog getDialog(String dialogId);
	
	NonPlayingCharacterRaceDefinition getNonPlayingCharacterRaceDefinition(String raceName);
}
