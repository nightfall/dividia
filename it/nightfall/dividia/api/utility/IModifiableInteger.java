package it.nightfall.dividia.api.utility;

public interface IModifiableInteger {
	
	void setValue(int newValue);
	
	int getValue();
	
	void addInt(int delta);
	
	int compare(IModifiableInteger comparating);

	public IModifiableInteger clone();
}
