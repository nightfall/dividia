package it.nightfall.dividia.api.utility;

public interface IMovementPlanner {
	
	/**
	 * Calculates and applies next step getting closer to the destination 
	 */	
	void applyNextStep(int tpf);
	
	/**
	 * Sets a new destination to the planner
	 * 
	 * @param  IGamePoint : new Destination point
	 */	
	void setDestination(IGamePoint destinationPoint);

	/**
	 * Checks whether all destinationPoints have been reached
	 * 
	 */	
	boolean isPathCompleted();
}
