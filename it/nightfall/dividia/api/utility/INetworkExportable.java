package it.nightfall.dividia.api.utility;

public interface INetworkExportable {
	ISummary getSummary();
}
