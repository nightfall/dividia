package it.nightfall.dividia.api.utility;

public interface INetworkImportable {
	void buildFromSummary(ISummary summary);
}
