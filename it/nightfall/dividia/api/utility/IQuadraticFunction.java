package it.nightfall.dividia.api.utility;

/**
 * QuadraticMathFunction with numeric coefficients and image caculation method
 *
 * @author Nightfall
 */

public interface IQuadraticFunction {
	
	/**
	 * Returns the value of a quadratic function calculated in x 
	 * 
	 * @return : function's image in x
	 */	
	public double getFunctionImage(double x);
	
	/**
	 * Returns the integer value of a quadratic function calculated in x 
	 * 
	 * @return : function's integer image in x
	 */		
	public int getIntegerFunctionImage(double x);
	
	/**
	 * Returns the integer difference between the image calculated in x2
	 * and the image in x1
	 * 
	 * @return : int
	 */	
	public int getIntegerFunctionImageVariation(double x1, double x2);
}
