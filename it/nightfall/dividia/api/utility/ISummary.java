package it.nightfall.dividia.api.utility;

import it.nightfall.dividia.api.utility.io.IConfigWriter;
import java.io.Serializable;
import java.util.Map;

public interface ISummary extends Serializable {
	void putData(String key, Serializable object);

	Serializable getData(String key);

	Map<String, Serializable> getInternalMap();

	public boolean getBoolean(String name);

	public int getInteger(String name);

	public long getLong(String name);

	public float getFloat(String name);

	public double getDouble(String name);

	public String getString(String name);

	public ISummary getSummary(String name);
	
	public boolean dataExists(String name);
	
	public IConfigWriter toConfigWriter(IConfigWriter config);
}
