package it.nightfall.dividia.api.utility.io;

import it.nightfall.dividia.api.utility.ISummary;
import java.util.Collection;

public interface IConfigReader {
	boolean getBoolean(String name);

	int getInteger(String name);

	int getHexadecimal(String name);

	float getFloat(String name);

	double getDouble(String name);

	String getString(String name);

	long getLong(String name);

	String getStringAttribute(String attributeName);

	int getIntegerAttribute(String attributeName);

	double getDoubleAttribute(String attributeName);

	float getFloatAttribute(String attributeName);

	long getLongAttribute(String attributeName);
	
	IConfigReader getConfigReader(String name);

	Collection<Boolean> getBooleanList(String name);

	Collection<String> getStringList(String name);

	Collection<Integer> getIntegerList(String name);

	Collection<Float> getFloatList(String name);

	Collection<Double> getDoubleList(String name);

	Collection<IConfigReader> getConfigReaderList(String name);

	ISummary toSummary();

	boolean doesElementExist(String name);
}
