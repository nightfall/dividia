package it.nightfall.dividia.api.utility.io;

import java.util.Collection;

public interface IConfigWriter {
	void saveFile(String path);

	void appendBoolean(String name, boolean value);

	void appendInteger(String name, int value);

	void appendFloat(String name, float value);

	void appendDouble(String name, double value);

	void appendLong(String name, long value);
	
	void appendString(String name, String value);

	void appendConfigWriter(IConfigWriter value);

	void appendBooleanList(String name, Collection<Boolean> value);

	void appendIntegerList(String name, Collection<Integer> value);

	void appendFloatList(String name, Collection<Float> value);

	void appendDoubleList(String name, Collection<Double> value);

	void appendLongList(String name, Collection<Long> value);
	
	void appendStringList(String name, Collection<String> value);

	void appendConfigWriterList(Collection<IConfigWriter> value);

	void appendIntegerAttribute(String name, int value);

	void appendFloatAttribute(String name, float value);

	void appendDoubleAttribute(String name, double value);

	void appendStringAttribute(String name, String value);
}
