package it.nightfall.dividia.api.utility.io;

public interface ISaveState {
	public void loadState(IConfigReader saveState);

	public void saveState(IConfigWriter saveState);
}
