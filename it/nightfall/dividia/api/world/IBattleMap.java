package it.nightfall.dividia.api.world;

import it.nightfall.dividia.api.battle.IBattleManager;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import java.util.List;

public interface IBattleMap extends IGameMap {
	IBattleManager getBattleManager();

	List<PlayingCharacter> getPlayingAffectedCharactersInArea(IGameArea areaOfExecution, PlayingCharacter performer, boolean isForEnemiesOnly);
}
