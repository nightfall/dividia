package it.nightfall.dividia.api.world;

import it.nightfall.dividia.api.world.summaries.IGeneralGameMapSummary;
import it.nightfall.dividia.api.characters.IInteractionCharacter;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.BagOfItems;
import it.nightfall.dividia.bl.world.Portal;
import it.nightfall.dividia.gui.utility.GuiStatus;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IGameMap{
	
	void addPlayer(Player player);

	void removePlayer(String player);

	void findInteraction(Player p);
	
	void addBag(BagOfItems addingBag);

	List<IInteractionCharacter> getAllNoMonsterCharacters();
	
	String getName();
	
	String getDisplayName();
	
	IGamePoint getDefaultStartingPosition();
	
	public boolean is2D();

	IDirection getDefaultStartingDirection();
        
    Map<String,Player> getPlayers();
	
	void addLoadingPlayer(Player player);
	
	void removeLoadingPlayer(String player);
	
	IGeneralGameMapSummary createMapSummary ();
	
	Map<String,Portal> getPortals();
	
	Map<String,Player> getLoadingPlayers();
	
	ISummary getSummary();

    Map<String,Player> getActiveAndLoadingPlayers();
	
	IGameMapDefinition getDefinition();
	
	boolean collisionDetected(Set<Integer>sectors, ICollisionShape shapeToCheck);
	
	boolean isPointInMap(IGamePoint point);
	
	double getWidth();
	
	double getHeight();
	
	void playerDied(Player aThis);
	
	void respawnPlayer(Player aThis);
	
	GuiStatus getGuiMode();

	public boolean doesPositionExist(IGamePoint position);

}
