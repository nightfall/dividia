/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.api.world;

import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.world.Teleport;
import java.util.Map;

/**
 *
 * @author Andrea
 */
public interface IMapsManager {
	
	void loadMap(String map,IGameMapDefinition def);
	void finishedLoadingMap (String playerName, String map);
	void summaryRequest (String playerName, String map);
	Map <String,IGameMap> getGameMaps();
	boolean existingMap(String mapId);
	IGameMap getMap(String mapId);
	void useTeleportForPlayer(IGameMap previousMap, Player player, Teleport teleport);
}
