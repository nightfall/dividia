package it.nightfall.dividia.api.world;

import it.nightfall.dividia.bl.characters.Monster;

public interface IMonsterMap extends IBattleMap {

	public void monsterDied(Monster aThis);

	public void monsterRespawn(Monster aThis);

}
