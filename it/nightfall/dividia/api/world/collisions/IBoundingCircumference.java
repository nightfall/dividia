/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.api.world.collisions;

/**
 *
 * @author Andrea
 */
public interface IBoundingCircumference extends ICollisionShape{
	
	double getRadius();
}
