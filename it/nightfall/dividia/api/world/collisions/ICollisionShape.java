/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.api.world.collisions;

import it.nightfall.dividia.api.characters.ICharacter;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import java.util.Set;

/**
 *
 * @author Andrea
 */
public interface ICollisionShape extends Cloneable{
	
	IGameArea getArea();
	
	IGamePoint getPosition();
	
	boolean intersect(ICollisionShape otherShape);

	Set<Integer> findBelongingSectors(IGameMapDefinition map);
	
	void translate (double deltaX, double deltaY);
	
	void translateToFitCharacter(IGamePoint position);
		
	Object clone();
	
	IGraphicShape createGraphicShape();
	
}
