/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.api.world.collisions;

import java.util.Collection;

/**
 *
 * @author Andrea
 */
public interface IMapSector {
	
	Collection<ICollisionShape> getCollideableObjects();
	
	void addCollisionObject(ICollisionShape obj);
	
	Collection<ICollisionShape> getCollisionObjects(ICollisionShape obj);
}
