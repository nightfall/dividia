package it.nightfall.dividia.api.world.definitions;

import it.nightfall.dividia.api.world.IBattleMap;

public interface IDungeonGameMapDefinition extends IMonsterGameMapDefinition {
	@Override
	IBattleMap getMapFromDefinition();
}
