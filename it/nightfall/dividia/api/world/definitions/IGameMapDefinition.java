package it.nightfall.dividia.api.world.definitions;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.api.world.collisions.IMapSector;
import it.nightfall.dividia.bl.characters.NonPlayingCharacter;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterDefinition;
import it.nightfall.dividia.bl.world.Portal;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IGameMapDefinition {
	String getName();

	String getDisplayName();

	IGamePoint getDefaultStartingPosition();

	boolean is2D();

	IDirection getDefaultStartingDirection();

	Map<String, Portal> getPortals();

	List<NonPlayingCharacterDefinition> getNpcDefinitions();

	IGameMap getMapFromDefinition();
	
	double getSectorHeight();
	
	double getSectorWidth();
	
	int getSectorsInColumns();
	
	int getSectorsInRows();

	boolean collisionDetected(Set<Integer> playerSectors, ICollisionShape playerShape);
	
	Set<ICollisionShape> getCollisionObjects();
	
	IMapSector[][] getSectors();

	public double getHeight();

	public double getWidth();
}
