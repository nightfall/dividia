package it.nightfall.dividia.api.world.definitions;

import it.nightfall.dividia.api.world.IGameMap;

public interface IGeneralGameMapDefinition extends IGameMapDefinition {
	@Override
	IGameMap getMapFromDefinition();
}
