package it.nightfall.dividia.api.world.definitions;

import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.bl.characters.definitions.MonsterDefinition;
import java.util.List;

public interface IMonsterGameMapDefinition extends IGeneralGameMapDefinition {
	@Override
	IBattleMap getMapFromDefinition();
	
	List<MonsterDefinition> getMonsterDefinitions();
	
}
