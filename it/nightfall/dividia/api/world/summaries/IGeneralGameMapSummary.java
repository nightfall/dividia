/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.api.world.summaries;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author falko91
 */
public interface IGeneralGameMapSummary extends Serializable {
	public Set<String> getPlayerSet();
	public Set<String> getItemSet();
	public void receiveSummary();
}
