/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.api.world.summaries;

import java.util.Set;

/**
 * This class represents a summary of a map which contains monsters.
 * @Nightfall
 */
public interface IMonsterGameMapSummary extends IGeneralGameMapSummary {
	public Set<String> getMonsterSet();
}
