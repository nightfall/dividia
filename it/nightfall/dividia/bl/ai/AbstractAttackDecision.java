package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.bl.characters.InProgressState;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.events.EventManager;

public abstract class AbstractAttackDecision extends AbstractDecision{
	
	protected IDirection castingDirection;
	protected boolean decisionStarted = false;
	protected boolean castingTerminated = false;


	protected AbstractAttackDecision() {
	}
	
	@Override
	public void applyNextStep(Monster monster, int tpf) {
		monster.setInProgressState(InProgressState.CASTING);		
		if(castingTerminated) {
			super.terminate(monster);
		}
	}
	
	public void castingTerminated() {
		castingTerminated = true;
	}
	
	public void setDirection(Monster monster) {
		monster.setDirection(castingDirection);
		EventManager.getInstance().characterMoved(monster);
		EventManager.getInstance().characterStopped(monster);
		
	}
}
