package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.bl.characters.InProgressState;
import it.nightfall.dividia.bl.characters.Monster;

public abstract class AbstractDecision implements IDecision{
	
	protected abstract boolean checkForInitialConditions(Monster monster);
	
	@Override
	public void terminate(Monster monster) {
		monster.setInProgressState(InProgressState.WAITING);
		monster.getDecisionalQueue().poll();
	}	

}	

