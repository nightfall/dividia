
package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.ai.IThinker;
import it.nightfall.dividia.api.utility.IClockListener;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.ClockGenerator;
import java.util.Collection;
import java.util.LinkedList;


public class AiManager implements IClockListener {
	
	private Collection<IThinker> thinkers;
	private ClockGenerator clock;
	

	public AiManager() {
		thinkers = new LinkedList<>();
		clock = new ClockGenerator(IGameConstants.AI_UPDATE_MS);
	}
	
	
	public synchronized void addMonster(Monster monster) {
		thinkers.add(monster.getThinker());
	}

	public void activate() {
		clock.addListener(this);
		Thread clockThread = new Thread(clock);
		clockThread.setName("Ai Clock");
		clockThread.setDaemon(true);
		clockThread.start();
	}

	@Override
	public synchronized void tick(int tpf) {
		for(IThinker thinker:thinkers) {
			if(thinker.getMonster().getDecisionalQueue().isEmpty()) {
				thinker.think();
			}
			thinker.getMonster().updateMonster(tpf);
		}
	}

	public synchronized void removeMonster(Monster monster) {
		thinkers.remove(monster.getThinker());
	}
	

}
