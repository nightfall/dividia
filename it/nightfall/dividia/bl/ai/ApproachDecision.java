
package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.bl.characters.InProgressState;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.PlayingCharacter;


public class ApproachDecision extends AbstractDecision {
	
	private PlayingCharacter target;
	private boolean decisionStarted = false;
	private float approachDistance;

	public ApproachDecision(PlayingCharacter target, float approachDistance) {
		this.target = target;
		this.approachDistance = approachDistance;
	}
	
	
	
	@Override
	public void applyNextStep(Monster monster, int tpf) {
		if(!decisionStarted) {
			monster.setInProgressState(InProgressState.WALKING);
			monster.getFollowPlanner().setTarget(target);
			decisionStarted = true;
		}
		if(monster.getFollowPlanner().applyNextApproachStep(tpf, approachDistance)) {
			super.terminate(monster);
		}
		else if(monster.getPosition().getDistanceFromAnotherPoint(monster.getLastCriticalPoint()) > monster.getPatrolManager().getTiredDistance()) {
			super.terminate(monster);
			monster.setTarget(null);
			monster.removeAllDecisions();
			IDecision backHomeDecision = new MovementDecision(monster.getPatrolManager().getMonsterStartingPoint());
			monster.addDecision(backHomeDecision);
            monster.resetLastCriticalPoint();
		}
			
	}

	@Override
	protected boolean checkForInitialConditions(Monster monster) {
		return true;
	}
	
}
