package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.battle.definitions.IAreaSpellDefinition;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.SharedFunctions;

public class AreaSpellDecision extends AbstractAttackDecision{

	private IAreaSpellDefinition spell;
	private boolean castingStarted = false;

	public AreaSpellDecision(IAreaSpellDefinition spell) {
		super();
		this.spell = spell;
	}
	
	
	@Override
	protected boolean checkForInitialConditions(Monster monster){
		return true;
	}

	@Override
	public void applyNextStep(Monster monster, int tpf) {
		super.applyNextStep(monster,tpf);
		if(!castingStarted) {
			if(spell.isForEnemiesOnly()) {
				IDirection direction = SharedFunctions.getDirectionBetweenPoints(monster.getPosition(), monster.getTarget().getPosition());
				castingDirection = direction;
				setDirection(monster);
			}
			monster.undertakeAction(spell);
			castingStarted = true;
		}
	}

	
	
}
