
package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.SharedFunctions;


public class BasicAttackDecision extends AbstractAttackDecision{

	private boolean castingStarted = false;
	
	public BasicAttackDecision() {
		super();
	}

	@Override
	protected boolean checkForInitialConditions(Monster monster) {
		return true;
	}

	@Override
	public void applyNextStep(Monster monster, int tpf) {
		
		super.applyNextStep(monster,tpf);
		if(!castingStarted) {
			IDirection direction = SharedFunctions.getDirectionBetweenPoints(monster.getPosition(), monster.getTarget().getPosition());
			castingDirection = direction;
			setDirection(monster);
			monster.undertakeAction();
			castingStarted = true;
		}
		
	}

}


