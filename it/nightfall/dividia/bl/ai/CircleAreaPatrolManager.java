
package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.utility.Direction;
import java.util.List;


public class CircleAreaPatrolManager extends  PatrolManager{

	
	private float areaRadius;

	public CircleAreaPatrolManager(IGamePoint monsterStartingPoint, float areaRadius, float maximumPauseTime, float minimumPauseTime, 
									float tiredDistance,IGameMap gameMap) {
		super(monsterStartingPoint, maximumPauseTime, minimumPauseTime, tiredDistance,gameMap);
		this.areaRadius = areaRadius;
	}
	
	
	
	@Override
	public void addNextWayPoint(List<IDecision> decisionList) {
		float randomDistance  = (float)Math.random() * areaRadius;
		double randomAngle = Math.random()*Math.PI*2;
		IDirection randomDirection = new Direction(randomAngle);
		IGamePoint nextPosition = randomDirection.getDistancedPointAlongDirection(monsterStartingPoint, randomDistance);
		while (!gameMap.isPointInMap(nextPosition)){
			nextPosition = randomDirection.getDistancedPointAlongDirection(monsterStartingPoint, randomDistance);
		}
		decisionList.add(new MovementDecision(nextPosition));
	}

	
	
}
