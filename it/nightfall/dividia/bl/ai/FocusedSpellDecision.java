package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.battle.definitions.IFocusedSpellDefinition;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.Monster;

public class FocusedSpellDecision extends AbstractAttackDecision {
	
	private IFocusedSpellDefinition spell;
	private IGamePoint focus;

	public FocusedSpellDecision(IFocusedSpellDefinition spell, IGamePoint focus) {
		super();
		this.spell = spell;
		this.focus = focus;
	}	
	
	@Override
	protected boolean checkForInitialConditions(Monster monster) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	protected void execute(Monster monster) {
		setDirection(monster);
		monster.undertakeAction(spell, focus);
	}
	
}
