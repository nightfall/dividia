package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.InProgressState;
import it.nightfall.dividia.bl.characters.Monster;

public class MovementDecision extends AbstractDecision{

	private IGamePoint destination;
	private boolean decisionStarted = false;

	public MovementDecision(IGamePoint destination) {
		this.destination = destination;
	}	
	
	public void tryToExecute(Monster monster) {	
		monster.setInProgressState(InProgressState.WALKING);
		if(checkForInitialConditions(monster)) {		
			execute(monster);
		}
		else {
			terminate(monster);
		}
	}

	@Override
	protected boolean checkForInitialConditions(Monster monster) {
		return true;
	}

	protected void execute(Monster monster) {		
		monster.getMovementPlanner().setDestination(destination);
	}

	@Override
	public void applyNextStep(Monster monster, int tpf) {
		if(!decisionStarted) {
			monster.setInProgressState(InProgressState.WALKING);
			monster.getMovementPlanner().setDestination(destination);
			decisionStarted = true;
		}
		if(!monster.getMovementPlanner().isPathCompleted()) {
			monster.getMovementPlanner().applyNextStep(tpf);
		}
		else {
			super.terminate(monster);
		}
			
	}
	
}
