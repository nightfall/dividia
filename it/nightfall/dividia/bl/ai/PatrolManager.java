
package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Monster;
import java.util.LinkedList;
import java.util.List;


public abstract class PatrolManager {
	protected IGamePoint monsterStartingPoint;
	protected float tiredDistance;
	protected float maximumPauseTime;
	protected float minimumPauseTime;
	protected IGameMap gameMap;
	protected boolean pauseRound = true;

	public PatrolManager(IGamePoint monsterStartingPoint, float maximumPauseTime, float minimumPauseTime, float tiredDistance, IGameMap gameMap) {
		this.monsterStartingPoint = monsterStartingPoint;
		this.maximumPauseTime = maximumPauseTime;
		this.minimumPauseTime = minimumPauseTime;
		this.tiredDistance = tiredDistance;
		this.gameMap = gameMap;
	}
	
	public void askForNextPatrolWayPoint(Monster monster) {
		List<IDecision> decisionList = new LinkedList<>();
		if(!pauseRound)  {
			addNextWayPoint(decisionList);
		}
		else {
			addNextPause(decisionList);
		}
		pauseRound = !pauseRound;
		
		monster.addDecisionList(decisionList);		
	}
	
	public abstract void addNextWayPoint(List<IDecision> decisionList);

	public void addNextPause(List<IDecision> decisionList) {
		float randomPause  = (float)Math.random() * (maximumPauseTime-minimumPauseTime) + minimumPauseTime;		
		decisionList.add(new PauseDecision(randomPause));
	}

	public IGamePoint getMonsterStartingPoint() {
		return monsterStartingPoint;
	}
	
	public float getTiredDistance() {
		return tiredDistance;
	}
	
}
