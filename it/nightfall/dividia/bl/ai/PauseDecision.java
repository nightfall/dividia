
package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.bl.characters.InProgressState;
import it.nightfall.dividia.bl.characters.Monster;


public class PauseDecision extends AbstractDecision {

	private float pauseTime;

	public PauseDecision(float pauseTime) {
		this.pauseTime = pauseTime;
	}
	
	
	
	@Override
	protected boolean checkForInitialConditions(Monster monster) {
		return true;
	}

	@Override
	public void applyNextStep(Monster monster, int tpf) {
		monster.setInProgressState(InProgressState.PAUSE);
		pauseTime-=(tpf/1000000000.0f);	
		monster.lookAround();
		if(pauseTime<=0 || monster.getTarget()!=null) {		
			super.terminate(monster);
		}
	}

}
