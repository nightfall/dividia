package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.battle.definitions.ITargetSpellDefinition;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public class TargetSpellDecision extends AbstractAttackDecision {
	
	private ITargetSpellDefinition spell;
	private PlayingCharacter target;

	public TargetSpellDecision(ITargetSpellDefinition spell, PlayingCharacter target) {
		super();
		this.spell = spell;
		this.target = target;
	}
	
	@Override
	protected boolean checkForInitialConditions(Monster monster) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	protected void execute(Monster monster) {
		setDirection(monster);
		monster.undertakeAction(spell, target);
	}
	
}
