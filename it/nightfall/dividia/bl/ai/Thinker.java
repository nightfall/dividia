package it.nightfall.dividia.bl.ai;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.ai.IThinker;
import it.nightfall.dividia.api.utility.IDecisionScore;
import it.nightfall.dividia.bl.characters.InProgressState;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class Thinker implements IThinker{

	private Monster monster;
	
	
	
	public Thinker(Monster monster){
		this.monster = monster;
	}
	
	
	/**
	 * evaluate all monster possibilities and appends a list of decisions to the monster
	 * @param monster 
	 */
	@Override
	public void think() {
		
		//TODO sistemare in accorto a quanto scelto sulle classi affini
		monster.setInProgressState(InProgressState.THINKING);
		
		//TODO va fatta anche quando il mostro si muove, non solo all'operazione think
		if(monster.getTarget() == null) {		
			monster.lookAround();
		}
		if(monster.getTarget()!=null) {
			/*List<IDecision> decisionList = new LinkedList<>();
			decisionList.add(new ApproachDecision(monster.getTarget(), 1));
			monster.addDecisionList(decisionList);	
			*/
		
			
			// TODO non cancellare
			Collection<IDecisionScore> decisionScores = new ArrayList<>();
			for (IPossibility currentPossibility : monster.getPossibilities()){
				IDecisionScore currentDecisionScore = currentPossibility.evaluate(monster);
				if(currentDecisionScore!=null) {
					decisionScores.add(currentDecisionScore);
				}
			}
			double maxScore = Double.NEGATIVE_INFINITY;
			double minScore = Double.POSITIVE_INFINITY;
			for(IDecisionScore decisionScore : decisionScores) {
				if(decisionScore.getScore() > maxScore) {
					maxScore = decisionScore.getScore();
				}
				if(decisionScore.getScore() < minScore) {
					minScore = decisionScore.getScore();
				}
			}

			double gaussianRandomIQ = SharedFunctions.getGaussianRandomIQ(monster.getIntelligenceQuotience());
			double decisionalScoreChoice = gaussianRandomIQ/100 * (maxScore - minScore) + minScore;

			double smallerDistance = Double.POSITIVE_INFINITY;
			List<IDecision> bestDecision = new LinkedList<>();
			for(IDecisionScore decisionScore : decisionScores) {
				double currentDistance = Math.abs(decisionalScoreChoice - decisionScore.getScore());		
				if(currentDistance < smallerDistance) {
					smallerDistance = currentDistance;
					bestDecision = decisionScore.getDecisionList();
				}
			}
			if(!bestDecision.isEmpty()) {
				monster.addDecisionList(bestDecision);
			}
            else {
                monster.addDecision(new PauseDecision(1));
            }
                
		}
		else {
			monster.getPatrolManager().askForNextPatrolWayPoint(monster);		
		}	
		if(monster.getDecisionalQueue().isEmpty()) {
			monster.getPatrolManager().askForNextPatrolWayPoint(monster);
			
		}
	}

	@Override
	public Monster getMonster() {
		return monster;
	}
	
	
}
