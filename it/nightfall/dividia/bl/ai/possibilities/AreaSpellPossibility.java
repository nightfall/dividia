package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.battle.definitions.IAreaSpellDefinition;
import it.nightfall.dividia.api.utility.IDecisionScore;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.bl.ai.ApproachDecision;
import it.nightfall.dividia.bl.ai.AreaSpellDecision;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.DecisionScore;
import java.util.LinkedList;
import java.util.List;

public class AreaSpellPossibility implements IPossibility {

	IAreaSpellDefinition areaSpell;

	public AreaSpellPossibility(IAreaSpellDefinition areaSpell) {
		this.areaSpell = areaSpell;
	}
	
	
	
	@Override
	public IDecisionScore evaluate(Monster monster) {
		if(!monster.canCast(areaSpell)) {
			return null;
		}
		SpellPossibilityInfo info = new SpellPossibilityInfo(monster, areaSpell);
		List<IDecision> decisionList = new LinkedList<>();
		if(areaSpell.isForEnemiesOnly()) {		
			IGameArea interestedArea = areaSpell.getAreaOfEffect().getGameAreaByRotation(info.getMonsterToTargetDirection());
			interestedArea.translate(monster.getPosition());
			if(!interestedArea.contains(monster.getTarget().getPosition())) {
				float approachDistance = areaSpell.getMaxApproachingDistance();
				if(info.monsterToTargetDistance<approachDistance) {
					approachDistance = -areaSpell.getMinApproachingDistance();			
				}
				ApproachDecision approachDecision = new ApproachDecision(monster.getTarget(), approachDistance);
				decisionList.add(approachDecision);			
			}
		}
		
		AreaSpellDecision decision = new AreaSpellDecision(areaSpell);
		decisionList.add(decision);
		
		//Assuming 100 is the default score
		return new DecisionScore(decisionList, Math.random()*100);
	}


	
}
