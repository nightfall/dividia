
package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.utility.IDecisionScore;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.bl.ai.ApproachDecision;
import it.nightfall.dividia.bl.ai.BasicAttackDecision;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.DecisionScore;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.util.LinkedList;
import java.util.List;


public class BasicAttackPossibility implements IPossibility{

	@Override
	public IDecisionScore evaluate(Monster monster) {
		if(monster.getCurrentExecution() != null) {
			return null;
		}
		PossibilityInfo info = new PossibilityInfo(monster);
		List<IDecision> decisionList = new LinkedList<>();
				
		IGameArea interestedArea = monster.getEquippedItems().getWeapon().getAttackRange().getGameAreaByRotation(info.getMonsterToTargetDirection());
		interestedArea.translate(monster.getPosition());
		if(!interestedArea.contains(monster.getTarget().getPosition())) {
			ApproachDecision approachDecision = new ApproachDecision(monster.getTarget(), SharedFunctions.getMaxApproachingDistance(monster.getEquippedItems().getWeapon().getAttackRange()));
			decisionList.add(approachDecision);			
		}		
		BasicAttackDecision decision = new BasicAttackDecision();
		decisionList.add(decision);		
		//Assuming 100 is the default score
		return new DecisionScore(decisionList, Math.random()*100);
	}

}
