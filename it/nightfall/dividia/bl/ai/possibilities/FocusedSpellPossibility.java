package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.battle.definitions.IFocusedSpellDefinition;
import it.nightfall.dividia.api.utility.IDecisionScore;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.ai.FocusedSpellDecision;
import it.nightfall.dividia.bl.ai.MovementDecision;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.DecisionScore;
import it.nightfall.dividia.bl.utility.SharedFunctions;

public class FocusedSpellPossibility implements IPossibility {

	private IFocusedSpellDefinition focusedSpell;

	public FocusedSpellPossibility(IFocusedSpellDefinition focusedSpell) {
		this.focusedSpell = focusedSpell;
	}
	
	//TODO bozza iniziale 
	@Override
	public IDecisionScore evaluate(Monster monster) {
		SpellPossibilityInfo info = new SpellPossibilityInfo(monster,focusedSpell);
		double distance = info.getMonsterPosition().getDistanceFromAnotherPoint(info.getTargetPosition());
		if(focusedSpell.getSelectionRadius() < distance) {
			IGamePoint tryFocus = info.getTargetPosition();
			IGameArea tryArea = focusedSpell.getAreaOfEffect().getGameAreaByTranslation(tryFocus);
			tryArea.rotate(info.getMonsterToTargetDirection());
			double chanceToSucced = PossibilitySharedFunctions.getChanceToHitTarget(tryArea, info.getTarget(), SharedFunctions.getFinalCastDuration(focusedSpell, monster));
			return standingEvaluation(info, info.getTargetPosition(),chanceToSucced);
		}
		IGamePoint tryFocus = info.closestToTargetPointInSelection(focusedSpell);
		IGameArea tryArea = focusedSpell.getAreaOfEffect().getGameAreaByTranslation(tryFocus);
		tryArea.rotate(info.getMonsterToTargetDirection()); 
		if(tryArea.contains(info.getTargetPosition())) {			
			double chanceToSucced = PossibilitySharedFunctions.getChanceToHitTarget(tryArea, info.getTarget(), SharedFunctions.getFinalCastDuration(focusedSpell, monster));
			return standingEvaluation(info, tryFocus, chanceToSucced);
		}
		return movementNeededEvaluation(info);
	}

	private IDecisionScore standingEvaluation(SpellPossibilityInfo info,IGamePoint focus, double chanceToSucced) {
		IDecision spellDecision = new FocusedSpellDecision(focusedSpell, focus);
		return null;
		
	}

	private IDecisionScore movementNeededEvaluation(SpellPossibilityInfo info) {
		double distance = info.getMonsterPosition().getDistanceFromAnotherPoint(info.getTargetPosition());
		IGamePoint destination = info.getMonsterToTargetDirection().getDistancedPointAlongDirection(info.getMonsterPosition(), distance - focusedSpell.getSelectionRadius());
		double score = 0;
		
		IGamePoint tryFocus = info.getTargetPosition();
		IGameArea tryArea = focusedSpell.getAreaOfEffect().getGameAreaByTranslation(tryFocus);
		IDecisionScore returningDecision = new DecisionScore(new MovementDecision(destination), score);		
		double timeOfActionInSeconds = PossibilitySharedFunctions.getApproxTimeForMovement(info.getMonster(), info.getMonsterPosition(), destination);
		double chanceToSucced = PossibilitySharedFunctions.getChanceToHitTarget(tryArea, info.getTarget(), info.getActionDurationInSeconds());
		returningDecision.appendDecisions(standingEvaluation(info, info.getTargetPosition(), chanceToSucced));
		return returningDecision;
	}
}
