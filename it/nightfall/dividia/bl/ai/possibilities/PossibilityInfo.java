package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.utility.SharedFunctions;

public class PossibilityInfo {
	
	protected Monster monster;
	protected Player target;
	protected IGamePoint monsterPosition;
	protected IGamePoint targetPosition;
	protected int actionDurationInMilliseconds;
	protected IDirection monsterToTargetDirection;
    protected float monsterToTargetDistance;
	
	PossibilityInfo (Monster monster) {
		this.monster = monster;
		this.target = monster.getTarget();
		this.monsterPosition = monster.getPosition();
		this.targetPosition = target.getPosition();
		monsterToTargetDirection = SharedFunctions.getDirectionBetweenPoints(monsterPosition, targetPosition);
		actionDurationInMilliseconds = 0;
        monsterToTargetDistance = (float) monsterPosition.getDistanceFromAnotherPoint(targetPosition);
		
	}

	public int getActionDurationInSeconds() {
		return actionDurationInMilliseconds;
	}

	public Monster getMonster() {
		return monster;
	}

	public IGamePoint getMonsterPosition() {
		return monsterPosition;
	}

	public Player getTarget() {
		return target;
	}

	public IGamePoint getTargetPosition() {
		return targetPosition;
	}

	public IDirection getMonsterToTargetDirection() {
		return monsterToTargetDirection;
	}	 
	
	
}
