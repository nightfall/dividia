package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;

public class PossibilitySharedFunctions {
	
	public static double getTimeSpendingScore(Monster monster, Player target, int millisecondsSpent) {
		return 0;
	}
	
	public static double getChanceToHitTarget(IGameArea areaOfEffect, Player target, int millisecOfDelay) {
		return 1;
	}
	
	public static double getApproxTimeForMovement(Monster monster, IGamePoint start, IGamePoint end) {
		return 0;
	}
}
