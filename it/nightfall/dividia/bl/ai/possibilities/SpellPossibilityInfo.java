package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.battle.definitions.IFocusedSpellDefinition;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.SharedFunctions;

public class SpellPossibilityInfo extends PossibilityInfo {

	private int castingDurationMs; 
	
	public SpellPossibilityInfo(Monster monster, ISpellDefinition spell) {
		
		super(monster);
		castingDurationMs = SharedFunctions.getFinalCastDuration(spell, monster);
	}	

	IGamePoint closestToTargetPointInSelection(IFocusedSpellDefinition focusedSpell) {
		return monsterToTargetDirection.getDistancedPointAlongDirection(monsterPosition, focusedSpell.getSelectionRadius());
	}
	
}
