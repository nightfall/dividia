package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.utility.IDecisionScore;
import it.nightfall.dividia.bl.battle.definitions.TargetAreaSpellDefinition;
import it.nightfall.dividia.bl.characters.Monster;

public class TargetAreaSpellPossibility implements IPossibility{

	private TargetAreaSpellDefinition spell;
	
	public TargetAreaSpellPossibility(TargetAreaSpellDefinition spell) {
		this.spell = spell;
	}

	@Override
	public IDecisionScore evaluate(Monster monster) {
		return null;
	}

}
