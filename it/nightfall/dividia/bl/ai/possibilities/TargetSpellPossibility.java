package it.nightfall.dividia.bl.ai.possibilities;

import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.utility.IDecisionScore;
import it.nightfall.dividia.bl.battle.definitions.TargetSpellDefinition;
import it.nightfall.dividia.bl.characters.Monster;

public class TargetSpellPossibility implements IPossibility{

	private TargetSpellDefinition spell;
	
	public TargetSpellPossibility(TargetSpellDefinition spell) {
		this.spell = spell;
	}

	@Override
	public IDecisionScore evaluate(Monster monster) {
		return null;
	}

}
