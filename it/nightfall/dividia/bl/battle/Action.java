package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.IDamageSource;
import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import java.util.LinkedList;
import java.util.List;

/**
 * Describes an action performed by a playing character.<br>
 * An action is created within an UndertakeAction method conveniently overloaded
 * in PlayingCharacter class.
 *
 * @author Nightfall
 * @see PlayingCharacter
 */
public class Action implements IAction {
	private final int damage;
	private final List<IAlteredStatusDefinition> alteredStatuses;
	private final IElement element;
	private final IDamageType actionType;
	private final IDamageSource actionSource;
	private final String actionSourceId;
	/**
	 * Creates an Action.
	 *
	 * @param element       The element of the damage this action is going to deal.
	 * @param type          The type of the damage this action is going to deal (see IDamageType).
	 * @param damage        How much damage this action is going to deal.
	 * @param alteredStatus The AlteredStatus-es this action is going to inflict.
	 * @see IDamageType
	 */
	public Action(IElement element, IDamageType type, IDamageSource source, String actionSourceId, int damage, List<IAlteredStatusDefinition> alteredStatuses){
		this.element = element;
		this.actionType = type;
		this.actionSource = source;
		this.damage = damage;
		if(alteredStatuses == null){
			this.alteredStatuses = new LinkedList<>();
		}else{
			this.alteredStatuses = alteredStatuses;
		}
		this.actionSourceId = actionSourceId;
	}

	@Override
	public int getDamage(){
		return damage;
	}

	@Override
	public List<IAlteredStatusDefinition> getAlteredStatuses(){
		return alteredStatuses;
	}

	@Override
	public IElement getElement(){
		return element;
	}

	@Override
	public IDamageType getActionType(){
		return actionType;
	}

	@Override
	public IDamageSource getDamageSource(){
		return actionSource;
	}

	@Override
	public String getSourceId() {
		return actionSourceId;
	}
}
