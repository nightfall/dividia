package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.IBattleManager;
import it.nightfall.dividia.api.battle.executions.IAreaExecution;
import it.nightfall.dividia.api.battle.executions.IAreaSpellExecution;
import it.nightfall.dividia.api.battle.executions.IBasicAttackExecution;
import it.nightfall.dividia.api.battle.executions.IExecution;
import it.nightfall.dividia.api.battle.executions.IFocusedSpellExecution;
import it.nightfall.dividia.api.battle.executions.ITargetAreaSpellExecution;
import it.nightfall.dividia.api.battle.executions.ITargetSpellExecution;
import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.GameArea;
import java.util.List;

public class BattleManager implements IBattleManager {
	private IBattleMap battleMap;

	public BattleManager(IBattleMap map){
		this.battleMap = map;
	}
	
	@Override
	public void executionSpreading(IExecution spellExc){
		Thread spellThread = new Thread(spellExc);
		spellThread.setName("execution");
		spellThread.start();
	}

	@Override
	public boolean checkExecutionInteraction(IAreaSpellExecution areaSpellExc){
		IGameArea interestedArea = getInterestedArea(areaSpellExc);
		List<PlayingCharacter> charactersInArea = battleMap.getPlayingAffectedCharactersInArea(interestedArea, areaSpellExc.getExecutor(), areaSpellExc.getSpell().isForEnemiesOnly());
		return propagateDamage(charactersInArea, areaSpellExc);
	}

	@Override
	public boolean checkExecutionInteraction(IFocusedSpellExecution focusedSpellExc){
		IGameArea interestedArea = getInterestedArea(focusedSpellExc);
		List<PlayingCharacter> interestedPlayingCharacters = battleMap.getPlayingAffectedCharactersInArea(interestedArea, focusedSpellExc.getExecutor(), focusedSpellExc.getSpell().isForEnemiesOnly());
		return propagateDamage(interestedPlayingCharacters, focusedSpellExc);
	}

	@Override
	public boolean checkExecutionInteraction(ITargetSpellExecution targetSpellExc){
		IGameArea interestedArea = new GameArea(IGameConstants.SPELL_RADIUS);
		interestedArea = interestedArea.getGameAreaByTranslation(targetSpellExc.getSpellPosition());
		interestedArea.rotate(targetSpellExc.getExecutor().getDirection());
		List<PlayingCharacter> charactersInArea = battleMap.getPlayingAffectedCharactersInArea(interestedArea, targetSpellExc.getExecutor(), targetSpellExc.getSpell().isForEnemiesOnly());
		return propagateDamage(charactersInArea, targetSpellExc);
	}

	@Override
	public boolean checkExecutionInteraction(IBasicAttackExecution basicalAttackExc){
		IGameArea interestedArea = getInterestedArea(basicalAttackExc);
		List<PlayingCharacter> charactersInArea = battleMap.getPlayingAffectedCharactersInArea(interestedArea, basicalAttackExc.getExecutor(), true);
		return propagateDamage(charactersInArea, basicalAttackExc);
	}

	@Override
	public boolean checkExecutionInteraction(ITargetAreaSpellExecution targetAreaSpellExc, boolean exploded){
		if(!exploded){
			ITargetSpellExecution notExploded = (ITargetSpellExecution) targetAreaSpellExc;
			return checkExecutionInteraction(notExploded);
		}
		IGameArea interestedArea = targetAreaSpellExc.getInterestedArea();
		List<PlayingCharacter> charactersInArea = battleMap.getPlayingAffectedCharactersInArea(interestedArea, targetAreaSpellExc.getExecutor(), targetAreaSpellExc.getSpell().isForEnemiesOnly());
		return propagateDamage(charactersInArea, targetAreaSpellExc);
	}

	private boolean propagateDamage(List<PlayingCharacter> charactersInArea, IExecution execution){
		if(charactersInArea.isEmpty()){
			return false;
		}
		for(PlayingCharacter playingCharacter : charactersInArea){
			if(!playingCharacter.isDead()) {
				IDamageSummary dmgSummary = playingCharacter.receiveAction(execution.getAction(), execution.getExecutor());
				if(dmgSummary.getDamage()>0) {
					EventManager.getInstance().damageReceived(playingCharacter, execution.getExecutor(), dmgSummary);
				}
			}
		}
		return true;
	}

	private IGameArea getInterestedArea(IAreaExecution execution){
		//TODO apply this fix to all areas!
		IGameArea interestedArea = execution.getAreaOfEffect().getGameAreaByRotation(execution.getDirection());
		interestedArea.translate(execution.getReferencePoint());
		return interestedArea;
	}
}
