package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.IDamageSource;
import java.io.Serializable;

public enum DamageSource implements IDamageSource, Serializable {
	SPELL("Spell"),
	ALTERED_STATUS("Altered Status"),
	BASIC_ATTACK("Basic Attack");
	private String name;

	DamageSource(String name){
		this.name = name;
	}

	@Override
	public String getName(){
		return name;
	}
}