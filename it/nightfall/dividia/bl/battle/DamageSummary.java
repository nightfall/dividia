package it.nightfall.dividia.bl.battle;
import it.nightfall.dividia.api.battle.IDamageSource;
import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.battle.IDamageType;
import java.util.Collections;
import java.util.Set;

public class DamageSummary implements IDamageSummary {
	private int damage;
	private Set<String> alteredStatus;
	private IDamageSource source;
	private IDamageType damageType;
	private String sourceId;

	public DamageSummary(int damage, Set<String> alteredStatus, IDamageSource source, String sourceId, IDamageType damageType){
		this.damage = damage;
		this.alteredStatus = alteredStatus;
		this.source = source;
		this.sourceId = sourceId;
		this.damageType = damageType;
	}

	@Override
	public Set<String> getAlteredStatus(){
		return Collections.unmodifiableSet(alteredStatus);
	}

	@Override
	public int getDamage(){
		return damage;
	}

	@Override
	public IDamageSource getSource() {
		return source;
	}

	@Override
	public String getSourceId() {
		return sourceId;
	}

	@Override
	public IDamageType getDamageType() {
		return damageType;
	}
}
