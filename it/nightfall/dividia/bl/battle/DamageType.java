package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.IDamageType;

public enum DamageType implements IDamageType{
	MAGICAL("Magical"),
	PHYSICAL("Physical"),
	PURE("Pure");
	private String name;

	DamageType(String name){
		this.name = name;
	}

	@Override
	public String getName(){
		return name;
	}
}
