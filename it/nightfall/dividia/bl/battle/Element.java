package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.IElement;

public enum Element implements IElement {
	FIRE("Fire"),
	WATER("Water"),
	FLOW("Flow"),
	EARTH("Earth"),
	ARCTIC("Arctic"),
	THUNDER("Thunder"),
	STEEL("Steel"),
	FIGHT("Fight"),
	DARK("Dark"),
	LUX("Lux"),
	PLASMA("Plasma");
	
	private String name;
	
	Element(String name){
		this.name = name;
	}
	
	@Override
	public String getName(){
		return name;
	}
}
