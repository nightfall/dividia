
package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.bl.characters.PlayingCharacter;


public class HoldOnHandler implements Runnable {

	private float sleepTime;
	private PlayingCharacter awaitingCharacter;

	public HoldOnHandler(float sleepTime, PlayingCharacter awaitingCharacter) {
		this.sleepTime = sleepTime;
		this.awaitingCharacter = awaitingCharacter;
	}
	
	
	
	@Override
	public void run() {
		try {
			Thread.sleep((int) (sleepTime * 1000));
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
		awaitingCharacter.holdOnTerminated();
	}

}
