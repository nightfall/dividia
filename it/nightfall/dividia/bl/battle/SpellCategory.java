package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.ISpellCategory;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import java.util.ArrayList;
import java.util.List;

public class SpellCategory implements ISpellCategory {
	private final String name;
	private final String displayName;
	private final List<String> spellEvolution;

	public SpellCategory(IConfigReader config){
		name = config.getString("name");
		displayName = config.getString("displayName");
		spellEvolution = new ArrayList<>(config.getConfigReader("spells").getStringList("spell"));
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}

	@Override
	public List<String> getSpellEvolution(){
		return spellEvolution;
	}
}
