/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Nightfall
 */
public enum StatusBattleCharacteristic implements IStatusBattleCharacteristic {
	//TODO move in characters package
	CURRENT_HP("currentHp", false),
	CURRENT_MP("currentMp", false),
	MAX_HP("maxHp", false),
	MAX_MP("maxMp", false),
	MAX_RAGE("maxRage", false),
	AVAILABLE_SP("availableSp", false),
	PHYSICAL_ATTACK_POWER("physicalAttackPower", false),
	ATTACK_SPEED("attackSpeed", false),
	MAGIC_ATTACK_POWER("magicAttackPower", false),
	CAST_SPEED("castSpeed", false),
	SPELL_REFRESH_SPEED("spellRefreshSpeed", false),
	PHYSICAL_DEFENCE("physicalDefence", true),
	MAGIC_DEFENCE("magicDefence", true),
	ELUSION("elusion", false),
	MOVEMENT_SPEED("movementSpeed", false),
	CURRENT_RAGE("currentRage", false),
	SUSCEPTIBILITY("susceptibility", false);
	private String name;
	private boolean allowsNegativeValues;

	StatusBattleCharacteristic(String name, boolean allowsNegativeValues){
		this.name = name;
		this.allowsNegativeValues = allowsNegativeValues;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public boolean doesAllowNegativeValues(){
		return allowsNegativeValues;
	}

	@Override
	public String getInstanceName(){
		return this.name();
	}
	
	public static Set<IStatusBattleCharacteristic> getBattleGrowingStats(){
		Set<IStatusBattleCharacteristic> growingStats = new HashSet<>();
		growingStats.add(MAX_HP);
		growingStats.add(MAX_MP);
		growingStats.add(AVAILABLE_SP);
		growingStats.add(PHYSICAL_ATTACK_POWER);
		growingStats.add(MAGIC_ATTACK_POWER);
		growingStats.add(PHYSICAL_DEFENCE);
		growingStats.add(MAGIC_DEFENCE);
		return growingStats;
	}
	
	/*public static Set<IStatusBattleCharacteristic> getStartingZeroStats(){
		Set<IStatusBattleCharacteristic> growingStats = new HashSet<>();
		growingStats.add(CURRENT_RAGE);
		growingStats.add(ELUSION);
		growingStats.add(AVAILABLE_SP);
		return growingStats;
	}
	
	public static Set<IStatusBattleCharacteristic> getStartingOneHundredStats(){
		Set<IStatusBattleCharacteristic> growingStats = new HashSet<>();
		growingStats.add(CAST_SPEED);
		growingStats.add(SPELL_REFRESH_SPEED);
		growingStats.add(MOVEMENT_SPEED);
		return growingStats;
	}*/

	public static Set<IStatusBattleCharacteristic> getStatusAlterationsStats(){
		Set<IStatusBattleCharacteristic> alterableStats = new HashSet<>();
		alterableStats.add(CURRENT_HP);
		alterableStats.add(CURRENT_MP);
		alterableStats.add(PHYSICAL_ATTACK_POWER);
		alterableStats.add(ATTACK_SPEED);
		alterableStats.add(MAGIC_ATTACK_POWER);
		alterableStats.add(CAST_SPEED);
		alterableStats.add(SPELL_REFRESH_SPEED);
		alterableStats.add(PHYSICAL_DEFENCE);
		alterableStats.add(MAGIC_DEFENCE);
		alterableStats.add(ELUSION);
		alterableStats.add(MOVEMENT_SPEED);
		alterableStats.add(CURRENT_RAGE);
		return alterableStats;
	}
	
	public static Set<IStatusBattleCharacteristic> getRaceNonGrowingStats(){
		Set<IStatusBattleCharacteristic> raceStats = new HashSet<>();
		raceStats.add(ELUSION);
		raceStats.add(MOVEMENT_SPEED);
		raceStats.add(ATTACK_SPEED);
		raceStats.add(SPELL_REFRESH_SPEED);
		raceStats.add(CAST_SPEED);
		return raceStats;
	}
	
	//TODO: Gestire la growing stats sugli AvailableSP
}
