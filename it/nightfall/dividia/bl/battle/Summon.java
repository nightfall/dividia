package it.nightfall.dividia.bl.battle;

import it.nightfall.dividia.api.battle.ISummon;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IQuadraticFunction;
import it.nightfall.dividia.bl.battle.definitions.SpellDefinition;
import it.nightfall.dividia.bl.utility.QuadraticFunction;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.util.List;

public class Summon implements ISummon {
	//TODO si assume che il livello iniziale sia 1, inserire nella documentazione
	private int level;
	private int currentExperience;
	private int totalLevelExperience;
	private int currentSpellIndex;
	private List<SpellDefinition> summonSpellEvolutions;
	public final static IQuadraticFunction GROWING_FUNCTION = inizializeGrowingFunction();
	//TODO totalLevelExperience recalculation at level change

	public Summon(String name){
	}

	@Override
	public int getCurrentExperience(){
		return currentExperience;
	}

	@Override
	public int getTotalLevelExperience(){
		return totalLevelExperience;
	}

	public int getCurrentSpellIndex(){
		return currentSpellIndex;
	}

	@Override
	public ISpellDefinition getCurrentSpell(){
		return summonSpellEvolutions.get(currentSpellIndex);
	}

	@Override
	public int getLevel(){
		return level;
	}

	/**
	 * returns the spell index at current level
	 * it's calculated imposing that
	 * - index 0 is used for the first spell
	 * - every spell spans the same range of levels
	 * - the last index is equal to the NUMBER_OF_SUMMON_EVOLUTIONS_CONSTANT - 1
	 *
	 */
	private int getIndexAtCurrentLevel(){
		double levelProgress = SharedFunctions.division(level, IGameConstants.SUMMON_MAXIMUM_LEVEL);
		assert levelProgress >= 0 && levelProgress <= 1;
		double decimalIndex = levelProgress / SharedFunctions.division(1, IGameConstants.NUMBER_OF_SUMMONS_EVOLUTIONS);
		return (int) Math.floor(decimalIndex);
	}

	/**
	 * gives experience to this summon
	 * checking if level changes
	 * and if summon spell gets improved
	 *
	 */
	private void levelUp(){
		assert level < IGameConstants.SUMMON_MAXIMUM_LEVEL;
		totalLevelExperience = getExperienceNeededAtNextLevel();
		level++;
	}

	@Override
	public void gainExperience(int gainedExperience){
		if(level >= IGameConstants.SUMMON_MAXIMUM_LEVEL){
			return;
		}
		if(currentExperience + gainedExperience <= totalLevelExperience){
			currentExperience += gainedExperience;
		}else{
			levelUp();
			if(getIndexAtCurrentLevel() > currentSpellIndex){
				currentSpellIndex++;
				assert currentSpellIndex < IGameConstants.NUMBER_OF_SUMMONS_EVOLUTIONS && currentSpellIndex > 0;
			}

			gainedExperience = gainedExperience - (totalLevelExperience - currentExperience);
			gainExperience(gainedExperience);
		}
	}

	@Override
	public double getCastFactor(){
		double levelRange = SharedFunctions.division(IGameConstants.SUMMON_MAXIMUM_LEVEL, IGameConstants.NUMBER_OF_SUMMONS_EVOLUTIONS);
		double previousStep = currentSpellIndex * levelRange;
		double levelProgress = SharedFunctions.division(level - previousStep, levelRange);
		assert levelProgress * IGameConstants.MIN_SUMMON_CAST_REDUCTION_FACTOR <= 1 && levelProgress * IGameConstants.MIN_SUMMON_CAST_REDUCTION_FACTOR >= IGameConstants.MIN_SUMMON_CAST_REDUCTION_FACTOR;
		return levelProgress * IGameConstants.MIN_SUMMON_CAST_REDUCTION_FACTOR;
	}
	
	/**
	 * Get the experience needed for a summon to complete such level
	 * 
	 * @param level 
	 *            : summon's level
	 * @return : experience needed
	 */	
	@Override
	public int getExperienceNeededAtNextLevel() {
		return GROWING_FUNCTION.getIntegerFunctionImage(level+1);
	}
	
	private static IQuadraticFunction inizializeGrowingFunction() {
		return new QuadraticFunction(IGameConstants.SUMMON_EPX_KNOWN_TERM, 
				IGameConstants.SUMMON_EPX_LINEAR_COEFFICIENT, IGameConstants.SUMMON_EPX_QUADRATIC_COEFFICIENT);
	}

	@Override
	public ISummary getSummary(){
		//TODO: Implementare
		return null;
	}
	
}
