package it.nightfall.dividia.bl.battle.definitions;

import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.battle.ISpellCaster;
import it.nightfall.dividia.api.battle.definitions.IAreaSpellDefinition;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.ai.possibilities.AreaSpellPossibility;
import it.nightfall.dividia.bl.utility.GameArea;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.awt.geom.Rectangle2D;

/**
 * An AreaSpell damages targets in an area.
 *
 * @author Nightfall
 */
public class AreaSpellDefinition extends SpellDefinition implements IAreaSpellDefinition {
	protected final IGameArea areaOfEffect;
	protected final float travelDuration;
	
	public AreaSpellDefinition(IConfigReader config){
		super(config);
		travelDuration = config.getFloat("travelDuration");
		areaOfEffect = new GameArea(config.getConfigReader("areaOfEffect"));
	}

	@Override
	public IGameArea getAreaOfEffect(){
		return areaOfEffect;
	}

	@Override
	public void cast(ISpellCaster caster){
		caster.castAreaSpell(this.name);
	}

	@Override
	public float getTravelDuration() {
		return travelDuration;
	}

	@Override
	public IPossibility getPossibility() {
		return new AreaSpellPossibility(this);
	}

	@Override
	public float getMaxApproachingDistance() {
		return SharedFunctions.getMaxApproachingDistance(areaOfEffect);
	}
	
	@Override
	public float getMinApproachingDistance() {
		return SharedFunctions.getMinApproachingDistance(areaOfEffect);
	}
}
