package it.nightfall.dividia.bl.battle.definitions;

import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.battle.ISpellCaster;
import it.nightfall.dividia.api.battle.definitions.IFocusedSpellDefinition;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.ai.possibilities.FocusedSpellPossibility;
import it.nightfall.dividia.bl.utility.GameArea;

public class FocusedSpellDefinition extends SpellDefinition implements IFocusedSpellDefinition {
	protected final IGameArea areaOfEffect;
	protected final double selectionRadius;

	public FocusedSpellDefinition(IConfigReader config){
		super(config);
		areaOfEffect = new GameArea(config.getConfigReader("areaOfEffect"));
		selectionRadius = config.getInteger("selectionRadius");
	}

	@Override
	public IGameArea getAreaOfEffect(){
		return areaOfEffect;
	}

	@Override
	public double getSelectionRadius(){
		return selectionRadius;
	}

	@Override
	public void cast(ISpellCaster caster){
		caster.castFocusedSpell(this.name);
	}

	@Override
	public IPossibility getPossibility() {
		return new FocusedSpellPossibility(this);
	}
}
