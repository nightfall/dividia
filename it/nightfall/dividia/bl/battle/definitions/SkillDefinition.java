package it.nightfall.dividia.bl.battle.definitions;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.definitions.ISkillDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.battle.Element;
import it.nightfall.dividia.bl.characters.Player;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SkillDefinition implements ISkillDefinition {
	private final String name;
	private final String displayName;
	private final int maxSP;
	private final int maxHPAlteration;
	private final int maxMPAlteration;
	private final int physicalAttackPowerAlteration;
	private final int physicalAttackSpeedAlteration;
	private final int magicalAttackPowerAlteration;
	private final int castSpeedAlteration;
	private final int cooldownAlteration;
	private final int physicalDefenceAlteration;
	private final int magicalDefenceAlteration;
	private final int elusionAlteration;
	private final int movementSpeedAlteration;
	private final int susceptibilityAlteration;
	private final Map<IElement, Integer> elementsResisteceAlteration;

	public SkillDefinition(IConfigReader config){
		name = config.getString("name");
		displayName = config.getString("displayName");
		maxSP = config.getInteger("maxSP");
		maxHPAlteration = config.getInteger("maxHPAlteration");
		maxMPAlteration = config.getInteger("maxMPAlteration");
		physicalAttackPowerAlteration = config.getInteger("physicalAttackPowerAlteration");
		physicalAttackSpeedAlteration = config.getInteger("physicalAttackSpeedAlteration");
		magicalAttackPowerAlteration = config.getInteger("magicalAttackPowerAlteration");
		castSpeedAlteration = config.getInteger("castSpeedAlteration");
		cooldownAlteration = config.getInteger("cooldownAlteration");
		physicalDefenceAlteration = config.getInteger("physicalDefenceAlteration");
		magicalDefenceAlteration = config.getInteger("magicalDefenceAlteration");
		elusionAlteration = config.getInteger("elusionAlteration");
		movementSpeedAlteration = config.getInteger("movementSpeedAlteration");
		susceptibilityAlteration = config.getInteger("susceptibilityAlteration");
		Collection<IConfigReader> resistenceAlterationList = config.getConfigReader("elementsResistenceAlteration").getConfigReaderList("alteration");
		Map<IElement, Integer> tempAlterations = new HashMap<>();
		for(IConfigReader alteration : resistenceAlterationList){
			tempAlterations.put(Element.valueOf(alteration.getString("element")), alteration.getInteger("value"));
		}
		elementsResisteceAlteration = tempAlterations;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}

	@Override
	public int getMaxSP(){
		return maxSP;
	}

	@Override
	public int getMaxHPAlteration(){
		return maxHPAlteration;
	}

	@Override
	public int getMaxMPAlteration(){
		return maxMPAlteration;
	}

	@Override
	public int getPhisicalAttackPowerAlteration(){
		return physicalAttackPowerAlteration;
	}

	@Override
	public int getPhisicalAttackSpeedAlteration(){
		return physicalAttackSpeedAlteration;
	}

	@Override
	public int getMagicalAttackPowerAlteration(){
		return magicalAttackPowerAlteration;
	}

	@Override
	public int getCastSpeedAlteration(){
		return castSpeedAlteration;
	}

	@Override
	public int getCooldownAlteration(){
		return cooldownAlteration;
	}

	@Override
	public int getPhisicalDefenceAlteration(){
		return physicalDefenceAlteration;
	}

	@Override
	public int getMagicalDefenceAlteration(){
		return magicalDefenceAlteration;
	}

	@Override
	public int getElusionAlteration(){
		return elusionAlteration;
	}

	@Override
	public int getMovementSpeedAlteration(){
		return movementSpeedAlteration;
	}

	@Override
	public int getSusceptibilityAlteration(){
		return susceptibilityAlteration;
	}

	@Override
	public Map<IElement, Integer> getElementsResisteceAlteration(){
		return elementsResisteceAlteration;
	}

	@Override
	public void modifyStatus(Player player, boolean bool){
	}
}
