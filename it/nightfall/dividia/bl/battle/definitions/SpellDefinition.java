package it.nightfall.dividia.bl.battle.definitions;

import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.ISpellCaster;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.battle.DamageType;
import it.nightfall.dividia.bl.battle.Element;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.characters.Status;
import it.nightfall.dividia.bl.utility.AlteredStatusSuccessRate;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class SpellDefinition implements ISpellDefinition {
	protected final String name;
	protected final String displayName;
	protected final int levelRequirement;
	protected final float castDuration;
	protected final float cooldown;
	protected final boolean forEnemiesOnly;
	protected final IDamageType spellType;
	protected final IElement spellElement;
	protected final int spellDamage;
	protected final int powerRatio;
	protected final int mpCost;
	protected final boolean rageSpell;
	protected final boolean summoningSpell;
	protected final List<AlteredStatusSuccessRate> alteredStatusesWithRates;
	protected final float holdOnDuration;

	public SpellDefinition(IConfigReader config){
		name = config.getString("name");
		holdOnDuration = config.getFloat("holdOnDuration");
		displayName = config.getString("displayName");
		levelRequirement = config.getInteger("levelRequirement");		
		castDuration = config.getFloat("castDuration");
		cooldown = config.getFloat("cooldown");
		forEnemiesOnly = config.getBoolean("forEnemiesOnly");
		spellType = DamageType.valueOf(config.getString("spellType"));
		spellElement = Element.valueOf(config.getString("spellElement"));
		spellDamage = config.getInteger("spellDamage");
		powerRatio = config.getInteger("powerRatio");
		mpCost = config.getInteger("mpCost");
		rageSpell = config.getBoolean("rageSpell");
		summoningSpell = config.getBoolean("summoningSpell");
		Collection<IConfigReader> tempList = config.getConfigReader("alteredStatusesWithRates").getConfigReaderList("alteredStatusRate");
		List<AlteredStatusSuccessRate> tempAlteredStatusesList = new ArrayList<>();
		for(IConfigReader tempNode : tempList){
			tempAlteredStatusesList.add(new AlteredStatusSuccessRate(tempNode));
		}
		alteredStatusesWithRates = tempAlteredStatusesList;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}

	@Override
	public IElement getSpellElement(){
		return spellElement;
	}

	@Override
	public IDamageType getSpellType(){
		return spellType;
	}

	@Override
	public boolean isSummoningSpell(){
		return summoningSpell;
	}

	@Override
	public int getActionDamage(Status playingCharacterStatus){
		int statusDamage;
		if(spellType == DamageType.MAGICAL){
			statusDamage = playingCharacterStatus.getCharacteristic(StatusBattleCharacteristic.MAGIC_ATTACK_POWER);
		}else{
			statusDamage = playingCharacterStatus.getCharacteristic(StatusBattleCharacteristic.PHYSICAL_ATTACK_POWER);
		}
		double affectedByStatusDamage = (statusDamage * (powerRatio / 100.0f)) + spellDamage;
		return (int) Math.round(affectedByStatusDamage);

	}

	@Override
	public int getSpellDamage(){
		return spellDamage;
	}

	@Override
	public int getPowerRatio(){
		return powerRatio;
	}
	
	@Override
	public List<AlteredStatusSuccessRate> getAlteredStatusesWithRates(){
		return alteredStatusesWithRates;
	}

	@Override
	public double getCastDuration(){
		return castDuration;
	}

	@Override
	public float getCooldown(){
		return cooldown;
	}

	@Override
	public boolean isForEnemiesOnly(){
		return forEnemiesOnly;
	}

	@Override
	public int getLevelRequirement(){
		return levelRequirement;
	}

	@Override
	public List<IAlteredStatusDefinition> getSuccededAlteredStatuses(){
		return SharedFunctions.getSuccededAlteredStatuses(this.alteredStatusesWithRates);
	}

	@Override
	public int getMpCost(){
		return mpCost;
	}

	@Override
	public boolean isRageSpell() {
		return rageSpell;
	}	

	@Override
	public double getPlayingCharacterCastDuration(PlayingCharacter character){
		Status tempStatus = character.getStatus();
		return (SharedFunctions.division(castDuration , tempStatus.getCharacteristic(StatusBattleCharacteristic.CAST_SPEED))) * 100;
	}
	
	@Override
	public abstract void cast(ISpellCaster caster);

	@Override
	public float getHoldOnDuration() {
		return holdOnDuration;
	}
	
	
	
	
}
