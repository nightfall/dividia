package it.nightfall.dividia.bl.battle.definitions;

import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.battle.ISpellCaster;
import it.nightfall.dividia.api.battle.definitions.ITargetAreaSpellDefinition;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.ai.possibilities.TargetAreaSpellPossibility;
import it.nightfall.dividia.bl.utility.GameArea;

public class TargetAreaSpellDefinition extends TargetSpellDefinition implements ITargetAreaSpellDefinition {
	private final IGameArea areaOfEffect;

	public TargetAreaSpellDefinition(IConfigReader config){
		super(config);
		areaOfEffect = new GameArea(config.getConfigReader("areaOfEffect"));
	}

	@Override
	public IGameArea getAreaOfEffect(){
		return areaOfEffect;
	}

	@Override
	public void cast(ISpellCaster caster){
		caster.castTargetAreaSpell(this.name);
	}

	@Override
	public IPossibility getPossibility() {
		return new TargetAreaSpellPossibility(this);
	}
}
