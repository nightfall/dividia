package it.nightfall.dividia.bl.battle.definitions;

import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.battle.ISpellCaster;
import it.nightfall.dividia.api.battle.definitions.ITargetSpellDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.ai.possibilities.TargetSpellPossibility;

public class TargetSpellDefinition extends SpellDefinition implements ITargetSpellDefinition {
	protected final double damageRadius;
	protected final double selectionRadius;
	protected final boolean multiTarget;
	protected final float duration;
	protected final int movementSpeed;

	public TargetSpellDefinition(IConfigReader config){
		super(config);
		damageRadius = config.getInteger("damageRadius");
		selectionRadius = config.getInteger("selectionRadius");
		multiTarget = config.getBoolean("multiTarget");
		duration = config.getFloat("duration");
		movementSpeed = config.getInteger("movementSpeed");
	}

	@Override
	public double getDamageRadius(){
		return damageRadius;
	}

	@Override
	public double getSelectionRadius(){
		return selectionRadius;
	}

	@Override
	public boolean isMultiTarget(){
		return multiTarget;
	}

	@Override
	public float getDuration(){
		return duration;
	}

	@Override
	public int getMovementSpeed(){
		return movementSpeed;
	}

	@Override
	public void cast(ISpellCaster caster){
		caster.castTargetSpell(this.name);
	}

	@Override
	public IPossibility getPossibility() {
		return new TargetSpellPossibility(this);
	}
}
