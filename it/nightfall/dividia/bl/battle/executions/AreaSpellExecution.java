package it.nightfall.dividia.bl.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.definitions.IAreaSpellDefinition;
import it.nightfall.dividia.api.battle.executions.IAreaSpellExecution;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.battle.HoldOnHandler;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.events.EventManager;

/**
 * Represent the execution of an Area Spell.
 *
 * @author Nightfall
 */
public class AreaSpellExecution extends SpellExecution implements IAreaSpellExecution{
	IAreaSpellDefinition spell;

	public AreaSpellExecution(PlayingCharacter executor, IAction action, IAreaSpellDefinition spell){
		super(executor, action, spell);
		this.spell = spell;
	}

	/**
	 * This method waits for the area spell animation and, at a certain frame,
	 * calls checkExecutionInteraction (a Battle Manager's method) that apply
	 * this spell on its area of effect.
	 */
	@Override
	public void run(){
		EventManager.getInstance().spellCastingStart(spell, executor);
		if(!executionSleep(finalCastDuration)){
			return;
		}
		executor.goOnCooldown(spell);
		HoldOnHandler holdOn = new HoldOnHandler(spell.getHoldOnDuration(), executor);
		Thread holdOnThread = new Thread(holdOn);
		holdOnThread.setName("AreaSpell HoldOn");
		holdOnThread.start();
		EventManager.getInstance().launchAreaSpell(spell, getDirection(), getReferencePoint(),executor.getCurrentMap(), executor.getId());		
		travelSleep((int) (spell.getTravelDuration() * 1000));		
		battleManager.checkExecutionInteraction(this);
	}

	@Override
	public IGameArea getAreaOfEffect(){
		return spell.getAreaOfEffect();
	}

	@Override
	public IGamePoint getReferencePoint(){
		return this.executor.getPosition();
	}

	@Override
	public IDirection getDirection(){
		return this.executor.getDirection();
	}

	@Override
	public IAreaSpellDefinition getSpell(){
		return spell;
	}
}
