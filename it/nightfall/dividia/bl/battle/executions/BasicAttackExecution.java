package it.nightfall.dividia.bl.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.executions.IBasicAttackExecution;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.battle.HoldOnHandler;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.SharedFunctions;

/**
 * Represents the execution of a basic attack.
 *
 */
public class BasicAttackExecution extends Execution implements IBasicAttackExecution{
	private IGameArea executionArea;

	public BasicAttackExecution(PlayingCharacter executor, IAction action, IGameArea executionArea){
		super(executor, action);
		this.executionArea = executionArea;
	}

	public IGameArea getExecutionArea(){
		return executionArea; //May eliminate this
	}

	@Override
	public IGameArea getAreaOfEffect(){
		return executionArea;
	}

	@Override
	public void run(){
		//TODO events
		EventManager.getInstance().performBasicAttack(executor);
		float attackSpeedFactor =  SharedFunctions.division(100, this.executor.getStatus().getCharacteristic(StatusBattleCharacteristic.ATTACK_SPEED));
		if(!executionSleep((long) (attackSpeedFactor*1000/4))){
			return;
		}
		//TODO remove constant and set animation speed on the GUI
		HoldOnHandler holdOn = new HoldOnHandler(attackSpeedFactor/4*3, executor);
		Thread holdOnThread = new Thread(holdOn);
		holdOnThread.setName("BasicAttack HoldOn");
		holdOnThread.start();
		battleManager.checkExecutionInteraction(this);
	}

	@Override
	public IGamePoint getReferencePoint(){
		return this.getExecutor().getPosition();
	}

	@Override
	public IDirection getDirection(){
		return this.getExecutor().getDirection();
	}
}
