package it.nightfall.dividia.bl.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.IBattleManager;
import it.nightfall.dividia.api.battle.executions.IExecution;
import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

/**
 * An execution represents the act of performing an action.<br>
 * An execution has a PlayingCharacter who performs the action (the executor) and an Action being performed.
 *
 * @author Nightfall
 * @see PlayingCharacter
 * @see IAction
 */
public abstract class Execution implements IExecution{
	protected PlayingCharacter executor;
	protected IAction action;
	protected IBattleManager battleManager;

	/**
	 * Creates a new Execution.
	 *
	 * @param executor The PlayingCharacter performing the action.
	 * @param action The action being performed.
	 */
	public Execution(PlayingCharacter executor, IAction action){
		this.executor = executor;
		this.action = action;
		this.battleManager = ((IBattleMap) executor.getCurrentMap()).getBattleManager();
	}

	protected boolean executionSleep(long finalCastDuration){
		try{
			Thread.sleep(finalCastDuration);
		}catch(InterruptedException ex){
			ex.printStackTrace();
		}
		if(executor.getCurrentExecution() == null){
			return false;
		}
		return true;
	}
	
	protected void travelSleep(long sleepTime) {
		try{
			Thread.sleep(sleepTime);
		}catch(InterruptedException ex){
			ex.printStackTrace();
		}
	}

	@Override
	public abstract void run();

	@Override
	public IAction getAction(){
		return action;
	}

	@Override
	public PlayingCharacter getExecutor(){
		return executor;
	}
}
