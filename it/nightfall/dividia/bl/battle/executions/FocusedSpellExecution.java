package it.nightfall.dividia.bl.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.definitions.IFocusedSpellDefinition;
import it.nightfall.dividia.api.battle.executions.IFocusedSpellExecution;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public class FocusedSpellExecution extends SpellExecution implements IFocusedSpellExecution{
	private IFocusedSpellDefinition spell;
	private IGamePoint focus;

	public FocusedSpellExecution(PlayingCharacter executor, IAction action, IFocusedSpellDefinition spell, IGamePoint focus){
		super(executor, action, spell);
		this.focus = focus;
		this.spell = spell;
	}

	@Override
	public IGamePoint getFocus(){
		return focus;
	}

	@Override
	public void run(){
		if(!executionSleep(finalCastDuration)){
			return;
		}
		executor.goOnCooldown(spell);
		battleManager.checkExecutionInteraction(this);
	}

	@Override
	public IGameArea getAreaOfEffect(){
		return getSpell().getAreaOfEffect();
	}

	@Override
	public IGamePoint getReferencePoint(){
		return this.focus;
	}

	@Override
	public IDirection getDirection(){
		return this.getExecutor().getDirection();
	}

	@Override
	public IFocusedSpellDefinition getSpell(){
		return (IFocusedSpellDefinition) this.spell;
	}
}
