package it.nightfall.dividia.bl.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.battle.executions.ISpellExecution;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public abstract class SpellExecution extends Execution implements ISpellExecution {
	protected int finalCastDuration;

	public SpellExecution(PlayingCharacter executor, IAction action, ISpellDefinition spell){
		super(executor, action);
		this.finalCastDuration = (int) Math.round(spell.getPlayingCharacterCastDuration(executor) * 1000);
		if(spell.isSummoningSpell()){
			this.finalCastDuration = (int) Math.round(this.finalCastDuration * this.executor.getSummons().get(spell).getCastFactor());
		}
	}
}
