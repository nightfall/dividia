package it.nightfall.dividia.bl.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.definitions.ITargetAreaSpellDefinition;
import it.nightfall.dividia.api.battle.executions.ITargetAreaSpellExecution;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public class TargetAreaSpellExecution extends TargetSpellExecution implements ITargetAreaSpellExecution{
	private IDirection spellDirection;
	private ITargetAreaSpellDefinition spell;
	private boolean detonateSpell = false;

	public TargetAreaSpellExecution(PlayingCharacter executor, IAction action, ITargetAreaSpellDefinition spell, PlayingCharacter target){
		super(executor, action, spell, target);
		this.spell = spell;
	}

	@Override
	public void run(){
		if(!executionSleep(finalCastDuration)){
			return;
		}
		executor.goOnCooldown(spell);
		while(true){
			if(spellPosition.getDistanceFromAnotherPoint(targetCharacter.getPosition()) < stepDistance){
				spellPosition = targetCharacter.getPosition().clone();
			}else{
				moveExecution();
			}
			if(battleManager.checkExecutionInteraction(this, detonateSpell)){
				return;
			}else{
				spellDuration -= IGameConstants.BL_UPDATE_S;
			}
			try{
				if(spellDuration > 0){
					Thread.sleep(IGameConstants.BL_UPDATE_MS);
				}else{
					battleManager.checkExecutionInteraction(this, true);
					return;
				}
			}catch(InterruptedException ex){
				ex.printStackTrace();
			}
		}
	}

	@Override
	public ITargetAreaSpellDefinition getSpell(){
		return spell;
	}

	@Override
	public IGameArea getInterestedArea(){
		IGameArea interestedArea = spell.getAreaOfEffect().getGameAreaByTranslation(this.spellPosition);
		interestedArea.rotate(this.spellDirection);
		return interestedArea;
	}

	@Override
	public IGameArea getAreaOfEffect(){
		return spell.getAreaOfEffect();
	}

	@Override
	public IGamePoint getReferencePoint(){
		return getSpellPosition();
	}

	@Override
	public IDirection getDirection(){
		return this.getDirection();
	}
}
