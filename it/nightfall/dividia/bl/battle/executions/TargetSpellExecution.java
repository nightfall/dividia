package it.nightfall.dividia.bl.battle.executions;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.definitions.ITargetSpellDefinition;
import it.nightfall.dividia.api.battle.executions.ITargetSpellExecution;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.utility.*;

public class TargetSpellExecution extends SpellExecution implements ITargetSpellExecution {
	private ITargetSpellDefinition spell;
	protected IGamePoint spellPosition;
	protected PlayingCharacter targetCharacter;
	protected float spellDuration;
	protected double stepDistance;

	public TargetSpellExecution(PlayingCharacter executor, IAction action, ITargetSpellDefinition spell, PlayingCharacter target){
		super(executor, action, spell);
		this.spell = spell;
		this.targetCharacter = target;
		this.spellPosition = executor.getPosition().clone();
		this.stepDistance = (spell.getMovementSpeed() / 100) * IGameConstants.MOVEMENT_SPEED_CONSTANT;
	}

	@Override
	public void run(){
		if(!executionSleep(finalCastDuration)){
			return;
		}
		executor.goOnCooldown(spell);
		while(true){
			if(spellPosition.getDistanceFromAnotherPoint(targetCharacter.getPosition()) < stepDistance){
				spellPosition = targetCharacter.getPosition().clone();
			}else{
				moveExecution();
			}
			if(battleManager.checkExecutionInteraction(this)){
				return;
			}else{
				spellDuration -= IGameConstants.BL_UPDATE_S;
			}
			try{
				if(spellDuration > 0){
					Thread.sleep(IGameConstants.BL_UPDATE_MS);
				}else{
					return;
				}
			}catch(InterruptedException ex){
				ex.printStackTrace();
			}
		}
	}

	protected void moveExecution(){
		double deltaX, deltaY;
		IDirection tempDirection = SharedFunctions.getDirectionBetweenPoints(this.spellPosition, targetCharacter.getPosition());
		deltaX = tempDirection.getVersorX() * stepDistance;
		deltaY = tempDirection.getVersorY() * stepDistance;
		spellPosition.translate(deltaX, deltaY);
	}

	@Override
	public float getSpellDuration(){
		return spellDuration;
	}

	@Override
	public IGamePoint getSpellPosition(){
		return spellPosition;
	}

	@Override
	public PlayingCharacter getTargetCharacter(){
		return targetCharacter;
	}

	@Override
	public ITargetSpellDefinition getSpell(){
		return spell;
	}
}
