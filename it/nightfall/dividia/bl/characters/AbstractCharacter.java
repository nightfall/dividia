package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.characters.ICharacter;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.bl.world.MonsterGameMap;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractCharacter implements ICharacter {
	private IGamePoint position;
	private IDirection direction;
	private IGameMap currentMap;
	private String displayName;
	private ICollisionShape collisionShape;
	private Set<Integer> belongingSectors;

	public AbstractCharacter(String displayName, IGameMap gameMap, IGamePoint position, IDirection direction){
		this.displayName = displayName;
		this.currentMap = gameMap;
		this.position = position;
		this.direction = direction;
	}

	@Override
	public IGamePoint getPosition(){
		return position;
	}
	
	@Override
	public void initPosition(IGamePoint position){
		//TODO creare una funzione che restituisce il punto libero più vicino
		//in caso di collisione
		this.position = position;
		collisionShape.translateToFitCharacter(position);
	}

	public void setPosition(IGamePoint position){
		double deltaX = position.getX() - this.position.getX();
		double deltaY = position.getY() - this.position.getY();
		
		ICollisionShape translatedShape = (ICollisionShape)collisionShape.clone();
		translatedShape.translate(deltaX, deltaY);
		//collisionShape.translateToFitCharacter(this);
		Set<Integer> belongingSectorsAfterMovement = translatedShape.findBelongingSectors(currentMap.getDefinition());
		
		if(!currentMap.collisionDetected(belongingSectorsAfterMovement,translatedShape) && currentMap.doesPositionExist(position)){
			this.position = position;
			this.collisionShape = translatedShape;
		}else{
			EventManager.getInstance().characterStopped((PlayingCharacter)this);
			//System.out.println("COLLISION DETECTED");
		}
	}
	
	public void translatePosition(double deltaX, double deltaY){
		//TODO cercare negli intorni della posizione corrente, non tutti
		setPosition(new GamePoint(position.getX() + deltaX, position.getY() + deltaY));
		
	}

	@Override
	public IDirection getDirection(){
		return direction;
	}

	@Override
	public void setDirection(IDirection direction){
		this.direction = direction;
	}

	@Override
	public IGameMap getCurrentMap(){
		return currentMap;
	}
	
	@Override
	public void setCurrentMap(IGameMap currentMap){
		this.currentMap = currentMap;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}
	
	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		tempSummary.putData("positionX", position.getX());
		tempSummary.putData("positionY", position.getY());
		tempSummary.putData("direction", direction.getDirectionAngle());
		tempSummary.putData("currentMap", currentMap.getName());
		tempSummary.putData("displayName",displayName);
		return tempSummary;
	}
	
	@Override
	public Collection<Integer> getSectors(){
		return belongingSectors;
	}

	public ICollisionShape getCollisionShape(){
		return collisionShape;
	}

	public void setCollisionShape(ICollisionShape boundingShape) {
		this.collisionShape = boundingShape;
	}
	
}
