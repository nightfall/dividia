package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.characters.IAlteredStatus;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.battle.Action;
import it.nightfall.dividia.bl.battle.DamageSource;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.Set;

public class AlteredStatus implements IAlteredStatus {
	private final IAlteredStatusDefinition alteredStatusInfo;
	private PlayingCharacter playerCaster;
	private PlayingCharacter playerAffected;
	private int remainingTicks;
	private float durationRemaining;
	private boolean exitAlteration = false;

	public AlteredStatus(IAlteredStatusDefinition alteredStatusDefinition, PlayingCharacter caster, PlayingCharacter affected){
		alteredStatusInfo = alteredStatusDefinition;
		playerCaster = caster;
		playerAffected = affected;
		remainingTicks = alteredStatusDefinition.getTotalTicks();
		durationRemaining = alteredStatusDefinition.getDuration();
	}

	@Override
	public void run(){
		applyAlterations();
		while(true){
			try{
				Thread.sleep((int) (alteredStatusInfo.getTickInterval() * 1000));
			}catch(InterruptedException ex){
				ex.printStackTrace();
			}
			synchronized(this){
				if(exitAlteration){
					removeAlterations();
					playerAffected.getStatus().removeAlteredStatus(alteredStatusInfo.getName());
					return;
				}
			}
			if(remainingTicks > 0){
				applyDamage();
				applyAlterations();
			}
			if(durationRemaining <= 0){
				if(alteredStatusInfo.hasRemoveAlterations()){
					removeAlterations();
				}
				if(remainingTicks == 0){
					playerAffected.getStatus().removeAlteredStatus(alteredStatusInfo.getName());
					return;
				}
			}else{
				durationRemaining -= alteredStatusInfo.getTickInterval();
			}
			

		}
		
	}

	@Override
	public IAlteredStatusDefinition getAlteredStatusInfo(){
		return alteredStatusInfo;
	}

	@Override
	public PlayingCharacter getPlayerCaster(){
		return playerCaster;
	}

	@Override
	public PlayingCharacter getPlayerAffected(){
		return playerAffected;
	}

	@Override
	public int getRemainingTicks(){
		return remainingTicks;
	}

	@Override
	public float getDurationRemaining(){
		return durationRemaining;
	}

	@Override
	public synchronized void stopAlterations(){
		exitAlteration = true;
	}

	private void applyDamage(){
		if(alteredStatusInfo.getDamage() > 0 && !playerAffected.isDead()){
			IAction tempAction = new Action(alteredStatusInfo.getElement(), alteredStatusInfo.getDamageType(), DamageSource.ALTERED_STATUS, alteredStatusInfo.getName(), alteredStatusInfo.getDamage(), null);
			IDamageSummary dmgSummary = playerAffected.receiveAction(tempAction, playerCaster);
			EventManager.getInstance().damageReceived(playerAffected, playerCaster, dmgSummary);
		}
	}

	private void applyAlterations(){
		Status status = playerAffected.getStatus();
		Set<IStatusBattleCharacteristic> tempAlterations = StatusBattleCharacteristic.getStatusAlterationsStats();
		for(IStatusBattleCharacteristic characteristic : tempAlterations){
			int alteration = alteredStatusInfo.getAlteration(characteristic);
			if(alteration != 0){
				status.updateCharacteristic(characteristic, alteration);
			}
		}
		remainingTicks--;
	}

	private void removeAlterations(){
		Status status = playerAffected.getStatus();
		Set<IStatusBattleCharacteristic> tempAlterations = StatusBattleCharacteristic.getStatusAlterationsStats();
		for(IStatusBattleCharacteristic characteristic : tempAlterations){
			status.updateCharacteristic(characteristic, alteredStatusInfo.getAlteration(characteristic) * (alteredStatusInfo.getTotalTicks() - remainingTicks) * (-1));
		}
		//System.out.println(playerAffected.getId()+" "+playerAffected.getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED));
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		return tempSummary;
	}
}
