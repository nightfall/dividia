package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.characters.IInProgressState;

public enum InProgressState implements IInProgressState{
	
	WAITING("Waiting"),
	THINKING("Thinking"),
	CASTING("Casting"),
	WALKING("Walking"),
	PAUSE("Pause");
	
	private String name;
	
	InProgressState(String name){
		this.name = name;
	}
	
	@Override
	public String getName(){
		return name;
	}
}

