package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.ai.IPossibility;
import it.nightfall.dividia.api.ai.IThinker;
import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.definitions.IMonsterRaceDefinition;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.utility.ICallbackWaiting;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.IMovementPlanner;
import it.nightfall.dividia.api.utility.IQuadraticFunction;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.IMonsterMap;
import it.nightfall.dividia.bl.ai.AbstractAttackDecision;
import it.nightfall.dividia.bl.ai.CircleAreaPatrolManager;
import it.nightfall.dividia.bl.ai.Thinker;
import it.nightfall.dividia.bl.ai.possibilities.BasicAttackPossibility;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.item.BagOfItems;
import it.nightfall.dividia.bl.utility.FollowPlanner;
import it.nightfall.dividia.bl.utility.FutureCallback;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.MovementPlanner;
import it.nightfall.dividia.bl.utility.QuadraticFunction;
import it.nightfall.dividia.bl.world.MonsterGameMap;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class Monster extends PlayingCharacter {
	private IElement element;
	private double visibilityAngleScaleFactor = 1;
	private double visibilityLenghtScaleFactor = 1;
	private double udibilityScaleFactor = 1;
	private Status status;
	private Player target;
	private int expRewardOnMonsterKill;
	public final static IQuadraticFunction EXP_REWARD_FUNCTION = inizializeExpRewardFunction();
	private InProgressState inProgressState;
	private Queue<IDecision> decisionalQueue;
	private IMovementPlanner movementPlanner;
	private FollowPlanner followPlanner;
	private IThinker intelligence;
	private CircleAreaPatrolManager patrolManager;
	private Collection<IPossibility> possibilities;
    private IGamePoint lastCriticalPoint;
	private IMonsterRaceDefinition race;

	/**
	 * Creates a monster with the specified attributes, loading monster stats from a file called as "name" attribute.
	 *
	 * @param name : Name of the monster that represent also the name of the file containing monster's stats.
	 * @param gameMap : GameMap where the monster is loaded.
	 * @param x : Monster x position.
	 * @param y : Monster y position.
	 * @param direction : Monster direction.
	 * @param monsterAi : Monster artificial intelligence.
	 */
	public Monster(String id, String raceName, IGameMap gameMap, IGamePoint position,
			IDirection direction, int level ) {
			super(id,raceName,gameMap,position,direction);
			setRace(GameStorage.getInstance().getRaceDefinition(raceName));
			//TODO da caricare da xml/plug-in
			GuiStatus guiStatus = GuiStatus.WORLD_3D;
			if (gameMap.is2D()){
				guiStatus = GuiStatus.WORLD_2D;
			}
			setCollisionShape(guiStatus);
			super.initPosition(position);
			this.status = new Status(this);
			expRewardOnMonsterKill = inizializeExperienceReward(level);
			decisionalQueue = new LinkedList<>();
			inProgressState = InProgressState.WAITING;
			intelligence = new Thinker(this);
			movementPlanner = new MovementPlanner(this);
			followPlanner = new FollowPlanner(this);
			patrolManager = new CircleAreaPatrolManager(position.clone(), race.getDefaultPatrolRadius(), race.getDefaultPatrolMaxPause(), race.getDefaultPatrolMinPause(), race.getDefaultTiredDistance(),gameMap); // default value
            lastCriticalPoint = position.clone();
			possibilities = new ArrayList<>();			
			for(String spellId: race.getStartingSpells()) {
				learnSpell(GameStorage.getInstance().getSpellDefinition(spellId));
			}
			
			//todo delete, testing basic attacks
			getEquippedItems().setWeapon((IWeapon) (GameStorage.getInstance().getItemDefinition(race.getWeaponDefinition()).getItemFromDefinition()));
			
			//getEquippedItems().setWeapon((IWeapon) (GameStorage.getInstance().getItemDefinition("longSword").getItemFromDefinition()));
			//TODO da caricare da xml/plug-in
			setCollisionShape(gameMap.getGuiMode());
			super.initPosition(position);
			
	}
	
	private int inizializeExperienceReward(int level){

		return EXP_REWARD_FUNCTION.getIntegerFunctionImage(level);
	}

	public void updateMonster(int tpf){
		if(!decisionalQueue.isEmpty()){
			IDecision ongoingDecision = decisionalQueue.peek();
			ongoingDecision.applyNextStep(this, tpf);
		}
	}

	public synchronized Queue<IDecision> getDecisionalQueue(){
		return decisionalQueue;
	}

	@Override
	public synchronized void holdOnTerminated(){ //Safe
		if(currentExecution!=null) {
			super.holdOnTerminated();
			AbstractAttackDecision spellDecision = (AbstractAttackDecision)decisionalQueue.peek();
			if(spellDecision!=null) {
				spellDecision.castingTerminated();
			}
		}
	}
	
	public void respawn() {
		IMonsterMap map = (IMonsterMap) getCurrentMap();
		setPosition(this.patrolManager.getMonsterStartingPoint().clone());
		this.getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_HP, getStatus().getCharacteristic(StatusBattleCharacteristic.MAX_HP));
		this.getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_MP, getStatus().getCharacteristic(StatusBattleCharacteristic.MAX_MP));
		map.monsterRespawn(this);

	}
	
	@Override
	public synchronized IDamageSummary receiveAction(IAction action, PlayingCharacter executor) {
		if(executor instanceof Player && this.target==null && !executor.isDead())  {

			this.target = (Player) executor;			
		}
		lastCriticalPoint = this.getPosition().clone();
		return super.receiveAction(action, executor);
	}

	@Override
	protected synchronized void announceDeath(PlayingCharacter killer){
		currentExecution = null;
		IMonsterMap map = (IMonsterMap)this.getCurrentMap();
		map.monsterDied(this);
		Player killerPlayer = (Player) killer;		
		this.setTarget(null);
		this.removeAllDecisions();
		final Monster finalThis = this;
		FutureCallback respawnCallBack = new FutureCallback(new ICallbackWaiting() {
			@Override
			public boolean execute() {
				finalThis.respawn();
				return true;
			}
		});
		respawnCallBack.runAndWait(race.getDefaultSecondsToRespawn()*1000);
		killerPlayer.gainExperience(this.getPlayerExperienceOnMonsterKill());
		killerPlayer.gainManaAfterMonsterKill(this.race.getDefaultManaReward());
		eventuallyDropBagOfItems();
		EventManager.getInstance().monsterDied(this,  killerPlayer);
	}

	@Override
	public void playerKilled(Player player) {
		if(race.isRecoveringStatus()) {
			status.recoverHealtAndMana();
		}
	}

	public IElement getElement(){
		return element;
	}

	public double getUdibilityRadius(){
		return udibilityScaleFactor * IGameConstants.MONSTER_STANDARD_UDIBILITY_RADIUS;
	}

	public double getVisibilityLenght(){
		return visibilityLenghtScaleFactor * IGameConstants.MONSTER_STANDARD_VISIBILITY_LENGHT;
	}

	public double getVisibilityAngle(){
		return visibilityAngleScaleFactor * IGameConstants.MONSTER_STANDARD_VISIBILITY_HALF_ANGLE;
	}

	public Player getTarget(){
		return target;
	}

	public int getPlayerExperienceOnMonsterKill(){
		return expRewardOnMonsterKill;
	}

	public Collection<IPossibility> getPossibilities(){
		return possibilities;
	}

	private static IQuadraticFunction inizializeExpRewardFunction(){
		return new QuadraticFunction(IGameConstants.MONSTER_EPX_REWARD_KNOWN_TERM,
									 IGameConstants.MONSTER_EPX_REWARD_LINEAR_COEFFICIENT, IGameConstants.MONSTER_EPX_REWARD_QUADRATIC_COEFFICIENT);
	}

	@Override
	public Status getStatus(){
		return status;
	}

	public void setLevel(int newLevel){
		assert newLevel <= IGameConstants.MONSTER_MAXIMUM_LEVEL;
		status.setLevel(race, newLevel);
	}

	public void setInProgressState(InProgressState inProgressState){
		this.inProgressState = inProgressState;
	}

	public InProgressState getInProgressState(){
		return inProgressState;
	}

	public IMovementPlanner getMovementPlanner(){
		return movementPlanner;
	}

	public int getIntelligenceQuotience(){
		return race.getIntelligenceQuotience();
	}

	public void addDecision(IDecision addingDecision){
		List<IDecision> addingList  = new LinkedList<>();
		addingList.add(addingDecision);
		addDecisionList(addingList);
	}
	
	public synchronized void addDecisionList(List<IDecision> addingList){
		decisionalQueue.addAll(addingList);
	}

	@Override
	public ISummary getSummary(){
		//return super.getSummary();
		return super.getStrictSummary();
		/*ISummary summary = new Summary();
		summary.putData("id", getId());
		summary.putData("direction", getDirection().getDecimalDegreeAngle());
		summary.putData("positionX", getPosition().getX());
		summary.putData("positionY", getPosition().getY());	
		summary.putData("movementSpeed", status.getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED));
		summary.putData("attackSpeed", status.getCharacteristic(StatusBattleCharacteristic.ATTACK_SPEED));
		summary.putData("race", race.getName());
		return summary;*/
	}

	public void inizialitePossibilities(){
		for(ISpellDefinition spell : getSpells().values()){
			possibilities.add(spell.getPossibility());
		}
		possibilities.add(new BasicAttackPossibility());
	}

	public IThinker getThinker(){
		return this.intelligence;
	}

	public CircleAreaPatrolManager getPatrolManager(){
		return patrolManager;
	}

	public FollowPlanner getFollowPlanner(){
		return this.followPlanner;
	}

	public synchronized void lookAround(){
		MonsterGameMap map = (MonsterGameMap) getCurrentMap();
		List<Player> playersInPatrolArea = map.getPlayersInPatrolArea(this);
		if(!playersInPatrolArea.isEmpty()){
			target = playersInPatrolArea.get(0);
		}
	}

	public void setTarget(Player target) {
		this.target = target;
	}

	public synchronized void removeAllDecisions() {
		EventManager.getInstance().characterStopped(this);
		currentExecution = null;
		decisionalQueue.clear();
	}

    public IGamePoint getLastCriticalPoint() {
        return lastCriticalPoint;
    }
    
    public void resetLastCriticalPoint() {
       lastCriticalPoint = patrolManager.getMonsterStartingPoint().clone();
    }

	private void eventuallyDropBagOfItems() {
		Map<String, Float> dropRates = race.getDropRates();
		BagOfItems droppingBag = new BagOfItems();
		for(String itemId:dropRates.keySet()) {
			if(Math.random()<dropRates.get(itemId)) {
				droppingBag.addItem(GameStorage.getInstance().getItemDefinition(itemId).getItemFromDefinition());
			}
		}
		if(!droppingBag.isEmpty()) {
			droppingBag.setPosition(getPosition().clone());
			getCurrentMap().addBag(droppingBag);
			
		}
			
	}

	@Override
	public IMonsterRaceDefinition getRace() {
		return race;
	}

	@Override
	public void setRace(IRaceDefinition race) {
		this.race = (IMonsterRaceDefinition) race;
	}
	
}
