package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.characters.IInteractionCharacter;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.api.quests.goals.ITalkToNPCGoal;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterRaceDefinition;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.utility.dialogues.LanguageManager;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.security.acl.Owner;
import java.util.Collection;
import java.util.LinkedList;

public class NonPlayingCharacter extends AbstractCharacter implements IInteractionCharacter {
	private LanguageManager NPClanguage;
	
	private String id;
	private NonPlayingCharacterRaceDefinition race;	

	public NonPlayingCharacter(String id, NonPlayingCharacterRaceDefinition race, String displayName, IGameMap map, IGamePoint position, IDirection direction){
		super(displayName, map, position, direction);
		this.id = id;
		this.race = race;
		initCollisionShape(map.getGuiMode());
		super.initPosition(position);
	}

	@Override
	public void speakToPlayer(Player player){
		
		if(player.getQuestManager().isHavingADialog()) {
			return;
		}
		Collection<IDialog> dialogs = new LinkedList<>();
		dialogs.add(race.getDefaultDialog());
		
		Collection<IQuestDefinition> availableQuests  = new LinkedList<>();
		for(IQuestDefinition quest:race.getQuests()) {
			if(!player.getQuestManager().getSolvedQuests().contains(quest.getName()) && !player.getQuestManager().getActiveQuests().containsKey(quest.getName())&& quest.meetsRequirements(player.getQuestManager().getSolvedQuests())) {
				dialogs.add(quest.getActivationDialog());
				availableQuests.add(quest);
			}
		}
		for(IQuest quest:player.getQuestManager().getActiveQuests().values()) {
			for(IGoal goal : quest.getActiveGoals().values()) {
				if(goal instanceof ITalkToNPCGoal) {
					ITalkToNPCGoal talkGoal = (ITalkToNPCGoal) goal;
					if(talkGoal.getGoalInfo().getNpc().equals(this.race.getRaceName())) {
						dialogs.add(talkGoal.getGoalInfo().getLinkedDialog());
					}
				}
			}
		}
		player.getQuestManager().setActiveDialogs(dialogs);
		player.getQuestManager().setAvailableQuests(availableQuests);
		EventManager.getInstance().dialogsMenuOpened(dialogs, player);
	
	}

	@Override
	public ISummary getSummary(){
		ISummary summary = super.getSummary();
		summary.putData("race", race.getRaceName());
		return summary;
	}

	public String getId() {
		return id;
	}

	public NonPlayingCharacterRaceDefinition getRace() {
		return race;
	}
	
	public void initCollisionShape(GuiStatus guiStatus){
		super.setCollisionShape((getRace().getBoundingShape(guiStatus)));
	}
	
	
}
