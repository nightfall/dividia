package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.IInteractionCharacter;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.item.IInventory;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.utility.ICallbackWaiting;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.INetworkImportable;
import it.nightfall.dividia.api.utility.IQuadraticFunction;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.api.utility.io.ISaveState;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.item.Inventory;
import it.nightfall.dividia.bl.quests.QuestManager;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.FutureCallback;
import it.nightfall.dividia.bl.utility.GameManager;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.QuadraticFunction;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.bl.utility.playerMovements.EightDirectionMovementManager;
import it.nightfall.dividia.bl.utility.playerMovements.FourDirectionMovementManager;
import it.nightfall.dividia.bl.world.ArenaGameMap;
import it.nightfall.dividia.bl.world.MapsManager;
import it.nightfall.dividia.bl.world.Teleport;
import it.nightfall.dividia.gui_api.utility.IDirectionMovementManager;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Player extends PlayingCharacter implements IInteractionCharacter, ISaveState, INetworkImportable {
	private IInventory inventory;
	private QuestManager questManager;
	private PlayerStatus status;
	public final static IQuadraticFunction GROWING_FUNCTION = inizializeGrowingFunction();
	private IDirectionMovementManager movementManager;
	private IRaceDefinition race;
	private List<String> orderedSpellList = new LinkedList<>();

	public Player(String id){
		super(id, null, null, null, null);
		inventory = new Inventory(this);
		questManager= new QuestManager(this);
	}

	public IInventory getInventory(){
		return inventory;
	}

	public QuestManager getQuestManager(){
		return questManager;
	}

	public int getCurrentExperience(){
		return status.getCurrentExperience();
	}

	public void setQuestManager(QuestManager questManager){
		this.questManager = questManager;
	}

	@Override
	protected void announceDeath(PlayingCharacter killer){
		currentExecution = null;
		IGameMap map = this.getCurrentMap();
		map.playerDied(this);
		final Player finalThis = this;
		FutureCallback respawnCallBack = new FutureCallback(new ICallbackWaiting() {
			@Override
			public boolean execute() {
				finalThis.respawn();
				return true;
			}
		});
		respawnCallBack.runAndWait(IGameConstants.PLAYERS_RESPAWN_TIME*1000);
		killer.playerKilled(this);
		EventManager.getInstance().playerDied(this, killer);
	}

	@Override
	public void playerKilled(Player player) {
		status.recoverHealtAndMana();
		
	}
	
	@Override
	public void setPosition(IGamePoint position){
		if(!this.isDead()) {
			super.setPosition(position);
		}
	}
	
	// EQUIPMENT FUNCTIONS
	/**
	 * This method changes the boots equipped by the player, modifying some stats according to the boots variations.
	 *
	 * @param equipment : Boots to equip.
	 */
	public void equipItem(IBoots equipment){
		IBoots unequippingItem = this.getEquippedItems().getBoots();
		if(getEquippedItems().hasBootsEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasBootsEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setBoots(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().bootsEquipped(equipment, this);
	}

	/**
	 * This method changes the shield equipped by the player, modifying some stats according to the shield variations.
	 *
	 * @param shield : Shield to equip.
	 */
	public void equipItem(IShield equipment){
		IShield unequippingItem = getEquippedItems().getShield();
		if(getEquippedItems().hasShieldEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasShieldEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setShield(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().shieldEquipped(equipment, this);
	}

	/**
	 * This method changes the helmet equipped by the player, modifying some stats according to the helmet variations.
	 *
	 * @param equipment : Helmet to equip.
	 */
	public void equipItem(IHelmet equipment){
		IHelmet unequippingItem = getEquippedItems().getHelmet();
		if(getEquippedItems().hasHelmetEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasHelmetEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setHelmet(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().helmetEquipped(equipment, this);
	}

	/**
	 * This method changes the ring equipped by the player, modifying some stats according to the ting variations.
	 *
	 * @param ring : Ring to equip.
	 */
	public void equipItem(IRing equipment){
		IRing unequippingItem = getEquippedItems().getRing();
		if(getEquippedItems().hasRingEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasRingEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setRing(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().ringEquipped(equipment, this);
	}

	/**
	 * This method changes the necklace equipped by the player, modifying some stats according to the necklace variations.
	 *
	 * @param necklace : Necklace to equip.
	 */
	public void equipItem(INecklace equipment){
		INecklace unequippingItem = getEquippedItems().getNecklace();
		if(getEquippedItems().hasNecklaceEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasNecklaceEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setNecklace(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().necklaceEquipped(equipment, this);
	}

	/**
	 * This method changes the weapon equipped by the player, modifying some stats according to the weapon variations.
	 *
	 * @param weapon : Weapon to equip.
	 */
	public void equipItem(IWeapon equipment){
		IWeapon unequippingItem = getEquippedItems().getWeapon();
		if(inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setWeapon(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().weaponEquipped(equipment, this);
	}

	/**
	 * This method changes the armor equipped by the player, modifying some stats according to the armor variations.
	 *
	 * @param armor : Armor to equip.
	 */
	public void equipItem(IArmor equipment){
		IArmor unequippingItem = getEquippedItems().getArmor();
		if(getEquippedItems().hasArmorEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasArmorEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setArmor(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().armorEquipped(equipment, this);
	}

	/**
	 * This method changes the gloves equipped by the player, modifying some stats according to the gloves variations.
	 *
	 * @param gloves : Gloves to equip.
	 */
	public void equipItem(IGloves equipment){
		IGloves unequippingItem = getEquippedItems().getGloves();
		if(getEquippedItems().hasGlovesEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasGlovesEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setGloves(equipment);
		status.statusUpdateWithEquipChange(unequippingItem, equipment);
		EventManager.getInstance().glovesEquipped(equipment, this);
	}

	/**
	 * This method changes the mystical sphere equipped by the player. The speed of this change depends on the mystical sphere change delay.
	 *
	 * @param ms : Mystical sphere to equip.
	 */
	public void equipMysticalSphere(IMysticalSphere equipment){
		IMysticalSphere unequippingItem = getEquippedItems().getMysticalSphere();
		if(getEquippedItems().hasMysticalSphereEquipped() && inventory.isCapacityEnoughToEquipItem(unequippingItem, equipment)){
			inventory.removeItem(equipment);
			inventory.addItem(unequippingItem);
		}else if(!getEquippedItems().hasMysticalSphereEquipped()){
			inventory.removeItem(equipment);
		}else{
			EventManager.getInstance().knapsackFull(this);
			return;
		}
		getEquippedItems().setMysticalSphere(equipment);
		EventManager.getInstance().mysticalSphereEquipped(equipment, this);
	}

	@Override
	public void speakToPlayer(Player p) {
		if(!(p.getCurrentMap() instanceof ArenaGameMap)) {
			Teleport teleportToArena = new Teleport(new GamePoint(50, 50), "arena");
			p.setDirection(new Direction(0));
			MapsManager.getInstance().useTeleportForPlayer(p.getCurrentMap(), p, teleportToArena);

			teleportToArena.setNextLocation(new GamePoint(52, 50));
			this.setDirection(new Direction(3.14));
			MapsManager.getInstance().useTeleportForPlayer(this.getCurrentMap(), this, teleportToArena);
		}
		//this player is receiving the event
	}

	private static IQuadraticFunction inizializeGrowingFunction(){
		return new QuadraticFunction(IGameConstants.PLAYER_EPX_KNOWN_TERM,
									 IGameConstants.PLAYER_EPX_LINEAR_COEFFICIENT, IGameConstants.PLAYER_EPX_QUADRATIC_COEFFICIENT);
	}

	public void gainExperience(int expReward){
		this.status.gainExperience(expReward, getRace());
	}

	@Override
	public Status getStatus(){
		return status;
	}

	public void setLevel(int newLevel){
		assert newLevel <= IGameConstants.PLAYER_MAXIMUM_LEVEL;
		status.setLevel(getRace(), newLevel);
	}

	public IDirectionMovementManager getMovementManager(){
		return movementManager;
	}

	public void switchDirectionMovementManager(boolean world2D){
		GameManager.getInstance().getClock().removeListener(movementManager);
		if(world2D){
			System.out.println("4 Direction Movement Manager has been set.");
			movementManager = new FourDirectionMovementManager(this);
		}else{
			System.out.println("8 Direction Movement Manager has been set.");
			movementManager = new EightDirectionMovementManager(this);
		}
		GameManager.getInstance().getClock().addListener(movementManager);
	}

	@Override
	public void goOnCooldown(ISpellDefinition spell){
		EventManager.getInstance().spellOnCooldown(this, spell);
		super.goOnCooldown(spell);
	}

	public void unequipArmor(){
		if(getEquippedItems().hasArmorEquipped()){
			IEquipmentItem item = getEquippedItems().getArmor();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setArmor(null);
				status.statusUpdateWithEquipChange(item, null);
				EventManager.getInstance().armorUnequipped(this);
			}
		}
	}

	public void unequipBoots(){
		if(getEquippedItems().hasBootsEquipped()){
			IEquipmentItem item = getEquippedItems().getBoots();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setBoots(null);
				status.statusUpdateWithEquipChange(item, null);
				EventManager.getInstance().bootsUnequipped(this);
			}
		}
	}

	public void unequipHelmet(){
		if(getEquippedItems().hasHelmetEquipped()){
			IEquipmentItem item = getEquippedItems().getHelmet();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setHelmet(null);
				status.statusUpdateWithEquipChange(item, null);
				EventManager.getInstance().helmetUnequipped(this);
			}
		}
	}

	public void unequipGloves(){
		if(getEquippedItems().hasGlovesEquipped()){
			IEquipmentItem item = getEquippedItems().getGloves();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setGloves(null);
				status.statusUpdateWithEquipChange(item, null);
				EventManager.getInstance().glovesUnequipped(this);
			}
		}
	}

	public void unequipNecklace(){
		if(getEquippedItems().hasNecklaceEquipped()){
			IEquipmentItem item = getEquippedItems().getNecklace();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setNecklace(null);
				status.statusUpdateWithEquipChange(item, null);
				EventManager.getInstance().necklaceUnequipped(this);
			}
		}
	}

	public void unequipRing(){
		if(getEquippedItems().hasRingEquipped()){
			IEquipmentItem item = getEquippedItems().getRing();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setRing(null);
				status.statusUpdateWithEquipChange(item, null);
				EventManager.getInstance().ringUnequipped(this);
			}
		}
	}

	public void unequipShield(){
		if(getEquippedItems().hasShieldEquipped()){
			IEquipmentItem item = getEquippedItems().getShield();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setShield(null);
				status.statusUpdateWithEquipChange(item, null);
				EventManager.getInstance().shieldUnequipped(this);
			}
		}
	}

	public void unequipMysticalSphere(){
		if(getEquippedItems().hasMysticalSphereEquipped()){
			IMysticalSphere item = getEquippedItems().getMysticalSphere();
			if(inventory.canPickUpItem(item.getItemDefinition().getWeight())){
				inventory.addItem(item);
				getEquippedItems().setMysticalSphere(null);
				EventManager.getInstance().mysticalSphereUnequipped(this);
			}
		}
	}

	public void unequipWeapon(){
		if(getEquippedItems().hasMysticalSphereEquipped()){
			inventory.addItem(getEquippedItems().getWeapon());
			getEquippedItems().setWeapon(null);
			EventManager.getInstance().weaponUnequipped(this);
		}
	}

	@Override
	public void loadState(IConfigReader saveState){
		//TODO move player to map should be the last?
		//ISummary summary = saveState.toSummary();
		//buildFromSummary(summary);
		setRace(GameStorage.getInstance().getRaceDefinition(saveState.getString("race")));
		MapsManager.getInstance().movePlayerToMap(this, saveState.getString("map"), new GamePoint(saveState.getDouble("positionX"), saveState.getDouble("positionY")));
		setDirection(new Direction(saveState.getDouble("direction")));
		if(this.getCurrentMap().is2D()){
			movementManager = new FourDirectionMovementManager(this);
		}else{
			movementManager = new EightDirectionMovementManager(this);
		}
		status = new PlayerStatus(this);
		int level = saveState.getInteger("level");
		status.setLevel(getRace(), level);
		getEquippedItems().loadState(saveState.getConfigReader("equippedItems"));
		getEquippedItems().equipAfterLoad(this);
		//TODO: test 
		for(String spellId : getRace().getStartingSpells()){
			learnSpell(GameStorage.getInstance().getSpellDefinition(spellId));
			orderedSpellList.add(spellId);
		}
		EventManager.getInstance().playerSummaryReady(this, getSummary());
	}

	@Override
	public void saveState(IConfigWriter saveState){
		getSummary().toConfigWriter(saveState);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = super.getSummary();
		ISummary inventorySummary = inventory.getSummary();
		ISummary questsSummary = questManager.getSummary();
		ISummary statusSummary = status.getSummary();
		tempSummary.putData("inventory", inventorySummary);
		tempSummary.putData("quests", questsSummary);
		tempSummary.putData("status", statusSummary);
		ISummary spellSummary = new Summary();
		ListIterator<String> it = orderedSpellList.listIterator();
		while(it.hasNext()) {
			spellSummary.putData(""+(it.nextIndex()), it.next());
		}
		tempSummary.putData("spells", spellSummary);
		return tempSummary;
	}
	

	@Override
	public void buildFromSummary(ISummary summary){
		setRace(GameStorage.getInstance().getRaceDefinition(summary.getString("race")));
		MapsManager.getInstance().movePlayerToMap(this, summary.getString("map"), new GamePoint(summary.getDouble("positionX"), summary.getDouble("positionY")));
		setDirection(new Direction(summary.getDouble("direction")));
		if(this.getCurrentMap().is2D()){
			movementManager = new FourDirectionMovementManager(this);
		}else{
			movementManager = new EightDirectionMovementManager(this);
		}
		status = new PlayerStatus(this);
		int level = summary.getSummary("status").getInteger("level");
		status.setLevel(getRace(), level);
		getEquippedItems().buildFromSummary(summary.getSummary("equippedItems"));
		getEquippedItems().equipAfterLoad(this);
		//TODO: test 
		for(String spellId : getRace().getStartingSpells()){
			learnSpell(GameStorage.getInstance().getSpellDefinition(spellId));
			orderedSpellList.add(spellId);
		}
		EventManager.getInstance().playerSummaryReady(this, getSummary());
	}

	@Override
	public IRaceDefinition getRace(){
		return race;
	}

	@Override
	public void setRace(IRaceDefinition race){
		this.race = race;
	}

	public void gainManaAfterMonsterKill(int defaultManaReward){
		status.updateCharacteristic(StatusBattleCharacteristic.CURRENT_MP, Math.round(defaultManaReward * race.getManaSelfRewardFactor()));
	}

	private void respawn() {
		IGameMap gameMap = this.getCurrentMap();
		this.getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_HP, getStatus().getCharacteristic(StatusBattleCharacteristic.MAX_HP));
		this.getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_MP, getStatus().getCharacteristic(StatusBattleCharacteristic.MAX_MP));
		gameMap.respawnPlayer(this);
		
	}
}
