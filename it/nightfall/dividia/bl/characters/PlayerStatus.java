package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.events.EventManager;

/**
 *
 * @author Nightfall
 */
public class PlayerStatus extends Status {
	private final Player player;
	private int currentExperience;
	private int totalLevelExperience;

	public PlayerStatus(Player player){
		super(player);
		this.player = player;
		
	}
	public int getCurrentExperience(){
		return currentExperience;
	}

	public int getExperienceNeededAtNextLevel(){
		return getExperienceNeededAtNewLevel(level + 1);
	}

	/**
	 * Get the experience needed for a player to complete such level
	 *
	 * @param level : playing character's level
	 * @return : experience needed
	 */
	public int getExperienceNeededAtNewLevel(int newLevel){
		return Player.GROWING_FUNCTION.getIntegerFunctionImage(newLevel);
	}

	public int getMaximumLevel(){
		return IGameConstants.PLAYER_MAXIMUM_LEVEL;
	}

	public void gainExperience(int gainedExperience, IRaceDefinition race){
		if(level >= IGameConstants.PLAYER_MAXIMUM_LEVEL){
			return;
		}
		if(currentExperience + gainedExperience <= totalLevelExperience){
			currentExperience += gainedExperience;
		}else{
			gainedExperience = gainedExperience - (totalLevelExperience - currentExperience);
			levelUp(race);
			EventManager.getInstance().playerLevelUpByExperience(player, level, totalLevelExperience);
			currentExperience = 0;
			gainExperience(gainedExperience, race);
		}
		EventManager.getInstance().playerGainExperience(player, currentExperience, gainedExperience);
	}

	private void levelUp(IRaceDefinition race){
		setLevel(race, level + 1);
		EventManager.getInstance().playerLevelUp(player, level, totalLevelExperience);
	}

	@Override
	public void setLevel(IRaceDefinition race, int newLevel){
		totalLevelExperience = getExperienceNeededAtNewLevel(newLevel);
		super.setLevel(race, newLevel);
		
	}
	
	@Override
	public ISummary getSummary(){
		ISummary tempSummary = super.getSummary();
		tempSummary.putData("currentExperience", currentExperience);
		tempSummary.putData("totalLevelExperience", totalLevelExperience);
		return tempSummary;
	}
}
