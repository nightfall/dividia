package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.battle.IAction;
import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.battle.ISummon;
import it.nightfall.dividia.api.battle.definitions.IAreaSpellDefinition;
import it.nightfall.dividia.api.battle.definitions.IFocusedSpellDefinition;
import it.nightfall.dividia.api.battle.definitions.ISkillDefinition;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.battle.definitions.ITargetAreaSpellDefinition;
import it.nightfall.dividia.api.battle.definitions.ITargetSpellDefinition;
import it.nightfall.dividia.api.battle.executions.IAreaSpellExecution;
import it.nightfall.dividia.api.battle.executions.IBasicAttackExecution;
import it.nightfall.dividia.api.battle.executions.IExecution;
import it.nightfall.dividia.api.battle.executions.IFocusedSpellExecution;
import it.nightfall.dividia.api.battle.executions.ITargetAreaSpellExecution;
import it.nightfall.dividia.api.battle.executions.ITargetSpellExecution;
import it.nightfall.dividia.api.characters.IAlteredStatus;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.battle.Action;
import it.nightfall.dividia.bl.battle.DamageSource;
import it.nightfall.dividia.bl.battle.DamageSummary;
import it.nightfall.dividia.bl.battle.DamageType;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.battle.executions.AreaSpellExecution;
import it.nightfall.dividia.bl.battle.executions.BasicAttackExecution;
import it.nightfall.dividia.bl.battle.executions.FocusedSpellExecution;
import it.nightfall.dividia.bl.battle.executions.TargetAreaSpellExecution;
import it.nightfall.dividia.bl.battle.executions.TargetSpellExecution;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.item.EquippedItems;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class represent a character who can move and perform/receive actions.
 *
 * @author Nightfall
 *
 */
public abstract class PlayingCharacter extends AbstractCharacter {
	private String id;
	private final Map<ISkillDefinition, Integer> skills = new HashMap<>();
	protected final Map<String, ISpellDefinition> spells = new HashMap<>();
	private final Map<String, Long> spellsCooldown = new HashMap<>();
	private final Map<ISpellDefinition, ISummon> summons = new HashMap<>();
	private EquippedItems equippedItems = new EquippedItems();
	protected IExecution currentExecution;

	PlayingCharacter(String id, String displayName, IGameMap currentMap, IGamePoint position, IDirection direction){
		super(displayName, currentMap, position, direction);
		this.id = id;
	}

	public String getId(){
		return id;
	}

	/**
	 * Updates character position basing on its direction, movement speed and the movement speed game constant, all provided by getStep() method.
	 */
	public void updatePosition(int tpf){
		double deltaX = getDirection().getVersorX() * getStep(tpf);
		double deltaY = getDirection().getVersorY() * getStep(tpf);
		translatePosition(deltaX, deltaY);
	}

	/**
	 * Determines what happens when a player receives an action.
	 *
	 * @param action The action received.
	 * @return A DamageSummary which summarizes the effects of the action on the player who received it.
	 */
	public synchronized IDamageSummary receiveAction(IAction action, PlayingCharacter executor){
		int finalDamage = 0, currentHP = getStatus().getCharacteristic(StatusBattleCharacteristic.CURRENT_HP);
		Set<String> alteredStatuses = new HashSet<>();
		if(action.getActionType() == DamageType.PURE){
			finalDamage = action.getDamage();
		}else{
			int defense = 0;
			if(action.getActionType() == DamageType.PHYSICAL){
				defense = getStatus().getCharacteristic(StatusBattleCharacteristic.PHYSICAL_DEFENCE);
			}else{
				defense = getStatus().getCharacteristic(StatusBattleCharacteristic.MAGIC_DEFENCE);
			}
			int filteredDamage = 0, elementResistance = getStatus().getElementResistance(action.getElement());
			filteredDamage = (int) (Math.round(action.getDamage() * ((1 - SharedFunctions.division(elementResistance,100)))));

			double damageMultiplier = 0;
			if(defense >= 0){
				damageMultiplier = SharedFunctions.division(100, (100 + defense));
			}else{
				damageMultiplier = 2 - SharedFunctions.division(100, (100 - defense));
			}
			finalDamage = SharedFunctions.calculateFinalDamage((int) (filteredDamage * damageMultiplier));
		}
		if(currentHP < finalDamage){
			finalDamage = currentHP;
		}
		if(finalDamage>0) {
			executor.getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_RAGE, IGameConstants.RAGE_INCREMENT);
			getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_HP, -finalDamage);
		}
		for(IAlteredStatusDefinition tempAlteredStatusDefinition : action.getAlteredStatuses()){
			IAlteredStatus tempAlteredStatus = new AlteredStatus(tempAlteredStatusDefinition, executor, this);
			if(getStatus().addAlteredStatus(tempAlteredStatus)){
				alteredStatuses.add(tempAlteredStatusDefinition.getName());
				EventManager.getInstance().alteredStatusApplied(this, executor, tempAlteredStatus);
			}
		}
		//TODO: prendere in considerazione l'elusione.
		if(isDead()){
			for(IAlteredStatus alteredStatus:getStatus().alteredStatuses.values()) {
				alteredStatus.stopAlterations();
			}
			EventManager.getInstance().characterDied(this, executor);
			announceDeath(executor);
		}
		return new DamageSummary(finalDamage, alteredStatuses, action.getDamageSource(), action.getSourceId(), action.getActionType());
	}

	public abstract IRaceDefinition getRace();

	public abstract void setRace(IRaceDefinition race);

	/**
	 * Checks whether a PlayingCharacter is dead.
	 *
	 * @return true if the PlayingCharacter is dead, false otherwise.
	 */
	public boolean isDead(){
		if(getStatus()!=null) {
			return getStatus().getCharacteristic(StatusBattleCharacteristic.CURRENT_HP) == 0 ? true : false;
		}
		return false;
	}

	protected abstract void announceDeath(PlayingCharacter killer);

	public boolean canCast(ISpellDefinition spell){
		if(currentExecution != null || isDead()){
			return false;
		}
		synchronized(spellsCooldown){
			if(spellsCooldown.containsKey(spell.getName())){
				Long tempTime = spellsCooldown.get(spell.getName());
				if(System.currentTimeMillis() - tempTime <= spell.getCooldown()*1000){
					return false;
				}
			}
		}
		Status status = getStatus();
		synchronized(status){
			if(status.getCharacteristic(StatusBattleCharacteristic.CURRENT_MP) < spell.getMpCost()){
				return false;
			}
			if(spell.isRageSpell() && !status.isRageFul()) {
				return false;
			}
		}
		return true;
	}

	public void goOnCooldown(ISpellDefinition spell){
		synchronized(spellsCooldown){
			spellsCooldown.put(spell.getName(), System.currentTimeMillis());
		}
	}

	public void undertakeAction(IFocusedSpellDefinition spell, IGamePoint focus){
		if(!canCast(spell)){
			return;
		}
		if(canExecuteAttacks()){
			applySpellMpAndRageCosts(spell);
			IAction action = new Action(spell.getSpellElement(), spell.getSpellType(), DamageSource.SPELL, spell.getName(), spell.getActionDamage(this.getStatus()), spell.getSuccededAlteredStatuses());
			IFocusedSpellExecution focusedSpellExecution = new FocusedSpellExecution(this, action, spell, focus);
			currentExecution = focusedSpellExecution;
			((IBattleMap) this.getCurrentMap()).getBattleManager().executionSpreading(focusedSpellExecution);
		}
	}

	public void undertakeAction(ITargetSpellDefinition spell, PlayingCharacter target){
		if(!canCast(spell)){
			return;
		}
		if (canExecuteAttacks()){
			applySpellMpAndRageCosts(spell);
			IAction action = new Action(spell.getSpellElement(), spell.getSpellType(), DamageSource.SPELL, spell.getName(), spell.getActionDamage(this.getStatus()), spell.getSuccededAlteredStatuses());
			ITargetSpellExecution spellExecution = new TargetSpellExecution(this, action, spell, target);
			IBattleMap map = (IBattleMap) this.getCurrentMap();
			currentExecution = spellExecution;
			map.getBattleManager().executionSpreading(spellExecution);
		}
	}

	/**
	 * Perform an area spell.
	 *
	 * @param spell
	 */
	public void undertakeAction(IAreaSpellDefinition spell){
		if(!canCast(spell)){
			return;
		}
		if(canExecuteAttacks()){
			applySpellMpAndRageCosts(spell);
			IAction action = new Action(spell.getSpellElement(), spell.getSpellType(), DamageSource.SPELL, spell.getName(), spell.getActionDamage(this.getStatus()), spell.getSuccededAlteredStatuses());
			IAreaSpellExecution areaSpellExecution = new AreaSpellExecution(this, action, spell);
			currentExecution = areaSpellExecution;
			((IBattleMap) this.getCurrentMap()).getBattleManager().executionSpreading(areaSpellExecution);
		}
	}

	// Danilo
	/**
	 * Casts a Target Area Spell.
	 *
	 * @param spell The Spell being casted.
	 * @param target The PlayingCharacter being targeted by the spell.
	 */
	public void undertakeAction(ITargetAreaSpellDefinition spell, PlayingCharacter target){
		if(!canCast(spell)){
			return;
		}
		if (canExecuteAttacks()){
			applySpellMpAndRageCosts(spell);
			IAction action = new Action(spell.getSpellElement(), spell.getSpellType(), DamageSource.SPELL, spell.getName(), spell.getActionDamage(this.getStatus()), spell.getSuccededAlteredStatuses());
			ITargetAreaSpellExecution targetAreaSpellExecution = new TargetAreaSpellExecution(this, action, spell, target);
			currentExecution = targetAreaSpellExecution;
			((IBattleMap) this.getCurrentMap()).getBattleManager().executionSpreading(targetAreaSpellExecution);
		}
	}

	/**
	 * decrease player's status mp basing on spell cost
	 *
	 * @return true if we can undertake the spell with current mp
	 */
	private boolean applySpellMpAndRageCosts(ISpellDefinition spell){
		if(spell.isRageSpell()) {
			this.getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_RAGE, -this.getStatus().getCharacteristic(StatusBattleCharacteristic.MAX_RAGE));
		}
		return (this.getStatus().updateCharacteristic(StatusBattleCharacteristic.CURRENT_MP, -spell.getMpCost()));
	}

	/**
	 * Performs a basic attack with the equipped weapon.
	 */
	public void undertakeAction(){
		if(currentExecution != null || isDead()){
			return;
		}
		if (canExecuteAttacks()){
			IAction action = new Action(equippedItems.getWeapon().getWeaponElement(), DamageType.PHYSICAL, DamageSource.BASIC_ATTACK, "basic attack", getStatus().getCharacteristic(StatusBattleCharacteristic.PHYSICAL_ATTACK_POWER), this.equippedItems.getWeapon().getSuccededAlteredStatuses());
			IBasicAttackExecution basicAttackExecution = new BasicAttackExecution(this, action, equippedItems.getWeapon().getAttackRange());
			currentExecution = basicAttackExecution;
			((IBattleMap) this.getCurrentMap()).getBattleManager().executionSpreading(basicAttackExecution);
		}
		
	}

	public void learnSkill(ISkillDefinition skill){
		this.skills.put(skill, 0);
	}

	/**
	 * This class change the skill points of a specific skill.
	 *
	 * @param skill : Represent the skill itself.
	 * @param bool : If true there is an increment of 1 on the skill points of the selected skill, -1 otherwise.
	 */
	public void modifySkillPoints(ISkillDefinition skill, boolean bool){
		if(bool == true){
			this.skills.put(skill, this.skills.get(skill) + 1);
		}else{
			this.skills.put(skill, this.skills.get(skill) - 1);
		}
	}

	/**
	 * With this class the specified spell becomes usable by the character
	 *
	 * @param spell : Spell to learn.
	 */
	public void learnSpell(ISpellDefinition spell){
		this.spells.put(spell.getName(), spell);
	}

	public boolean hasLearntSpell(String spell){
		return spells.containsKey(spell);
	}

	/**
	 * Add an altered status to the player, calling addAlteredStatus function from player's status.
	 *
	 * @param status : AlteredStatus to append.
	 */
	protected void addAlteredStatus(IAlteredStatus status){
		this.getStatus().addAlteredStatus(status);
	}

	public abstract Status getStatus();

	public Map<String, ISpellDefinition> getSpells(){
		return Collections.unmodifiableMap(spells);
	}

	public Map<ISpellDefinition, ISummon> getSummons(){
		return summons;
	}

	public Map<ISkillDefinition, Integer> getSkills(){
		return skills;
	}

	public void holdOnTerminated(){ //Safe
		currentExecution = null;
	}

	public synchronized IExecution getCurrentExecution(){
		return currentExecution;
	}

	public int getLevel(){
		return this.getStatus().getLevel();
	}

	public EquippedItems getEquippedItems(){
		return equippedItems;
	}

	/**
	 * Returns the step length based on current movement speed and the movement speed game constant.
	 */
	public double getStep(int tpf){
		return IGameConstants.MOVEMENT_SPEED_CONSTANT * this.getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED) * 1.0 * (tpf * 1.0 / 1000000000) / 100;
		//double movementSpeedFactor = SharedFunctions.division(getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED), 100);
		//double step = movementSpeedFactor * IGameConstants.MOVEMENT_SPEED_CONSTANT;
		//return step;
	}

	@Override
	public void setPosition(IGamePoint position){
		super.setPosition(position);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = super.getSummary();
		ISummary skillsSummary = new Summary();
		for(Map.Entry<ISkillDefinition, Integer> skillData : skills.entrySet()){
			skillsSummary.putData(skillData.getKey().getName(), skillData.getValue());
		}
		HashSet<String> spellsSet = new HashSet<>();
		for(Map.Entry<String, ISpellDefinition> spellData : spells.entrySet()){
			spellsSet.add(spellData.getKey());
		}
		ISummary summonsSummary = new Summary();
		for(Map.Entry<ISpellDefinition, ISummon> summonData : summons.entrySet()){
			summonsSummary.putData(summonData.getKey().getName(), summonData.getValue().getSummary());
		}
		tempSummary.putData("id", id);
		tempSummary.putData("skills", skillsSummary);
		tempSummary.putData("spells", spellsSet);
		tempSummary.putData("summons", summonsSummary);
		tempSummary.putData("race", getRace().getName());
		tempSummary.putData("equippedItems", equippedItems.getSummary());
		return tempSummary;
	}
	
	public void setCollisionShape(GuiStatus guiStatus){
		super.setCollisionShape((getRace().getBoundingShape(guiStatus)));
	}

	public ISummary getStrictSummary() {
		ISummary summary = new Summary();
		summary.putData("id", getId());
		summary.putData("direction", getDirection().getDecimalDegreeAngle());
		summary.putData("positionX", getPosition().getX());
		summary.putData("positionY", getPosition().getY());	
		summary.putData("movementSpeed", getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED));
		summary.putData("attackSpeed", getStatus().getCharacteristic(StatusBattleCharacteristic.ATTACK_SPEED));
		summary.putData("race", getRace().getName());
		return summary;
	}

	private boolean canExecuteAttacks() {
		return super.getCurrentMap() instanceof IBattleMap;
	}

	public abstract void playerKilled(Player player);

	
}
