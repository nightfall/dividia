package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.GamePoint;

public class Smith extends NonPlayingCharacter{
	public Smith(String name, IGameMap map, GamePoint position, Direction direction){
		super(name, null, null, map, position, direction);
	}
}
