package it.nightfall.dividia.bl.characters;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.characters.IAlteredStatus;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import it.nightfall.dividia.api.utility.INetworkExportable;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.battle.Element;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.ModifiableInteger;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Status implements INetworkExportable{
	protected int level;
	protected Map<IElement, Integer> elementResistances;
	protected Map<String, IAlteredStatus> alteredStatuses;
	protected Map<IStatusBattleCharacteristic, IModifiableInteger> statusBattleCharacteristics;
	protected final PlayingCharacter playingCharacter;

	public Status(PlayingCharacter playingCharacter){
		this.playingCharacter = playingCharacter;
		statusBattleCharacteristics = new HashMap<>();
		for(IStatusBattleCharacteristic c : StatusBattleCharacteristic.values()){
			statusBattleCharacteristics.put(c, new ModifiableInteger(0));
		}
		for(IStatusBattleCharacteristic c : StatusBattleCharacteristic.getRaceNonGrowingStats()){
			IRaceDefinition race = playingCharacter.getRace();
			statusBattleCharacteristics.put(c, race.getNonGrowingValue(c).clone());
		}
		alteredStatuses = new HashMap<>();
		elementResistances = new HashMap<>();
		for(IElement element : Element.values()){
			elementResistances.put(element, 0);
		}
		statusBattleCharacteristics.put(StatusBattleCharacteristic.MAX_RAGE, new ModifiableInteger((100)));
	}

	public int getLevel(){
		return level;
	}

	public synchronized int getCharacteristic(IStatusBattleCharacteristic stat){
		assert statusBattleCharacteristics.containsKey(stat);
		return statusBattleCharacteristics.get(stat).getValue();
	}

	public synchronized boolean updateCharacteristic(IStatusBattleCharacteristic stat, int delta){
 		int value = statusBattleCharacteristics.get(stat).getValue();
		value += delta;
		if(value < 0 && !stat.doesAllowNegativeValues()){
			return false;
		}else{
			statusBattleCharacteristics.get(stat).addInt(delta);
			
			if(stat.equals(StatusBattleCharacteristic.CURRENT_MP)) {
				delta = validateMaximumStat(stat,StatusBattleCharacteristic.MAX_MP,delta);
			}
			else if(stat.equals(StatusBattleCharacteristic.CURRENT_HP)) {
				delta = validateMaximumStat(stat,StatusBattleCharacteristic.MAX_HP,delta);
			}
			else if(stat.equals(StatusBattleCharacteristic.CURRENT_RAGE)) {
				delta = validateMaximumStat(stat,StatusBattleCharacteristic.MAX_RAGE,delta);
			}
			
			if(stat.equals(StatusBattleCharacteristic.MAX_HP)) {
				int maxHp = statusBattleCharacteristics.get(StatusBattleCharacteristic.MAX_HP).getValue();
				int currentHp = statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_HP).getValue();
				if(currentHp>maxHp) {
					statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_HP).addInt(-currentHp+maxHp);
					EventManager.getInstance().playingCharacterCharacteristicChanged(playingCharacter, StatusBattleCharacteristic.CURRENT_HP, maxHp, -currentHp+maxHp);
				}
			}
			else if(stat.equals(StatusBattleCharacteristic.MAX_MP)) {
				int maxMp = statusBattleCharacteristics.get(StatusBattleCharacteristic.MAX_MP).getValue();
				int currentMp = statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_MP).getValue();
				if(currentMp>maxMp) {
					statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_MP).addInt(-currentMp+maxMp);
					EventManager.getInstance().playingCharacterCharacteristicChanged(playingCharacter, StatusBattleCharacteristic.CURRENT_MP, maxMp, -currentMp+maxMp);
				}
			}
			
			
			EventManager.getInstance().playingCharacterCharacteristicChanged(playingCharacter, stat, value, delta);
		}
		return true;
	}

	public Map<IElement, Integer> getElementResistances(){
		return Collections.unmodifiableMap(elementResistances);
	}

	public synchronized int getElementResistance(IElement element){
		return elementResistances.get(element);
	}

	public Map<String, IAlteredStatus> getAlteredStatuses(){
		return Collections.unmodifiableMap(alteredStatuses);
	}

	public synchronized void setElementResistence(IElement element, int resistance){
		elementResistances.put(element, resistance);				
	}

	public synchronized void addElementResistence(IElement element, int resistance){
		elementResistances.put(element, resistance);
	}

	//TODO is this function working properly??
	public boolean addAlteredStatus(IAlteredStatus alteredStatus){
		if(alteredStatuses.containsKey(alteredStatus.getAlteredStatusInfo().getName())) {
			return false;
			//alteredStatuses.get(alteredStatus.getAlteredStatusInfo().getName()).stopAlterations();
		}
		alteredStatuses.put(alteredStatus.getAlteredStatusInfo().getName(), alteredStatus);
		//IAlteredStatus oldAlteredStatus = alteredStatuses.put(UUID.randomUUID().toString(), alteredStatus);
		
		/*if(oldAlteredStatus != null){
			oldAlteredStatus.stopAlterations();
			isNew = false;
		}*/
		Thread tempThread = new Thread(alteredStatus);
		tempThread.setName("AlteredStatus");
		tempThread.start();
		return true;
	}

	public void removeAlteredStatus(String name){
		alteredStatuses.remove(name);
	}

	public synchronized void statusUpdateWithEquipChange(IEquipmentItem unequippingItem, IEquipmentItem equippingItem){
		//Map<IStatusBattleCharacteristic, IModifiableInteger> statusUpdates = new HashMap<IStatusBattleCharacteristic, IModifiableInteger>();
		assert (unequippingItem != null || equippingItem != null);
		Map<IStatusBattleCharacteristic, IModifiableInteger> unequipAlterations = new HashMap<>();
		Map<IStatusBattleCharacteristic, IModifiableInteger> equipAlterations = new HashMap<>();
		if(unequippingItem != null){
			unequipAlterations = unequippingItem.getItemDefinition().getStatsAlterations();
		}
		if(equippingItem != null){
			equipAlterations = equippingItem.getItemDefinition().getStatsAlterations();
		}

		for(IStatusBattleCharacteristic c : unequipAlterations.keySet()){
			this.updateCharacteristic(c, - unequipAlterations.get(c).getValue());
		}
		for(IStatusBattleCharacteristic c : equipAlterations.keySet()){
				this.updateCharacteristic(c, equipAlterations.get(c).getValue());
		}
	}

	public synchronized void setLevel(IRaceDefinition playingCharacterRace, int newLevel){
		for(IStatusBattleCharacteristic c : StatusBattleCharacteristic.getBattleGrowingStats()){
			int newValue = playingCharacterRace.getGrowingFunction(c).getIntegerFunctionImage(newLevel);
			//int variation = playingCharacterRace.getGrowingFunction(c).getIntegerFunctionImageVariation(level, newLevel);
			int variation = newValue-statusBattleCharacteristics.get(c).getValue();
			updateCharacteristic(c, variation);
			if(c.equals(StatusBattleCharacteristic.MAX_HP)){
				this.updateCharacteristic(StatusBattleCharacteristic.CURRENT_HP, variation);
			}
			else if(c.equals(StatusBattleCharacteristic.MAX_MP)){
				this.updateCharacteristic(StatusBattleCharacteristic.CURRENT_MP, variation);
			}
			
		}
		level = newLevel;
	}

	@Override
	public synchronized ISummary getSummary(){
		ISummary tempSummary = new Summary();
		ISummary resistencesSummary = new Summary();
		ISummary alteredStatusesSummary = new Summary();
		ISummary battleCharacteristicsSummary = new Summary();
		for(Map.Entry<IElement, Integer> elData : elementResistances.entrySet()){
			resistencesSummary.putData(elData.getKey().getName(), elData.getValue());
		}
		for(Map.Entry<String, IAlteredStatus> alData : alteredStatuses.entrySet()){
			alteredStatusesSummary.putData(alData.getKey(), alData.getValue().getSummary());
		}
		for(Map.Entry<IStatusBattleCharacteristic, IModifiableInteger> battleCharacteristicData : statusBattleCharacteristics.entrySet()){
			battleCharacteristicsSummary.putData(battleCharacteristicData.getKey().getName(), battleCharacteristicData.getValue().getValue());
		}
		tempSummary.putData("resistences", resistencesSummary);
		tempSummary.putData("alteredStatuses", alteredStatusesSummary);
		tempSummary.putData("battleCharacteristics", battleCharacteristicsSummary);
		tempSummary.putData("level", level);
		return tempSummary;
	}

	private int validateMaximumStat(IStatusBattleCharacteristic stat, StatusBattleCharacteristic maxStat, int delta) {
		int current = statusBattleCharacteristics.get(stat).getValue();
		int max = statusBattleCharacteristics.get(maxStat).getValue();
		if(current > max) {
			statusBattleCharacteristics.get(stat).setValue(statusBattleCharacteristics.get(maxStat).getValue());
			return delta-(current-max);
		}
		return delta;
	}

	public synchronized boolean isRageFul() {
		return statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_RAGE).getValue() == statusBattleCharacteristics.get(StatusBattleCharacteristic.MAX_RAGE).getValue();
	}

	public synchronized void recoverHealtAndMana() {
		int maxHp = statusBattleCharacteristics.get(StatusBattleCharacteristic.MAX_HP).getValue();
		int currentHp = statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_HP).getValue();
		statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_HP).addInt(-currentHp+maxHp);
		EventManager.getInstance().playingCharacterCharacteristicChanged(playingCharacter, StatusBattleCharacteristic.CURRENT_HP, maxHp, -currentHp+maxHp);
		
		int maxMp = statusBattleCharacteristics.get(StatusBattleCharacteristic.MAX_MP).getValue();
		int currentMp = statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_MP).getValue();
		statusBattleCharacteristics.get(StatusBattleCharacteristic.CURRENT_MP).addInt(-currentMp+maxMp);
		EventManager.getInstance().playingCharacterCharacteristicChanged(playingCharacter, StatusBattleCharacteristic.CURRENT_MP, maxMp, -currentMp+maxMp);
	}
}
