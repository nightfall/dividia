package it.nightfall.dividia.bl.characters.definitions;

import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.battle.DamageType;
import it.nightfall.dividia.bl.battle.Element;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AlteredStatusDefinition implements IAlteredStatusDefinition {
	private final String name;
	private final String displayName;
	private final int totalTicks;
	private final float tickInterval;
	private final long duration;
	private final boolean hasRemoveAlterations;
	private final int damage;
	private final IDamageType damageType;
	private final IElement element;
	private final Map<IStatusBattleCharacteristic, Integer> statusAlterations = new HashMap<>();

	public AlteredStatusDefinition(IConfigReader config){
		name = config.getString("name");
		displayName = config.getString("displayName");
		totalTicks = config.getInteger("totalTicks");
		duration = config.getInteger("duration");
		hasRemoveAlterations = config.getBoolean("hasRemoveAlterations");
		damage = config.getInteger("damage");
		damageType = DamageType.valueOf(config.getString("damageType"));
		element = Element.valueOf(config.getString("element"));
		tickInterval = config.getFloat("tickInterval");
		Set<IStatusBattleCharacteristic> tempAlterations = StatusBattleCharacteristic.getStatusAlterationsStats();
		for(IStatusBattleCharacteristic alteration: tempAlterations){
			statusAlterations.put(alteration, config.getInteger(alteration.getName()));
		}
	}

	@Override
	public int getDamage(){
		return damage;
	}

	@Override
	public long getDuration(){
		return duration;
	}

	@Override
	public boolean hasRemoveAlterations(){
		return hasRemoveAlterations;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public int getTotalTicks(){
		return totalTicks;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}
	
	@Override
	public int getAlteration(IStatusBattleCharacteristic characteristic){
		return statusAlterations.get(characteristic);
	}
	
	@Override
	public IDamageType getDamageType(){
		return damageType;
	}

	@Override
	public IElement getElement(){
		return element;
	}

	@Override
	public float getTickInterval(){
		return tickInterval;
	}
}
