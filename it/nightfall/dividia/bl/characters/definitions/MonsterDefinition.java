
package it.nightfall.dividia.bl.characters.definitions;

import it.nightfall.dividia.api.characters.definitions.IMonsterRaceDefinition;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.util.UUID;


public class MonsterDefinition {
	
	private IMonsterRaceDefinition raceDefinition;
	private int level;
	private IGamePoint startingPosition;
	private IDirection startingDirection;
	
	
	public MonsterDefinition(IConfigReader config) {
		level = config.getInteger("level");
		startingPosition = new GamePoint(config.getFloat("positionX"),config.getFloat("positionY"));
		startingDirection = new Direction(config.getFloat("direction"));
		String raceId = config.getString("race");
		raceDefinition = (IMonsterRaceDefinition) GameStorage.getInstance().getRaceDefinition(raceId);
	}
	
	public Monster getMonsterFromDefinition(IGameMap map) {
		String id = UUID.randomUUID().toString();
		Monster monster = new Monster(id, raceDefinition.getName(), map, startingPosition, startingDirection,level);
		monster.setLevel(level);
		return monster;
	}
	
	
}
