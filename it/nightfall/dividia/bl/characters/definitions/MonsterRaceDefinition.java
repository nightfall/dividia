package it.nightfall.dividia.bl.characters.definitions;

import it.nightfall.dividia.api.characters.definitions.IMonsterRaceDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MonsterRaceDefinition extends RaceDefinition implements IMonsterRaceDefinition {
	private int intelligenceQuotience;
	private float defaultPatrolRadius;
	private float defaultTiredDistance;
	private float defaultPatrolMaxPause;
	private float defaultPatrolMinPause;
	private int defaultSecondsToRespawn;
	private Map<String, Float> dropRates = new HashMap<>();
	private int manaReward;
	private String weaponDefinition;
	private boolean recoverStatus;

	public MonsterRaceDefinition(IConfigReader config) {
		super(config);
		if(config.doesElementExist("monsterParameters")) {
			IConfigReader monsterParametersReader = config.getConfigReader("monsterParameters");
			weaponDefinition = monsterParametersReader.getString("weapon");
			intelligenceQuotience = monsterParametersReader.getInteger("intelligenceQuotience");
			manaReward = monsterParametersReader.getInteger("manaReward");
			defaultPatrolRadius = monsterParametersReader.getFloat("defaultPatrolRadius");
			defaultTiredDistance = monsterParametersReader.getFloat("defaultTiredDistance");
			defaultPatrolMaxPause = monsterParametersReader.getFloat("defaultPatrolMaxPause");
			defaultPatrolMinPause = monsterParametersReader.getFloat("defaultPatrolMinPause");
			defaultSecondsToRespawn = monsterParametersReader.getInteger("defaultSecondsToRespawn");
			Collection<IConfigReader> dropRatesReaders = config.getConfigReader("dropRates").getConfigReaderList("dropRate");
			for(IConfigReader singleDropRateReader: dropRatesReaders) {
				String item = singleDropRateReader.getString("item");
				float chance = singleDropRateReader.getFloat("chance");
				dropRates.put(item, chance);
			}
			recoverStatus = config.getBoolean("recoverStatus");
		}
	}

	@Override
	public String getWeaponDefinition() {
		return weaponDefinition;
	}	
	
	@Override
	public int getIntelligenceQuotience() {
		return intelligenceQuotience;
	}

	@Override
	public float getDefaultPatrolRadius() {
		return defaultPatrolRadius;
	}

	@Override
	public float getDefaultTiredDistance() {
		return defaultTiredDistance;
	}

	@Override
	public float getDefaultPatrolMaxPause() {
		return defaultPatrolMaxPause;
	}

	@Override
	public float getDefaultPatrolMinPause() {
		return defaultPatrolMinPause;
	}

	@Override
	public Map<String, Float> getDropRates() {
		return dropRates;
	}

	@Override
	public int getDefaultManaReward() {
		return manaReward;
	}

	@Override
	public boolean isRecoveringStatus() {
		return recoverStatus;
	}

	@Override
	public int getDefaultSecondsToRespawn() {
		return defaultSecondsToRespawn;
	}
	
	
	
}
