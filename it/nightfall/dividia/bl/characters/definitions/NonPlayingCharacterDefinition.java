package it.nightfall.dividia.bl.characters.definitions;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.NonPlayingCharacter;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.dialogues.LanguageManager;
import java.util.UUID;

public class NonPlayingCharacterDefinition {
	private IGamePoint startingPosition;
	private IDirection startingDirection;
	private String race;
	
	
	public NonPlayingCharacterDefinition(IConfigReader config) {
		startingPosition = new GamePoint(config.getFloat("positionX"),config.getFloat("positionY"));
		startingDirection = new Direction(config.getFloat("direction"));
		race = config.getString("race");
	}
	
	public NonPlayingCharacter getNpcFromDefinition(IGameMap map) {
		String id = UUID.randomUUID().toString();
		NonPlayingCharacterRaceDefinition raceDefinition = GameStorage.getInstance().getNonPlayingCharacterRaceDefinition(race);
		NonPlayingCharacter npc = new NonPlayingCharacter(id, raceDefinition, raceDefinition.getDisplayName(), map, startingPosition, startingDirection);
		return npc;
	}
}
