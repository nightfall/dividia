package it.nightfall.dividia.bl.characters.definitions;

import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.utility.dialogues.LanguageManager;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NonPlayingCharacterRaceDefinition {
	
	private LanguageManager languageManager;
	private IDialog defaultDialog;
	private String raceName;
	private String displayName;
	private List<IQuestDefinition> quests = new LinkedList<>();
	private Map<GuiStatus,ICollisionShape> boundingShapes = new HashMap();
			
	public NonPlayingCharacterRaceDefinition(IConfigReader config) {
		raceName = config.getString("name");
		displayName = config.getString("displayName");
		languageManager = new LanguageManager(this);
		String dialogId = config.getConfigReader("defaultDialog").getString("dialog");
		defaultDialog = GameStorage.getInstance().getDialog(dialogId);
		Collection<String> questsId = config.getConfigReader("quests").getStringList("quest");
		for(String singleQuest : questsId) {
			quests.add(GameStorage.getInstance().getQuestDefinition(singleQuest));
		}
			//bounding shapes loading
		Collection<IConfigReader> shapeReaders = config.getConfigReader("shapes").getConfigReaderList("shape");
		for (IConfigReader currentShapeReader : shapeReaders){
			String type = currentShapeReader.getStringAttribute("type");
			String className = currentShapeReader.getStringAttribute("className");
			GuiStatus guiStatus = GuiStatus.WORLD_3D;
			if (type.equals("2D")){
				guiStatus = GuiStatus.WORLD_2D;
			}
			//TODO handle single exceptions
			try {
				Class shapeClass = Class.forName(className);
				ICollisionShape shape = (ICollisionShape)shapeClass.getConstructor(IConfigReader.class).newInstance(currentShapeReader);
				boundingShapes.put(guiStatus, shape);
			} catch (Exception ex) {
				Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
			} 
		}
		
		
	}

	public LanguageManager getLanguageManager() {
		return languageManager;
	}

	public IDialog getDefaultDialog() {
		return defaultDialog;
	}

	public String getRaceName() {
		return raceName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public List<IQuestDefinition> getQuests() {
		return quests;
	}
	
	
	public ICollisionShape getBoundingShape(GuiStatus guiStatus) {
		return (ICollisionShape) boundingShapes.get(guiStatus).clone();
	}
	
}
