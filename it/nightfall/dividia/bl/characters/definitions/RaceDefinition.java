package it.nightfall.dividia.bl.characters.definitions;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import it.nightfall.dividia.api.utility.IQuadraticFunction;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.utility.ModifiableInteger;
import it.nightfall.dividia.bl.utility.QuadraticFunction;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RaceDefinition implements IRaceDefinition {
	private final String name;
	private final String displayName;
	private final Map<IStatusBattleCharacteristic,IQuadraticFunction> growingFunctions = new HashMap<>();
	private final Map<IStatusBattleCharacteristic, IModifiableInteger> nonGrowingValues = new HashMap<>();
	private static final Set<IStatusBattleCharacteristic> statusBattleGrowingCharacteristics = StatusBattleCharacteristic.getBattleGrowingStats();
	private static final Set<IStatusBattleCharacteristic> nonGrowingCharacteristics = StatusBattleCharacteristic.getRaceNonGrowingStats();
	private Collection<String> startingSpells = new LinkedList<>();
	private float manaSelfRewardFactor;
	private Map<GuiStatus,ICollisionShape> boundingShapes = new HashMap();
	
	
	
	public RaceDefinition(IConfigReader config){
		name = config.getString("name");
		displayName = config.getString("displayName");
		for(IStatusBattleCharacteristic c: statusBattleGrowingCharacteristics){
			growingFunctions.put(c, instantiateGrowingFunction(c.getName(), config));
		}
		for(IStatusBattleCharacteristic c: nonGrowingCharacteristics){
			nonGrowingValues.put(c, instantiateNonGrowingFunction(c.getName(), config));
		}
		Collection<IConfigReader> spellReaders = config.getConfigReader("startingSpells").getConfigReaderList("spell");
		for(IConfigReader singleSpell:spellReaders) {
			String spellId = singleSpell.getString("id");
			startingSpells.add(spellId);
		}
		manaSelfRewardFactor = config.getFloat("manaSelfRewardFactor");
		
		//bounding shapes loading
		Collection<IConfigReader> shapeReaders = config.getConfigReader("shapes").getConfigReaderList("shape");
		for (IConfigReader currentShapeReader : shapeReaders){
			String type = currentShapeReader.getStringAttribute("type");
			String className = currentShapeReader.getStringAttribute("className");
			GuiStatus guiStatus = GuiStatus.WORLD_3D;
			if (type.equals("2D")){
				guiStatus = GuiStatus.WORLD_2D;
			}
			//TODO handle single exceptions
			try {
				Class shapeClass = Class.forName(className);
				ICollisionShape shape = (ICollisionShape)shapeClass.getConstructor(IConfigReader.class).newInstance(currentShapeReader);
				boundingShapes.put(guiStatus, shape);
			} catch (Exception ex) {
				Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
			} 
		}
	}

	private IQuadraticFunction instantiateGrowingFunction(String functionID, IConfigReader raceConfigs){
		return new QuadraticFunction(raceConfigs.getDouble(functionID + "_0"), raceConfigs.getDouble(functionID + "_1"), raceConfigs.getDouble(functionID + "_2"));
	}
	
	@Override
	public IQuadraticFunction getGrowingFunction(IStatusBattleCharacteristic c) {
		return growingFunctions.get(c);
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}

	private IModifiableInteger instantiateNonGrowingFunction(String statName, IConfigReader raceConfigs) {
		return new ModifiableInteger(raceConfigs.getInteger(statName));
	}

	@Override
	public IModifiableInteger getNonGrowingValue(IStatusBattleCharacteristic c) {
		return nonGrowingValues.get(c);
	}

	@Override
	public Collection<String> getStartingSpells() {
		return startingSpells;
	}

	@Override
	public float getManaSelfRewardFactor() {
		return manaSelfRewardFactor;
	}

	@Override
	public ICollisionShape getBoundingShape(GuiStatus guiStatus) {
		return (ICollisionShape) boundingShapes.get(guiStatus).clone();
	}
	
}
