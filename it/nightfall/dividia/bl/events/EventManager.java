package it.nightfall.dividia.bl.events;

import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.IAlteredStatus;
import it.nightfall.dividia.api.events.IEventManager;
import it.nightfall.dividia.api.events.IGameMapEvents;
import it.nightfall.dividia.api.events.IMonsterEvents;
import it.nightfall.dividia.api.events.INetworkEvents;
import it.nightfall.dividia.api.events.IPlayerEvents;
import it.nightfall.dividia.api.events.IPlayingCharacterEvents;
import it.nightfall.dividia.api.events.IQuestEvents;
import it.nightfall.dividia.api.events.ISpellEvents;
import it.nightfall.dividia.api.events.IStatusEvents;
import it.nightfall.dividia.api.item.IBagOfItems;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.network.IEventDispatcher;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.world.Teleport;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class EventManager implements IEventManager {
	private static EventManager eventManager = null;
	private Collection<IQuestEvents> questListeners = new HashSet<>();
	private Collection<IMonsterEvents> monsterListeners = new HashSet<>();
	private Collection<IPlayerEvents> playerListeners = new HashSet<>();
	private Collection<IGameMapEvents> gameMapListeners = new HashSet<>();
	private Collection<IPlayingCharacterEvents> playingCharacterListeners = new HashSet<>();
	private Collection<INetworkEvents> networkListeners = new HashSet<>();
	private Collection<IStatusEvents> statusListeners = new HashSet<>();
	private Collection<ISpellEvents> spellListeners = new HashSet<>();
	private Collection<Object> singleCallListeners = new HashSet<>();
	/*
	 * Event handlers section
	 */

	private EventManager(){
	}

	public static EventManager getInstance(){
		if(eventManager == null){
			eventManager = new EventManager();
		}
		return eventManager;
	}

	@Override
	public void addSingleCallListener(Object listener){
		singleCallListeners.add(listener);
	}

	@Override
	public void addListener(IQuestEvents listener){
		questListeners.add(listener);
	}

	@Override
	public synchronized void addListener(IMonsterEvents listener){
		monsterListeners.add(listener);
	}

	@Override
	public void addListener(IPlayerEvents listener){
		playerListeners.add(listener);
	}

	@Override
	public void addListener(IGameMapEvents listener){
		gameMapListeners.add(listener);
	}

	@Override
	public void addListener(IPlayingCharacterEvents listener){
		playingCharacterListeners.add(listener);
	}

	@Override
	public void addListener(INetworkEvents listener){
		networkListeners.add(listener);
	}

	@Override
	public void addListener(IStatusEvents listener){
		statusListeners.add(listener);
	}

	@Override
	public void addListener(ISpellEvents listener){
		spellListeners.add(listener);
	}

	@Override
	public void addListener(IEventDispatcher listener){
		questListeners.add(listener);
		monsterListeners.add(listener);
		playerListeners.add(listener);
		gameMapListeners.add(listener);
		playingCharacterListeners.add(listener);
		statusListeners.add(listener);
		networkListeners.add(listener);
		spellListeners.add(listener);
	}
	/*
	 * Event handlers removal section
	 */

	@Override
	public void removeListener(IQuestEvents listener){
		questListeners.remove(listener);
	}

	@Override
	public synchronized void removeListener(IMonsterEvents listener){
		monsterListeners.remove(listener);
	}

	@Override
	public void removeListener(IPlayerEvents listener){
		playerListeners.remove(listener);
	}

	@Override
	public void removeListener(IGameMapEvents listener){
		gameMapListeners.remove(listener);
	}

	@Override
	public void removeListener(IPlayingCharacterEvents listener){
		playingCharacterListeners.remove(listener);
	}

	@Override
	public void removeListener(INetworkEvents listener){
		networkListeners.remove(listener);
	}

	@Override
	public void removeListener(IStatusEvents listener){
		statusListeners.remove(listener);
	}

	@Override
	public void removeListener(ISpellEvents listener){
		spellListeners.remove(listener);
	}

	@Override
	public synchronized void removeListener(IEventDispatcher listener){
		questListeners.remove(listener);
		monsterListeners.remove(listener);
		playerListeners.remove(listener);
		gameMapListeners.remove(listener);
		playingCharacterListeners.remove(listener);
		statusListeners.remove(listener);
		networkListeners.remove(listener);
		spellListeners.remove(listener);
	}

	/*
	 * Event raisers section
	 */
	@Override
	public void questActivated(IQuest quest){
		for(IQuestEvents listener : questListeners){
			listener.questActivated(quest);
		}
	}

	@Override
	public void questCompleted(IQuest quest){
		for(IQuestEvents listener : questListeners){
			listener.questCompleted(quest);
		}
	}

	@Override
	public void goalActivated(IGoal goal){
		for(IQuestEvents listener : questListeners){
			listener.goalActivated(goal);
		}
	}

	@Override
	public void goalCompleted(IGoal goal){
		for(IQuestEvents listener : questListeners){
			listener.goalCompleted(goal);
		}
	}

	@Override
	public void goalUpdated(IGoal goal){
		for(IQuestEvents listener : questListeners){
			listener.goalUpdated(goal);
		}
	}

	@Override
	public synchronized void monsterDied(Monster monster, Player killer){
		Iterator<IMonsterEvents> iterator = monsterListeners.iterator();
		while(iterator.hasNext()){
			IMonsterEvents currentListener = iterator.next();
			currentListener.monsterDied(monster, killer);
			if(singleCallListeners.contains(currentListener)){
				singleCallListeners.remove(currentListener);
				iterator.remove();
			}
		}
	}

	@Override
	public void playerDied(Player player, PlayingCharacter killer){
		for(IPlayerEvents listener : playerListeners){
			listener.playerDied(player, killer);
		}
	}

	@Override
	public void itemPicked(IItem item, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.itemPicked(item, player);
		}
	}

	@Override
	public void playerEnteredMap(ISummary playerSummary, IGameMap currentMap){
		for(IPlayerEvents listener : playerListeners){
			listener.playerEnteredMap(playerSummary, currentMap);
		}
	}

	@Override
	public void bagOfItemsDropped(IBagOfItems droppingBag, IGameMap map){
		for(IGameMapEvents listener : gameMapListeners){
			listener.bagOfItemsDropped(droppingBag, map);
		}
	}

	@Override
	public void bagOfItemsRemoved(IBagOfItems removingBag, IGameMap map){
		for(IGameMapEvents listener : gameMapListeners){
			listener.bagOfItemsRemoved(removingBag, map);
		}
	}

	@Override
	public void knapsackFull(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.knapsackFull(player);
		}
	}

	@Override
	public void dialogSuccessfullyCompleted(String dialogID, Player player){
		Iterator<IPlayerEvents> iterator = playerListeners.iterator();
		while(iterator.hasNext()){
			IPlayerEvents currentListener = iterator.next();
			currentListener.dialogSuccessfullyCompleted(dialogID, player);
			if(singleCallListeners.contains(currentListener)){
				singleCallListeners.remove(currentListener);
				iterator.remove();
			}
		}
	}

	@Override
	public void characterDied(PlayingCharacter character, PlayingCharacter killer){
		for(IPlayingCharacterEvents listener : playingCharacterListeners){
			listener.characterDied(character, killer);
		};
	}

	@Override
	public void characterMoved(PlayingCharacter character){
		for(IPlayingCharacterEvents listener : playingCharacterListeners){
			listener.characterMoved(character);
		}
	}

	@Override
	public void focusMoved(String playerId, double x, double y, String focusStatus){
		for(IPlayerEvents listener : playerListeners){
			listener.focusMoved(playerId, x, y, focusStatus);
		}
	}

	@Override
	public void characterStopped(PlayingCharacter character){
		for(IPlayingCharacterEvents listener : playingCharacterListeners){
			listener.characterStopped(character);
		}
	}

	@Override
	public void damageReceived(PlayingCharacter affected, PlayingCharacter attacker, IDamageSummary summary){
		for(IPlayingCharacterEvents listener : playingCharacterListeners){
			listener.damageReceived(affected, attacker, summary);
		}
	}

	@Override
	public void userDisconnected(Player player, String username){
		for(INetworkEvents listener : networkListeners){
			listener.userDisconnected(player, username);
		}
	}

	@Override
	public void playingCharacterCharacteristicChanged(PlayingCharacter playingCharacter, IStatusBattleCharacteristic characteristic, int newValue, int delta){
		for(IStatusEvents listener : statusListeners){
			listener.playingCharacterCharacteristicChanged(playingCharacter, characteristic, newValue, delta);
		}
	}

	@Override
	public void playerCharacteristicChanged(Player player, IStatusBattleCharacteristic characteristic, int newValue, int delta){
		for(IStatusEvents listener : statusListeners){
			listener.playerCharacteristicChanged(player, characteristic, newValue, delta);
		}
	}

	@Override
	public void playerLevelUp(Player player, int newLevel, int maxExperience){
		for(IStatusEvents listener : statusListeners){
			listener.playerLevelUp(player, newLevel, maxExperience);
		}
	}

	@Override
	public void playerLevelUpByExperience(Player player, int newLevel, int maxExperience){
		for(IStatusEvents listener : statusListeners){
			listener.playerLevelUpByExperience(player, newLevel, maxExperience);
		}
	}

	@Override
	public void playerGainExperience(Player player, int newValue, int delta){
		for(IStatusEvents listener : statusListeners){
			listener.playerGainExperience(player, newValue, delta);
		}
	}

	@Override
	public void teleportUsed(IGameMap previousMap, Player player, Teleport teleport) {
		for(IPlayerEvents listener : playerListeners){
			listener.teleportUsed(previousMap,player, teleport);
		}
	}

	@Override
	public void mapSummaryReady(String playerName, ISummary gameMapSummary){
		for(INetworkEvents listener : networkListeners){
			listener.mapSummaryReady(playerName, gameMapSummary);
		}
	}

	@Override
	public void playerSummaryReady(Player player, ISummary playerSummary){
		for(INetworkEvents listener : networkListeners){
			listener.playerSummaryReady(player, playerSummary);
		}
	}

	@Override
	public void spellOnCooldown(Player player, ISpellDefinition spell){
		for(INetworkEvents listener : networkListeners){
			listener.spellOnCooldown(player, spell);
		}
	}

	@Override
	public void launchAreaSpell(ISpellDefinition spell, IDirection castingDirection, IGamePoint castingPoint, IGameMap map, String casterId){
		for(INetworkEvents listener : networkListeners){
			listener.launchAreaSpell(spell, castingDirection, castingPoint, map, casterId);
		}
	}

	@Override
	public void ping(String username){
		for(INetworkEvents listener : networkListeners){
			listener.ping(username);
		}
	}

	@Override
	public void focusReset(Player playerId, double x, double y){
		for(IPlayerEvents listener : playerListeners){
			listener.focusReset(playerId, x, y);
		}
	}

	@Override
	public void spellCastingStart(ISpellDefinition spell, PlayingCharacter executor){
		for(IPlayerEvents listener : playerListeners){
			listener.spellCastingStart(spell, executor);
		}
	}

	@Override
	public void performBasicAttack(PlayingCharacter executor){
		for(IPlayingCharacterEvents listener : playingCharacterListeners){
			listener.performBasicAttack(executor);
		}
	}

	@Override
	public void armorEquipped(IArmor armor, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.armorEquipped(armor, player);
		}
	}

	@Override
	public void bootsEquipped(IBoots boots, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.bootsEquipped(boots, player);
		}
	}

	@Override
	public void helmetEquipped(IHelmet helmet, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.helmetEquipped(helmet, player);
		}
	}

	@Override
	public void glovesEquipped(IGloves gloves, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.glovesEquipped(gloves, player);
		}
	}

	@Override
	public void necklaceEquipped(INecklace necklace, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.necklaceEquipped(necklace, player);
		}
	}

	@Override
	public void ringEquipped(IRing ring, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.ringEquipped(ring, player);
		}
	}

	@Override
	public void shieldEquipped(IShield shield, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.shieldEquipped(shield, player);
		}
	}

	@Override
	public void weaponEquipped(IWeapon weapon, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.weaponEquipped(weapon, player);
		}
	}

	@Override
	public void mysticalSphereEquipped(IMysticalSphere mysticalSphere, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.mysticalSphereEquipped(mysticalSphere, player);
		}
	}

	@Override
	public void armorUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.armorUnequipped(player);
		}
	}

	@Override
	public void bootsUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.bootsUnequipped(player);
		}
	}

	@Override
	public void helmetUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.helmetUnequipped(player);
		}
	}

	@Override
	public void glovesUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.glovesUnequipped(player);
		}
	}

	@Override
	public void necklaceUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.necklaceUnequipped(player);
		}
	}

	@Override
	public void ringUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.ringUnequipped(player);
		}
	}

	@Override
	public void shieldUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.shieldUnequipped(player);
		}
	}

	@Override
	public void weaponUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.weaponUnequipped(player);
		}
	}

	@Override
	public void mysticalSphereUnequipped(Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.mysticalSphereUnequipped(player);
		}
	}

	@Override
	public void alteredStatusApplied(PlayingCharacter playingCharacter, PlayingCharacter executor, IAlteredStatus tempAlteredStatus){
		for(IPlayingCharacterEvents listener : playingCharacterListeners){
			listener.alteredStatusApplied(playingCharacter, executor, tempAlteredStatus);
		}
	}

	@Override
	public synchronized void monsterEnteredMap(ISummary summary, IGameMap map){
		for(IMonsterEvents listener : monsterListeners){
			listener.monsterEnteredMap(summary, map);
		}
	}

	@Override
	public void itemAdded(IItem item, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.itemAdded(item, player);
		}
	}

	@Override
	public void itemRemoved(IItem item, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.itemRemoved(item, player);
		}
	}

	@Override
	public void dialogStarted(String id, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.dialogStarted(id, player);
		}
	}

	@Override
	public void dialogsMenuOpened(Collection<IDialog> dialogs, Player player){
		for(IPlayerEvents listener : playerListeners){
			listener.dialogsMenuOpened(dialogs, player);
		}
	}
}
