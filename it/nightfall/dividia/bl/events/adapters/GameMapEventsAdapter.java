package it.nightfall.dividia.bl.events.adapters;

import it.nightfall.dividia.api.events.IGameMapEvents;
import it.nightfall.dividia.api.item.IBagOfItems;
import it.nightfall.dividia.api.world.IGameMap;

public class GameMapEventsAdapter implements IGameMapEvents {

	@Override
	public void bagOfItemsDropped(IBagOfItems droppingBag, IGameMap map) {
	}

	@Override
	public void bagOfItemsRemoved(IBagOfItems removingBag, IGameMap map) {
	}
}
