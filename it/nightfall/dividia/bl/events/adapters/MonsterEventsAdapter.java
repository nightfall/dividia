package it.nightfall.dividia.bl.events.adapters;

import it.nightfall.dividia.api.events.IMonsterEvents;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;

public class MonsterEventsAdapter implements IMonsterEvents {
	@Override
	public void monsterDied(Monster monster, Player killer){
	}
	
	@Override
	public void monsterEnteredMap(ISummary summary, IGameMap currentMap) {
	}
}
