package it.nightfall.dividia.bl.events.adapters;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.events.INetworkEvents;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.summaries.IGeneralGameMapSummary;
import it.nightfall.dividia.bl.characters.Player;

public class NetworkEventsAdapter implements INetworkEvents {
	@Override
	public void userDisconnected(Player player, String username){
	}

	@Override
	public void mapSummaryReady(String player, ISummary generalGameMapSummary){
	}

	@Override
	public void playerSummaryReady(Player player, ISummary summary){
	}

	@Override
	public void spellOnCooldown(Player player, ISpellDefinition spell){
	}

	@Override
	public void ping(String username){
	}

	@Override
	public void launchAreaSpell(ISpellDefinition spell, IDirection castingDirection, IGamePoint castingPoint, IGameMap map, String casterId) {
	}
}
