package it.nightfall.dividia.bl.events.adapters;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.events.IPlayerEvents;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.world.Teleport;
import java.util.Collection;

public class PlayerEventsAdapter implements IPlayerEvents {
	@Override
	public void playerDied(Player player, PlayingCharacter killer){
	}

	@Override
	public void itemPicked(IItem item, Player player){
	}

	@Override
	public void dialogSuccessfullyCompleted(String dialogID, Player player){
	}

	@Override
	public void teleportUsed(IGameMap previousMap, Player player, Teleport teleport){
	}

	@Override
	public void knapsackFull(Player player){
	}

	@Override
	public void focusMoved(String playerId, double x, double y, String focusStatus){
	}

	@Override
	public void focusReset(Player player, double x, double y){
	}

	@Override
	public void spellCastingStart(ISpellDefinition spell, PlayingCharacter executor){
	}

	@Override
	public void playerEnteredMap(ISummary playerSummary, IGameMap map){
	}

	@Override
	public void armorUnequipped(Player player){
	}

	@Override
	public void bootsUnequipped(Player player){
	}

	@Override
	public void helmetUnequipped(Player player){
	}

	@Override
	public void glovesUnequipped(Player player){
	}

	@Override
	public void necklaceUnequipped(Player player){
	}

	@Override
	public void ringUnequipped(Player player){
	}

	@Override
	public void shieldUnequipped(Player player){
	}

	@Override
	public void weaponUnequipped(Player player){
	}

	@Override
	public void mysticalSphereUnequipped(Player player){
	}

	@Override
	public void itemRemoved(IItem item, Player player){
	}

	@Override
	public void dialogStarted(String id, Player player){
	}

	@Override
	public void dialogsMenuOpened(Collection<IDialog> dialogs, Player player){
	}

	@Override
	public void itemAdded(IItem item, Player player){
	}

	@Override
	public void armorEquipped(IArmor armor, Player player){
	}

	@Override
	public void bootsEquipped(IBoots boots, Player player){
	}

	@Override
	public void helmetEquipped(IHelmet helmet, Player player){
	}

	@Override
	public void glovesEquipped(IGloves gloves, Player player){
	}

	@Override
	public void necklaceEquipped(INecklace necklace, Player player){
	}

	@Override
	public void ringEquipped(IRing ring, Player player){
	}

	@Override
	public void shieldEquipped(IShield shield, Player player){
	}

	@Override
	public void weaponEquipped(IWeapon weapon, Player player){
	}

	@Override
	public void mysticalSphereEquipped(IMysticalSphere mysticalSphere, Player player){
	}
}
