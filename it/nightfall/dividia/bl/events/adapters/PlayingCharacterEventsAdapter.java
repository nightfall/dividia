package it.nightfall.dividia.bl.events.adapters;

import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.characters.IAlteredStatus;
import it.nightfall.dividia.api.events.IPlayingCharacterEvents;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.battle.definitions.SpellDefinition;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public class PlayingCharacterEventsAdapter implements IPlayingCharacterEvents {
	@Override
	public void characterMoved(PlayingCharacter character){
	}
	
	@Override
	public void characterStopped(PlayingCharacter character){
	}

	@Override
	public void damageReceived(PlayingCharacter affected, PlayingCharacter attacker, IDamageSummary summary){
	}

	@Override
	public void characterDied(PlayingCharacter character, PlayingCharacter killer){
	}

	@Override
	public void performBasicAttack(PlayingCharacter executor) {
	}

	@Override
	public void alteredStatusApplied(PlayingCharacter playingCharacter, PlayingCharacter executor, IAlteredStatus tempAlteredStatus) {
	}
}
