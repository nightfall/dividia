package it.nightfall.dividia.bl.events.adapters;

import it.nightfall.dividia.api.events.IQuestEvents;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.IGoal;

public class QuestEventsAdapter implements IQuestEvents {
	@Override
	public void questCompleted(IQuest quest){
	}

	@Override
	public void questActivated(IQuest quest){
	}

	@Override
	public void goalCompleted(IGoal goal){
	}

	@Override
	public void goalActivated(IGoal goal){
	}

	@Override
	public void goalUpdated(IGoal goal){
	}
}
