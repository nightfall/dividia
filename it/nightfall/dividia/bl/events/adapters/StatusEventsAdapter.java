package it.nightfall.dividia.bl.events.adapters;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.events.IStatusEvents;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;

public class StatusEventsAdapter implements IStatusEvents {
	@Override
	public void playingCharacterCharacteristicChanged(PlayingCharacter playingCharacter, IStatusBattleCharacteristic characteristic, int newValue, int delta){
	}

	@Override
	public void playerCharacteristicChanged(Player player, IStatusBattleCharacteristic characteristic, int newValue, int delta){
	}

	@Override
	public void playerLevelUp(Player player, int newLevel, int maxExperience){
	}

	@Override
	public void playerGainExperience(Player player, int newValue, int delta){
	}

	@Override
	public void playerLevelUpByExperience(Player player, int newLevel, int maxExperience) {
	}
}
