package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.UUID;

public abstract class AbstractItem extends SortableAdapter implements IItem {
	private IItemDefinition itemDefinition;
	protected String id;
	protected long pickUpDate;

	public AbstractItem(IItemDefinition abstractItemDef){
		itemDefinition = abstractItemDef;
		id = UUID.randomUUID().toString();
	}

	@Override
	public IItemDefinition getItemDefinition(){
		return itemDefinition;
	}

	@Override
	public long getPickUpDate(){
		return pickUpDate;
	}

	@Override
	public String getId(){
		return id;
	}

	@Override
	public String getName(){
		return itemDefinition.getDisplayName();
	}
	
	@Override
	public int getWeight(){
		return itemDefinition.getWeight();
	}
	
	public boolean equals(AbstractItem other){
		return other.id.equals(other.id);
	}

	@Override
	public abstract void use(Player player);

	@Override
	public void pickUp(Player player){
		pickUpDate = System.currentTimeMillis();
		addToInventory(player.getInventory());
		EventManager.getInstance().itemPicked(this, player);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		tempSummary.putData("id", id);
		tempSummary.putData("name", itemDefinition.getName());
		tempSummary.putData("pickUpDate", pickUpDate);
		return tempSummary;
	}

	@Override
	public void buildFromSummary(ISummary summary){
		id = summary.getString("id");
		pickUpDate = summary.getLong("pickUpDate");
	}
	
	@Override
	public abstract void addToInventory(IGenericInventory intentory);

	@Override
	public void loadState(IConfigReader saveState){
	}

	@Override
	public void saveState(IConfigWriter saveState){
		saveState.appendString("name", itemDefinition.getName());
	}
}
