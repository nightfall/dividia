package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IDroppable;
import it.nightfall.dividia.api.item.IInventory;
import it.nightfall.dividia.api.item.definitions.ISellableItemDefinition;

public abstract class AbstractRemovableItem extends AbstractSellableItem implements IDroppable {
	public AbstractRemovableItem(ISellableItemDefinition abstractSellableItemDef){
		super(abstractSellableItemDef);
	}

	@Override
	public void drop(BagOfItems bag, IInventory inventory){
		bag.addItem(this);
		//TODO: Implementare
	}
}
