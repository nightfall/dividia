package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.ISellableItem;
import it.nightfall.dividia.api.item.definitions.ISellableItemDefinition;

public abstract class AbstractSellableItem extends AbstractItem implements ISellableItem {
	private ISellableItemDefinition sellableItemDefinition;

	public AbstractSellableItem(ISellableItemDefinition abstractSellableItemDef){
		super(abstractSellableItemDef);
		sellableItemDefinition = abstractSellableItemDef;
	}

	@Override
	public ISellableItemDefinition getItemDefinition(){
		return sellableItemDefinition;
	}
	
	@Override
	public int getPrice(){
		return sellableItemDefinition.getPrice();
	}
}
