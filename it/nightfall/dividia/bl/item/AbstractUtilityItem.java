package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IUtilityItem;
import it.nightfall.dividia.api.item.definitions.IUtilityItemDefinition;
import it.nightfall.dividia.bl.item.definitions.AbstractUtilityItemDefinition;

public abstract class AbstractUtilityItem extends AbstractItem implements IUtilityItem {
	private AbstractUtilityItemDefinition utilityDefinition;

	public AbstractUtilityItem(AbstractUtilityItemDefinition abstractUtilityItemDef){
		super(abstractUtilityItemDef);
		this.utilityDefinition = abstractUtilityItemDef;
	}

	@Override
	public IUtilityItemDefinition getItemDefinition(){
		return utilityDefinition;
	}
}
