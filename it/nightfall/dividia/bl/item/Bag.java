package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IBag;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class Bag<Type extends IItemDefinition> extends SortableAdapter implements IBag {
	private String id;
	private Type info;
	private int quantity;
	protected HashMap<String, IItem> items = new HashMap<>();

	public Bag(Type info){
		quantity = 1;
		this.info = info;
		id = UUID.randomUUID().toString();
	}

	@Override
	public String getId(){
		return id;
	}
	
	@Override
	public IItemDefinition getItemDefinition(){
		return info;
	}
	
	@Override
	public void addItem(IItem item){
		items.put(item.getId(), item);
	}

	@Override
	public void removeItem(String id){
		items.remove(id);
	}
	
	@Override
	public abstract IItem getItem(String id);
	
	@Override
	public int getQuantity(){
		return quantity;
	}

	@Override
	public void incrementQuantity(){
		quantity++;

	}

	@Override
	public void decrementQuantity(){
		quantity--;
	}

	@Override
	public void decrementBy(int num){
		quantity -= num;
	}

	@Override
	public void incrementBy(int num){
		quantity += num;
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		ISummary dataSummary = new Summary();
		for(Map.Entry<String, IItem> data : items.entrySet()){
			dataSummary.putData(data.getKey(), data.getValue().getSummary());
		}
		tempSummary.putData("info", info.getName());
		tempSummary.putData("quantity", quantity);
		tempSummary.putData("items", items);
		return tempSummary;
	}
}
