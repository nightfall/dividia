package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IBagOfItems;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.Summary;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * This class represent a bag of items.
 *
 * @author Nightfall
 *
 */
public class BagOfItems implements IBagOfItems {
	private Collection<IItem> itemList;
	private IGamePoint position;
	private String id;

	@Override
	public IGamePoint getPosition(){
		return position;
	}
	
	@Override
	public void setPosition(IGamePoint position) {
		this.position = position;
	}

	
	public BagOfItems(Collection<IItem> itemList){
		this.itemList = itemList;
		id = UUID.randomUUID().toString();
	}

	public BagOfItems(){
		this.itemList = new ArrayList<>();
		id = UUID.randomUUID().toString();
	}

	@Override
	public Collection<IItem> getItemList(){
		return itemList;
	}

	@Override
	public void addItem(IItem item){
		itemList.add(item);
	}

	@Override
	public int getBagWeight(){
		int weight = 0;
		for(IItem i : itemList){
			weight += i.getItemDefinition().getWeight();
		}
		return weight;
	}

	@Override
	public boolean isEmpty(){
		return itemList.isEmpty();
	}

	//TODO interfaccia che apre gli oggetti presenti nella bag
	@Override
	public void bagPickUp(Player player){
		for(IItem i : itemList){
			i.pickUp(player);
		}
		// TODO event to report pickUp success or fail
	}

	@Override
	public String getID(){
		return id;
	}

	public ISummary getSummary() {
		ISummary summary = new Summary();
		
		for (IItem currentItem : itemList){
			summary.putData(currentItem.getId(), currentItem.getSummary());
		}
		return summary;
	}
	
	
}
