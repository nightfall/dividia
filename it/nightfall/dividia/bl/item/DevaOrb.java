package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.definitions.IDevaOrbDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;

public class DevaOrb extends AbstractRemovableItem implements IDevaOrb {
	public DevaOrb(IDevaOrbDefinition definition){
		super(definition);
	}

	@Override
	public IDevaOrbDefinition getItemDefinition(){
		return (IDevaOrbDefinition) super.getItemDefinition();
	}

		
	@Override
	public String getType(){
		return getItemDefinition().getElement().getName();
	}
	
	@Override
	public void use(Player player){
		player.getEquippedItems().getWeapon().devaOrbFusion(this);
	}

	@Override
	public void addToInventory(IGenericInventory inventory){
		inventory.addItem(this);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
	
	@Override
	public void removeFromInventory(IGenericInventory inventory) {
		inventory.removeItem(this);
	}
}
