package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.IDevaOrbsBag;
import it.nightfall.dividia.api.item.definitions.IDevaOrbDefinition;

public class DevaOrbsBag extends Bag<IDevaOrbDefinition> implements IDevaOrbsBag {
	public DevaOrbsBag(IDevaOrbDefinition info){
		super(info);
	}

	@Override
	public IDevaOrbDefinition getDevaOrb(){
		return (IDevaOrbDefinition) super.getItemDefinition();
	}

	@Override
	public IDevaOrb getItem(String id){
		return (IDevaOrb) items.get(id);
	}
}
