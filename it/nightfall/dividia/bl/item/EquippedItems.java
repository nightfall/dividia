package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IEquippedItems;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.bl.utility.io.ConfigWriter;
import java.io.Serializable;
import java.util.Map;

public class EquippedItems implements IEquippedItems {
	private IArmor armor;
	private IBoots boots;
	private IHelmet helmet;
	private IGloves gloves;
	private INecklace necklace;
	private IRing ring;
	private IShield shield;
	private IWeapon weapon;
	private IMysticalSphere mysticalSphere;

	@Override
	public IArmor getArmor(){
		return armor;
	}

	@Override
	public IBoots getBoots(){
		return boots;
	}

	@Override
	public IGloves getGloves(){
		return gloves;
	}

	@Override
	public IHelmet getHelmet(){
		return helmet;
	}

	@Override
	public INecklace getNecklace(){
		return necklace;
	}

	@Override
	public IRing getRing(){
		return ring;
	}

	@Override
	public IShield getShield(){
		return shield;
	}

	@Override
	public IWeapon getWeapon(){
		return weapon;
	}

	@Override
	public IMysticalSphere getMysticalSphere(){
		return mysticalSphere;
	}

	// EQUIPMENT INFO
	@Override
	public boolean hasMysticalSphereEquipped(){
		return mysticalSphere != null;
	}

	@Override
	public boolean hasShieldEquipped(){
		return shield != null;
	}

	@Override
	public boolean hasNecklaceEquipped(){
		return necklace != null;
	}

	@Override
	public boolean hasBootsEquipped(){
		return boots != null;
	}

	@Override
	public boolean hasHelmetEquipped(){
		return helmet != null;
	}

	@Override
	public boolean hasRingEquipped(){
		return ring != null;
	}

	@Override
	public boolean hasGlovesEquipped(){
		return gloves != null;
	}

	@Override
	public boolean hasArmorEquipped(){
		return armor != null;
	}

	@Override
	public void setArmor(IArmor armor){
		this.armor = armor;
	}

	@Override
	public void setBoots(IBoots boots){
		this.boots = boots;
	}

	@Override
	public void setGloves(IGloves gloves){
		this.gloves = gloves;
	}

	@Override
	public void setHelmet(IHelmet helmet){
		this.helmet = helmet;
	}

	@Override
	public void setMysticalSphere(IMysticalSphere mysticalSphere){
		this.mysticalSphere = mysticalSphere;
	}

	@Override
	public void setNecklace(INecklace necklace){
		this.necklace = necklace;
	}

	@Override
	public void setRing(IRing ring){
		this.ring = ring;
	}

	@Override
	public void setShield(IShield shield){
		this.shield = shield;
	}

	@Override
	public void setWeapon(IWeapon weapon){
		this.weapon = weapon;
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		if(armor != null){
			tempSummary.putData("armor", armor.getSummary());
		}
		if(boots != null){
			tempSummary.putData("boots", boots.getSummary());
		}
		if(gloves != null){
			tempSummary.putData("gloves", gloves.getSummary());
		}
		if(helmet != null){
			tempSummary.putData("helmet", helmet.getSummary());
		}
		if(mysticalSphere != null){
			tempSummary.putData("mysticalSphere", mysticalSphere.getSummary());
		}
		if(necklace != null){
			tempSummary.putData("necklace", necklace.getSummary());
		}
		if(ring != null){
			tempSummary.putData("ring", ring.getSummary());
		}
		if(shield != null){
			tempSummary.putData("shield", shield.getSummary());
		}
		if(weapon != null){
			tempSummary.putData("weapon", weapon.getSummary());
		}
		return tempSummary;
	}

	@Override
	public void loadState(IConfigReader saveState){
		if(saveState.doesElementExist("weapon")){
			IConfigReader weaponConfig = saveState.getConfigReader("weapon");
			weapon = (IWeapon) GameStorage.getInstance().getItemDefinition(weaponConfig.getString("name")).getItemFromDefinition();
		}
	}

	@Override
	public void saveState(IConfigWriter saveState){
		if(armor != null){
			IConfigWriter tempConfig = new ConfigWriter("armor", saveState);
			armor.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(boots != null){
			IConfigWriter tempConfig = new ConfigWriter("boots", saveState);
			boots.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(gloves != null){
			IConfigWriter tempConfig = new ConfigWriter("gloves", saveState);
			gloves.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(helmet != null){
			IConfigWriter tempConfig = new ConfigWriter("helmet", saveState);
			helmet.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(mysticalSphere != null){
			IConfigWriter tempConfig = new ConfigWriter("mysticalSphere", saveState);
			mysticalSphere.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(necklace != null){
			IConfigWriter tempConfig = new ConfigWriter("necklace", saveState);
			necklace.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(ring != null){
			IConfigWriter tempConfig = new ConfigWriter("ring", saveState);
			ring.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(shield != null){
			IConfigWriter tempConfig = new ConfigWriter("shield", saveState);
			shield.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
		if(weapon != null){
			IConfigWriter tempConfig = new ConfigWriter("weapon", saveState);
			weapon.saveState(tempConfig);
			saveState.appendConfigWriter(tempConfig);
		}
	}

	@Override
	public void equipAfterLoad(PlayingCharacter playingCharater){
		playingCharater.getStatus().statusUpdateWithEquipChange(null, weapon);
		//TODO: Completare
	}

	@Override
	public void buildFromSummary(ISummary summary){
		Map<String, Serializable> data = summary.getInternalMap();
		for(Map.Entry<String,Serializable> importingItem : data.entrySet()){
			ISummary tempSummary = (ISummary) importingItem.getValue();
			IItem tempItem = GameStorage.getInstance().getItemDefinition(tempSummary.getString("name")).getItemFromDefinition();
			tempItem.buildFromSummary(tempSummary);
			switch(importingItem.getKey()){
				case "armor":
					armor = (IArmor) tempItem;
					break;
				case "boots":
					boots = (IBoots) tempItem;
					break;
				case "gloves":
					gloves = (IGloves) tempItem;
					break;
				case "helmet":
					helmet = (IHelmet) tempItem;
					break;
				case "mysticalSphere":
					mysticalSphere = (IMysticalSphere) tempItem;
					break;
				case "necklace":
					necklace = (INecklace) tempItem;
					break;
				case "ring":
					ring = (IRing) tempItem;
					break;
				case "shield":
					shield = (IShield) tempItem;
					break;
				case "weapon":
					weapon = (IWeapon) tempItem;
					break;
			}
		}
	}
}
