package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.*;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.item.definitions.IKnapsackDefinition;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.item.storages.*;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.Collection;

public class Inventory implements IInventory {
	private IKnapsackDefinition knapsack;
	private int currentWeight = 0;
	private IDStorage idStorage = new IDStorage();
	private DroppableStorage droppableStorage = new DroppableStorage();
	private EquipmentStorage equipmentStorage = new EquipmentStorage();
	private KeyStorage keyStorage = new KeyStorage();
	private UtilityStorage utilityStorage = new UtilityStorage();
	private MysticalSphereStorage mysticalSphereStorage = new MysticalSphereStorage();
	private VialStorage vialStorage = new VialStorage();
	private TropoGemStorage tropoGemStorage = new TropoGemStorage();
	private DevaOrbStorage devaOrbStorage = new DevaOrbStorage();
	private Player owner;


	public Inventory(Player owner) {
		knapsack = GameStorage.getInstance().getKnapsackDefinition("traveller");
		equipmentStorage = new EquipmentStorage();
		this.owner = owner;
	}

	@Override
	public BagOfItems dropItems(Collection<String> items){
		BagOfItems bagToDrop = new BagOfItems();
		for(String currentID : items){
			IDroppable currentItem = droppableStorage.getDroppableItem(currentID);
			if(currentItem != null){
				currentWeight -= currentItem.getItemDefinition().getWeight();
				currentItem.drop(bagToDrop, this);
			}
		}
		return bagToDrop;
	}

	@Override
	public DroppableStorage getDroppableStorage(){
		return droppableStorage;
	}

	@Override
	public EquipmentStorage getEquipmentStorage(){
		return equipmentStorage;
	}

	@Override
	public KeyStorage getKeyStorage(){
		return keyStorage;
	}

	@Override
	public UtilityStorage getUtilityStorage(){
		return utilityStorage;
	}

	@Override
	public MysticalSphereStorage getMysticalSphereStorage(){
		return mysticalSphereStorage;
	}

	@Override
	public VialStorage getVialStorage(){
		return vialStorage;
	}

	@Override
	public TropoGemStorage getTropoGemStorage(){
		return tropoGemStorage;
	}

	@Override
	public DevaOrbStorage getDevaOrbStorage(){
		return devaOrbStorage;
	}

	@Override
	public boolean containsItem(String name){
		return idStorage.containsName(name);
	}

	@Override
	public IKnapsackDefinition getKnapsack(){
		return knapsack;
	}

	@Override
	public boolean canPickUpItem(int itemWeight){
		if(currentWeight + itemWeight > knapsack.getMaxWeight()){
			return false;
		}
		return true;
	}

	@Override
	public void switchKnapsack(IKnapsackDefinition knapsack){
		if(knapsack.getMaxWeight() > this.knapsack.getMaxWeight()){
			this.knapsack = knapsack;
		}
		//TODO gestire il caso opposto, migliorare la gestione dello zaino
	}

	@Override
	public int getCurrentWeight(){
		return currentWeight;
	}

	@Override
	public IDStorage getIDStorage(){
		return idStorage;
	}

	@Override
	public boolean isCapacityEnoughToEquipItem(IItem equippedItem, IItem equippingItem){
		int delta = equippedItem.getItemDefinition().getWeight() - equippingItem.getItemDefinition().getWeight();
		if(delta + currentWeight > knapsack.getMaxWeight()){
			return false;
		}
		return true;
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		tempSummary.putData("knapsack", knapsack.getName());
		tempSummary.putData("currentWeight", currentWeight);
		tempSummary.putData("idStorage", idStorage.getSummary());
		tempSummary.putData("equipmentStorage", equipmentStorage.getSummary());
		tempSummary.putData("keyStorage", keyStorage.getSummary());
		tempSummary.putData("utilityStorage", utilityStorage.getSummary());
		tempSummary.putData("mysticalSphereStorage", mysticalSphereStorage.getSummary());
		tempSummary.putData("vialStorage", vialStorage.getSummary());
		tempSummary.putData("tropoGemStorage", tropoGemStorage.getSummary());
		tempSummary.putData("devaOrbStorage", devaOrbStorage.getSummary());
		return tempSummary;
	}

	@Override
	public void addItem(IEquipmentItem item){
		registerToInventory(item);
		droppableStorage.add((IDroppable) item);
		equipmentStorage.add(item);
	}

	@Override
	public void addItem(IKeyItem item){
		registerToInventory(item);
		keyStorage.add(item);
	}

	@Override
	public void addItem(IUtilityItem item){
		registerToInventory(item);
		utilityStorage.add(item);
	}

	@Override
	public void addItem(IMysticalSphere item){
		registerToInventory(item);
		mysticalSphereStorage.add(item);
	}

	@Override
	public void addItem(IVial item){
		registerToInventory(item);
		droppableStorage.add((IDroppable) item);
		vialStorage.add(item);
	}

	@Override
	public void addItem(ITropoGem item){
		registerToInventory(item);
		droppableStorage.add((IDroppable) item);
		tropoGemStorage.add(item);
	}

	@Override
	public void addItem(IDevaOrb item){
		registerToInventory(item);
		droppableStorage.add((IDroppable) item);
		devaOrbStorage.add(item);
	}

	@Override
	public void registerToInventory(IItem item){
		idStorage.add(item);
		currentWeight += item.getItemDefinition().getWeight();
		EventManager.getInstance().itemAdded(item, owner);
	}

	@Override
	public void removeItem(IEquipmentItem item){
		removeFromInventory(item);
		droppableStorage.remove((IDroppable) item);
		equipmentStorage.remove(item);
	}

	@Override
	public void removeItem(IKeyItem item){
		removeFromInventory(item);
		keyStorage.remove(item);
	}

	@Override
	public void removeItem(IUtilityItem item){
		removeFromInventory(item);
		utilityStorage.remove(item);
	}

	@Override
	public void removeItem(IMysticalSphere item){
		removeFromInventory(item);
		mysticalSphereStorage.remove(item);
	}

	@Override
	public void removeItem(IVial item){
		removeFromInventory(item);
		droppableStorage.remove((IDroppable) item);
		vialStorage.remove(item);
	}

	@Override
	public void removeItem(ITropoGem item){
		removeFromInventory(item);
		droppableStorage.remove((IDroppable) item);
		tropoGemStorage.remove(item);
	}

	@Override
	public void removeItem(IDevaOrb item){
		removeFromInventory(item);
		droppableStorage.remove((IDroppable) item);
		devaOrbStorage.remove(item);
	}

	@Override
	public void removeFromInventory(IItem item){
		idStorage.remove(item);
		currentWeight -= item.getItemDefinition().getWeight();
		EventManager.getInstance().itemRemoved(item, owner);
	}
	
	@Override
	public void removeKeyItemFromDefinition(IItemDefinition item) {		
		String firstIDFromSet = idStorage.getFirstIDFromSet(item.getName());		
		IItem removingItem  = idStorage.getItem(firstIDFromSet);
		removingItem.removeFromInventory(this);
		
	}
}
