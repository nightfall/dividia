package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.IKeyItem;
import it.nightfall.dividia.api.item.definitions.IKeyItemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.definitions.KeyItemDefinition;

public class KeyItem extends AbstractItem implements IKeyItem {
	public KeyItem(KeyItemDefinition keyDef){
		super(keyDef);
	}

	@Override
	public void use(Player player){
		//TODO: Remove
	}

	@Override
	public IKeyItemDefinition getItemDefinition(){
		return (IKeyItemDefinition) super.getItemDefinition();
	}

	@Override
	public void addToInventory(IGenericInventory inventory){
		inventory.addItem(this);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
	
	@Override
	public void removeFromInventory(IGenericInventory inventory) {
		inventory.removeItem(this);
	}
}
