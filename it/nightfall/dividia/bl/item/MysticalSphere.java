package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.definitions.IMysticalSphereDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;

/**
 * This class represent a mystical sphere. A mystical sphere is a magic item
 * that allows the player to perform a spell decreasing mystical sphere's vials,
 * instead of player's mp.
 *
 * @author Nightfall
 *
 */
public class MysticalSphere extends AbstractItem implements IMysticalSphere {
	private int currentMaxVialsCapacity;
	private int currentVials;

	public MysticalSphere(IMysticalSphereDefinition mysticalSphereDef){
		super(mysticalSphereDef);
	}

	@Override
	public int getCurrentMaxVialsCapacity(){
		return currentMaxVialsCapacity;
	}

	@Override
	public void incrementMaxVials(){
		if(currentMaxVialsCapacity != getTotalVialsCapacity()){
			currentMaxVialsCapacity += 1;
		}
	}

	@Override
	public int getTotalVialsCapacity(){
		return getItemDefinition().getTotalVialsCapacity();
	}

	@Override
	public int getEquipDelayTime(){
		return getItemDefinition().getEquipDelayTime();
	}

	@Override
	public int getCurrentVials(){
		return currentVials;
	}

	@Override
	public void fullRecharge(){
		currentVials = currentMaxVialsCapacity;
	}

	@Override
	public void rechargeOf(int vialsNum){
		currentVials += vialsNum;
		if(vialsNum > currentMaxVialsCapacity){
			currentVials = currentMaxVialsCapacity;
		}
	}

	@Override
	public void use(Player player){
		player.equipMysticalSphere(this);
	}

	@Override
	public IMysticalSphereDefinition getItemDefinition(){
		return (IMysticalSphereDefinition) super.getItemDefinition();
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = super.getSummary();
		tempSummary.putData("currentMaxVialsCapacity", currentMaxVialsCapacity);
		tempSummary.putData("currentVials", currentVials);
		return tempSummary;
	}

	@Override
	public void addToInventory(IGenericInventory inventory){
		inventory.addItem(this);
	}
	
	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
		currentMaxVialsCapacity = summary.getInteger("currentMaxVialsCapacity");
		currentVials = summary.getInteger("currentVials");
	}
	
	@Override
	public void removeFromInventory(IGenericInventory inventory) {
		inventory.removeItem(this);
	}
}
