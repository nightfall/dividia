package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;

public class SortableAdapter implements ISortableItem {
	@Override
	public String getId(){
		return "";
	}

	@Override
	public String getName(){
		return "";
	}

	@Override
	public String getType(){
		return "";
	}

	@Override
	public int getPrice(){
		return 0;
	}

	@Override
	public int getWeight(){
		return 0;
	}

	@Override
	public int getQuantity(){
		return 0;
	}

	@Override
	public IItemDefinition getItemDefinition(){
		return null;
	}
}
