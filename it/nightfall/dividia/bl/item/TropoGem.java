package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.definitions.ITropoGemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;

public class TropoGem extends AbstractRemovableItem implements ITropoGem {
	public TropoGem(ITropoGemDefinition tropoGemDefinition){
		super(tropoGemDefinition);
	}

	@Override
	public ITropoGemDefinition getItemDefinition(){
		return (ITropoGemDefinition) super.getItemDefinition();
	}

	@Override
	public void use(Player player){
		EquippedItems currentPlayerEquip = player.getEquippedItems();
		switch(getItemDefinition().getType()){
			case ARMOR:
				currentPlayerEquip.getArmor().setTropoGem(this);
				break;
			case WEAPON:
				currentPlayerEquip.getWeapon().setTropoGem(this);
				break;
			case SHIELD:
				currentPlayerEquip.getShield().setTropoGem(this);
				break;
			case NECKLACE:
				currentPlayerEquip.getNecklace().setTropoGem(this);
				break;
			case HELMET:
				currentPlayerEquip.getHelmet().setTropoGem(this);
				break;
			case RING:
				currentPlayerEquip.getRing().setTropoGem(this);
				break;
			case GLOVES:
				currentPlayerEquip.getGloves().setTropoGem(this);
				break;
			case BOOTS:
				currentPlayerEquip.getBoots().setTropoGem(this);
				break;
		}
	}

	@Override
	public void addToInventory(IGenericInventory inventory){
		inventory.addItem(this);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
	
	@Override
	public void removeFromInventory(IGenericInventory inventory) {
		inventory.removeItem(this);
	}
}
