package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.ITropoGemsBag;
import it.nightfall.dividia.api.item.definitions.ITropoGemDefinition;

public class TropoGemsBag extends Bag<ITropoGemDefinition> implements ITropoGemsBag {
	public TropoGemsBag(ITropoGemDefinition info){
		super(info);
	}

	@Override
	public ITropoGemDefinition getTropoGem(){
		return (ITropoGemDefinition) super.getItemDefinition();
	}

	@Override
	public ITropoGem getItem(String id){
		return (ITropoGem) items.get(id);
	}
}
