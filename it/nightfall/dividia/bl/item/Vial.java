package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.IVial;
import it.nightfall.dividia.api.item.definitions.IVialDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.definitions.VialDefinition;

public class Vial extends AbstractRemovableItem implements IVial {
	public Vial(VialDefinition vialDefinition){
		super(vialDefinition);
	}

	@Override
	public void use(Player player){
		//TODO: player.getInventory().findMysticalSphere(getAssociatedMysticalSphere()).incrementMaxVials();
		//TODO: Remove
	}

	@Override
	public String getAssociatedMysticalSphere(){
		return getItemDefinition().getAssociatedMysticalSphere();
	}

	@Override
	public IVialDefinition getItemDefinition(){
		return (IVialDefinition) super.getItemDefinition();
	}

	@Override
	public void addToInventory(IGenericInventory inventory){
		inventory.addItem(this);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
	
	@Override
	public void removeFromInventory(IGenericInventory inventory) {
		inventory.removeItem(this);
	}
}
