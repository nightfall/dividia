package it.nightfall.dividia.bl.item;

import it.nightfall.dividia.api.item.IVial;
import it.nightfall.dividia.api.item.IVialsBag;
import it.nightfall.dividia.api.item.definitions.IVialDefinition;

public class VialsBag extends Bag<IVialDefinition> implements IVialsBag {
	public VialsBag(IVialDefinition vial){
		super(vial);
	}

	@Override
	public IVialDefinition getVial(){
		return (IVialDefinition) super.getItemDefinition();
	}

	@Override
	public IVial getItem(String id){
		return (IVial) items.get(id);
	}
}
