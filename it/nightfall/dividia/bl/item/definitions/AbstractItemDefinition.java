package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;

/**
 * This class represent a game item definition.
 *
 * @author Nightfall
 *
 */
public abstract class AbstractItemDefinition implements IItemDefinition {
	protected final String name;
	protected final String displayName;
	protected final int weight;
	protected final String description;

	public AbstractItemDefinition(IConfigReader configReader){
		name = configReader.getString("name");
		displayName = configReader.getString("displayName");
		weight = configReader.getInteger("weight");
		description = configReader.getString("description");
	}

	@Override
	public int getWeight(){
		return weight;
	}

	@Override
	public String getName(){
		return this.name;
	}

	@Override
	public String getDescription(){
		return description;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}

	@Override
	public abstract IItem getItemFromDefinition();
}
