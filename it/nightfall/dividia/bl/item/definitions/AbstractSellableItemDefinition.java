package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.item.definitions.ISellableItemDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;

/**
 * This class represent a general game item, that can be sold.
 *
 * @author Nightfall
 *
 */
public abstract class AbstractSellableItemDefinition extends AbstractItemDefinition implements ISellableItemDefinition {
	private final int price;
	private final double devalueRate;

	public AbstractSellableItemDefinition(IConfigReader configReader){
		super(configReader);
		price = configReader.getInteger("price");
		devalueRate = configReader.getDouble("devalueRate");
	}

	@Override
	public int devaluePrice(){
		return (int) (this.price - ((this.price * devalueRate) / 100));
	}

	@Override
	public int getPrice(){
		return price;
	}

	@Override
	public double getDevalueRate(){
		return this.devalueRate;
	}
}
