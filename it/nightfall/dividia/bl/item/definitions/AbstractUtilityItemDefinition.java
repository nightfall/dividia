package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.item.IUtilityItem;
import it.nightfall.dividia.api.item.definitions.IUtilityItemDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;

/**
 * This class represent a generic utility item.
 * 
 * @author Nightfall
 */

public abstract class AbstractUtilityItemDefinition extends AbstractItemDefinition implements IUtilityItemDefinition {

	public AbstractUtilityItemDefinition (IConfigReader configReader){
		super(configReader);
	}
	@Override
	abstract public IUtilityItem getItemFromDefinition();
}
