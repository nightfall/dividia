package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.definitions.IDevaOrbDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.battle.Element;
import it.nightfall.dividia.bl.item.DevaOrb;



/**
 * This class represent a Deva orb definition. 
 * @author Nightfall
 *
 */
public class DevaOrbDefinition  extends AbstractSellableItemDefinition implements IDevaOrbDefinition {

	private final IElement element;

	public DevaOrbDefinition(IConfigReader configReader) {
		super(configReader);
		element = Element.valueOf(configReader.getString("element"));
	}
	
	@Override
	public IElement getElement() {
		return element;
	}


	@Override
	public IDevaOrb getItemFromDefinition() {
		return new DevaOrb(this);
	}

}
