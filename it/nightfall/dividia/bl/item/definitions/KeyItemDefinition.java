package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.item.IKeyItem;
import it.nightfall.dividia.api.item.definitions.IKeyItemDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.KeyItem;

public class KeyItemDefinition extends AbstractItemDefinition implements IKeyItemDefinition {

	public KeyItemDefinition (IConfigReader configReader){
		super(configReader);
	}
	@Override
	public IKeyItem getItemFromDefinition() {
		return new KeyItem(this);
	}

}
