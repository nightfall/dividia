/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.item.definitions.IKnapsackDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;

/**
 * This class represent a knapsack.
 *
 * @author Nightfall
 */
public class KnapsackDefinition implements IKnapsackDefinition {
	private String name;
	private String displayName;
	private int maxWeight;
	
	public KnapsackDefinition(IConfigReader configReader){
		this.name = configReader.getString("name");
		this.displayName = configReader.getString("displayName");
		this.maxWeight = configReader.getInteger("maxWeight");
	}

	@Override
	public int getMaxWeight(){
		return maxWeight;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}
}
