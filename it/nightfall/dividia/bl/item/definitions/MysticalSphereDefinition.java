package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.definitions.IMysticalSphereDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.MysticalSphere;

/**
 * This class represent a mystical sphere definition.
 *
 * @author Nightfall
 *
 */
public class MysticalSphereDefinition extends AbstractItemDefinition implements IMysticalSphereDefinition {
	private final int totalVialsCapacity;
	private final int equipDelayTime;

	public MysticalSphereDefinition(IConfigReader configReader){
		super(configReader);
		totalVialsCapacity = configReader.getInteger("totalVialsCapacity");
		equipDelayTime = configReader.getInteger("equipDelayTime");
	}

	@Override
	public int getTotalVialsCapacity(){
		return totalVialsCapacity;
	}

	@Override
	public int getEquipDelayTime(){
		return equipDelayTime;
	}

	@Override
	public IMysticalSphere getItemFromDefinition(){
		return new MysticalSphere(this);
	}
}
