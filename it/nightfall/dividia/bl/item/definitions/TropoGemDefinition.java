package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.definitions.ITropoGemDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.item.TropoGem;
import it.nightfall.dividia.bl.item.equipment_items.EquipmentItemType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TropoGemDefinition extends AbstractSellableItemDefinition implements ITropoGemDefinition{

	private final Map <IStatusBattleCharacteristic, Integer> statsAlterationsMap;
	private final EquipmentItemType type; 

	
	public TropoGemDefinition(IConfigReader configReader) {
		super(configReader);
		statsAlterationsMap = new HashMap <>();
		Collection<IConfigReader> configReaders = new ArrayList<>(configReader.getConfigReader("statsAlterations").getConfigReaderList("statsAlteration"));
		for (IConfigReader currentConfigReader : configReaders){
			String statusCharacteristicName = configReader.getString("statusCharacteristic");
			Integer value = configReader.getInteger("alteration");
			statsAlterationsMap.put(StatusBattleCharacteristic.valueOf(statusCharacteristicName), value);
		}
		type = EquipmentItemType.valueOf(configReader.getString("equipmentItemType"));
	}

	@Override
	public ITropoGem getItemFromDefinition() {
		return new TropoGem(this);
	}

	@Override
	public EquipmentItemType getType() {
		return type;
	}

	@Override
	public Map<IStatusBattleCharacteristic, Integer> getStatsAlterationsMap() {
		return statsAlterationsMap;
	}

}
