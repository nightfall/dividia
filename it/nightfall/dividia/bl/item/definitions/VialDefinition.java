package it.nightfall.dividia.bl.item.definitions;

import it.nightfall.dividia.api.item.IVial;
import it.nightfall.dividia.api.item.definitions.IVialDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.Vial;
/**
 * This class represent a vial definition. A vial is the energy used by mystical sphere to cast spells.
 * @author Nightfall
 *
 */
public class VialDefinition extends AbstractSellableItemDefinition implements
		IVialDefinition {

	private final String associatedMysticalSphere;

	public VialDefinition (IConfigReader configReader){
		super(configReader);
		associatedMysticalSphere = configReader.getString("associatedMysticalSphere");
	}
	@Override
	public String getAssociatedMysticalSphere() {
		return associatedMysticalSphere;
	}

	@Override
	public IVial getItemFromDefinition() {
		return new Vial(this);
	}

}
