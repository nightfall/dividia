package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.definitions.ITropoGemDefinition;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.item.equipment_items.definitions.IEquipmentItemDefinition;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.AbstractRemovableItem;
import it.nightfall.dividia.bl.item.equipment_items.definitions.AbstractEquipmentItemDefinition;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractEquipmentItem extends AbstractRemovableItem implements IEquipmentItem {
	private IEquipmentItemDefinition equipmentItemDefinition;
	protected HashMap<IStatusBattleCharacteristic, IModifiableInteger> finalStatsAlteration;
	protected ITropoGem tropoGem;

	public AbstractEquipmentItem(AbstractEquipmentItemDefinition equipmentItemDef){
		super(equipmentItemDef);
		this.equipmentItemDefinition = equipmentItemDef;
		this.finalStatsAlteration = equipmentItemDef.getStatsAlterations();
		tropoGem = null;
	}

	@Override
	public Map<IStatusBattleCharacteristic, IModifiableInteger> getFinalStatsAlterations(){
		return finalStatsAlteration;
	}

	@Override
	public void upgrade(IEquipmentItemDefinition newDefinition){
		equipmentItemDefinition = newDefinition;
	}

	@Override
	public ITropoGem getTropoGem(){
		return tropoGem;
	}

	@Override
	public int getLevelRequirement(){
		return equipmentItemDefinition.getLevelRequirement();
	}

	/**
	 * This method calls equipItem method of the specified player, passing the specific Equipment Item to it.
	 *
	 * @param player : Player who equips the item.
	 */
	@Override
	public abstract void use(Player player);

	@Override
	public IEquipmentItemDefinition getItemDefinition(){
		return equipmentItemDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		for(IStatusBattleCharacteristic c : tropoGem.getItemDefinition().getStatsAlterationsMap().keySet()){
			finalStatsAlteration.get(c).addInt(tropoGem.getItemDefinition().getStatsAlterationsMap().get(c));
			//TODO controllare possibili errori 2 
		}
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = super.getSummary();
		//HashSet<Integer> finalStatsAlterationHashSet = new HashSet<>();
		//tempSummary.putData("finalStatsAlteration", finalStatsAlteration);
		if(tropoGem != null){
			tempSummary.putData("tropoGem", tropoGem.getSummary());
		}
		return tempSummary;
	}

	@Override
	public void addToInventory(IGenericInventory inventory){
		inventory.addItem(this);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
		if(summary.dataExists("tropoGem")){
			ISummary tropoGemSummary = summary.getSummary("tropoGem");
			ITropoGemDefinition tropoGemDefinition = (ITropoGemDefinition) GameStorage.getInstance().getItemDefinition(tropoGemSummary.getString("name"));
			tropoGem = tropoGemDefinition.getItemFromDefinition();
			tropoGem.buildFromSummary(tropoGemSummary);
			setTropoGem(tropoGem);
		}
	}
	
	@Override
	public void removeFromInventory(IGenericInventory inventory) {
		inventory.removeItem(this);
	}
}
