package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.definitions.IArmorDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.ArmorDefinition;

public class Armor extends AbstractEquipmentItem implements IArmor {
	private IArmorDefinition armorDefinition;

	public Armor(ArmorDefinition armorDef){
		super(armorDef);
		this.armorDefinition = armorDef;
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public IArmorDefinition getItemDefinition(){
		return armorDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
}
