package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.definitions.IBootsDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.BootsDefinition;

public class Boots extends AbstractEquipmentItem implements IBoots {
	private IBootsDefinition bootsDefinition;

	public Boots(BootsDefinition bootsDef){
		super(bootsDef);
		this.bootsDefinition = bootsDef;
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public IBootsDefinition getItemDefinition(){
		return bootsDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
}
