package it.nightfall.dividia.bl.item.equipment_items;

public enum EquipmentItemType {

	ARMOR ("Armor"),
	NECKLACE ("Necklace"),
	SHIELD ("Shield"),
	WEAPON ("Weapon"),
	RING ("Ring"),
	GLOVES ("Gloves"),
	BOOTS ("Boots"),
	HELMET ("Helmet");
	
	private String string;
	
	private EquipmentItemType (String string){
		this.string = string;
	}
	
	@Override
	public String toString() {
		return string;
	}
}
