package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.definitions.IGlovesDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.GlovesDefinition;

public class Gloves extends AbstractEquipmentItem implements IGloves {
	private IGlovesDefinition glovesDefinition;

	public Gloves(GlovesDefinition glovesDef){
		super(glovesDef);
		this.glovesDefinition = glovesDef;
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public IGlovesDefinition getItemDefinition(){
		return glovesDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
	
}
