package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.definitions.IHelmetDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.HelmetDefinition;

public class Helmet extends AbstractEquipmentItem implements IHelmet {
	private IHelmetDefinition helmetDefinition;

	public Helmet(HelmetDefinition helmetDef){
		super(helmetDef);
		this.helmetDefinition = helmetDef;
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public IHelmetDefinition getItemDefinition(){
		return helmetDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
}
