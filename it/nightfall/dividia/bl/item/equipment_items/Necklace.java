package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.definitions.INecklaceDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.NecklaceDefinition;

public class Necklace extends AbstractEquipmentItem implements INecklace {
	private INecklaceDefinition necklaceDefinition;

	public Necklace(NecklaceDefinition necklaceDef){
		super(necklaceDef);
		this.necklaceDefinition = necklaceDef;
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public INecklaceDefinition getItemDefinition(){
		return necklaceDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
}
