package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.definitions.IRingDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.RingDefinition;

public class Ring extends AbstractEquipmentItem implements IRing {
	private IRingDefinition ringDefinition;

	public Ring(RingDefinition ringDef){
		super(ringDef);
		this.ringDefinition = ringDef;
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public IRingDefinition getItemDefinition(){
		return ringDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
}
