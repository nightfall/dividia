package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.definitions.IShieldDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.ShieldDefinition;

public class Shield extends AbstractEquipmentItem implements IShield {
	private IShieldDefinition shieldDefinition;

	public Shield(ShieldDefinition shieldDef){
		super(shieldDef);
		this.shieldDefinition = shieldDef;
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public IShieldDefinition getItemDefinition(){
		return shieldDefinition;
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
	}
}
