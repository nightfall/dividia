package it.nightfall.dividia.bl.item.equipment_items;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.definitions.IDevaOrbDefinition;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.item.equipment_items.definitions.IWeaponDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.item.equipment_items.definitions.WeaponDefinition;
import it.nightfall.dividia.bl.utility.AlteredStatusSuccessRate;
import it.nightfall.dividia.bl.utility.GameArea;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.util.List;

public class Weapon extends AbstractEquipmentItem implements IWeapon {
	private IWeaponDefinition weaponDefinition;
	private IDevaOrb devaOrb;

	public Weapon(WeaponDefinition weapon){
		super(weapon);
		this.weaponDefinition = weapon;
		devaOrb = null;
	}

	public IDevaOrb getDevaOrb(){
		return devaOrb;
	}

	@Override
	public GameArea getAttackRange(){
		return weaponDefinition.getAttackRange();
	}

	@Override
	public List<AlteredStatusSuccessRate> getWeaponAlteredStatuses(){
		return weaponDefinition.getWeaponAlteredStatuses();
	}

	@Override
	public IElement getWeaponElement(){
		return weaponDefinition.getWeaponElement();
	}

	@Override
	public List<IAlteredStatusDefinition> getSuccededAlteredStatuses(){
		return SharedFunctions.getSuccededAlteredStatuses(weaponDefinition.getWeaponAlteredStatuses());
	}

	@Override
	public void use(Player player){
		player.equipItem(this);
	}

	@Override
	public IWeaponDefinition getItemDefinition(){
		return weaponDefinition;
	}

	@Override
	public void devaOrbFusion(IDevaOrb devaOrb){
		if(!(this.devaOrb == null)){
			//TODO evento non puoi piu' cambiare la deva orb da questa arma, esci
		}
		this.devaOrb = devaOrb;
	}

	/*
	 * Save - Load methods
	 * 
	 */
	@Override
	public void loadState(IConfigReader saveState){
		if(saveState.doesElementExist("devaOrb")){
			IConfigReader configReader = saveState.getConfigReader("devaOrb");
			String devaOrbName = configReader.getString("name");
			IDevaOrbDefinition devaOrbDef = (IDevaOrbDefinition) GameStorage.getInstance().getItemDefinition(devaOrbName);
			devaOrb = devaOrbDef.getItemFromDefinition();
		}
		//devaOrb.loadState(configReader);
		//TODO: DevaOrb LoadState
		//TODO: Tropogem?!
	}

	@Override
	public void setTropoGem(ITropoGem tropoGem){
		this.tropoGem = tropoGem;
		super.setTropoGem(tropoGem);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = super.getSummary();
		if(devaOrb != null){
			tempSummary.putData("devaOrb", devaOrb.getSummary());
		}
		return tempSummary;
	}

	@Override
	public void buildFromSummary(ISummary summary){
		super.buildFromSummary(summary);
		if(summary.dataExists("devaOrb")){
			ISummary devaOrbSummary = summary.getSummary("devaOrb");
			IDevaOrbDefinition devaOrbDefinition = (IDevaOrbDefinition) GameStorage.getInstance().getItemDefinition(devaOrbSummary.getString("name"));
			devaOrb = devaOrbDefinition.getItemFromDefinition();
			devaOrb.buildFromSummary(devaOrbSummary);
		}
	}
}
