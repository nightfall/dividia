package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.item.equipment_items.definitions.IEquipmentItemDefinition;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.battle.Element;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.item.definitions.AbstractSellableItemDefinition;
import it.nightfall.dividia.bl.utility.ModifiableInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class represent an item that can be equipped.
 *
 * @author Nightfall
 *
 */
public abstract class AbstractEquipmentItemDefinition extends AbstractSellableItemDefinition implements IEquipmentItemDefinition {
	protected final int levelRequirement;
	protected final Set<String> userRaces;
	protected final Map<IElement, IModifiableInteger> elementResistancesAlteration;
	protected final HashMap<IStatusBattleCharacteristic, IModifiableInteger> statsAlterations;

	public AbstractEquipmentItemDefinition(IConfigReader configReader){
		super(configReader);
		levelRequirement = configReader.getInteger("levelRequirement");
		userRaces = new HashSet<>(configReader.getConfigReader("userRaces").getStringList("userRace"));
		elementResistancesAlteration = new HashMap<>();
		statsAlterations = new HashMap<>();

		/*
		 * Reading elementResistencesAlteration
		 */
		Collection<IConfigReader> configReaders = configReader.getConfigReader("elementResistancesAlteration").getConfigReaderList("elementResestanceAlteration");
		for(IConfigReader currentConfigReaderElement : configReaders){
			String elementName = currentConfigReaderElement.getString("element"); //TODO è corretto usare il current, si?, 4 sostituzioni effettuate
			IModifiableInteger resistance = new ModifiableInteger(currentConfigReaderElement.getInteger("resistance"));
			elementResistancesAlteration.put(Element.valueOf(elementName), resistance);
		}
		/*
		 * Reading statsAlterations
		 */
		configReaders = configReader.getConfigReader("statsAlterations").getConfigReaderList("statAlteration");
		for(IConfigReader currentConfigReaderStats : configReaders){
			String statusCharacteristicName = currentConfigReaderStats.getString("characteristic");
			IModifiableInteger amount = new ModifiableInteger(currentConfigReaderStats.getInteger("alteration"));
			statsAlterations.put(StatusBattleCharacteristic.valueOf(statusCharacteristicName), amount);
		}
	}

	public void upgrade(String filePath){
		// TODO read from file stats to modify
	}

	@Override
	public int getLevelRequirement(){
		return levelRequirement;
	}

	@Override
	public HashMap<IStatusBattleCharacteristic, IModifiableInteger> getStatsAlterations(){
		return statsAlterations;
	}

	@Override
	public Map<IElement, IModifiableInteger> getElementResistancesAlteration(){
		return elementResistancesAlteration;
	}

	@Override
	public Set<String> getUserRaces(){
		return userRaces;
	}

	@Override
	public abstract IEquipmentItem getItemFromDefinition();
}
