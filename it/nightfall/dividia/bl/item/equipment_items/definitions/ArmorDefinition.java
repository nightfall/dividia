package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.definitions.IArmorDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.equipment_items.Armor;

/**
 * This class represent an armor definition. An armor is a defence item that can
 * alterate some player's stats.
 *
 * @author Nightfall
 *
 */
public class ArmorDefinition extends AbstractEquipmentItemDefinition implements IArmorDefinition {
	public ArmorDefinition(IConfigReader configReader){
		super(configReader);

	}

	@Override
	public IArmor getItemFromDefinition(){
		return new Armor(this);
	}
}
