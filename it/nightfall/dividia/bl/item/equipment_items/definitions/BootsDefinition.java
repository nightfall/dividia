package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.definitions.IBootsDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.equipment_items.Boots;

/**
 * This class represent a boots definition. Boots are a kind of item that can
 * alterate some player's stats.
 *
 * @author Nightfall
 *
 */
public class BootsDefinition extends AbstractEquipmentItemDefinition implements IBootsDefinition {
	public BootsDefinition(IConfigReader configReader){
		super(configReader);
	}

	@Override
	public IBoots getItemFromDefinition(){
		return new Boots(this);
	}
}
