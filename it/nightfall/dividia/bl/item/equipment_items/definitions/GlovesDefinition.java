package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.definitions.IGlovesDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.equipment_items.Gloves;

/**
 * This class represent a gloves definition. Gloves are a kind of item that can
 * alterate some player's stats.
 *
 * @author Nightfall
 *
 */
public class GlovesDefinition extends AbstractEquipmentItemDefinition implements IGlovesDefinition {
	public GlovesDefinition(IConfigReader configReader){
		super(configReader);
	}

	@Override
	public IGloves getItemFromDefinition(){
		return new Gloves(this);
	}
}
