package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.definitions.IHelmetDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.equipment_items.Helmet;

/**
 * This class represent a helmet definition. A helmet is a defence item that can
 * alterate some player's stats.
 *
 * @author Nightfall
 *
 */
public class HelmetDefinition extends AbstractEquipmentItemDefinition implements IHelmetDefinition {
	public HelmetDefinition(IConfigReader configReader){
		super(configReader);
	}

	@Override
	public IHelmet getItemFromDefinition(){
		return new Helmet(this);
	}
}
