package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.definitions.INecklaceDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.equipment_items.Necklace;

/**
 * This class represent a necklace definition. A necklace is a kind of item that
 * can alterate some player's stats.
 *
 * @author Nightfall
 *
 */
public class NecklaceDefinition extends AbstractEquipmentItemDefinition implements INecklaceDefinition {
	public NecklaceDefinition(IConfigReader configReader){
		super(configReader);
	}

	@Override
	public INecklace getItemFromDefinition(){
		return new Necklace(this);
	}
}
