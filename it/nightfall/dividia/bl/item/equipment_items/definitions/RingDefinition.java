package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.definitions.IRingDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.equipment_items.Ring;

/**
 * This class represent a ring definition. A ring is a kind of item that can
 * alterate some player's stats.
 *
 * @author Nightfall
 *
 */
public class RingDefinition extends AbstractEquipmentItemDefinition implements IRingDefinition {
	public RingDefinition(IConfigReader configReader){
		super(configReader);
	}

	@Override
	public IRing getItemFromDefinition(){
		return new Ring(this);
	}
}
