package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.definitions.IShieldDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.item.equipment_items.Shield;

/**
 * This class represent a shield definition. A shield is a defence item that can
 * alterate some player's stats.
 *
 * @author Nightfall
 *
 */
public class ShieldDefinition extends AbstractEquipmentItemDefinition implements IShieldDefinition {
	public ShieldDefinition(IConfigReader configReader){
		super(configReader);
	}

	@Override
	public IShield getItemFromDefinition(){
		return new Shield(this);
	}
}
