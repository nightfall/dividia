package it.nightfall.dividia.bl.item.equipment_items.definitions;

import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.item.equipment_items.definitions.IWeaponDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.battle.Element;
import it.nightfall.dividia.bl.item.equipment_items.Weapon;
import it.nightfall.dividia.bl.utility.AlteredStatusSuccessRate;
import it.nightfall.dividia.bl.utility.GameArea;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class represent a weapon definition. A weapon is an attack item that can alterate some player's stats and it is used by player for performing the basic attack.
 *
 * @author Nightfall
 *
 */
public class WeaponDefinition extends AbstractEquipmentItemDefinition implements IWeaponDefinition {
	private final GameArea attackRange;
	private final List<AlteredStatusSuccessRate> weaponAlteredStatuses;
	private final IElement weaponElement;

	public WeaponDefinition(IConfigReader configReader){
		super(configReader);
		weaponElement = Element.valueOf(configReader.getString("weaponElement"));
		Collection<IConfigReader> configs = configReader.getConfigReader("weaponAlteredStatusSuccessRates").getConfigReaderList("weaponAlteredStatusSuccessRate");
		weaponAlteredStatuses = new ArrayList<>();
		for(IConfigReader currentConfigReader : configs){
			weaponAlteredStatuses.add(new AlteredStatusSuccessRate(currentConfigReader));
		}
		attackRange = new GameArea(configReader.getConfigReader("attackRange"));
	}

	@Override
	public GameArea getAttackRange(){
		return attackRange;
	}

	@Override
	public List<AlteredStatusSuccessRate> getWeaponAlteredStatuses(){
		return weaponAlteredStatuses;
	}

	@Override
	public IElement getWeaponElement(){
		return weaponElement;
	}

	@Override
	public List<IAlteredStatusDefinition> getSuccededAlteredStatuses(){
		return SharedFunctions.getSuccededAlteredStatuses(this.weaponAlteredStatuses);
	}

	@Override
	public IWeapon getItemFromDefinition(){
		return new Weapon(this);
	}
}
