package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.IDevaOrbsBag;
import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.DevaOrbsBag;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class DevaOrbStorage implements IStorage<IDevaOrb> {
	private HashMap<String, IDevaOrbsBag> mainStorage = new HashMap<>();

	@Override
	public void add(IDevaOrb item){
		String devaOrbName = item.getItemDefinition().getName();
		IDevaOrbsBag bag = mainStorage.get(devaOrbName);
		if(bag != null){
			bag.incrementQuantity();
		}else{
			bag = new DevaOrbsBag(item.getItemDefinition());
			mainStorage.put(devaOrbName, bag);
		}
		bag.addItem(item);
	}

	@Override
	public void remove(IDevaOrb item){
		String devaOrbName = item.getItemDefinition().getName();
		IDevaOrbsBag bag = mainStorage.get(devaOrbName);
		bag.removeItem(item.getId());
		if(bag.getQuantity() == 1){
			mainStorage.remove(devaOrbName);
		}else{
			bag.decrementQuantity();
		}
	}

	@Override
	public boolean contains(IDevaOrb obj){
		return mainStorage.containsKey(obj.getItemDefinition().getName());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public String toString(){
		return mainStorage.toString();
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, IDevaOrbsBag> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey(), data.getValue().getSummary());
		}
		return tempSummary;
	}
	
	public IDevaOrbsBag getBag(String name){
		return mainStorage.get(name);
	}
}
