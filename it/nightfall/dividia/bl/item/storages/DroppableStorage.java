package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IDroppable;
import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class DroppableStorage implements IStorage<IDroppable> {
	private HashMap<String, IDroppable> mainStorage = new HashMap<>();

	@Override
	public void add(IDroppable item){
		mainStorage.put(item.getId(), item);
	}

	@Override
	public void remove(IDroppable item){
		mainStorage.remove(item.getId());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public boolean contains(IDroppable item){
		return mainStorage.containsValue(item);
	}

	public HashMap<String, IDroppable> getMainStorage(){
		return mainStorage;
	}

	public IDroppable getDroppableItem(String id){
		return mainStorage.get(id);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, IDroppable> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey(), data.getValue().getSummary());
		}
		return tempSummary;
	}
}
