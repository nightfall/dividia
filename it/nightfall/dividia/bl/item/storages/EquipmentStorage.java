package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class EquipmentStorage implements IStorage<IEquipmentItem> {
	private HashMap<String, IEquipmentItem> mainStorage = new HashMap<>();

	@Override
	public void add(IEquipmentItem item){
		mainStorage.put(item.getId(), item);
	}

	@Override
	public void remove(IEquipmentItem item){
		mainStorage.remove(item.getId());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public boolean contains(IEquipmentItem item){
		return mainStorage.containsValue(item);
	}

	public HashMap<String, IEquipmentItem> getMainStorage(){
		return mainStorage;
	}

	public void useItem(IEquipmentItem item){
		assert (mainStorage.containsValue(item));
		//TODO implementare il resto
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, IEquipmentItem> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey(), data.getValue().getSummary());
		}
		return tempSummary;
	}
}