package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IDStorage implements IStorage<IItem> {
	private HashMap<String, HashSet<String>> mainStorage = new HashMap<>();
	private HashMap<String, IItem> itemStorage = new HashMap<>();

	public IDStorage(){
	}

	public IDStorage(ISummary summary){
	}

	@Override
	public void add(IItem item){
		String newItemName = item.getItemDefinition().getName();
		String newItemID = item.getId();
		Set<String> correspondingSet = mainStorage.get(newItemName);
		if(mainStorage.containsKey(newItemName)){
			correspondingSet.add(newItemID);
		}else{
			HashSet<String> newSet = new HashSet<>();
			newSet.add(item.getId());
			mainStorage.put(item.getItemDefinition().getName(), newSet);
		}
		itemStorage.put(item.getId(), item);
	}

	@Override
	public void remove(IItem item){
		String removingItemID = item.getId();
		String removingItemName = item.getItemDefinition().getName();
		Set<String> correspondingList = mainStorage.get(removingItemName);
		assert mainStorage.containsKey(item.getItemDefinition().getName());
		correspondingList.remove(removingItemID);
		if(correspondingList.isEmpty()){
			mainStorage.remove(removingItemName);
		}
		itemStorage.remove(item.getId());
	}

	public boolean containsName(String name){
		return mainStorage.containsKey(name);
	}

	public HashMap<String, HashSet<String>> getCorrelationStorage(){
		return mainStorage;
	}

	public String getFirstIDFromSet(String name){
		Set<String> ids = mainStorage.get(name);
		return ids.iterator().next();
	}

	@Override
	public boolean contains(IItem obj){
		return mainStorage.containsKey(obj.getItemDefinition().getName());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, HashSet<String>> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey().toString(), data.getValue());
		}
		return tempSummary;
	}
	
	public IItem getItem(String id) {
		return itemStorage.get(id);
	}
}
