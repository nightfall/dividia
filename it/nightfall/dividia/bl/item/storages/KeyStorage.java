package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IKeyItem;
import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class KeyStorage implements IStorage<IKeyItem> {
	private HashMap<String, IKeyItem> mainStorage = new HashMap<>();

	@Override
	public void add(IKeyItem item){
		mainStorage.put(item.getId(), item);
	}

	@Override
	public void remove(IKeyItem item){
		mainStorage.remove(item.getId());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public boolean contains(IKeyItem item){
		return mainStorage.containsValue(item);
	}

	public HashMap<String, IKeyItem> getMainStorage(){
		return mainStorage;
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, IKeyItem> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey(), data.getValue().getSummary());
		}
		return tempSummary;
	}
}
