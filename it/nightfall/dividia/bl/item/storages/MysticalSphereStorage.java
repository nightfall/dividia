package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class MysticalSphereStorage implements IStorage<IMysticalSphere> {
	private HashMap<String, IMysticalSphere> mainStorage = new HashMap<>();

	@Override
	public void add(IMysticalSphere item){
		mainStorage.put(item.getId(), item);
	}

	@Override
	public void remove(IMysticalSphere item){
		mainStorage.remove(item.getId());
	}

	public IMysticalSphere getMysticalSphere(String id){
		return mainStorage.get(id);
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public boolean contains(IMysticalSphere item){
		return mainStorage.containsValue(item);
	}

	public HashMap<String, IMysticalSphere> getMainStorage(){
		return mainStorage;
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, IMysticalSphere> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey(), data.getValue().getSummary());
		}
		return tempSummary;
	}
}