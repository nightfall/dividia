package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.ITropoGemsBag;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.TropoGemsBag;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class TropoGemStorage implements IStorage<ITropoGem> {
	private HashMap<String, ITropoGemsBag> mainStorage = new HashMap<>();

	@Override
	public void add(ITropoGem item){
		String tropoGemName = item.getItemDefinition().getName();
		ITropoGemsBag bag = mainStorage.get(tropoGemName);
		if(bag != null){
			bag.incrementQuantity();
		}else{
			bag = new TropoGemsBag(item.getItemDefinition());
			mainStorage.put(tropoGemName, bag);
		}
		bag.addItem(item);
	}

	@Override
	public void remove(ITropoGem item){
		String tropoGemName = item.getItemDefinition().getName();
		ITropoGemsBag bag = mainStorage.get(tropoGemName);
		bag.removeItem(item.getId());
		if(bag.getQuantity() == 1){
			mainStorage.remove(tropoGemName);
		}else{
			bag.decrementQuantity();
		}
	}

	@Override
	public boolean contains(ITropoGem obj){
		return mainStorage.containsKey(obj.getItemDefinition().getName());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, ITropoGemsBag> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey().toString(), data.getValue().getSummary());
		}
		return tempSummary;
	}
	
	public ITropoGemsBag getBag(String name){
		return mainStorage.get(name);
	}
}
