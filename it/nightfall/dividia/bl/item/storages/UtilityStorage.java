package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.item.IUtilityItem;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class UtilityStorage implements IStorage<IUtilityItem> {
	private HashMap<String, IUtilityItem> mainStorage = new HashMap<>();

	@Override
	public void add(IUtilityItem item){
		mainStorage.put(item.getId(), item);
	}

	@Override
	public void remove(IUtilityItem item){
		mainStorage.remove(item.getId());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	public IUtilityItem getUtilityItem(String id){
		return mainStorage.get(id);
	}

	@Override
	public boolean contains(IUtilityItem item){
		return mainStorage.containsValue(item);
	}

	public HashMap<String, IUtilityItem> getMainStorage(){
		return mainStorage;
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, IUtilityItem> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey(), data.getValue().getSummary());
		}
		return tempSummary;
	}
}