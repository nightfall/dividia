package it.nightfall.dividia.bl.item.storages;

import it.nightfall.dividia.api.item.IStorage;
import it.nightfall.dividia.api.item.IVial;
import it.nightfall.dividia.api.item.IVialsBag;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.VialsBag;
import it.nightfall.dividia.bl.utility.Summary;
import java.util.HashMap;
import java.util.Map;

public class VialStorage implements IStorage<IVial> {
	private HashMap<String, IVialsBag> mainStorage = new HashMap<>();

	@Override
	public void add(IVial vial){
		String vialName = vial.getItemDefinition().getName();
		IVialsBag bag = mainStorage.get(vialName);
		if(bag != null){
			bag.incrementQuantity();
		}else{
			bag = new VialsBag(vial.getItemDefinition());
			mainStorage.put(vialName, bag);
		}
		bag.addItem(vial);
	}

	@Override
	public void remove(IVial item){
		String vialName = item.getItemDefinition().getName();
		IVialsBag bag = mainStorage.get(vialName);
		bag.removeItem(item.getId());
		if(bag.getQuantity() == 1){
			mainStorage.remove(vialName);
		}else{
			bag.decrementQuantity();
		}
	}

	public HashMap<String, IVialsBag> getMainStorage(){
		return mainStorage;
	}

	@Override
	public boolean contains(IVial obj){
		return mainStorage.containsKey(obj.getItemDefinition().getName());
	}

	@Override
	public boolean isEmpty(){
		return mainStorage.isEmpty();
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		for(Map.Entry<String, IVialsBag> data : mainStorage.entrySet()){
			tempSummary.putData(data.getKey().toString(), data.getValue().getSummary());
		}
		return tempSummary;
	}

	public IVialsBag getBag(String name){
		return mainStorage.get(name);
	}
}
