package it.nightfall.dividia.bl.network;

import it.nightfall.dividia.api.network.IMessagesDispatcher;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AbstractDispatcher implements IMessagesDispatcher {
	private final BlockingQueue<Integer> pausingQueue = new ArrayBlockingQueue<>(1);
	private AtomicBoolean running = new AtomicBoolean(true);
	private AtomicBoolean paused = new AtomicBoolean(false);

	@Override
	public void stop(){
		running.set(false);
	}

	@Override
	public void pause(){
		if(!paused.get()){
			paused.set(true);

		}
	}

	@Override
	public void resume(){
		try{
			if(paused.get()){
				pausingQueue.put(0);
				paused.set(false);
			}
		}catch(InterruptedException ex){
			ex.printStackTrace();
		}
	}

	protected boolean isRunning(){
		return running.get();
	}

	protected void checkPause() throws InterruptedException{
		if(paused.get()){
			pausingQueue.take();
		}
	}
}
