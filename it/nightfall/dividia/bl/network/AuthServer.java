package it.nightfall.dividia.bl.network;

import it.nightfall.dividia.api.network.IAuthServer;
import it.nightfall.dividia.api.network.IClientEventListener;
import it.nightfall.dividia.api.network.IIntentionManager;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.events.adapters.NetworkEventsAdapter;
import it.nightfall.dividia.bl.network.dcp.LoginServer;
import it.nightfall.dividia.bl.utility.GameManager;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class AuthServer extends UnicastRemoteObject implements IAuthServer {
	private static IAuthServer authServer = null;
	private Map<String, IIntentionManager> loggedUsers = new HashMap<>();

	public static IAuthServer getInstance(){
		if(authServer == null){
			try{
				authServer = new AuthServer();
			}catch(RemoteException ex){
				ex.printStackTrace();
			}
		}
		return authServer;
	}
	
	AuthServer() throws RemoteException{
		EventManager.getInstance().addListener(new NetworkEventsAdapter() {
			@Override
			public void userDisconnected(Player player, String username){
				IIntentionManager intentionManager = loggedUsers.get(username);
				try{
					UnicastRemoteObject.unexportObject(intentionManager, false);
				}catch(NoSuchObjectException ex){
					
				}
				loggedUsers.remove(username);
			}
		});
	}

	@Override
	public void authorize(String username, String oneTimeKey, IClientEventListener clientEventsListener) throws RemoteException{
		if(!(loggedUsers.containsKey(username)) && (LoginServer.getInstance().autheticateSession(username, oneTimeKey))){
			IIntentionManager intentionManager = (IIntentionManager) UnicastRemoteObject.exportObject(new IntentionManager(username), 0);
			EventDispatcher.getInstance().addClient(username, clientEventsListener);
			clientEventsListener.sessionAuthenticated(intentionManager, GameManager.getInstance().getSaveGamesSummary(username));
			loggedUsers.put(username, intentionManager);
		}else{
			clientEventsListener.sessionRefused();
		}
	}
}
