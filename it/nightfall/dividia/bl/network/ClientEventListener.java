package it.nightfall.dividia.bl.network;

import it.nightfall.dividia.api.battle.IDamageSource;
import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.network.IClientEventListener;
import it.nightfall.dividia.api.network.IGUIDispatcher;
import it.nightfall.dividia.api.network.IIntentionManager;
import it.nightfall.dividia.api.network.IReceivedMessage;
import it.nightfall.dividia.api.utility.ICallbackWaiting;
import it.nightfall.dividia.api.utility.IFutureCallback;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.battle.DamageSource;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.utility.FutureCallback;
import it.nightfall.dividia.bl.utility.GameManager;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.gui.controls.LifebarControl;
import it.nightfall.dividia.gui.controls.SpellArcControl;
import it.nightfall.dividia.gui.events.GUIDispatcher;
import it.nightfall.dividia.gui.events.PlayerLevelUpByExperienceEvent;
import it.nightfall.dividia.gui.events.PlayingCharacterDeathHandler;
import it.nightfall.dividia.gui.items.GUIEquippedItems;
import it.nightfall.dividia.gui.items.GUIInventory;
import it.nightfall.dividia.gui.sounds.BackgroundsManager;
import it.nightfall.dividia.gui.sounds.SoundsManager;
import it.nightfall.dividia.gui.sounds.SpellSoundsManager;
import it.nightfall.dividia.gui.spellsAndEffects.EffectsManager;
import it.nightfall.dividia.gui.utility.DamageReceivedEvent;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.GuiStatus;
import it.nightfall.dividia.gui.utility.input.InGameMapping;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.sounds.ICharacterSoundActionsListener;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import it.nightfall.dividia.testing.screencontrollers.DialogScreenController;
import it.nightfall.dividia.testing.screencontrollers.EquipmentItemsController;
import it.nightfall.dividia.testing.screencontrollers.InventoryController;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.LinkedList;

public class ClientEventListener implements IClientEventListener {
	private final String username;
	private final IGUIDispatcher dispatcher = GUIDispatcher.getInstance();
	private final IFutureCallback connectionTester;

	public ClientEventListener(final String username) throws RemoteException{
		this.username = username;
		connectionTester = new FutureCallback(new ICallbackWaiting() {
			@Override
			public boolean execute(){
				try{
					ClientTest.intentionManager.ping();
				}catch(RemoteException ex){
					//TODO: Timeout
				}
				return true;
			}
		});
	}

	@Override
	public void monsterDied(final String monster, final String killer) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				GUIEventsHandler.getInstance().putEvent(new PlayingCharacterDeathHandler(monster));
				LifebarControl.getLifebarControl().hideEntity(monster);
			}
		});
	}

	@Override
	public void playerDied(final String player, final String killer) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				GUIEventsHandler.getInstance().putEvent(new PlayingCharacterDeathHandler(player));
			}
		});
	}

	@Override
	public void itemPicked(final ISummary item) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	/**
	 * Informs the GUI to load a game map.
	 *
	 * @param player player that used teleport
	 * @param nextMap map to load
	 * @throws RemoteException
	 */
	@Override
	public void teleportUsed(final String player, final String nextMap) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				LifebarControl.getLifebarControl().clearEntities();
				SpatialsManager.getInstance().removePlayingCharacterSpatial(player);
				if(player.equals(username)) {
					//dispatcher.pause();
					SpatialsManager.getInstance().loadMapScene(nextMap);
					BackgroundsManager.getInstance().changeMap(nextMap);
					//TODO delete
					//ClientTest.intentionManager.finishLoadingMapScene(nextMap);
				}
			}
		});
	}

	@Override
	public void characterMoved(final String character, final double x, final double y, final double direction) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				SpatialsManager.getInstance().characterMoved(character, x, y, direction);
				/*ICharacterSoundActionsListener characterSoundsManager = SoundsManager.getInstance().getCharacterSoundsManager(character);
				 if(characterSoundsManager!=null) {
				 characterSoundsManager.characterStartedMoving();
				 }*/

				if(character.equals(username)){
					LifebarControl.getLifebarControl().updateCurrentPlayerPosition((float) x, (float) y);
				}else{
					LifebarControl.getLifebarControl().updateEntityPosition(character, (float) x, (float) y);
				}
			}
		});
	}

	@Override
	public void focusMoved(final String character, final double x, final double y, final String focusStatus) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				if(character.equals(username)){
					SpatialsManager.getInstance().focusMoved(character, x, y, focusStatus);
				}
			}
		});
	}

	@Override
	public void characterStopped(final String character) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				SpatialsManager.getInstance().characterStopped(character);
				ICharacterSoundActionsListener characterSoundsManager = SoundsManager.getInstance().getCharacterSoundsManager(character);
				if(characterSoundsManager != null){
					characterSoundsManager.characterStopped();
				}
			}
		});
	}

	@Override
	public void damageReceived(final String affected, final String attacker, final int damage, final IDamageSource source, final String attackId, final IDamageType damageType) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				ICharacterSoundActionsListener characterSoundsManager = SoundsManager.getInstance().getCharacterSoundsManager(attacker);
				if(characterSoundsManager != null && source.equals(DamageSource.BASIC_ATTACK)){
					characterSoundsManager.basicAttackHit();
				}
				if(source.equals(DamageSource.SPELL)){
					SpellSoundsManager.getInstance().spellHitSound(attackId);
				}
				if(affected.equals(username)){
					GUIEventsHandler.getInstance().putEvent(new DamageReceivedEvent(damage, true, damageType));
				}else if(attacker.equals(username)){
					GUIEventsHandler.getInstance().putEvent(new DamageReceivedEvent(damage, false, damageType));
				}
			}
		});
	}

	@Override
	public void questCompleted(final String quest) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	@Override
	public void questActivated(final String quest) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	@Override
	public void goalCompleted(final String goal) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	@Override
	public void goalActivated(final String goal) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	@Override
	public void goalUpdated(final String goal) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	@Override
	public void sessionAuthenticated(final IIntentionManager intentionManager, ISummary savegamesSummary) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				ClientTest.setIntentionManager(intentionManager);
				if(!GameManager.getInstance().isInSinglePlayer()){
					connectionTester.runAndWait(5000); //TODO: Set TIMEOUT
				}
				//start loading scene
				SpatialsManager.getInstance().SpatialsManagerInit(ClientTest.getInstance().getAssetManager(), ClientTest.getInstance().getRootNode(), ClientTest.getInstance().getCamera(), ClientTest.getInstance().getInputManager(), ClientTest.getInstance().getGuiNode(), null); //TODO guiFont
			}
		});
	}

	@Override
	public void sessionRefused() throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	@Override
	public void userDisconnected(final String username) throws RemoteException{
		final boolean isMe = username.equals(this.username);
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				if(isMe){
					GUIDispatcher.getInstance().stop();
					GUIDispatcher.getInstance().addMessage(new IReceivedMessage() {
						@Override
						public void perform() throws RemoteException{
							UnicastRemoteObject.unexportObject(ClientEventListener.this, true);
						}
					});
					GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
						@Override
						public void execute(){
							ClientTest.getInstance().stop();
						}
					});
				}
			}
		});
	}

	@Override
	public void playingCharacterCharacteristicChanged(final String playingCharacter, final String characteristic, final int newValue, final int delta) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				StatusBattleCharacteristic statusCharacteristic = StatusBattleCharacteristic.valueOf(characteristic);
				//Whatever has to be done on every client
				switch(statusCharacteristic) {
					case MOVEMENT_SPEED:
						SpatialsManager.getInstance().characterSpeedChanged(playingCharacter, newValue);
						break;
					case ATTACK_SPEED:
						SpatialsManager.getInstance().characterAttackSpeedChanged(playingCharacter, newValue);
						break;
				}
				//Whatever has to be done only on the corresponding client
				if(playingCharacter.equals(username)){
					switch(statusCharacteristic){
						case MOVEMENT_SPEED:
							SpatialsManager.getInstance().focusSpeedChanged(newValue);
							break;
						case CURRENT_HP:
							LifebarControl.getLifebarControl().updateHealth(delta);
							break;
						case CURRENT_MP:
							LifebarControl.getLifebarControl().updateMana(delta);
							break;
						case CURRENT_RAGE:
							LifebarControl.getLifebarControl().updateRage(delta);
							break;
						case MAX_HP:
							LifebarControl.getLifebarControl().setMaxHealth(newValue);
							break;
						case MAX_MP:
							LifebarControl.getLifebarControl().setMaxMana(newValue);
							break;
						case MAX_RAGE:
							LifebarControl.getLifebarControl().setMaxRage(newValue);
							break;
					}
				}
			}
		});
	}

	@Override
	public void playerCharacteristicChanged(final String player, final String characteristic, final int newValue, final int delta) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
			}
		});
	}

	@Override
	public void playerLevelUp(final String player, final int newLevel, final int maxExperience) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				SpellArcControl.getSpellArcControl().setMaxExperience(maxExperience);
			}
		});
	}

	@Override
	public void playerLevelUpByExperience(final String id, final int newLevel, final int maxExperience) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				GUIEventsHandler.getInstance().putEvent(new PlayerLevelUpByExperienceEvent(id));
			}
		});

	}

	@Override
	public void playerGainExperience(final String player, final int newValue, final int delta) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				SpellArcControl.getSpellArcControl().updateExperience(newValue);
			}
		});
	}

	@Override
	public void receiveMapSummary(final String player, final ISummary mapSummary) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				SpatialsManager.getInstance().setSummary(mapSummary);
			}
		});
	}

	@Override
	public void playerSummaryReceived(String player, ISummary playerSummary) throws RemoteException{
		GUIInventory.initInventory(playerSummary.getSummary("inventory"));
		GUIEquippedItems.getInstance().initFromSummary(playerSummary.getSummary("equippedItems"));
		ISummary spells = playerSummary.getSummary("spells");
		for(int i = 0; i < 4; i++){
			EffectsManager.getInstance().addSpell(spells.getString("" + i));
		}

	}

	@Override
	public void spellOnCooldown(String spell) throws RemoteException{
		EffectsManager.getInstance().goOnCooldown(spell);
		SpellArcControl.getSpellArcControl().spellOnCooldown(spell);
	}

	@Override
	public void launchAreaSpell(String spell, double directionX, double directionY, double castingX, double castingY, String casterId) throws RemoteException{
		EffectsManager.getInstance().launchAreaEffect(spell, directionX, directionY, castingX, castingY, casterId, casterId.equals(username));
		SpellSoundsManager.getInstance().spellCastSound(spell);
	}

	@Override
	public void ping() throws RemoteException{
	}

	@Override
	public void focusReset(final String playerIdM, final double x, final double y) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				if(playerIdM.equals(username)){
					SpatialsManager.getInstance().focusReset(x, y);
				}
			}
		});
	}

	@Override
	public void spellCastingStart(final String spellName, final String playerId, final String raceId) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				ICharacterSoundActionsListener characterSoundsManager = SoundsManager.getInstance().getCharacterSoundsManager(playerId);
				if(characterSoundsManager != null){
					characterSoundsManager.spellCast(spellName);
				}

				SpatialsManager.getInstance().startCharacterSpellAnimation(spellName, playerId, raceId);
			}
		});
	}

	@Override
	public void performBasicAttack(final String executor) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				SpatialsManager.getInstance().performBasicAttack(executor);
				ICharacterSoundActionsListener characterSoundsManager = SoundsManager.getInstance().getCharacterSoundsManager(executor);
				if(characterSoundsManager != null){
					characterSoundsManager.basicAttackCast();
				}
			}
		});
	}

	@Override
	public void playerEnteredMap(final ISummary playerSummary) throws RemoteException{
		final String player = playerSummary.getString("id");
		final double x = playerSummary.getDouble("positionX");
		final double y = playerSummary.getDouble("positionY");
		final double direction = playerSummary.getDouble("direction");
		//TODO gestire membri non necessari, spatial manager puo' memorizzare lo spatial (e' uguale per tutto il gioco)
		final String race = playerSummary.getString("race");
		ISummary statusSummary = playerSummary.getSummary("status");
		ISummary characteristicsSummary = statusSummary.getSummary("battleCharacteristics");
		final int characterSpeed = characteristicsSummary.getInteger("movementSpeed");
		final int attackSpeed = characteristicsSummary.getInteger("attackSpeed");

		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				GuiStatus guiCurrentStatus = InGameMapping.getInstance().getGuiStatus();
				if(guiCurrentStatus == GuiStatus.WORLD_3D){
					if(player.equals(username)){
						ClientTest.intentionManager.resetFocus();
						SpatialsManager.getInstance().addMovingCharacterSpatial(player, x, y, direction, characterSpeed, attackSpeed, true, race);
						
						SoundsManager.getInstance().addCharactersSoundsManagers(player, race, true);
						LifebarControl.getLifebarControl().updateCurrentPlayerPosition((float) x, (float) y);
					}else{
						SpatialsManager.getInstance().addMovingCharacterSpatial(player, x, y, direction, characterSpeed, attackSpeed, false, race);
						SoundsManager.getInstance().addCharactersSoundsManagers(player, race, false);
					}
				}else if(guiCurrentStatus == GuiStatus.WORLD_2D){
					SpatialsManager.getInstance().addMovingCharacterSpatial2D(player, x, y, characterSpeed, race, username);
				}
				if(player.equals(username)) {					
					GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
						@Override
						public void execute() {
							SpatialsManager.getInstance().addMapIcon(playerSummary.getString("currentMap"));
						}
					});
				}
				LifebarControl.getLifebarControl().setRotation(direction);
			}
		});
	}

	@Override
	public String getUsername() throws RemoteException{
		return username;
	}

	private IItem buildItem(ISummary item){
		IItemDefinition tempDef = GameStorage.getInstance().getItemDefinition(item.getString("name"));
		IItem tempItem = tempDef.getItemFromDefinition();
		tempItem.buildFromSummary(item);
		return tempItem;
	}
	
	@Override
	public void armorEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setArmor((IArmor) buildItem(equip));
	}

	@Override
	public void bootsEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setBoots((IBoots) buildItem(equip));
	}

	@Override
	public void helmetEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setHelmet((IHelmet) buildItem(equip));
	}

	@Override
	public void glovesEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setGloves((IGloves) buildItem(equip));
	}

	@Override
	public void necklaceEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setNecklace((INecklace) buildItem(equip));
	}

	@Override
	public void ringEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setRing((IRing) buildItem(equip));
	}

	@Override
	public void shieldEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setShield((IShield) buildItem(equip));
	}

	@Override
	public void weaponEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setWeapon((IWeapon) buildItem(equip));
	}

	@Override
	public void mysticalSphereEquipped(ISummary equip) throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setMysticalSphere((IMysticalSphere) buildItem(equip));
	}

	@Override
	public void armorUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setArmor(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void bootsUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setBoots(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void helmetUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setHelmet(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void glovesUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setGloves(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void necklaceUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setNecklace(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void ringUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setRing(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void shieldUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setShield(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void weaponUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setWeapon(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void mysticalSphereUnequipped() throws RemoteException{
		GUIEquippedItems.getInstance().getEquippedItems().setMysticalSphere(null);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void alteredStatusApplied(String affectedId, String executor, String alteredStatusName) throws RemoteException{
		EffectsManager.getInstance().launchFollowingEffect(affectedId, alteredStatusName);
		EquipmentItemsController.getInstance().redraw();
	}

	@Override
	public void monsterEnteredMap(ISummary summary, String name) throws RemoteException{
		SpatialsManager.getInstance().loadSingleCharacter(summary);
		LifebarControl.getLifebarControl().showEntity(summary.getString("id"));
	}

	@Override
	public void bagOfItemsDropped(final float positionX, final float positionY, final String bagId) throws RemoteException{
		IGUIEvent dropEvent = new IGUIEvent() {
			@Override
			public void execute(){
				SpatialsManager.getInstance().addDroppingBag(new GamePoint(positionX, positionY), bagId);
			}
		};
		GUIEventsHandler.getInstance().putEvent(dropEvent);

	}

	@Override
	public void bagOfItemsRremoved(final String bagId) throws RemoteException{
		IGUIEvent removeEvent = new IGUIEvent() {
			@Override
			public void execute(){
				SpatialsManager.getInstance().removeBag(bagId);
			}
		};
		GUIEventsHandler.getInstance().putEvent(removeEvent);
	}

	@Override
	public void itemAdded(final ISummary item) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				IItem tempItem = buildItem(item);
				tempItem.addToInventory(GUIInventory.getInstance());
				InventoryController.getInstance().redraw();
			}
		});
	}

	@Override
	public void itemRemoved(final String itemId) throws RemoteException{
		dispatcher.addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				IItem item = GUIInventory.getInstance().getIdStorage().getItem(itemId);
				item.removeFromInventory(GUIInventory.getInstance());
				InventoryController.getInstance().redraw();
			}
		});
	}

	@Override
	public void dialogStarted(String id) throws RemoteException {
		ClientTest.getInstance().changeNiftyProcessor("dialog");
		DialogScreenController.getInstance().setDialog(GameStorage.getInstance().getDialog(id));
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("dialog");
			}
		});
	}

	@Override
	public void dialogsMenuOpened(Collection<String> dialogIds) throws RemoteException{
		ClientTest.getInstance().changeNiftyProcessor("dialog");
		Collection<IDialog> tempDialogs = new LinkedList<>();
		for(String id : dialogIds){
			tempDialogs.add(GameStorage.getInstance().getDialog(id));
		}
		DialogScreenController.getInstance().setDialogList(tempDialogs);
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("choiceDialog");
			}
		});
	}
}
