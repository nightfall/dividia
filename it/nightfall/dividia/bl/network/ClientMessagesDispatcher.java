package it.nightfall.dividia.bl.network;

import it.nightfall.dividia.api.network.IBLDispatcher;
import it.nightfall.dividia.api.network.IClientEventListener;
import it.nightfall.dividia.api.network.INetworkMessage;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ClientMessagesDispatcher extends AbstractDispatcher implements IBLDispatcher {
	private final IClientEventListener clientEventListener;
	private final BlockingQueue<INetworkMessage> networkMessages = new LinkedBlockingQueue<>();
	
	public ClientMessagesDispatcher(IClientEventListener clientEventListener){
		this.clientEventListener = clientEventListener;
	}

	@Override
	public void run(){
		while(super.isRunning()){
			try{
				INetworkMessage message = getNextMessage();
				super.checkPause();
				message.dispatch(clientEventListener);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void addMessage(INetworkMessage message){
		try{
			networkMessages.put(message);
		}catch(InterruptedException ex){
			ex.printStackTrace();
		}
	}



	private INetworkMessage getNextMessage(){
		try{
			return networkMessages.take();
		}catch(InterruptedException ex){
			ex.printStackTrace();
			return null;
		}
	}
}
