package it.nightfall.dividia.bl.network;

import it.nightfall.dividia.api.battle.IDamageSource;
import it.nightfall.dividia.api.battle.IDamageSummary;
import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.IAlteredStatus;
import it.nightfall.dividia.api.item.IBagOfItems;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.equipment_items.IArmor;
import it.nightfall.dividia.api.item.equipment_items.IBoots;
import it.nightfall.dividia.api.item.equipment_items.IGloves;
import it.nightfall.dividia.api.item.equipment_items.IHelmet;
import it.nightfall.dividia.api.item.equipment_items.INecklace;
import it.nightfall.dividia.api.item.equipment_items.IRing;
import it.nightfall.dividia.api.item.equipment_items.IShield;
import it.nightfall.dividia.api.item.equipment_items.IWeapon;
import it.nightfall.dividia.api.network.IBLDispatcher;
import it.nightfall.dividia.api.network.IClientEventListener;
import it.nightfall.dividia.api.network.IEventDispatcher;
import it.nightfall.dividia.api.network.INetworkMessage;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.world.Teleport;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
//TODO fare controlli su chi inviare i messaggi.
public class EventDispatcher implements IEventDispatcher {
	private static IEventDispatcher eventDispatcher;
	private Map<String, IBLDispatcher> listenersMap = new HashMap<>();
	private Collection<IBLDispatcher> linkedMessagesDispatcher = listenersMap.values();

	public static IEventDispatcher getInstance(){
		if(eventDispatcher == null){
			eventDispatcher = new EventDispatcher();
			EventManager.getInstance().addListener(eventDispatcher);
		}
		return eventDispatcher;
	}

	EventDispatcher(){
	}

	@Override
	public void addClient(String username, IClientEventListener listener){
		IBLDispatcher messagesDispatcher = new ClientMessagesDispatcher(listener);
		listenersMap.put(username, messagesDispatcher);
		Thread clientThread = new Thread(messagesDispatcher);
		clientThread.setName("MessagesDispatcher Client " + username);
		clientThread.start();
	}

	@Override
	public IBLDispatcher getClientDispatcher(String username){
		return listenersMap.get(username);
	}

	@Override
	public void monsterDied(Monster monster, Player killer){
		final String monsterId = monster.getId(), playerId = killer.getId();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.monsterDied(monsterId, playerId);
			}
		};
		addMessageToASingleMap(monster.getCurrentMap(), message);
	}

	@Override
	public void playerDied(Player player, PlayingCharacter killer){
		final String playerId = player.getId(), killerId = killer.getId();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.playerDied(playerId, killerId);
			}
		};
		addMessageToASingleMap(player.getCurrentMap(), message);
	}

	@Override
	public void itemPicked(IItem item, Player player){
		final ISummary itemSummary = item.getSummary();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.itemPicked(itemSummary);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void dialogSuccessfullyCompleted(String dialogID, Player player){
	}

	@Override
	public void teleportUsed(IGameMap previousMap, Player player, Teleport teleport){
		final String playerId = player.getId(), nextMap = teleport.getNextMap();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.teleportUsed(playerId, nextMap);
			}
		};
		/*
		 IGameMap playerMap = player.getCurrentMap();
		 Collection<String> playersInCurrentMap = new HashSet<>();
		 playersInCurrentMap.addAll(playerMap.getPlayers().keySet());
		 playersInCurrentMap.addAll(playerMap.getLoadingPlayers().keySet());
		 for(String tempPlayer : playersInCurrentMap){
		 listenersMap.get(tempPlayer).addMessage(message);
		 }
		 */
		addMessageToASingleMap(previousMap, message);
		if(!player.getCurrentMap().equals(previousMap)) {
			listenersMap.get(player.getId()).addMessage(message);
		}
		//System.out.println(player.getCurrentMap().getName());
		//MapsManager.getInstance().buildSummary(nextMap,playerId);
	}

	@Override
	public void knapsackFull(Player player){
	}

	@Override
	public void playerEnteredMap(final ISummary playerSummary, final IGameMap map){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.playerEnteredMap(playerSummary);
			}
		};
		//todo forse va inviato solamente al players che so trovano nella mappa di arrivo
		addMessageToASingleMap(map, message);
	}

	@Override
	public void characterDied(PlayingCharacter character, PlayingCharacter killer){
	}

	@Override
	public void characterMoved(PlayingCharacter character){
		final String characterId = character.getId();
		final IGamePoint characterPosition = character.getPosition();
		final IDirection characterDirection = character.getDirection();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.characterMoved(characterId, characterPosition.getX(), characterPosition.getY(), characterDirection.getDirectionAngle());
			}
		};
		addMessageToASingleMap(character.getCurrentMap(), message);
	}

	@Override
	public void focusMoved(String playerId, double x, double y, String focusStatus){
		final String playerIdM = playerId;
		final double xM = x;
		final double yM = y;
		final String focusStatusM = focusStatus;
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.focusMoved(playerIdM, xM, yM, focusStatusM);
			}
		};
		listenersMap.get(playerId).addMessage(message);
	}

	@Override
	public void characterStopped(PlayingCharacter character){
		final String characterId = character.getId();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.characterStopped(characterId);
			}
		};
		addMessageToASingleMap(character.getCurrentMap(), message);
	}

	@Override
	public void damageReceived(PlayingCharacter affected, PlayingCharacter attacker, IDamageSummary summary){
		final String affectedId = affected.getId();
		final String attackerId = attacker.getId();
		final int damage = summary.getDamage();
		final IDamageSource source = summary.getSource();
		final String sourceId = summary.getSourceId();
		final IDamageType damageType = summary.getDamageType();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.damageReceived(affectedId, attackerId, damage, source, sourceId, damageType);
			}
		};
		if(affected instanceof Player){
			listenersMap.get(affectedId).addMessage(message);
		}
		if(attacker instanceof Player){
			listenersMap.get(attackerId).addMessage(message);
		}
	}

	@Override
	public void questCompleted(IQuest quest){
	}

	@Override
	public void questActivated(IQuest quest){
	}

	@Override
	public void goalCompleted(IGoal goal){
	}

	@Override
	public void goalActivated(IGoal goal){
	}

	@Override
	public void goalUpdated(IGoal goal){
	}

	@Override
	public void userDisconnected(Player player, String username){
		final String user = username;
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.userDisconnected(user);
			}
		};
		listenersMap.get(username).stop();
		for(IBLDispatcher client : linkedMessagesDispatcher){
			client.addMessage(message);
		}
	}

	@Override
	public void playingCharacterCharacteristicChanged(PlayingCharacter playingCharacter, IStatusBattleCharacteristic characteristic, int newValue, int delta){
		final String playingCharacterId = playingCharacter.getId(), statusChar = characteristic.getInstanceName();
		final int fNewValue = newValue, fDelta = delta;
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.playingCharacterCharacteristicChanged(playingCharacterId, statusChar, fNewValue, fDelta);
			}
		};
		addMessageToASingleMap(playingCharacter.getCurrentMap(), message);
	}

	@Override
	public void playerCharacteristicChanged(Player player, IStatusBattleCharacteristic characteristic, int newValue, int delta){
	}

	@Override
	public void playerLevelUp(final Player player, final int newLevel, final int maxExperience){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.playerLevelUp(player.getId(), newLevel, maxExperience);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void playerLevelUpByExperience(final Player player, final int newLevel, final int maxExperience){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.playerLevelUpByExperience(player.getId(), newLevel, maxExperience);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void playerGainExperience(final Player player, final int newValue, final int delta){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.playerGainExperience(player.getId(), newValue, delta);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void mapSummaryReady(final String player, final ISummary generalGameMapSummary){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.receiveMapSummary(player, generalGameMapSummary);
			}
		};
		listenersMap.get(player).addMessage(message);
	}

	@Override
	public void playerSummaryReady(final Player player, final ISummary summary){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.playerSummaryReceived(player.getId(), summary);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void spellOnCooldown(final Player player, final ISpellDefinition spell){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.spellOnCooldown(spell.getName());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void launchAreaSpell(final ISpellDefinition spell, final IDirection castingDirection, final IGamePoint castingPoint, final IGameMap map, final String casterId){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.launchAreaSpell(spell.getName(), castingDirection.getVersorX(), castingDirection.getVersorY(), castingPoint.getX(), castingPoint.getY(), casterId);
			}
		};
		addMessageToASingleMap(map, message);

	}

	@Override
	public void ping(String username){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.ping();
			}
		};
		listenersMap.get(username).addMessage(message);
	}

	@Override
	public void focusReset(Player player, final double x, final double y){
		final String playerIdM = player.getId();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.focusReset(playerIdM, x, y);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void spellCastingStart(final ISpellDefinition spell, final PlayingCharacter executor){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.spellCastingStart(spell.getName(), executor.getId(), executor.getRace().getName());
			}
		};
		addMessageToASingleMap(executor.getCurrentMap(), message);
	}

	@Override
	public void performBasicAttack(final PlayingCharacter executor){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.performBasicAttack(executor.getId());
			}
		};
		addMessageToASingleMap(executor.getCurrentMap(), message);

	}

	@Override
	public void armorUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.armorUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void bootsUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.bootsUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void helmetUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.helmetUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void glovesUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.glovesUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void necklaceUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.necklaceUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void ringUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.ringUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void shieldUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.shieldUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void weaponUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.weaponUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void mysticalSphereUnequipped(Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.mysticalSphereUnequipped();
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void alteredStatusApplied(final PlayingCharacter playingCharacter, final PlayingCharacter executor, final IAlteredStatus tempAlteredStatus){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.alteredStatusApplied(playingCharacter.getId(), executor.getId(), tempAlteredStatus.getAlteredStatusInfo().getName());
			}
		};
		addMessageToASingleMap(playingCharacter.getCurrentMap(), message);
	}

	@Override
	public void monsterEnteredMap(final ISummary summary, final IGameMap currentMap){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.monsterEnteredMap(summary, currentMap.getName());
			}
		};
		addMessageToASingleMap(currentMap, message);
	}

	@Override
	public void bagOfItemsDropped(final IBagOfItems droppingBag, final IGameMap map){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.bagOfItemsDropped((float)droppingBag.getPosition().getX(), (float)droppingBag.getPosition().getY() , droppingBag.getID());
			}
		};
		addMessageToASingleMap(map, message);
	}

	@Override
	public void bagOfItemsRemoved(final IBagOfItems removingBag, final IGameMap map){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.bagOfItemsRremoved(removingBag.getID());
			}
		};
		addMessageToASingleMap(map, message);
	}

	private void addMessageToASingleMap(IGameMap map, INetworkMessage message){

		for(String tempPlayer : map.getActiveAndLoadingPlayers().keySet()){
			listenersMap.get(tempPlayer).addMessage(message);
		}
	}

	@Override
	public void itemAdded(final IItem item, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.itemAdded(item.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void itemRemoved(IItem item, Player player){
		final String itemId = item.getId();
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.itemRemoved(itemId);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void dialogStarted(final String id, final Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.dialogStarted(id);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void dialogsMenuOpened(Collection<IDialog> dialogs, final Player player){
		final Collection<String> dialogIds = new LinkedList<>();
		for(IDialog dialog : dialogs){
			dialogIds.add(dialog.getId());
		}
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.dialogsMenuOpened(dialogIds);
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void armorEquipped(final IArmor armor, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.armorEquipped(armor.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void bootsEquipped(final IBoots boots, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.bootsEquipped(boots.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void helmetEquipped(final IHelmet helmet, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.helmetEquipped(helmet.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void glovesEquipped(final IGloves gloves, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.glovesEquipped(gloves.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void necklaceEquipped(final INecklace necklace, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.necklaceEquipped(necklace.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void ringEquipped(final IRing ring, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.ringEquipped(ring.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void shieldEquipped(final IShield shield, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.shieldEquipped(shield.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void weaponEquipped(final IWeapon weapon, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.weaponEquipped(weapon.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}

	@Override
	public void mysticalSphereEquipped(final IMysticalSphere mysticalSphere, Player player){
		INetworkMessage message = new INetworkMessage() {
			@Override
			public void dispatch(IClientEventListener client) throws RemoteException{
				client.mysticalSphereEquipped(mysticalSphere.getSummary());
			}
		};
		listenersMap.get(player.getId()).addMessage(message);
	}
}
