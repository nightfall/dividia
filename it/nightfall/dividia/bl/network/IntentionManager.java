package it.nightfall.dividia.bl.network;

import it.nightfall.dividia.api.battle.definitions.IAreaSpellDefinition;
import it.nightfall.dividia.api.network.IIntentionManager;
import it.nightfall.dividia.api.utility.ICallbackWaiting;
import it.nightfall.dividia.api.utility.IFutureCallback;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.FutureCallback;
import it.nightfall.dividia.bl.utility.GameManager;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.bl.utility.io.ConfigWriter;
import it.nightfall.dividia.bl.utility.playerMovements.Movement;
import it.nightfall.dividia.bl.world.MapsManager;
import java.rmi.RemoteException;

public class IntentionManager implements IIntentionManager {
	private final String savegameDirectory;
	private final String username;
	private Player linkedPlayer;
	private final IFutureCallback connectionTester;
	
	public IntentionManager(final String username) throws RemoteException{
		this.username = username;
		savegameDirectory = IGameConstants.SERVER_SAVEGAMES_DIRECTORY + SharedFunctions.getPath(username + "|");
		connectionTester = new FutureCallback(new ICallbackWaiting() {
			@Override
			public boolean execute(){
				EventManager.getInstance().ping(username);
				return false;
			}
		});
		if(!GameManager.getInstance().isInSinglePlayer()){
			connectionTester.runAndWait(5000); //TODO: Set TIMEOUT
		}
	}
	
	@Override
	public void movePlayer(String direction) throws RemoteException{
		linkedPlayer.getMovementManager().updateDirection(Movement.valueOf(direction));
		connectionTester.resetCycle();
	}
	
	@Override
	public void basicAttack() throws RemoteException{
		linkedPlayer.undertakeAction();
		connectionTester.resetCycle();
	}
	
	@Override
	public void detonateTargetAreaSpell(String spellName) throws RemoteException{
		connectionTester.resetCycle();
	}
	
	@Override
	public void interact() throws RemoteException{
        //TODO codice provvisorio cambia solo i mondi, dovrebbe chiamare map.findInteraction
        /*
		SpatialsManager.removePlayingCharacterSpatial(linkedPlayer.getId());
        boolean switchTo2D = ClientTest.getGameInputManager().getGuiStatus() == GuiStatus.WORLD_3D ? true : false;
        linkedPlayer.switchDirectionMovementManager(switchTo2D);
        if(switchTo2D){
            ClientTest.getGameInputManager().setGuiStatus(GuiStatus.WORLD_2D);
        } else {
            ClientTest.getGameInputManager().setGuiStatus(GuiStatus.WORLD_3D);
        }
        ClientTest.getGameInputManager().switchClientDirectionManager();
        double nextX = linkedPlayer.getPosition().getX();
        double nextY = linkedPlayer.getPosition().getY();
        int charSpeed = linkedPlayer.getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED);
        GameManager.addPlayer(linkedPlayer);
		* */
		//TODO non e' meglio creare il metodo find interaction in player che a sua volta chiamera'
		//	   il metodo adatto nella mappa corrente?
		//System.out.println("Finding interaction in " + linkedPlayer.getPosition());
		linkedPlayer.getCurrentMap().findInteraction(linkedPlayer);
     }

	@Override
	public void disconnect() throws RemoteException{
		//TODO Exit map
		IConfigWriter savingConfig = new ConfigWriter("root");
		linkedPlayer.saveState(savingConfig);
		savingConfig.saveFile(IGameConstants.SERVER_SAVEGAMES_DIRECTORY + SharedFunctions.getPath(username + "|" + username + ".xml"));
		if(GameManager.getInstance().isInSinglePlayer()){
			GameManager.getInstance().stopEngine();
		}
		EventManager.getInstance().userDisconnected(linkedPlayer, username);
	}
	
	@Override
	public void loadGameSave(String gameSaveId) throws RemoteException{
		linkedPlayer = new Player(username);
		IConfigReader config = new ConfigReader(savegameDirectory + gameSaveId + ".xml");
		linkedPlayer.loadState(config);
		//GameManager.getInstance().addPlayer(linkedPlayer);
		connectionTester.resetCycle();
	}
	@Override
	public void finishLoadingMapScene(String nextMap) throws RemoteException{
		/*
		MapsManager.getInstance().summaryRequest(username, nextMap);
		connectionTester.resetCycle();
		*/
	}
	
	@Override
	public void loadingMapCompleted(String map) throws RemoteException{
		MapsManager.getInstance().finishedLoadingMap(username, map);
		connectionTester.resetCycle();
	}
	
	@Override
	public void pauseEvents() throws RemoteException{
		EventDispatcher.getInstance().getClientDispatcher(username).pause();
		connectionTester.resetCycle();
	}
	
	@Override
	public void resumeEvents() throws RemoteException{
		EventDispatcher.getInstance().getClientDispatcher(username).resume();
		connectionTester.resetCycle();
	}

	@Override
	public void ping() throws RemoteException{
		connectionTester.resetCycle();
	}

	@Override
	public void castAreaSpell(String spellName) throws RemoteException{
		if(linkedPlayer.hasLearntSpell(spellName)){
			IAreaSpellDefinition spell = (IAreaSpellDefinition) GameStorage.getInstance().getSpellDefinition(spellName);
			linkedPlayer.undertakeAction(spell);
		}
		connectionTester.resetCycle();
	}

	@Override
	public void castFocusedSpell(String spellName) throws RemoteException{
		connectionTester.resetCycle();
	}

	@Override
	public void castTargetSpell(String spellName) throws RemoteException{
		connectionTester.resetCycle();
	}

	@Override
	public void castTargetAreaSpell(String spellName) throws RemoteException{
		connectionTester.resetCycle();
	}

	@Override
	public void resetFocus() throws RemoteException {
		linkedPlayer.getMovementManager().resetFocus();
		connectionTester.resetCycle();
	}

	@Override
	public void unequipArmor() throws RemoteException{
		linkedPlayer.unequipArmor();
	}

	@Override
	public void unequipBoots() throws RemoteException{
		linkedPlayer.unequipBoots();
	}

	@Override
	public void unequipHelmet() throws RemoteException{
		linkedPlayer.unequipHelmet();
	}

	@Override
	public void unequipGloves() throws RemoteException{
		linkedPlayer.unequipGloves();
	}

	@Override
	public void unequipNecklace() throws RemoteException{
		linkedPlayer.unequipNecklace();
	}

	@Override
	public void unequipRing() throws RemoteException{
		linkedPlayer.unequipRing();
	}

	@Override
	public void unequipShield() throws RemoteException{
		linkedPlayer.unequipShield();
	}

	@Override
	public void unequipMysticalSphere() throws RemoteException{
		linkedPlayer.unequipMysticalSphere();
	}

	@Override
	public void dialogChoosen(String dialogId) throws RemoteException {
		linkedPlayer.getQuestManager().dialogChoosen(dialogId);
	}
	
	@Override
	public void dialogCompleted(String dialogId) throws RemoteException {
		linkedPlayer.getQuestManager().dialogCompleted(dialogId);
	}

	@Override
	public void equipItem(String itemId) throws RemoteException{
		linkedPlayer.getInventory().getIDStorage().getItem(itemId).use(linkedPlayer);
	}
}
