package it.nightfall.dividia.bl.network.dcp;

import it.nightfall.dividia.api.network.IDCPSession;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;

public abstract class AbstractSession implements IDCPSession{
	protected Socket socket;
	protected DataInputStream reader;
	protected DataOutputStream writer;
	protected Cipher decrypter;
	protected Cipher encrypter;
	protected KeyFactory keyFactory;
	protected PublicKey publicKey;
	protected PrivateKey privateKey;
	protected PublicKey receivedKey;

	public AbstractSession(Socket socket){
		try{
			this.socket = socket;
			reader = new DataInputStream(socket.getInputStream());
			writer = new DataOutputStream(socket.getOutputStream());
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			encrypter = Cipher.getInstance("RSA/None/NoPadding");
			decrypter = Cipher.getInstance("RSA/None/NoPadding");
			keyFactory = KeyFactory.getInstance("RSA");
			keyGen.initialize(2048);
			KeyPair keyPair = keyGen.generateKeyPair();
			publicKey = keyPair.getPublic();
			privateKey = keyPair.getPrivate();
			encrypter.init(Cipher.ENCRYPT_MODE, privateKey);
			sendPublicKey();
			readPublicKey();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private void sendBytes(byte[] message){
		try{
			writer.writeInt(message.length);
			writer.write(message);
			writer.flush();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private byte[] readBytes(){
		try{
			int length = reader.readInt();
			byte[] message = new byte[length];
			reader.readFully(message);
			return message;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}

	protected void sendMessage(String message){
		try{
			byte[] encryptedMessage = encrypter.doFinal(message.getBytes());
			sendBytes(encryptedMessage);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	protected String readMessage(){
		try{
			byte[] message = decrypter.doFinal(readBytes());
			return new String(message);
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}

	private void sendPublicKey() throws InvalidKeySpecException{
		RSAPublicKeySpec encodedKey = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
		String message = encodedKey.getModulus().toString(16) + " " + encodedKey.getPublicExponent().toString(16);
		sendBytes(message.getBytes());
	}

	private void readPublicKey() throws InvalidKeySpecException, InvalidKeyException{
		String[] encodedSharedKey = new String(readBytes()).split(" ");
		if(encodedSharedKey.length != 2){
			throw new InvalidKeySpecException();
		}
		RSAPublicKeySpec tempKeySpec = new RSAPublicKeySpec(new BigInteger(encodedSharedKey[0],16), new BigInteger(encodedSharedKey[1], 16));
		receivedKey = keyFactory.generatePublic(tempKeySpec);
		decrypter.init(Cipher.DECRYPT_MODE, receivedKey);
	}

	@Override
	public void disconnect(){
		sendMessage("BYE");
	}
}
