package it.nightfall.dividia.bl.network.dcp;

import it.nightfall.dividia.api.network.IAuthServer;
import it.nightfall.dividia.api.network.IClientEventListener;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.testing.appstates.MainMenu;
import java.net.Socket;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ClientSession extends AbstractSession {
	public static final int LOGIN_INTENTION = 0;
	public static final int REGISTRATION_INTENTION = 1;
	private ClientStates state = ClientStates.VERSION;
	private final String username, nickname, password;
	private final int intention;
	private IClientEventListener eventsListener;
	private MainMenu mainMenuCallback;

	public ClientSession(Socket socket, int intention, String user, String nick, String pw, IClientEventListener listener, MainMenu callback){
		super(socket);
		this.username = user;
		this.nickname = nick;
		this.password = pw;
		this.intention = intention;
		this.eventsListener = listener;
		this.mainMenuCallback = callback;
	}

	@Override
	public void run(){
		String message;
		String[] splitMessage;
		while(state != ClientStates.INVALID){
			switch(state){
				case VERSION:
					sendMessage("VER DVA " + IGameConstants.DIVIDIA_VERSION);
					break;
				case ACTION:
					if(intention == LOGIN_INTENTION){
						sendMessage("ACT LGN");
					}else{
						sendMessage("ACT REG");
					}
					break;
				case LOGIN_USER:
					sendMessage("LGN USR " + username);
					break;
				case LOGIN_PASSWORD:
					sendMessage("LGN PSW " + password);
					break;
				case REGISTRATION_USER:
					sendMessage("REG USR " + username);
					break;
				case REGISTRATION_PASSWORD:
					sendMessage("REG PSW " + password);
					break;
				case REGISTRATION_NICK:
					sendMessage("REG NCK " + nickname);
					break;
			}
			message = readMessage();
			splitMessage = message.split(" ");
			if(SharedFunctions.checkNetworkMessage(state, message)){
				switch(state){
					case VERSION:
						state = ClientStates.ACTION;
						break;
					case ACTION:
						if(intention == LOGIN_INTENTION){
							state = ClientStates.LOGIN_USER;
						}else{
							state = ClientStates.REGISTRATION_USER;
						}
						break;
					case LOGIN_USER:
						state = ClientStates.LOGIN_PASSWORD;
						break;
					case LOGIN_PASSWORD:
						startRMISession(splitMessage[1], splitMessage[2], splitMessage[3], splitMessage[4]);
						break;
					case REGISTRATION_USER:
						state = ClientStates.REGISTRATION_PASSWORD;
						break;
					case REGISTRATION_PASSWORD:
						state = ClientStates.REGISTRATION_NICK;
						break;
					case REGISTRATION_NICK:
						startRMISession(splitMessage[1], splitMessage[2], splitMessage[3], splitMessage[4]);
						break;
				}
			}else{
				mainMenuCallback.showError(state);
				disconnect();
			}

		}
	}

	@Override
	public void disconnect(){
		super.disconnect();
		state = ClientStates.INVALID;
	}

	private void startRMISession(String clientIP, String serverIP, String port, String otp){
		System.setProperty("java.rmi.server.hostname", clientIP);
		try{
			Registry tempRegistry = LocateRegistry.getRegistry(serverIP, Integer.parseInt(port));
			IAuthServer authenticationServer = (IAuthServer) tempRegistry.lookup("DividiaServer");
			IClientEventListener eventListenerStub = (IClientEventListener) UnicastRemoteObject.exportObject(eventsListener, 0);
			authenticationServer.authorize(username, otp, eventListenerStub);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
