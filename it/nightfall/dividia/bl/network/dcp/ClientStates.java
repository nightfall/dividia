package it.nightfall.dividia.bl.network.dcp;

import it.nightfall.dividia.api.network.IConnectionState;

public enum ClientStates implements IConnectionState {
	VERSION(new String[]{"ACK", "VER"}, 2),
	ACTION(new String[]{"ACK", "ACT"}, 3),
	LOGIN_USER(new String[]{"ACK", "LGN", "USR"}, 3),
	LOGIN_PASSWORD(new String[]{"AUTH"}, 5),
	REGISTRATION_USER(new String[]{"ACK", "REG", "USR"}, 3),
	REGISTRATION_PASSWORD(new String[]{"ACK", "REG", "PSW"}, 3),
	REGISTRATION_NICK(new String[]{"AUTH"}, 5),
	INVALID(new String[]{}, 0);
	private String[] requirements;
	private int paramLength;

	ClientStates(String[] req, int length){
		requirements = req;
		paramLength = length;
	}

	@Override
	public String[] getRequirements(){
		return requirements;
	}

	@Override
	public int getParamLength(){
		return paramLength;
	}
}
