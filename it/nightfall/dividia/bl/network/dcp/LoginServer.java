package it.nightfall.dividia.bl.network.dcp;

import it.nightfall.dividia.api.network.IDCPServer;
import it.nightfall.dividia.api.network.IDCPSession;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.network.AuthServer;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.bl.utility.io.ConfigWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LoginServer implements IDCPServer {
	private static final String SERVER_PATH = SharedFunctions.getPath(".|server|");
	private static IDCPServer loginServer;
	private HashMap<String, IDCPSession> activeSessions = new HashMap<>();
	private Map<String, String> usersTable = new HashMap<>();
	private Map<String, String> usersNick = new HashMap<>();
	private Map<String, String> authorizedUsers = new HashMap<>();
	private MessageDigest digest;
	private Registry rmiRegistry;

	public static IDCPServer getInstance(){
		if(loginServer == null){
			loginServer = new LoginServer();
		}
		return loginServer;
	}

	private LoginServer(){
		Collection<IConfigReader> usersData = new ConfigReader(SERVER_PATH + "users.xml").getConfigReaderList("user");
		for(IConfigReader userData : usersData){
			usersTable.put(userData.getString("username"), userData.getString("password"));
			usersNick.put(userData.getString("username"), userData.getString("nickname"));
		}
		try{
			digest = MessageDigest.getInstance("SHA1");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void flushUsersDatabase(){
		IConfigWriter usersData = new ConfigWriter("usersDB"), tempUserData;
		for(String user : usersTable.keySet()){
			tempUserData = new ConfigWriter("user", usersData);
			tempUserData.appendString("username", user);
			tempUserData.appendString("password", usersTable.get(user));
			tempUserData.appendString("nickname", usersNick.get(user));
			usersData.appendConfigWriter(tempUserData);
		}
		usersData.saveFile(SERVER_PATH + "users.xml");
	}

	@Override
	public boolean userExists(String username){
		return usersTable.containsKey(username);
	}

	@Override
	public boolean nickExists(String nickname){
		return usersNick.containsValue(nickname);
	}

	@Override
	public boolean passwordValid(String username, String password){
		return usersTable.get(username).equals(hashPassword(password));
	}

	@Override
	public String hashPassword(String password){
		byte[] digestedPassword = digest.digest(password.getBytes());
		return new BigInteger(1, digestedPassword).toString(16);
	}

	@Override
	public void registerUser(String username, String password, String nickname){
		usersTable.put(username, hashPassword(password));
		usersNick.put(username, nickname);
		flushUsersDatabase();
		String userDir = IGameConstants.SERVER_SAVEGAMES_DIRECTORY + username;
		new File(userDir).mkdir();
		Path defSavegame = FileSystems.getDefault().getPath(SERVER_PATH + "defaultSavegame.xml");
		Path destSavegame = FileSystems.getDefault().getPath(SharedFunctions.getPath(userDir + "|" + username + ".xml"));
		try{
			Files.copy(defSavegame, destSavegame, java.nio.file.StandardCopyOption.REPLACE_EXISTING);
		}catch(IOException ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void sessionTerminated(String identifier){
		activeSessions.remove(identifier);
	}

	@Override
	public void run(){
		startRMISessionManager();
		try{
			ServerSocket centralSocket = new ServerSocket(IGameConstants.SOCKET_PORT);
			while(true){
				Socket connectedClient = centralSocket.accept();
				String otp = UUID.randomUUID().toString();
				IDCPSession serverSession = new ServerSession(connectedClient, otp, this);
				activeSessions.put(otp, serverSession);
				Thread loginThread = new Thread(serverSession);
				loginThread.setName("Client Login");
				loginThread.start();
			}
		}catch(Exception ex){

			ex.printStackTrace();
		}
	}

	@Override
	public void authorizeSession(String username, String uuid){
		authorizedUsers.put(username, uuid);
	}

	@Override
	public boolean autheticateSession(String username, String uuid){
		String otp = authorizedUsers.get(username);
		if(otp != null && otp.equals(uuid)){
			authorizedUsers.remove(username);
			return true;
		}
		return false;
	}

	private void startRMISessionManager(){
		try{
			rmiRegistry = LocateRegistry.createRegistry(IGameConstants.RMI_PORT);
			rmiRegistry.bind("DividiaServer", AuthServer.getInstance());
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}