package it.nightfall.dividia.bl.network.dcp;

import it.nightfall.dividia.api.network.IDCPServer;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.net.Socket;

public class ServerSession extends AbstractSession {
	protected String identifier;
	IDCPServer dcpServer;
	ServerStates state = ServerStates.VERSION;

	public ServerSession(Socket socket, String id, IDCPServer server){
		super(socket);
		identifier = id;
		dcpServer = server;
	}

	@Override
	public void run(){
		String message, username = null, nickname, password = null;
		String[] splitMessage;
		while(state != ServerStates.INVALID){
			message = readMessage();
			splitMessage = message.split(" ");
			if(SharedFunctions.checkNetworkMessage(state, message)){
				switch(state){
					case VERSION:
						if(validVersion(message)){
							sendMessage("ACK VER");
							state = ServerStates.ACTION;
						}else{
							sendMessage("UPD DVA " + IGameConstants.DIVIDIA_VERSION);
							state = ServerStates.INVALID;
						}
						break;
					case ACTION:
						String action = splitMessage[1];
						switch(action){
							case "LGN":
								sendMessage("ACK ACT LGN");
								state = ServerStates.LOGIN_USER;
								break;
							case "REG":
								sendMessage("ACK ACT REG");
								state = ServerStates.REGISTRATION_USER;
								break;
							default:
								disconnect();
								break;
						}
						break;
					case LOGIN_USER:
						username = splitMessage[2];
						if(dcpServer.userExists(username)){
							sendMessage("ACK LGN USR");
							username = splitMessage[2];
							state = ServerStates.LOGIN_PASSWORD;
							break;
						}
						disconnect();
						break;
					case LOGIN_PASSWORD:
						password = splitMessage[2];
						if(dcpServer.passwordValid(username, password)){
							dcpServer.authorizeSession(username, identifier);
							sendMessage("AUTH " + socket.getInetAddress().getHostAddress() + " " + socket.getLocalAddress().getHostAddress() + " " + IGameConstants.RMI_PORT + " " + identifier);
							state = ServerStates.INVALID;
							break;
						}
						disconnect();
						break;
					case REGISTRATION_USER:
						username = splitMessage[2];
						if(!dcpServer.userExists(username)){
							sendMessage("ACK REG USR");
							state = ServerStates.REGISTRATION_PASSWORD;
							break;
						}
						disconnect();
						break;
					case REGISTRATION_PASSWORD:
						password = splitMessage[2];
						sendMessage("ACK REG PSW");
						state = ServerStates.REGISTRATION_NICK;
						break;
					case REGISTRATION_NICK:
						nickname = splitMessage[2];
						if(!dcpServer.nickExists(nickname)){
							dcpServer.registerUser(username, password, nickname);
							dcpServer.authorizeSession(username, identifier);
							sendMessage("AUTH " + socket.getInetAddress().getHostAddress() + " " + socket.getLocalAddress().getHostAddress() + " " + IGameConstants.RMI_PORT + " " + identifier);
							state = ServerStates.INVALID;
							break;
						}
						disconnect();
						break;
				}
			}else{
				disconnect();
			}
		}
	}

	private boolean validVersion(String versionLine){
		String serverVersion = "VER DVA " + IGameConstants.DIVIDIA_VERSION;
		return serverVersion.equals(versionLine);
	}

	@Override
	public void disconnect(){
		super.disconnect();
		state = ServerStates.INVALID;
		dcpServer.sessionTerminated(identifier);
	}
}
