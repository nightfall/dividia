package it.nightfall.dividia.bl.network.dcp;

import it.nightfall.dividia.api.network.IConnectionState;

enum ServerStates implements IConnectionState {
	VERSION(new String[]{"VER", "DVA"}, 3),
	ACTION(new String[]{"ACT"}, 2),
	LOGIN_USER(new String[]{"LGN", "USR"}, 3),
	LOGIN_PASSWORD(new String[]{"LGN", "PSW"}, 3),
	REGISTRATION_USER(new String[]{"REG", "USR"}, 3),
	REGISTRATION_NICK(new String[]{"REG", "NCK"}, 3),
	REGISTRATION_PASSWORD(new String[]{"REG", "PSW"}, 3),
	AUTHORIZED(new String[]{}, 0),
	INVALID(new String[]{}, 0);
	private String[] requirements;
	private int paramLength;

	ServerStates(String[] req, int length){
		requirements = req;
		paramLength = length;
	}

	@Override
	public String[] getRequirements(){
		return requirements;
	}

	@Override
	public int getParamLength(){
		return paramLength;
	}
}