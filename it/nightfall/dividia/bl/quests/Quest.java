package it.nightfall.dividia.bl.quests;

import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.bl.utility.io.ConfigWriter;
import it.nightfall.dividia.bl.world.MapsManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Concrete representation of a Quest
 */
public class Quest implements IQuest {
	private IQuestDefinition questInfo;
	private HashSet<String> solvedGoals = new HashSet<>();
	private Map<String, IGoal> activeGoals = new HashMap<>();
	private HashSet<String> remainingGoals = new HashSet<>();
	private Player owner;

	/**
	 * Build the quest from a given definition
	 *
	 * @param questDefinition Quest definition
	 */
	public Quest(IQuestDefinition questDefinition){
		questInfo = questDefinition;
		remainingGoals.addAll(questInfo.getGoalDefinitions());
	}

	@Override
	public void targetSolved(IGoal goal){
		IGoalDefinition goalDetails = goal.getGoalInfo();
		solvedGoals.add(goalDetails.getName());
		activeGoals.remove(goal.getGoalInfo().getName());
		if(remainingGoals.isEmpty() && activeGoals.isEmpty()){
			EventManager.getInstance().questCompleted(this);
		}else{
			Iterator<String> it = remainingGoals.iterator();
			while(it.hasNext()) {
				String remainingGoal = it.next();
				goalDetails = GameStorage.getInstance().getGoalDefinition(remainingGoal);
				if(goalDetails.meetsRequirements(solvedGoals)){
					IGoal newActiveGoal = goalDetails.getGoalFromDefinition();
					activeGoals.put(goalDetails.getName(), newActiveGoal);
					it.remove();
					newActiveGoal.activateGoal(this);
				}
			}
		}
		getGoalRewardsAndCheckForTeleport(goal);
	}

	@Override
	public Map<String, IGoal> getActiveGoals(){
		return Collections.unmodifiableMap(activeGoals);
	}

	@Override
	public Set<String> getRemainingGoals(){
		return Collections.unmodifiableSet(remainingGoals);
	}

	@Override
	public Set<String> getSolvedGoals(){
		return Collections.unmodifiableSet(solvedGoals);
	}

	@Override
	public IQuestDefinition getQuestInfo(){
		return questInfo;
	}

	@Override
	public Player getOwner(){
		return owner;
	}

	@Override
	public void activateQuest(Player player){
		owner = player;
		EventManager.getInstance().questActivated(this);
		Iterator<String> iterator = remainingGoals.iterator();
		while(iterator.hasNext()) {
			String remainingGoal = iterator.next();
			if(GameStorage.getInstance().getGoalDefinition(remainingGoal).meetsRequirements(solvedGoals)) {
				IGoalDefinition activatingGoalDefinition = GameStorage.getInstance().getGoalDefinition(remainingGoal);
				IGoal activatingGoal = activatingGoalDefinition.getGoalFromDefinition();
				activeGoals.put(activatingGoalDefinition.getName(), activatingGoal);
				iterator.remove();
				activatingGoal.activateGoal(this);
			}
		}
		/*for(String remainingGoal:remainingGoals) {
			if(GameStorage.getInstance().getGoalDefinition(remainingGoal).meetsRequirements(solvedGoals)) {
				IGoalDefinition activatingGoalDefinition = GameStorage.getInstance().getGoalDefinition(remainingGoal);
				IGoal activatingGoal = activatingGoalDefinition.getGoalFromDefinition();
				activeGoals.put(activatingGoalDefinition.getName(), activatingGoal);
				remainingGoals.remove(remainingGoal);
				activatingGoal.activateGoal(this);
			}
		}*/
	}

	@Override
	public void loadState(IConfigReader saveState){
		Collection<String> tempList = saveState.getConfigReader("solvedGoals").getStringList("goal");
		solvedGoals.addAll(tempList);
		remainingGoals.removeAll(tempList);
		Collection<IConfigReader> tempList2 = saveState.getConfigReader("activeGoals").getConfigReaderList("goal");
		String goalName;
		for(IConfigReader goal : tempList2){
			goalName = goal.getString("name");
			IGoal tempGoal = GameStorage.getInstance().getGoalDefinition(goalName).getGoalFromDefinition();
			tempGoal.loadState(goal);
			activeGoals.put(goalName, tempGoal);
			remainingGoals.remove(goalName);
			tempGoal.activateGoal(this);
		}
	}

	@Override
	public void saveState(IConfigWriter saveState){
		saveState.appendString("name", questInfo.getName());
		IConfigWriter configNode1 = new ConfigWriter("solvedGoals", saveState);
		IConfigWriter configNode2 = new ConfigWriter("activeGoals", saveState);
		configNode1.appendStringList("goal", solvedGoals);
		saveState.appendConfigWriter(configNode1);
		Collection<IConfigWriter> tempList = new ArrayList<>();
		IConfigWriter savingGoal;
		for(IGoal goal : activeGoals.values()){
			savingGoal = new ConfigWriter("goal", configNode2);
			goal.saveState(savingGoal);
			tempList.add(savingGoal);
		}
		configNode2.appendConfigWriterList(tempList);
		saveState.appendConfigWriter(configNode2);
	}

	@Override
	public boolean isGoalActive(String goalName){
		return activeGoals.containsKey(goalName);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		ISummary activeGoalsSummary = new Summary();
		for(Map.Entry<String, IGoal> goalData : activeGoals.entrySet()){
			activeGoalsSummary.putData(goalData.getKey(), goalData.getValue().getSummary());
		}
		tempSummary.putData("solvedGoals", solvedGoals);
		tempSummary.putData("activeGoals", activeGoalsSummary);
		return tempSummary;
	}

	private void getGoalRewardsAndCheckForTeleport(IGoal goal) {
		List<IItemDefinition> rewards = goal.getGoalInfo().getRewards();
		for(IItemDefinition singleReward : rewards) {
			IItem item = singleReward.getItemFromDefinition();
			item.pickUp(owner);
		}
		if(goal.getGoalInfo().getTeleportAfterGoal()!=null) {
			MapsManager.getInstance().useTeleportForPlayer(owner.getCurrentMap(), owner,goal.getGoalInfo().getTeleportAfterGoal());
		}
	}
}
