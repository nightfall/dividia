package it.nightfall.dividia.bl.quests;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Concrete instance of a quest definition, it stores the quest data
 */
public class QuestDefinition implements IQuestDefinition {
	private final String name;
	private final String displayName;
	private final String description;
	private final boolean visibleToPlayer;
	private final int levelRequirement;
	private final List<IItemDefinition> rewards = new LinkedList<>();
	private final Set<String> requirements;
	private final Set<String> questGoals;
	private final IDialog activationDialog;

	/**
	 * Loads Quest definition data from a given IConfigReader node
	 *
	 * @param config IConfigReader node
	 */
	public QuestDefinition(IConfigReader config){
		name = config.getString("name");
		displayName = config.getString("displayName");
		description = config.getString("description");
		visibleToPlayer = config.getBoolean("visibleToPlayer");
		levelRequirement = config.getInteger("levelRequirement");
		List<String> rewardIds = new LinkedList<>(config.getConfigReader("rewards").getStringList("reward"));
		for(String reward: rewardIds) {
			rewards.add(GameStorage.getInstance().getItemDefinition(reward));
		}
		requirements = new HashSet<>(config.getConfigReader("requirements").getStringList("quest"));
		questGoals = new HashSet<>(config.getConfigReader("goals").getStringList("goal"));
		activationDialog = GameStorage.getInstance().getDialog(config.getString("dialog"));
	}

	@Override
	public String getDescription(){
		return description;
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}

	@Override
	public boolean isVisibleToPlayer(){
		return visibleToPlayer;
	}

	@Override
	public int getLevelRequirement(){
		return levelRequirement;
	}

	@Override
	public List<IItemDefinition> getRewards(){
		return Collections.unmodifiableList(rewards);
	}

	@Override
	public Set<String> getRequirements(){
		return Collections.unmodifiableSet(requirements);
	}

	@Override
	public Set<String> getGoalDefinitions(){
		return questGoals;
	}

	@Override
	public boolean meetsRequirements(Set<String> completedQuests){
		if(requirements.isEmpty()){
			return true;
		}
		return completedQuests.containsAll(requirements);
	}

	@Override
	public IQuest getQuestFromDefinition(){
		return new Quest(this);
	}

	@Override
	public IDialog getActivationDialog() {
		return activationDialog;
	}
	
	
}
