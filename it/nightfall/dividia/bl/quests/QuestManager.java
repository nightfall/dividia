package it.nightfall.dividia.bl.quests;

import it.nightfall.dividia.api.events.IQuestEvents;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.api.quests.IQuestManager;
import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.events.adapters.QuestEventsAdapter;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.utility.io.ConfigWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Concrete representation of a Quest Manager
 */
public class QuestManager implements IQuestManager {
	private Player owner;
	private HashSet<String> solvedQuests = new HashSet<>();
	private Map<String, IQuest> activeQuests = new HashMap<>();
	private IQuestEvents interestedEvents;
	private Collection<IDialog> activeDialogs = null;
	private Collection<IQuestDefinition> availableQuests = null;
	

	/**
	 * Build the quest manager and set the owner to the given player
	 *
	 * @param player Owner
	 */
	public QuestManager(Player player){
		owner = player;
		interestedEvents = new QuestEventsAdapter() {
			@Override
			public void questCompleted(IQuest quest){
				if(quest.getOwner().equals(owner)) {
					solvedQuests.add(quest.getQuestInfo().getName());
					activeQuests.remove(quest.getQuestInfo().getName());
					getQuestRewards(quest);
				}
			}
			
			@Override
            public void questActivated(IQuest quest){
				if(quest.getOwner().equals(owner)) {
					activeQuests.put(quest.getQuestInfo().getName(), quest);
				}
            }
		};
		EventManager.getInstance().addListener(interestedEvents);
	}

	@Override
	public Map<String, IQuest> getActiveQuests(){
		return Collections.unmodifiableMap(activeQuests);
	}

	@Override
	public Player getOwner(){
		return owner;
	}

	@Override
	public Set<String> getSolvedQuests(){
		return Collections.unmodifiableSet(solvedQuests);
	}

	@Override
	public void activateQuestManager(){
		EventManager.getInstance().addListener(interestedEvents);
	}

	@Override
	public void loadState(IConfigReader saveState){
		solvedQuests.addAll(saveState.getConfigReader("solvedQuests").getStringList("quest"));
		Collection<IConfigReader> tempList2 = saveState.getConfigReader("activeQuests").getConfigReaderList("quest");
		String questName;
		for(IConfigReader quest : tempList2){
			questName = quest.getString("name");
			IQuest tempQuest = GameStorage.getInstance().getQuestDefinition(questName).getQuestFromDefinition();
			tempQuest.loadState(quest);
			activeQuests.put(questName, tempQuest);
			tempQuest.activateQuest(owner);
		}
	}

	@Override
	public void saveState(IConfigWriter saveState){
		IConfigWriter configNode1 = new ConfigWriter("solvedQuests", saveState);
		IConfigWriter configNode2 = new ConfigWriter("activeQuests", saveState);
		configNode1.appendStringList("quest", solvedQuests);
		saveState.appendConfigWriter(configNode1);
		Collection<IConfigWriter> tempList = new ArrayList<>();
		IConfigWriter savingQuest;
		for(IQuest quest : activeQuests.values()){
			savingQuest = new ConfigWriter("quest", configNode2);
			quest.saveState(savingQuest);
			tempList.add(savingQuest);
		}
		configNode2.appendConfigWriterList(tempList);
		saveState.appendConfigWriter(configNode2);
	}

	@Override
	public boolean isGoalActive(String goalName){
		for(IQuest quest : activeQuests.values()){
			if(quest.isGoalActive(goalName)){
				return true;
			}
		}
		return false;
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		ISummary activeQuestsSummary = new Summary();
		for(Map.Entry<String, IQuest> questData : activeQuests.entrySet()){
			activeQuestsSummary.putData(questData.getKey(), questData.getValue().getSummary());
		}
		tempSummary.putData("solvedQuests", solvedQuests);
		tempSummary.putData("activeQuests", activeQuestsSummary);
		return tempSummary;
	}	
	
	private void getQuestRewards(IQuest quest) {
		List<IItemDefinition> rewards = quest.getQuestInfo().getRewards();
		for(IItemDefinition singleReward : rewards) {
			IItem item = singleReward.getItemFromDefinition();
			item.pickUp(owner);
		}
	}

	public void setActiveDialogs(Collection<IDialog> activeDialogs) {
		this.activeDialogs = activeDialogs;
	}

	public Collection<IDialog> getActiveDialogs() {
		return activeDialogs;
	}
	
	public boolean isHavingADialog() {
		return activeDialogs != null;
	}

	public void dialogChoosen(String dialogId) {
		IDialog dialog = GameStorage.getInstance().getDialog(dialogId);
		if(dialog!=null) {
			if(activeDialogs.contains(dialog)) {
				EventManager.getInstance().dialogStarted(dialogId, owner);
			}
		}		
	}
	
	public void dialogCompleted(String dialogId) {
		IDialog dialog = GameStorage.getInstance().getDialog(dialogId);
		if(dialog!=null) {
			if(activeDialogs.contains(dialog)) {
				activeDialogs = null;
				EventManager.getInstance().dialogSuccessfullyCompleted(dialogId, owner);
				for(IQuestDefinition questDefinition: availableQuests) {
					if(questDefinition.getActivationDialog().equals(dialog)) {
						IQuest quest = new Quest(questDefinition);
						quest.activateQuest(owner);
					}
				}
				availableQuests = null;
			}
		}	
	}

	public void setAvailableQuests(Collection<IQuestDefinition> availableQuests) {
		this.availableQuests = availableQuests;
	}
	
}
