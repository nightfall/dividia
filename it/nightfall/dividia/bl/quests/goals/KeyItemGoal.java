package it.nightfall.dividia.bl.quests.goals;

import it.nightfall.dividia.api.events.IPlayerEvents;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.IKeyItemGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IKeyItemGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.events.adapters.PlayerEventsAdapter;
import it.nightfall.dividia.bl.quests.goals.definitions.KeyItemGoalDefinition;
import it.nightfall.dividia.bl.utility.Summary;

/**
 * Concrete representation of an active KeyItem goal
 */
public class KeyItemGoal implements IKeyItemGoal {
	private IKeyItemGoalDefinition goalInfo;
	private IQuest quest;
	private int itemsPicked = 0;
	private IPlayerEvents interestedEvents;

	/**
	 * Create the correct listener, initialise variables and stores goal definition
	 * 
	 * @param goalDefinition KeyItemGoal definition
	 */
	public KeyItemGoal(KeyItemGoalDefinition goalDefinition){
		goalInfo = goalDefinition;
		interestedEvents = new PlayerEventsAdapter() {
			@Override
			public void itemPicked(IItem item, Player player){
				if(quest.getOwner() == player){
					checkPickedItem(item);
				}
			}
		};
	}

	@Override
	public void activateGoal(IQuest quest){
		this.quest = quest;
		EventManager.getInstance().addListener(interestedEvents);
		EventManager.getInstance().goalActivated(this);
	}

	@Override
	public IQuest getQuest(){
		return quest;
	}

	@Override
	public IKeyItemGoalDefinition getGoalInfo(){
		return goalInfo;
	}

	@Override
	public int getItemsPicked(){
		return itemsPicked;
	}

	private void checkPickedItem(IItem item){
		if(item.getItemDefinition().getName().equals(goalInfo.getRequiredItem())){
			itemsPicked++;
			EventManager.getInstance().goalUpdated(this);
			if(itemsPicked == goalInfo.getTotalItemsRequired()){
				EventManager.getInstance().goalCompleted(this);
				EventManager.getInstance().removeListener(interestedEvents);
				quest.targetSolved(this);
			}
		}
	}

	@Override
	public void loadState(IConfigReader saveState){
		itemsPicked = saveState.getInteger("itemsPicked");
	}

	@Override
	public void saveState(IConfigWriter saveState){
		saveState.appendString("name", goalInfo.getName());
		saveState.appendInteger("itemsPicked", itemsPicked);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		tempSummary.putData("itemsPicked", itemsPicked);
		return tempSummary;
	}
}
