package it.nightfall.dividia.bl.quests.goals;

import it.nightfall.dividia.api.events.IMonsterEvents;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.IMultiMonsterKillGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IMultiMonsterKillGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.events.adapters.MonsterEventsAdapter;
import it.nightfall.dividia.bl.quests.goals.definitions.MultiMonsterKillGoalDefinition;
import it.nightfall.dividia.bl.utility.Summary;

/**
 * Concrete representation of an active MultiMonsterKill goal
 */
public class MultiMonsterKillGoal implements IMultiMonsterKillGoal {
	private IMultiMonsterKillGoalDefinition goalInfo;
	private IQuest quest;
	private int monsterKills = 0;
	private IMonsterEvents interestedEvents;

	/**
	 * Create the correct listener, initialize variables and stores goal definition
	 *
	 * @param goalDefinition MultiMonsterKillGoal definition
	 */
	public MultiMonsterKillGoal(MultiMonsterKillGoalDefinition goalDefinition){
		goalInfo = goalDefinition;
		interestedEvents = new MonsterEventsAdapter() {
			@Override
			public void monsterDied(Monster monster, Player killer){
				if(quest.getOwner() == killer){
					monsterKilled(monster);
				}
			}
		};
	}

	@Override
	public void activateGoal(IQuest quest){
		this.quest = quest;
		EventManager.getInstance().addListener(interestedEvents);
		EventManager.getInstance().goalActivated(this);
	}

	@Override
	public IQuest getQuest(){
		return quest;
	}

	@Override
	public IMultiMonsterKillGoalDefinition getGoalInfo(){
		return goalInfo;
	}

	@Override
	public int getMonsterKills(){
		return monsterKills;
	}

	private void monsterKilled(Monster monster){
		if(monster.getDisplayName().equals(goalInfo.getMonsterName())){
			monsterKills++;
			EventManager.getInstance().goalUpdated(this);
			if(monsterKills == goalInfo.getTotalKillsRequired()){
				EventManager.getInstance().goalCompleted(this);
				EventManager.getInstance().removeListener(interestedEvents);
				quest.targetSolved(this);
			}
		}
	}

	@Override
	public void loadState(IConfigReader saveState){
		monsterKills = saveState.getInteger("monsterKills");
	}

	@Override
	public void saveState(IConfigWriter saveState){
		saveState.appendString("name", goalInfo.getName());
		saveState.appendInteger("monsterKills", monsterKills);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		tempSummary.putData("monsterKills", monsterKills);
		return tempSummary;
	}
}
