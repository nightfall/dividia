package it.nightfall.dividia.bl.quests.goals;

import it.nightfall.dividia.api.events.IMonsterEvents;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.IMultiMonsterRaceKillGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IMultiMonsterRaceKillGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.events.adapters.MonsterEventsAdapter;
import it.nightfall.dividia.bl.quests.goals.definitions.MultiMonsterRaceKillGoalDefinition;
import it.nightfall.dividia.bl.utility.Summary;

/**
 * Concrete representation of an active MultiMonsterRaceKill goal
 */
public class MultiMonsterRaceKillGoal implements IMultiMonsterRaceKillGoal {
	private IMultiMonsterRaceKillGoalDefinition goalInfo;
	private IQuest quest;
	private int monsterKills = 0;
	private IMonsterEvents interestedEvents;

	/**
	 * Create the correct listener, initialise variables and stores goal definition
	 *
	 * @param goalDefinition MultiMonsterRaceKillGoal definition
	 */
	public MultiMonsterRaceKillGoal(MultiMonsterRaceKillGoalDefinition goalDefinition){
		goalInfo = goalDefinition;
		interestedEvents = new MonsterEventsAdapter() {
			@Override
			public void monsterDied(Monster monster, Player killer){
				if(quest.getOwner() == killer){
					monsterKilled(monster);
				}
			}
		};
	}

	@Override
	public void activateGoal(IQuest quest){
		this.quest = quest;
		EventManager.getInstance().addListener(interestedEvents);
		EventManager.getInstance().goalActivated(this);
	}

	@Override
	public IQuest getQuest(){
		return quest;
	}

	@Override
	public IMultiMonsterRaceKillGoalDefinition getGoalInfo(){
		return goalInfo;
	}

	@Override
	public int getMonsterKills(){
		return monsterKills;
	}

	private void monsterKilled(Monster monster){
		String killedRace = monster.getRace().getName();
		if(killedRace.equals(goalInfo.getMonsterRaceName())){
			monsterKills++;
			EventManager.getInstance().goalUpdated(this);
			if(monsterKills == goalInfo.getTotalKillsRequired()){
				EventManager.getInstance().goalCompleted(this);
				quest.targetSolved(this);
			}
		}
	}

	@Override
	public void loadState(IConfigReader saveState){
		monsterKills = saveState.getInteger("monsterKills");
	}

	@Override
	public void saveState(IConfigWriter saveState){
		saveState.appendString("name", goalInfo.getName());
		saveState.appendInteger("monsterKills", monsterKills);
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		tempSummary.putData("monsterKills", monsterKills);
		return tempSummary;
	}
}
