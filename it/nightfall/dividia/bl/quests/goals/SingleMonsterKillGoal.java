package it.nightfall.dividia.bl.quests.goals;

import it.nightfall.dividia.api.events.IMonsterEvents;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.ISingleMonsterKillGoal;
import it.nightfall.dividia.api.quests.goals.definitions.ISingleMonsterKillGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.events.adapters.MonsterEventsAdapter;
import it.nightfall.dividia.bl.quests.goals.definitions.SingleMonsterKillGoalDefinition;
import it.nightfall.dividia.bl.utility.Summary;

/**
 * Concrete representation of an active SingleMonsterKill goal
 */
public class SingleMonsterKillGoal implements ISingleMonsterKillGoal {
	private ISingleMonsterKillGoalDefinition goalInfo;
	private IQuest quest;
	private IMonsterEvents interestedEventsListener;

	/**
	 * Create the correct listener, initialise variables and stores goal definition
	 *
	 * @param goalDefinition SingleMonsterKillGoal definition
	 */
	public SingleMonsterKillGoal(SingleMonsterKillGoalDefinition goalDefinition){
		goalInfo = goalDefinition;
		interestedEventsListener = new MonsterEventsAdapter() {
			@Override
			public void monsterDied(Monster monster, Player killer){
				if(quest.getOwner() == killer){
					monsterKilled(monster);
				}
			}
		};
	}

	@Override
	public void activateGoal(IQuest quest){
		this.quest = quest;
		EventManager.getInstance().addListener(interestedEventsListener);
		EventManager.getInstance().goalActivated(this);
	}

	@Override
	public IQuest getQuest(){
		return quest;
	}

	@Override
	public ISingleMonsterKillGoalDefinition getGoalInfo(){
		return goalInfo;
	}

	private void monsterKilled(Monster monster){
		if(monster.getRace().getName().equals(goalInfo.getMonsterName())){
			//TODO: Generare evento per i reward
			EventManager.getInstance().goalUpdated(this);
			EventManager.getInstance().goalCompleted(this);
			EventManager.getInstance().addSingleCallListener(interestedEventsListener);
			quest.targetSolved(this);
		}
	}

	@Override
	public void loadState(IConfigReader saveState){
	}

	@Override
	public void saveState(IConfigWriter saveState){
		saveState.appendString("name", goalInfo.getName());
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		return tempSummary;
	}
}
