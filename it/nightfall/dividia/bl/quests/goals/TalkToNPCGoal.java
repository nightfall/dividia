package it.nightfall.dividia.bl.quests.goals;

import it.nightfall.dividia.api.events.IPlayerEvents;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.IQuest;
import it.nightfall.dividia.api.quests.goals.ITalkToNPCGoal;
import it.nightfall.dividia.api.quests.goals.definitions.ITalkToNPCGoalDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.events.adapters.PlayerEventsAdapter;
import it.nightfall.dividia.bl.quests.goals.definitions.TalkToNPCGoalDefinition;
import it.nightfall.dividia.bl.utility.Summary;

/**
 * Concrete representation of an active TalkToNPC goal
 */
public class TalkToNPCGoal implements ITalkToNPCGoal {
	private TalkToNPCGoalDefinition goalInfo;
	private IQuest quest;
	private IPlayerEvents interestedEventsListener;

	/**
	 * Create the correct listener, initialise variables and stores goal definition
	 *
	 * @param goalDefinition TalkToNPCGoal definition
	 */
	public TalkToNPCGoal(TalkToNPCGoalDefinition goalDefinition){
		goalInfo = goalDefinition;
		interestedEventsListener = new PlayerEventsAdapter() {
			@Override
			public void dialogSuccessfullyCompleted(String dialogID, Player player){
				if(quest.getOwner() == player){
					dialogCompleted(dialogID);
				}
			}
		};
	}

	@Override
	public void activateGoal(IQuest quest){
		this.quest = quest;
		EventManager.getInstance().addListener(interestedEventsListener);
		EventManager.getInstance().goalActivated(this);
	}

	@Override
	public IQuest getQuest(){
		return quest;
	}

	@Override
	public ITalkToNPCGoalDefinition getGoalInfo(){
		return goalInfo;
	}

	@Override
	public void loadState(IConfigReader saveState){
	}

	@Override
	public void saveState(IConfigWriter saveState){
		saveState.appendString("name", goalInfo.getName());
	}

	private void dialogCompleted(String dialogIdentifier){
		if(dialogIdentifier.equals(goalInfo.getLinkedDialog().getId())){
			EventManager.getInstance().goalUpdated(this);
			EventManager.getInstance().goalCompleted(this);
			EventManager.getInstance().addSingleCallListener(interestedEventsListener);
			for(IItemDefinition item : goalInfo.getRequiredItems()) {
				quest.getOwner().getInventory().removeKeyItemFromDefinition(item);
			}
			quest.targetSolved(this);
		}
	}

	@Override
	public ISummary getSummary(){
		ISummary tempSummary = new Summary();
		return tempSummary;
	}
}
