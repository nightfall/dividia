package it.nightfall.dividia.bl.quests.goals.definitions;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.goals.IGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IGoalDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.world.Teleport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Abstract implementation of a generic Goal definition
 */
public abstract class AbstractGoalDefinition implements IGoalDefinition {
	private final String name;
	private final String displayName;
	private final String description;
	private final boolean visibleToPlayer;
	private final Set<String> requirements;
	private final List<IItemDefinition> rewards = new LinkedList<>();
	private Teleport teleportAfterGoal = null;

	/**
	 * Read basic info about the definition from the IConfigReader node
	 *
	 * @param config IConfigReader node
	 */
	public AbstractGoalDefinition(IConfigReader config){
		name = config.getString("name");
		displayName = config.getString("displayName");
		description = config.getString("description");
		visibleToPlayer = config.getBoolean("visibleToPlayer");
		requirements = new HashSet<>(config.getConfigReader("requirements").getStringList("goal"));
		Collection<String>rewardIds = config.getConfigReader("rewards").getStringList("reward");
		for(String singleReward:rewardIds) {
			rewards.add(GameStorage.getInstance().getItemDefinition(singleReward));
		}
		if(config.doesElementExist("teleportAfterGoal")) {
			IConfigReader teleportReader = config.getConfigReader("teleportAfterGoal");
			teleportAfterGoal = new Teleport(teleportReader);
		}
	}

	@Override
	public String getName(){
		return name;
	}

	@Override
	public String getDescription(){
		return description;
	}

	@Override
	public String getDisplayName(){
		return displayName;
	}

	@Override
	public Set<String> getRequirements(){
		return Collections.unmodifiableSet(requirements);
	}
	
	@Override
	public List<IItemDefinition> getRewards(){
		return Collections.unmodifiableList(rewards);
	}

	@Override
	public boolean isVisibleToPlayer(){
		return visibleToPlayer;
	}

	@Override
	public boolean meetsRequirements(Set<String> completedGoals){
		if(requirements.isEmpty()){
			return true;
		}
		return completedGoals.containsAll(requirements);
	}
	
	@Override
	public Teleport getTeleportAfterGoal() {
		return teleportAfterGoal;
	}
	
	

	@Override
	public abstract IGoal getGoalFromDefinition();
}
