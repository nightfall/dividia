package it.nightfall.dividia.bl.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.IKeyItemGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IKeyItemGoalDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.quests.goals.KeyItemGoal;

/**
 * Implementation of an IKeyItemGoalDefinition, this class stores information
 * about it and can create a new instance of a KeyItemGoal from its data
 */
public class KeyItemGoalDefinition extends AbstractGoalDefinition implements IKeyItemGoalDefinition {
	private final String requiredItem;
	private final int totalItemsRequired;

	/**
	 * Loads info about the goal from an IConfigReader node
	 *
	 * @param config IConfigReader node
	 */
	public KeyItemGoalDefinition(IConfigReader config){
		super(config);
		requiredItem = config.getString("requiredItem");
		totalItemsRequired = config.getInteger("totalItemsRequired");
	}

	@Override
	public String getRequiredItem(){
		return requiredItem;
	}

	@Override
	public int getTotalItemsRequired(){
		return totalItemsRequired;
	}

	@Override
	public IKeyItemGoal getGoalFromDefinition(){
		return new KeyItemGoal(this);
	}
}
