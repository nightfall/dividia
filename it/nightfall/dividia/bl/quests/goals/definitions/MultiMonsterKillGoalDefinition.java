package it.nightfall.dividia.bl.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.IMultiMonsterKillGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IMultiMonsterKillGoalDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.quests.goals.MultiMonsterKillGoal;

/**
 * Implementation of an IMultiMonsterKillGoalDefinition, this class stores information
 * about it and can create a new instance of a MultiMonsterKillGoal from its data
 */
public class MultiMonsterKillGoalDefinition extends AbstractGoalDefinition implements IMultiMonsterKillGoalDefinition {
	private final String monsterName;
	private final int totalKillsRequired;

	/**
	 * Loads info about the goal from an IConfigReader node
	 *
	 * @param config IConfigReader node
	 */
	public MultiMonsterKillGoalDefinition(IConfigReader config){
		super(config);
		monsterName = config.getString("monsterName");
		totalKillsRequired = config.getInteger("totalKillsRequired");
	}

	@Override
	public String getMonsterName(){
		return monsterName;
	}

	@Override
	public int getTotalKillsRequired(){
		return totalKillsRequired;
	}

	@Override
	public IMultiMonsterKillGoal getGoalFromDefinition(){
		return new MultiMonsterKillGoal(this);
	}
}
