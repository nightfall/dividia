package it.nightfall.dividia.bl.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.IMultiMonsterRaceKillGoal;
import it.nightfall.dividia.api.quests.goals.definitions.IMultiMonsterRaceKillGoalDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.quests.goals.MultiMonsterRaceKillGoal;

/**
 * Implementation of an IMultiMonsterRaceKillGoalDefinition, this class stores information
 * about it and can create a new instance of a MultiMonsterRaceKillGoal from its data
 */
public class MultiMonsterRaceKillGoalDefinition extends AbstractGoalDefinition implements IMultiMonsterRaceKillGoalDefinition {
	private final String monsterRaceName;
	private final int totalKillsRequired;

	/**
	 * Loads info about the goal from an IConfigReader node
	 *
	 * @param config IConfigReader node
	 */
	public MultiMonsterRaceKillGoalDefinition(IConfigReader config){
		super(config);
		monsterRaceName = config.getString("monsterRaceName");
		totalKillsRequired = config.getInteger("totalKillsRequired");
	}

	@Override
	public int getTotalKillsRequired(){
		return totalKillsRequired;
	}

	@Override
	public String getMonsterRaceName(){
		return monsterRaceName;
	}

	@Override
	public IMultiMonsterRaceKillGoal getGoalFromDefinition(){
		return new MultiMonsterRaceKillGoal(this);
	}
}
