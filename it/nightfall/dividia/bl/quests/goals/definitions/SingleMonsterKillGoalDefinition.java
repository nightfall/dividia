package it.nightfall.dividia.bl.quests.goals.definitions;

import it.nightfall.dividia.api.quests.goals.ISingleMonsterKillGoal;
import it.nightfall.dividia.api.quests.goals.definitions.ISingleMonsterKillGoalDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.quests.goals.SingleMonsterKillGoal;

/**
 * Implementation of an ISingleMonsterKillGoalDefinition, this class stores information
 * about it and can create a new instance of a SingleMonsterKillGoal from its data
 */
public class SingleMonsterKillGoalDefinition extends AbstractGoalDefinition implements ISingleMonsterKillGoalDefinition {
	private final String monsterName;
	

	/**
	 * Loads info about the goal from an IConfigReader node
	 *
	 * @param config IConfigReader node
	 */
	public SingleMonsterKillGoalDefinition(IConfigReader config){
		super(config);
		monsterName = config.getString("monsterName");
		
	}

	@Override
	public String getMonsterName(){
		return monsterName;
	}

	

	@Override
	public ISingleMonsterKillGoal getGoalFromDefinition(){
		return new SingleMonsterKillGoal(this);
	}
}
