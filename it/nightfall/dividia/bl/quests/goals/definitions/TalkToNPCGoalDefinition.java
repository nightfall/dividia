package it.nightfall.dividia.bl.quests.goals.definitions;

import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.quests.goals.ITalkToNPCGoal;
import it.nightfall.dividia.api.quests.goals.definitions.ITalkToNPCGoalDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterRaceDefinition;
import it.nightfall.dividia.bl.quests.goals.TalkToNPCGoal;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Implementation of an ITalkToNPCGoalDefinition, this class stores information
 * about it and can create a new instance of a TalkToNPCGoal from its data
 */
public class TalkToNPCGoalDefinition extends AbstractGoalDefinition implements ITalkToNPCGoalDefinition {
	private IDialog npcDialog;
	private String npc;
	private Collection<IItemDefinition> requiredItems = new LinkedList<>();
	
	/**
	 * Loads info about the goal from an IConfigReader node
	 *
	 * @param config IConfigReader node
	 */
	public TalkToNPCGoalDefinition(IConfigReader config){
		super(config);
		npcDialog = GameStorage.getInstance().getDialog(config.getString("npcDialog"));
		Collection<String> itemsId = config.getConfigReader("requiredItems").getStringList("item");
		for(String itemId:itemsId) {
			requiredItems.add(GameStorage.getInstance().getItemDefinition(itemId));
		}
		npc = config.getString("npc");
	}

	@Override
	public ITalkToNPCGoal getGoalFromDefinition(){
		return new TalkToNPCGoal(this);
	}

	@Override
	public IDialog getLinkedDialog(){
		return npcDialog;
	}
	
	@Override
	public Collection<IItemDefinition> getRequiredItems() {
		return requiredItems;
	}

	@Override
	public String getNpc() {
		return npc;
	}
	
	
}
