package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.utility.io.IConfigReader;

public class AlteredStatusSuccessRate {
	private final IAlteredStatusDefinition alteredStatus;
	private final double successRate;

	public AlteredStatusSuccessRate(IConfigReader config){
		successRate = config.getDouble("successRate");
		alteredStatus = GameStorage.getInstance().getAlteredStatusDefinition(config.getString("alteredStatus"));
	}

	public IAlteredStatusDefinition getAlteredStatus(){
		return alteredStatus;
	}

	public double getSuccessRate(){
		return successRate;
	}
}
