package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.IClockListener;
import java.util.ArrayList;
import java.util.Collection;

public class ClockGenerator implements Runnable {
	private Collection<IClockListener> listeners = new ArrayList<>();
	private boolean keepRunning = true;
	private int updateMs;

	public ClockGenerator(int updateMs) {
		this.updateMs = updateMs;
	}
	
	public void addListener(IClockListener clockListener){
		listeners.add(clockListener);
	}

	public void removeListener(IClockListener clockListener){
		listeners.remove(clockListener);
	}

	public void stop(){
		keepRunning = false;
	}
	
	@Override
	public void run(){
		long lastTick = System.nanoTime(), tempTickDifference, lastWait;
		while(keepRunning){
			tempTickDifference = System.nanoTime() - lastTick;
			if(tempTickDifference < updateMs * 1000000){
				try{
					//TODO gestire un eventuale differenza negativa
					lastWait = updateMs - tempTickDifference / 1000000;
					Thread.sleep(lastWait);
				}catch(InterruptedException ex){
					ex.printStackTrace();
				}
			}
			tempTickDifference = System.nanoTime() - lastTick;
			lastTick = System.nanoTime();
			for(IClockListener listener : listeners){
				listener.tick((int) tempTickDifference);
			}
		}
	}
}
