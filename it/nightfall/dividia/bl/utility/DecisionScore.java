package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.ai.IDecision;
import it.nightfall.dividia.api.utility.IDecisionScore;
import java.util.LinkedList;
import java.util.List;

public class DecisionScore implements IDecisionScore{

	private List<IDecision> decisionList;
	private double score;

	public DecisionScore(List<IDecision> decisionList, double score) {
		this.decisionList = decisionList;
		this.score = score;
	}
	
	public DecisionScore(IDecision decision, double score) {
		this.decisionList = new LinkedList<>();
		decisionList.add(decision);
		this.score = score;
	}
	
	public DecisionScore() {
		this.decisionList = new LinkedList<>();
		this.score = 0;
	}

	@Override
	public List<IDecision> getDecisionList() {
		return decisionList;
	}
	
	@Override
	public double getScore(){
		return score;
	}

	@Override
	public void appendDecisions(IDecisionScore followingDecision) {
		this.decisionList.addAll(followingDecision.getDecisionList());
		this.score += followingDecision.getScore();
		
	}
}
