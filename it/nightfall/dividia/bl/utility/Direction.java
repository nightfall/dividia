package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;

public final class Direction implements IDirection {
	
	private double directionAngle; 	  
	private static double PI = Math.PI;
	
	public Direction(double directionAngle) { 		
        this.directionAngle = SharedFunctions.pi2Modulus(directionAngle);
    }
	
    public Direction(double x, double y) { 		
        directionAngle = getRadianAngle(x,y); 
    }
	
	public Direction(IGamePoint directionPoint) { 
        this(directionPoint.getX(),directionPoint.getY());
    } 
	
	@Override
	public IDirection clone(){
		return new Direction(directionAngle);
	}
   
	@Override
	public double getVersorX() {
		return Math.cos(directionAngle);
    }

	@Override
    public double getVersorY() {
		return Math.sin(directionAngle);
    }
    
    public static double getRadianAngle(double x, double y) {
        if(y>=0 && x==0) {
			return Math.PI / 2;
		}
        
        if(y<0 && x==0) {
			return (3d/2) * Math.PI;
		}
        
        double atan=Math.atan(Math.abs(y) * 1.0 / Math.abs(x));
  
        if(x>=0 && y>=0) {
			return atan;
		}
        
        if(x<0 && y>=0) {
			return Math.PI - atan;
		}
        
        if(x<0 && y<=0) {
			return Math.PI + atan;
		}
        
        if(x>0 && y<=0) {
			return 2 * Math.PI - atan;
		}
        
        return 0;
    }

	@Override
	public double getDirectionAngle() {
		return directionAngle;
	}
	
	@Override
    public void switchDirection(IGamePoint directionPoint){
        directionAngle = getRadianAngle(directionPoint.getX(),directionPoint.getY());
    }

	@Override
	public void rotateDirection(double rotationAngle){
        directionAngle = SharedFunctions.pi2Modulus(directionAngle + rotationAngle);
    }
	
	@Override
    public double getDecimalDegreeAngle(){
        return Math.toDegrees(directionAngle);
    }
	
	@Override
	public double angleBetweenDirections(IDirection d2) {
		return SharedFunctions.pi2Modulus(d2.getDirectionAngle()-this.directionAngle);
	}

	@Override
	public IGamePoint getDistancedPointAlongDirection(IGamePoint referencePoint, double distance) {
		IGamePoint point = new GamePoint(distance * getVersorX(), distance * getVersorY());
		point.translate(referencePoint);
		return point;
		
	}

	@Override
	public IDirection oppositeDirection() {
		return new Direction(Math.PI + this.directionAngle);
	}
        
    public static IDirection getDirectionBetweenPoints(IGamePoint p1, IGamePoint p2) {
		double deltaX = p2.getX() - p1.getX();
		double deltaY = p2.getY() - p1.getY();
		return new Direction(deltaX,deltaY);
	}
        
    @Override
    public String toString() {
        return ("Direction in degrees: " + this.getDecimalDegreeAngle());
    }
	
}
