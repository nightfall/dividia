
package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.events.EventManager;


public class FollowPlanner {
	
	private PlayingCharacter target;
	private Monster follower;

	public FollowPlanner(Monster follower) {
		this.follower = follower;
	}

	public void setTarget(PlayingCharacter target) {
		this.target = target;
	}	
	
	public boolean applyNextApproachStep(int tpf, float fixedDistance) {
		IGamePoint currentPosition = follower.getPosition();
		IGamePoint destination = target.getPosition();
		IDirection nextDirection = SharedFunctions.getDirectionBetweenPoints(currentPosition,destination);
		double distance = currentPosition.getDistanceFromAnotherPoint(destination);
		
		if(fixedDistance<0 && fixedDistance>=-distance  || fixedDistance>=0 && fixedDistance>=distance) {
			EventManager.getInstance().characterStopped(follower);
			return true;			
		}
		if(fixedDistance<0) {
			nextDirection = nextDirection.oppositeDirection();
		}
		follower.setDirection(nextDirection);
		follower.updatePosition(tpf);
		EventManager.getInstance().characterMoved(follower);
		
		return false;
	}

}
