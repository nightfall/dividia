package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.ICallbackWaiting;
import it.nightfall.dividia.api.utility.IFutureCallback;

public class FutureCallback implements IFutureCallback {
	private ICallbackWaiting waitingObject;
	private boolean resetCycle = false;
	private int cycleLength = 0;
	
	public FutureCallback(ICallbackWaiting needsToWait){
		waitingObject = needsToWait;
	}

	@Override
	public synchronized void runAndWait(int time){
		cycleLength = time;
		Thread futureCallback = new Thread(this);
		futureCallback.setName("Future Callback");
		futureCallback.start();
	}

	@Override
	public synchronized void resetCycle(){
		resetCycle = true;
	}

	@Override
	public synchronized void updateNextWaitCycle(int time){
		cycleLength = time;
		resetCycle();
	}

	@Override
	public void run(){
		boolean validCycle = true;
		while(validCycle){
			try{
				Thread.sleep(cycleLength);
			}catch(InterruptedException ex){
			}
			synchronized(this){
				if(!resetCycle){
					validCycle = !waitingObject.execute();
				}else{
					validCycle = true;
				}
			}
		}
	}
}
