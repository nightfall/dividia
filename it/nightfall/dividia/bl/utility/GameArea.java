package it.nightfall.dividia.bl.utility;

import com.sun.org.apache.bcel.internal.classfile.Code;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class GameArea implements IGameArea {
    private Area area;
	ArrayList<IGamePoint> perimeter = new ArrayList<>();
    
	public GameArea(IConfigReader config) {
		Collection<IConfigReader> points = config.getConfigReaderList("point");
		List<IGamePoint> pointsList= new LinkedList<>();
		for(IConfigReader c: points) {
			pointsList.add(new GamePoint(c));
		}
		Path2D areaPolygon = new Path2D.Double();
		boolean firstPoint = true;
		for(IGamePoint p: pointsList) {
			if(firstPoint) {
				areaPolygon.moveTo(p.getX(), p.getY());
				firstPoint=false;
			}
			else {
				areaPolygon.lineTo(p.getX(), p.getY());
			}
			perimeter.add(new GamePoint(p.getX(), p.getY()));
		}
		areaPolygon.closePath();
        area=new Area(areaPolygon);
	}
    public GameArea(List<GamePoint> points){
        
        
		Path2D areaPolygon = new Path2D.Double();
		boolean firstPoint = true;
		for(IGamePoint p: points) {
			if(firstPoint) {
				areaPolygon.moveTo(p.getX(), p.getY());
				firstPoint=false;
			}
			else {
				areaPolygon.lineTo(p.getX(), p.getY());
			}
			perimeter.add(new GamePoint(p.getX(), p.getY()));
		}
		areaPolygon.closePath();
        area=new Area(areaPolygon);
    }
	
	public GameArea(double radius){
        
		Ellipse2D.Double circle=new Ellipse2D.Double(0,0,radius*2,radius*2);
		area=new Area(circle);
    }
    
    public GameArea(Area area, ArrayList<IGamePoint> perimeter){        
        this.area=area;
		this.perimeter = perimeter;
    }
	
	 public GameArea(Area area){        
        this.area=area;
    }
    
	@Override
    public Area getArea(){
        return this.area;
    }
    
	@Override
    public boolean contains(IGamePoint p){
        return area.contains(p.getX(), p.getY());        
    }
    
	@Override
    public void translate(IGamePoint vectorPoint){
        AffineTransform translation;
        translation=AffineTransform.getTranslateInstance(vectorPoint.getX(), vectorPoint.getY());
        area.transform(translation);
    }
	
	@Override
	public IGameArea getGameAreaByTranslation(IGamePoint translation){
        IGameArea a = clone();
        a.translate(translation);
        return a;
    }
	
	@Override
	public void rotate(IDirection newDirection) {
		AffineTransform rotation;
		rotation = AffineTransform.getRotateInstance(newDirection.getDirectionAngle());
		area.transform(rotation);
		//TODO class tests
	}

	@Override
	public IGameArea getGameAreaByRotation(IDirection directionAngle) {
		IGameArea a = clone();
		a.rotate(directionAngle);
		return a;
	}

	@Override
	public IGameArea clone() {
		ArrayList<IGamePoint> perimeterClone = (ArrayList<IGamePoint>) perimeter.clone();
		return new GameArea(area.createTransformedArea(new AffineTransform()),perimeterClone);
	}
	
	/**
	 * Check the intersection of two Game Areas.
	 * @param otherArea
	 * @return true if the two input areas has an intersection, false otherwise.
	 */
	@Override
	public boolean intersect (IGameArea otherArea){
		Area cloned = this.clone().getArea();
		cloned.intersect(otherArea.getArea());
		return !cloned.isEmpty();
	}

	@Override
	public ArrayList<IGamePoint> getPerimeter() {
		return perimeter;
	}

}
