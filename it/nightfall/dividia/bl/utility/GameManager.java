package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.network.IDCPServer;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.network.dcp.LoginServer;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.bl.world.MapsManager;
import java.io.File;
import java.security.Security;
import java.util.HashMap;
import java.util.Map;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class GameManager {
	private static GameManager gameManager = new GameManager();
	private Map<String, Player> playersList = new HashMap<>();
	private ClockGenerator clock = new ClockGenerator(IGameConstants.BL_UPDATE_MS);
	private IDCPServer server;
	private boolean inSinglePlayer = true;
	
	public static GameManager getInstance(){
		return gameManager;
	}

	public void addPlayer(Player player){
		playersList.put(player.getId(), player);
		clock.addListener(player.getMovementManager());
	}

	public void removePlayer(Player player){
		playersList.remove(player.getId());
		clock.removeListener(player.getMovementManager());
		//TODO EventManager.playerExitMap(player [...]);
	}

	public Player getPlayer(String username){
		return playersList.get(username);
	}

	public void startEngine(){
		Thread clockThread = new Thread(clock);
		clockThread.setName("BL Clock");
		clockThread.start();
		MapsManager.getInstance();
	}

	public void startServer(){
		inSinglePlayer = false;
		IConfigReader serverConfig = new ConfigReader(SharedFunctions.getPath(".|server|config.xml"));
		Security.addProvider(new BouncyCastleProvider());
		System.setProperty("java.rmi.server.hostname", serverConfig.getString("ip"));
		try{
			server = LoginServer.getInstance();
			Thread loginServerThread = new Thread(server);
			loginServerThread.setName("Login Server");
			loginServerThread.start();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public void stopEngine(){
		clock.stop();
	}
	
	public void stopServer(){
		
	}
	public ISummary getSaveGamesSummary(String player){
		String playerDir = IGameConstants.SERVER_SAVEGAMES_DIRECTORY + SharedFunctions.getPath(player + "|");
		for(int i = 0; i < 4; i++){
			File f = new File(playerDir + i + ".xml");
			if(f.exists()){

			}			
		}
		return null;
	}

	public boolean isInSinglePlayer(){
		return inSinglePlayer;
	}

	public ClockGenerator getClock() {
		return clock;
	}
	
	
	
}
