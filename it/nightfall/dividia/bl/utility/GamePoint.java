package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;

public class GamePoint implements IGamePoint{
    
    private double x;
    private double y;
	private final static IGamePoint originPoint = new GamePoint(0,0);
    
	
	public GamePoint(IConfigReader config) {
		x = config.getDouble("x");
		y = config.getDouble("y");
		
	}
	
    public GamePoint(double x, double y){
        this.x=x;
        this.y=y;
    }

	@Override
    public double getX() {
        return x;
    }

	@Override
    public double getY() {
        return y;
    }
	
	@Override
    public void translate(double deltaX, double deltaY){
        x=x+deltaX;
        y=y+deltaY;
    }
	
	@Override
	public double getDistanceFromAnotherPoint(IGamePoint p2){
		double deltaX=this.x - p2.getX();
		double deltaY=this.y - p2.getY();
        return Math.sqrt(deltaX*deltaX + deltaY*deltaY);
    }	
	
	@Override
	public GamePoint clone(){
		return new GamePoint(this.x, this.y);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final GamePoint other = (GamePoint) obj;
		if (this.x != other.x) {
			return false;
		}
		if (this.y != other.y) {
			return false;
		}
		return true;
	}

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 43 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
            hash = 43 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
            return hash;
        }

        
	@Override
	public void translate(IGamePoint vector) {
		translate(vector.getX(), vector.getY());
	}
        
	@Override 
    public String toString() {
        return ("Point: " + x +" " + y);
    }
	
	@Override 
    public void rotatePoint(IDirection rotateDirection) {
		IDirection nextDirection = new Direction(Direction.getRadianAngle(this.x, this.y) + rotateDirection.getDirectionAngle());
		IGamePoint destinationPoint = nextDirection.getDistancedPointAlongDirection(originPoint, this.pointVectorLength());
		this.x = destinationPoint.getX();
		this.y = destinationPoint.getY();
    }
	
	private double pointVectorLength() {
		return Math.sqrt(x*x + y*y);
	}
	
	@Override
	public IGamePoint getPointFromTranslation (double offset){
		return new GamePoint(x+offset,y+offset);
	}
}
