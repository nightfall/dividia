package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.battle.definitions.ISkillDefinition;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.item.definitions.IItemDefinition;
import it.nightfall.dividia.api.item.definitions.IKnapsackDefinition;
import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.api.quests.goals.definitions.IGoalDefinition;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGameStorage;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.battle.definitions.AreaSpellDefinition;
import it.nightfall.dividia.bl.battle.definitions.FocusedSpellDefinition;
import it.nightfall.dividia.bl.battle.definitions.SkillDefinition;
import it.nightfall.dividia.bl.battle.definitions.TargetAreaSpellDefinition;
import it.nightfall.dividia.bl.battle.definitions.TargetSpellDefinition;
import it.nightfall.dividia.bl.characters.definitions.AlteredStatusDefinition;
import it.nightfall.dividia.bl.characters.definitions.MonsterRaceDefinition;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterRaceDefinition;
import it.nightfall.dividia.bl.characters.definitions.RaceDefinition;
import it.nightfall.dividia.bl.item.definitions.DevaOrbDefinition;
import it.nightfall.dividia.bl.item.definitions.KeyItemDefinition;
import it.nightfall.dividia.bl.item.definitions.KnapsackDefinition;
import it.nightfall.dividia.bl.item.definitions.MysticalSphereDefinition;
import it.nightfall.dividia.bl.item.definitions.VialDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.ArmorDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.BootsDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.GlovesDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.HelmetDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.NecklaceDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.RingDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.ShieldDefinition;
import it.nightfall.dividia.bl.item.equipment_items.definitions.WeaponDefinition;
import it.nightfall.dividia.bl.quests.QuestDefinition;
import it.nightfall.dividia.bl.quests.goals.definitions.KeyItemGoalDefinition;
import it.nightfall.dividia.bl.quests.goals.definitions.MultiMonsterKillGoalDefinition;
import it.nightfall.dividia.bl.quests.goals.definitions.MultiMonsterRaceKillGoalDefinition;
import it.nightfall.dividia.bl.quests.goals.definitions.SingleMonsterKillGoalDefinition;
import it.nightfall.dividia.bl.quests.goals.definitions.TalkToNPCGoalDefinition;
import it.nightfall.dividia.bl.utility.dialogues.Dialog;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.bl.world.definitions.ArenaGameMapDefinition;
import it.nightfall.dividia.bl.world.definitions.DungeonGameMapDefinition;
import it.nightfall.dividia.bl.world.definitions.GeneralGameMapDefinition;
import it.nightfall.dividia.bl.world.definitions.MonsterGameMapDefinition;
import java.io.File;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class GameStorage implements IGameStorage {
	private static IGameStorage gameStorage = null;
	private Map<String, IAlteredStatusDefinition> alteredStatusDefinitions = new HashMap<>();
	private Map<String, IGoalDefinition> goalDefinitions = new HashMap<>();
	private Map<String, IDialog> dialogs = new HashMap<>();
	private Map<String, IQuestDefinition> questDefinitions = new HashMap<>();
	private Map<String, NonPlayingCharacterRaceDefinition> nonPlayingCharacterRaceDefinitions = new HashMap<>();
	private Map<String, IItemDefinition> itemDefinitions = new HashMap<>();
	private Map<String, ISpellDefinition> spellDefinitions = new HashMap<>();
	private Map<String, ISkillDefinition> skillDefinitions = new HashMap<>();
	private Map<String, IRaceDefinition> raceDefinitions = new HashMap<>();
	private Map<String, IGameMapDefinition> mapDefinitions = new HashMap<>();
	private Map<String, IKnapsackDefinition> knapsackDefinitions = new HashMap<>();

	public static void initStorage(){
		gameStorage = new GameStorage();
		gameStorage.loadStorage();
	}
	public static IGameStorage getInstance(){
		return gameStorage;
	}
	
	@Override
	public IAlteredStatusDefinition getAlteredStatusDefinition(String alteredStatusName){
		return alteredStatusDefinitions.get(alteredStatusName);
	}

	@Override
	public IGoalDefinition getGoalDefinition(String goalName){
		return goalDefinitions.get(goalName);
	}

	@Override
	public IQuestDefinition getQuestDefinition(String questName){
		return questDefinitions.get(questName);
	}

	@Override
	public IItemDefinition getItemDefinition(String itemName){
		return itemDefinitions.get(itemName);
	}

	@Override
	public ISpellDefinition getSpellDefinition(String spellName){
		return spellDefinitions.get(spellName);
	}

	@Override
	public ISkillDefinition getSkillDefinition(String skillName){
		return skillDefinitions.get(skillName);
	}
	
	@Override
	public IDialog getDialog(String dialogId) {
		return dialogs.get(dialogId);
	}

	@Override
	public IRaceDefinition getRaceDefinition(String raceName){
		return raceDefinitions.get(raceName);
	}

	@Override
	public IGameMapDefinition getMapDefinition(String mapName){
		return mapDefinitions.get(mapName);
	}

	@Override
	public IKnapsackDefinition getKnapsackDefinition(String knapsackName){
		return knapsackDefinitions.get(knapsackName);
	}
	
	@Override
	public NonPlayingCharacterRaceDefinition getNonPlayingCharacterRaceDefinition(String raceName){
		return nonPlayingCharacterRaceDefinitions.get(raceName);
	}
	
	@Override
	public void loadStorage(){
		loadDefinitionFromType(KeyItemDefinition.class, "items|key", itemDefinitions);
		loadDefinitionFromType(VialDefinition.class, "items|vial", itemDefinitions);
		loadDefinitionFromType(ArmorDefinition.class, "items|armor", itemDefinitions);
		loadDefinitionFromType(BootsDefinition.class, "items|boots", itemDefinitions);
		loadDefinitionFromType(GlovesDefinition.class, "items|gloves", itemDefinitions);
		loadDefinitionFromType(HelmetDefinition.class, "items|helmet", itemDefinitions);
		loadDefinitionFromType(MysticalSphereDefinition.class, "items|mysticalsphere", itemDefinitions);
		loadDefinitionFromType(NecklaceDefinition.class, "items|necklace", itemDefinitions);
		loadDefinitionFromType(RingDefinition.class, "items|ring", itemDefinitions);
		loadDefinitionFromType(ShieldDefinition.class, "items|shield", itemDefinitions);
		loadDefinitionFromType(AlteredStatusDefinition.class, "alteredstatuses", alteredStatusDefinitions);
		loadDefinitionFromType(WeaponDefinition.class, "items|weapon", itemDefinitions);
		loadDefinitionFromType(DevaOrbDefinition.class, "items|devaorb", itemDefinitions);
		loadDefinitionFromType(Dialog.class, "nonPlayingCharacters|dialogs", dialogs);
		loadDefinitionFromType(SingleMonsterKillGoalDefinition.class, "goals|single", goalDefinitions);
		loadDefinitionFromType(MultiMonsterKillGoalDefinition.class, "goals|multi", goalDefinitions);
		loadDefinitionFromType(MultiMonsterRaceKillGoalDefinition.class, "goals|monster", goalDefinitions);
		loadDefinitionFromType(KeyItemGoalDefinition.class, "goals|keyitem", goalDefinitions);
		loadDefinitionFromType(TalkToNPCGoalDefinition.class, "goals|talktonpc", goalDefinitions);
		loadDefinitionFromType(QuestDefinition.class, "quests", questDefinitions);
		loadDefinitionFromType(NonPlayingCharacterRaceDefinition.class, "nonPlayingCharacters|races", nonPlayingCharacterRaceDefinitions);
		loadDefinitionFromType(SkillDefinition.class, "skills", skillDefinitions);
		loadDefinitionFromType(AreaSpellDefinition.class, "spells|area", spellDefinitions);
		loadDefinitionFromType(FocusedSpellDefinition.class, "spells|focused", spellDefinitions);
		loadDefinitionFromType(TargetSpellDefinition.class, "spells|target", spellDefinitions);
		loadDefinitionFromType(TargetAreaSpellDefinition.class, "spells|targetarea", spellDefinitions);
		loadDefinitionFromType(RaceDefinition.class, "races", raceDefinitions);
		loadDefinitionFromType(MonsterRaceDefinition.class, "races|monsters", raceDefinitions);
		loadDefinitionFromType(ArenaGameMapDefinition.class, "maps|arena", mapDefinitions);
		loadDefinitionFromType(DungeonGameMapDefinition.class, "maps|dungeon", mapDefinitions);
		loadDefinitionFromType(GeneralGameMapDefinition.class, "maps|general", mapDefinitions);
		loadDefinitionFromType(MonsterGameMapDefinition.class, "maps|monster", mapDefinitions);
		loadDefinitionFromType(KnapsackDefinition.class, "items|knapsack", knapsackDefinitions);
		//TODO: Come si caricano gli Utility Items?
	}

	private void loadDefinitionFromType(Class definitionClass, String directory, Map destinationMap){
		String dirPath = IGameConstants.CONFIG_PATH + SharedFunctions.getPath(directory + "|");
		IConfigReader config;
		try{
			Constructor definitionConstructor = definitionClass.getDeclaredConstructor(IConfigReader.class);
			File definitionsDirectory = new File(dirPath);
			File[] files = definitionsDirectory.listFiles();
			if(files.length > 0){
				for(File tempFile : files){
					if(tempFile.isFile()){
						config = new ConfigReader(tempFile.getAbsolutePath());
						destinationMap.put(config.getString("name"), definitionConstructor.newInstance(config));
					}
				}
			}
		}catch(Exception ex){
			//System.out.println(dirPath);
			ex.printStackTrace();
		}
	}

	
}
