package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.IModifiableInteger;

/**
 *
 * @author Nightfall
 */
public class ModifiableInteger implements IModifiableInteger{

	private int value;

	public ModifiableInteger(int value) {
		this.value = value;
	}

	@Override
	public void setValue(int newValue) {
		this.value=newValue;
	}

	@Override
	public int getValue() {
		return this.value;
	}

	@Override
	public void addInt(int delta) {
		this.value+=delta;
	}

	@Override
	public int compare(IModifiableInteger comparating) {
		int compare = comparating.getValue();
		if(this.value > compare) {
			return 1;
		}
		else if(this.value < compare) {
			return -1;
		}
		return 0;
	}

	@Override
	public IModifiableInteger clone() {
		return new ModifiableInteger(value);
	}
	
}
