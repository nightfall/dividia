package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.IMovementPlanner;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.events.EventManager;
import java.util.LinkedList;
import java.util.Queue;

public class MovementPlanner implements IMovementPlanner{

	private Monster monster;
	private Queue<IGamePoint> path;

	public MovementPlanner(Monster monster) {
		this.monster = monster;
		path = new LinkedList<>();
		
	}
	
	
	@Override
	public void applyNextStep(int tpf) {
		
		IGamePoint currentPosition = monster.getPosition();
		IGamePoint partialDestination = path.peek();
		IDirection nextDirection = SharedFunctions.getDirectionBetweenPoints(currentPosition,partialDestination);
		double step = monster.getStep(tpf); //ricalcolata perchè potrebbe cambiare tra un passo e l'altro
		double distance = currentPosition.getDistanceFromAnotherPoint(partialDestination);
		monster.setDirection(nextDirection);
		if(distance <= step) {
			monster.setPosition(partialDestination);
			path.poll();			
		}
		else {
			monster.updatePosition(tpf);
		}
		EventManager.getInstance().characterMoved(monster);
		if(path.isEmpty()) {
			EventManager.getInstance().characterStopped(monster);
		}
		
	}

	@Override
	public void setDestination(IGamePoint destinationPoint) {
		IGamePoint currentPosition = monster.getPosition();
		path.clear();
		pathFinding(currentPosition,destinationPoint);
	}

	
	private void pathFinding(IGamePoint currentPosition, IGamePoint destinationPoint) {
		
		//TODO considerare i punti di visibilità
		path.add(destinationPoint);	
	}

	@Override
	public boolean isPathCompleted() {
		return path.isEmpty();
	}
	
}
