/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.utility;

import java.util.Random;

/**
 *
 * @author Nightfall
 */
public class ProbabilityManager {
	public static boolean hasEventOccured(double chanceToSucced){
		Random randomGenerator=new Random();
		double extractedNumber=randomGenerator.nextDouble();
		return (chanceToSucced >= extractedNumber);
	}
	
}
