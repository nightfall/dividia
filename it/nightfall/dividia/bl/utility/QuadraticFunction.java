package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.IQuadraticFunction;


public class QuadraticFunction implements IQuadraticFunction{

	private double knownTerm;
	private double linearCoefficient;
	private double quadraticCoefficient;

	public QuadraticFunction(double knownTerm, double linearCoefficient, double quadraticCoefficient) {
		this.knownTerm = knownTerm;
		this.linearCoefficient = linearCoefficient;
		this.quadraticCoefficient = quadraticCoefficient;
	}
	
	
	@Override
	public double getFunctionImage(double x) {
		return quadraticCoefficient * x*x + linearCoefficient * x + knownTerm;
	}
	
	@Override
	public int getIntegerFunctionImage(double x) {
		return (int) Math.round(getFunctionImage(x));
	}
	
	@Override
	public int getIntegerFunctionImageVariation(double x1, double x2) {
		return (int) Math.round(getFunctionImage(x2) - getFunctionImage(x1));
	}
	
	
}
