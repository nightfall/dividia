package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.characters.definitions.IAlteredStatusDefinition;
import it.nightfall.dividia.api.network.IConnectionState;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.utility.playerMovements.Movement;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SharedFunctions {
	/**
	 * Get a list the altered statuses witch succed in with their success rates
	 *
	 * @param incomingAlteredStatus : it's a list of the alteredStatus that have
	 * to be processed
	 * @return : list of succeded altered statuses
	 */
	public static List<IAlteredStatusDefinition> getSuccededAlteredStatuses(List<AlteredStatusSuccessRate> incomingAlteredStatus){
		List<IAlteredStatusDefinition> succededAlteredStatus = new ArrayList<>();
		for(AlteredStatusSuccessRate a : incomingAlteredStatus){
			if(ProbabilityManager.hasEventOccured(a.getSuccessRate()/100)){
				succededAlteredStatus.add(a.getAlteredStatus());
			}
		}
		return succededAlteredStatus;
	}
	
	/*
	 * Returns a direction from a 2D movement
	 */
	public static IDirection getDirectionFromMovement2D (Movement movement){
		switch(movement){
				case FORWARD:
					return new Direction(0,1);
				case BACKWARD:
					return new Direction(0, -1);
				case LEFT:
					return new Direction (-1,0);
				case RIGHT:
					return new Direction(1, 0);
			}
		return new Direction(0,0);
	}

	/**
	 * Given the normal damage, this function calculates the final damage to be
	 * inflict adding or subtracting a number between a range.
	 *
	 * @param rawDamage : Raw damage of an attack
	 * @return : Final damage to inflict
	 */
	public static int calculateFinalDamage(int rawDamage){
		double multiplier = 0;
		double range = IGameConstants.MAX_MULTIPLIER_DAMAGE
					   - IGameConstants.MIN_MULTIPLIER_DAMAGE;
		multiplier = (IGameConstants.MIN_MULTIPLIER_DAMAGE + (Math.random() * (range * 2)))
					 - range;
		return (int) Math.round(rawDamage * multiplier);

	}

	public static float division(float numerator, float denominator){
		return numerator / denominator;
	}
	
	
	public static double division(double numerator, double denominator){
		return numerator / denominator;
	}

	public static IDirection getDirectionBetweenPoints(IGamePoint p1, IGamePoint p2){
		double deltaX = p2.getX() - p1.getX();
		double deltaY = p2.getY() - p1.getY();
		return new Direction(deltaX, deltaY);
	}

	public static Direction getDirectionBetweenObserverAndPoint(IGamePoint p1, IDirection p1Direction, IGamePoint p2){
		double deltaX = p2.getX() - p1.getX();
		double deltaY = p2.getY() - p1.getY();
		IDirection distanceDirection = new Direction(deltaX, deltaY);
		return new Direction(p1Direction.angleBetweenDirections(distanceDirection));
		//TODO class test
	}

	public static boolean gamePointIsInInteractionAngle(double halfInteractionAngle, IGamePoint observerLocation, IDirection observerDirection, IGamePoint observedPoint){

		IDirection directionBetweenObserverAndPoint = getDirectionBetweenObserverAndPoint(observerLocation, observerDirection, observedPoint);
		if(directionBetweenObserverAndPoint.getDirectionAngle() <= halfInteractionAngle || directionBetweenObserverAndPoint.getDirectionAngle() >= 2 * Math.PI - halfInteractionAngle){
			return true;
		}
		return false;
	}

	public static boolean checkNetworkMessage(IConnectionState state, String line){
		String[] requirements = state.getRequirements();
		int paramLength = state.getParamLength();
		String[] splitMessage = line.split(" ");
		if(splitMessage.length == paramLength){
			for(int i = 0; i < requirements.length; i++){
				if(!splitMessage[i].equals(requirements[i])){
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Extract a gaussian random IQ basing on monster IQ and monster IQ standard
	 * deviation.
	 *
	 * @param intelligenceQuotient : monster IQ
	 * @return : double a random gaussian IQ close to the starting IQ
	 */
	public static double getGaussianRandomIQ(double intelligenceQuotient){
		Random generator = new Random();
		double gaussianDelta = generator.nextGaussian();
		int sign = generator.nextInt();
		if(sign % 2 == 0){
			sign = 1;
		}else{
			sign = -1;
		}
		double variation = IGameConstants.MONSTER_IQ_STANDARD_DEV;
		variation = variation * gaussianDelta * sign;
		intelligenceQuotient += variation;
		if(intelligenceQuotient > 100){
			intelligenceQuotient = 200 - intelligenceQuotient;
		}else if(intelligenceQuotient < 0){
			intelligenceQuotient = -intelligenceQuotient;
		}

		return intelligenceQuotient;
	}

	/**
	 * Calculates and returns the casting duration in ms of a spell
	 *
	 * @param spell : spell which is going to be casted
	 * @param executor : spell performer
	 * @return int : numeber of ms needed to perform the spell
	 */
	public static int getFinalCastDuration(ISpellDefinition spell, PlayingCharacter executor){
		return (int) Math.round(spell.getPlayingCharacterCastDuration(executor) * 1000);
	}
	
	/**
	 * Calculates and returns the modulus of a double number calculated on a double divisor
	 * 
	 * @param number
	 *            : the number we want to calculate the modulus
	 * @param divisor
	 *            : operation divisor
	 * @return double: the operation result
	 */
	public static double modulus(double number, double divisor) {
		double remainder = number % divisor;
		while(remainder<0){
			remainder += divisor;
		}
		return remainder;
	}
		
	/**
	 * Calculates and returns the modulus of a double number calculated on 2 * PI
	 * 
	 * @param number
	 *            : the number we want to calculate the modulus
	 * @return double: the operation result
	 */
	public static double pi2Modulus(double number) {
		double divisor = Math.PI * 2;	
		return modulus(number, divisor);
	}	
	
	public static String getPath(String pipePath){
		return pipePath.replace("|", IGameConstants.SEPARATOR);
	}

	public static float getMaxApproachingDistance(IGameArea areaOfEffect) {
		Rectangle2D bounds = areaOfEffect.getArea().getBounds2D();
		return getInterpolationVaelue((float)bounds.getMinX(),(float)bounds.getMaxX(),0.75f);
	}
	
	public static float getMinApproachingDistance(IGameArea areaOfEffect) {
		Rectangle2D bounds = areaOfEffect.getArea().getBounds2D();
		return getInterpolationVaelue((float)bounds.getMinX(),(float)bounds.getMaxX(),0.25f);
	}
	
	public static float getInterpolationVaelue(float min, float max, float t) {
		return max*t + min*(1-t);
	}
	
	public static int getRandomInt(int maxNumber) {
		double randomDouble = Math.random();
		return (int) Math.floor(randomDouble*maxNumber);
		
	}
	
	public static double getJMESizeFromPixel(int pixelCoord){
		return 0.008125 * pixelCoord;
	}
	
}
