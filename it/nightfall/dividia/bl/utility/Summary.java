package it.nightfall.dividia.bl.utility;

import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigWriter;
import it.nightfall.dividia.bl.utility.io.ConfigWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

public class Summary implements ISummary {
	private final Map<String, Serializable> data = new HashMap<>();

	@Override
	public void putData(String key, Serializable object){
		data.put(key, object);
	}

	@Override
	public Serializable getData(String key){
		return data.get(key);
	}

	@Override
	public Map<String, Serializable> getInternalMap(){
		return data;
	}

	@Override
	public boolean getBoolean(String name){
		return (boolean) data.get(name);
	}

	@Override
	public int getInteger(String name){
		return (int) data.get(name);
	}

	@Override
	public long getLong(String name){
		return (long) data.get(name);
	}

	@Override
	public float getFloat(String name){
		return (float) data.get(name);
	}

	@Override
	public double getDouble(String name){
		return (double) data.get(name);
	}

	@Override
	public String getString(String name){
		return (String) data.get(name);
	}

	@Override
	public ISummary getSummary(String name){
		return (ISummary) data.get(name);
	}

	@Override
	public boolean dataExists(String name){
		return data.containsKey(name);
	}

	@Override
	public IConfigWriter toConfigWriter(IConfigWriter config){
		for(Entry<String, Serializable> entry : data.entrySet()){
			IConfigWriter tempConfig = new ConfigWriter("element", config);
			Serializable value = entry.getValue();
			String valueType = value.getClass().getSimpleName();
			tempConfig.appendStringAttribute("class", valueType);
			tempConfig.appendString("key", entry.getKey());
			switch(valueType){
				case "Integer":
					tempConfig.appendInteger("value", (int) value);
					break;
				case "Float":
					tempConfig.appendFloat("value", (float) value);
					break;
				case "Double":
					tempConfig.appendDouble("value", (double) value);
					break;
				case "Long":
					tempConfig.appendLong("value", (long) value);
					break;
				case "Boolean":
					tempConfig.appendBoolean("value", (boolean) value);
					break;
				case "String":
					tempConfig.appendString("value", (String) value);
					break;
				case "Summary":
					tempConfig.appendConfigWriter(((ISummary) value).toConfigWriter(new ConfigWriter("value", tempConfig)));
					break;
				case "HashSet":
					tempConfig.appendStringList("element", (HashSet<String>) value);
					break;
			}
			config.appendConfigWriter(tempConfig);
		}
		return config;
	}
}