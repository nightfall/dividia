package it.nightfall.dividia.bl.utility.dialogues;

/**
 *
 * @author Nightfall
 */
public abstract class AbstractDialogue implements IDialog{
	
	protected String text;
	protected String title;
	protected String id;

	@Override
	public String getId() {
		return id;
	}	

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String getEntryComment() {
		return title;
	}	

}
