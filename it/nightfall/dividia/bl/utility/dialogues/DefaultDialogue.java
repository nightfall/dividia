package it.nightfall.dividia.bl.utility.dialogues;

import it.nightfall.dividia.bl.characters.Player;

/**
 *
 * @author Nightfall
 */
public class DefaultDialogue extends AbstractDialogue implements ILeafDialogue {

	@Override
	public boolean isDialogueVisible(Player p) {
		return true;
	}
	
	@Override
	public void exit() {
		throw new UnsupportedOperationException("Not supported yet.");
	}	

	@Override
	public String getExitComment() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
}
