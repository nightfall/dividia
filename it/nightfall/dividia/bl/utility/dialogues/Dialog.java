/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.utility.dialogues;

import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.characters.Player;

/**
 *
 * @author bernardo
 */
public class Dialog implements IDialog {
	
	private String id;
	private String entryComment;
	private String exitComment;
	private String text;
	
	public Dialog(IConfigReader config) {
		id = config.getString("name");
		entryComment = config.getString("entryComment");
		exitComment = config.getString("exitComment");
		//todo get the real text
		text = config.getString("text");
		
	}

	@Override
	public String getId() {
		return  id;
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public String getEntryComment() {
		return entryComment;
	}

	@Override
	public boolean isDialogueVisible(Player p) {
		return true;
	}

	@Override
	public String getExitComment() {
		return exitComment;
	}
	
}
