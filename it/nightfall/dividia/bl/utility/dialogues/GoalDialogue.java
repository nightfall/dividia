package it.nightfall.dividia.bl.utility.dialogues;

import it.nightfall.dividia.api.quests.goals.definitions.ITalkToNPCGoalDefinition;
import it.nightfall.dividia.bl.characters.Player;

/**
 *
 * @author Nightfall
 */
public class GoalDialogue extends AbstractDialogue implements ILeafDialogue{

	private ITalkToNPCGoalDefinition goal;
	
	@Override
	public boolean isDialogueVisible(Player p) {
		
			if(p.getQuestManager().isGoalActive(goal.getName()))
				return true;
			return false;				
		
	}
	
	@Override
	public void exit() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public String getExitComment() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
}
