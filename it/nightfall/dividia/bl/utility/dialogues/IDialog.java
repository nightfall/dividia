package it.nightfall.dividia.bl.utility.dialogues;

import it.nightfall.dividia.bl.characters.Player;

public interface IDialog {
	
	public String getId();
			
	public String getText();
	
	public String getEntryComment();
	
	public String getExitComment();
	
	public boolean isDialogueVisible(Player p);
	
}
