package it.nightfall.dividia.bl.utility.dialogues;

/**
 *
 * @author Binno
 */
public interface ILeafDialogue extends IDialog {
	
	public void exit();
	
}
