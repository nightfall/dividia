package it.nightfall.dividia.bl.utility.dialogues;

import java.util.List;

/**
 *
 * @author Nightfall
 */
public interface IRootDialogue extends IDialog{
	
	public List<IDialog> getChildrenDialogues();
	
}
