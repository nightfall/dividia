package it.nightfall.dividia.bl.utility.dialogues;

import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterRaceDefinition;

public class LanguageManager {
	
    private IDialog dialogue;
	private NonPlayingCharacterRaceDefinition characterRace;

	public LanguageManager(NonPlayingCharacterRaceDefinition characterRace) {
		this.characterRace = characterRace;
	}
	
	public IDialog getDialogue() {
		
		return dialogue;
	}
	
	public IDialog getInteractionDialogue(Player p) {
		
		return null;
		//TODO method implementation
	}
	
	
}
