package it.nightfall.dividia.bl.utility.dialogues;

import it.nightfall.dividia.api.quests.IQuestDefinition;
import it.nightfall.dividia.bl.characters.Player;

/**
 *
 * @author Nightfall
 */
public class QuestDialogue extends AbstractDialogue implements ILeafDialogue{

	private IQuestDefinition quest;
			
	@Override
	public boolean isDialogueVisible(Player p) {
	
		if(! p.getQuestManager().getActiveQuests().containsKey(quest.getName()))
			if(! p.getQuestManager().getSolvedQuests().contains(quest.getName()))
			{
				for(String s: quest.getRequirements())
					if(! p.getQuestManager().getSolvedQuests().contains(s))
						return false;
				if(quest.getLevelRequirement()> p.getStatus().getLevel())
					return false;
				return true;
			}
		return false;
		
	}
	
	@Override
	public void exit() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public String getExitComment() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
}
