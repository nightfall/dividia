package it.nightfall.dividia.bl.utility.dialogues;

import it.nightfall.dividia.bl.characters.Player;
import java.util.List;

/**
 *
 * @author Nightfall
 */
public class RootDialogue extends AbstractDialogue implements IRootDialogue{
	
	protected List<IDialog> childrenDialogues;
	
	@Override
	public boolean isDialogueVisible(Player p) {
		for(IDialog d: childrenDialogues)
			if(d.isDialogueVisible(p))
				return true;
		return false;
	}
	
	@Override
	public List<IDialog> getChildrenDialogues() {
		return childrenDialogues;
	}

	@Override
	public String getExitComment() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
}
