package it.nightfall.dividia.bl.utility.io;

import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.Summary;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ConfigReader implements IConfigReader {
	private Element xmlNode;

	public ConfigReader(String filename){
		File xmlFile = new File(filename);
		Document xmlDocument;
		try{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			xmlDocument = dBuilder.parse(xmlFile);
			xmlDocument.getDocumentElement().normalize();
			xmlNode = xmlDocument.getDocumentElement();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private ConfigReader(Element node){
		xmlNode = node;
	}

	@Override
	public boolean getBoolean(String name){
		return Boolean.parseBoolean(getSingleContent(name));
	}

	@Override
	public int getHexadecimal(String name){
		return (int) Long.parseLong(getSingleContent(name), 16);
	}

	@Override
	public int getInteger(String name){
		return Integer.parseInt(getSingleContent(name));
	}

	@Override
	public float getFloat(String name){
		return Float.parseFloat(getSingleContent(name));
	}

	@Override
	public double getDouble(String name){
		return Double.parseDouble(getSingleContent(name));
	}

	@Override
	public long getLong(String name){
		return Long.parseLong(getSingleContent(name));
	}

	@Override
	public String getString(String name){
		return getSingleContent(name);
	}

	@Override
	public IConfigReader getConfigReader(String name){
		NodeList tempNodeList = xmlNode.getElementsByTagName(name);
		return new ConfigReader((Element) tempNodeList.item(0));
	}

	@Override
	public Collection<Boolean> getBooleanList(String name){
		List<Boolean> tempBooleanList = new ArrayList<>();
		NodeList tempNodeList = xmlNode.getElementsByTagName(name);
		for(int i = 0; i < tempNodeList.getLength(); i++){
			tempBooleanList.add(Boolean.parseBoolean(tempNodeList.item(i).getTextContent()));
		}
		return tempBooleanList;
	}

	@Override
	public Collection<Integer> getIntegerList(String name){
		List<Integer> tempIntegerList = new ArrayList<>();
		NodeList tempNodeList = xmlNode.getElementsByTagName(name);
		for(int i = 0; i < tempNodeList.getLength(); i++){
			tempIntegerList.add(Integer.parseInt(tempNodeList.item(i).getTextContent()));
		}
		return tempIntegerList;
	}

	@Override
	public Collection<Float> getFloatList(String name){
		List<Float> tempFloatList = new ArrayList<>();
		NodeList tempNodeList = xmlNode.getElementsByTagName(name);
		for(int i = 0; i < tempNodeList.getLength(); i++){
			tempFloatList.add(Float.parseFloat(tempNodeList.item(i).getTextContent()));
		}
		return tempFloatList;
	}

	@Override
	public Collection<Double> getDoubleList(String name){
		List<Double> tempDoubleList = new ArrayList<>();
		NodeList tempNodeList = xmlNode.getElementsByTagName(name);
		for(int i = 0; i < tempNodeList.getLength(); i++){
			tempDoubleList.add(Double.parseDouble(tempNodeList.item(i).getTextContent()));
		}
		return tempDoubleList;
	}

	@Override
	public Collection<String> getStringList(String name){
		List<String> tempStringList = new ArrayList<>();
		NodeList tempNodeList = xmlNode.getElementsByTagName(name);
		for(int i = 0; i < tempNodeList.getLength(); i++){
			tempStringList.add(tempNodeList.item(i).getTextContent());
		}
		return tempStringList;
	}

	@Override
	public Collection<IConfigReader> getConfigReaderList(String name){
		List<IConfigReader> tempConfigReaderList = new ArrayList<>();
		NodeList tempNodeList = xmlNode.getElementsByTagName(name);
		for(int i = 0; i < tempNodeList.getLength(); i++){
			tempConfigReaderList.add(new ConfigReader((Element) tempNodeList.item(i)));
		}
		return tempConfigReaderList;
	}

	private String getSingleContent(String nodeName){
		return xmlNode.getElementsByTagName(nodeName).item(0).getTextContent();
	}

	@Override
	public boolean doesElementExist(String nodeName){
		return xmlNode.getElementsByTagName(nodeName).getLength() != 0;
	}

	@Override
	public String getStringAttribute(String attributeName){
		return xmlNode.getAttribute(attributeName);
	}

	@Override
	public double getDoubleAttribute(String attributeName){
		return Double.parseDouble(xmlNode.getAttribute(attributeName));
	}

	@Override
	public float getFloatAttribute(String attributeName){
		return Float.parseFloat(xmlNode.getAttribute(attributeName));
	}

	@Override
	public int getIntegerAttribute(String attributeName){
		return Integer.parseInt(xmlNode.getAttribute(attributeName));
	}

	@Override
	public long getLongAttribute(String attributeName){
		return Long.parseLong(xmlNode.getAttribute(attributeName));
	}

	@Override
	public ISummary toSummary(){
		ISummary tempSummary = new Summary();
		Collection<IConfigReader> elements = getConfigReaderList("element");
		for(IConfigReader entry : elements){
			String elementName = entry.getString("key");
			String valueType = entry.getStringAttribute("class");
			switch(valueType){
				case "Integer":
					tempSummary.putData(elementName, entry.getInteger("value"));
					break;
				case "Float":
					tempSummary.putData(elementName, entry.getFloat("value"));
					break;
				case "Double":
					tempSummary.putData(elementName, entry.getDouble("value"));
					break;
				case "Long":
					tempSummary.putData(elementName, entry.getLong("value"));
					break;
				case "Boolean":
					tempSummary.putData(elementName, entry.getBoolean("value"));
					break;
				case "String":
					tempSummary.putData(elementName, entry.getString("value"));
					break;
				case "Summary":
					tempSummary.putData(elementName, entry.toSummary());
					break;
				case "HashSet":
					HashSet<String> tempHash = new HashSet<>();
					Collection<String> tempCollection = entry.getStringList("element");
					for(String stringElement : tempCollection){
						tempHash.add(stringElement);
					}
					tempSummary.putData(elementName, tempHash);
				case "Null":
					tempSummary.putData(elementName, null);
			}
		}
		return tempSummary;
	}
}