package it.nightfall.dividia.bl.utility.io;

import it.nightfall.dividia.api.utility.io.IConfigWriter;
import java.io.File;
import java.util.Collection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ConfigWriter implements IConfigWriter {
	private Element xmlNode;
	private Document xmlDocument;

	public ConfigWriter(String nodeName, IConfigWriter parent){
		ConfigWriter castedParent = (ConfigWriter) parent;
		xmlDocument = castedParent.getDocument();
		xmlNode = xmlDocument.createElement(nodeName);
		castedParent.getRootNode().appendChild(xmlNode);
	}

	public ConfigWriter(String nodeName){
		try{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			xmlDocument = docBuilder.newDocument();
			xmlNode = xmlDocument.createElement(nodeName);
			xmlDocument.appendChild(xmlNode);
		}catch(ParserConfigurationException ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void appendBoolean(String name, boolean value){
		addNode(name, Boolean.toString(value));
	}

	@Override
	public void appendInteger(String name, int value){
		addNode(name, new Integer(value).toString());
	}

	@Override
	public void appendFloat(String name, float value){
		addNode(name, new Float(value).toString());
	}

	@Override
	public void appendDouble(String name, double value){
		addNode(name, new Double(value).toString());
	}

	@Override
	public void appendLong(String name, long value){
		addNode(name, new Long(value).toString());
	}

	@Override
	public void appendString(String name, String value){
		addNode(name, value);
	}

	@Override
	public void appendConfigWriter(IConfigWriter value){
		ConfigWriter tempValue = (ConfigWriter) value;
		Element tempNode = tempValue.getRootNode();
		xmlNode.appendChild(tempNode);
	}

	@Override
	public void appendBooleanList(String name, Collection<Boolean> value){
		for(Boolean singleValue : value){
			appendBoolean(name, singleValue);
		}
	}

	@Override
	public void appendIntegerList(String name, Collection<Integer> value){
		for(Integer singleValue : value){
			appendInteger(name, singleValue);
		}
	}

	@Override
	public void appendFloatList(String name, Collection<Float> value){
		for(Float singleValue : value){
			appendFloat(name, singleValue);
		}
	}

	@Override
	public void appendDoubleList(String name, Collection<Double> value){
		for(Double singleValue : value){
			appendDouble(name, singleValue);
		}
	}

	@Override
	public void appendLongList(String name, Collection<Long> value){
		for(Long singleValue : value){
			appendLong(name, singleValue);
		}
	}

	@Override
	public void appendStringList(String name, Collection<String> value){
		for(String singleValue : value){
			appendString(name, singleValue);
		}
	}

	@Override
	public void appendConfigWriterList(Collection<IConfigWriter> value){
		for(IConfigWriter singleValue : value){
			appendConfigWriter(singleValue);
		}
	}

	@Override
	public void appendIntegerAttribute(String name, int value){
		addAttribute(name, new Integer(value).toString());
	}

	@Override
	public void appendFloatAttribute(String name, float value){
		addAttribute(name, new Float(value).toString());
	}

	@Override
	public void appendDoubleAttribute(String name, double value){
		addAttribute(name, new Double(value).toString());
	}

	@Override
	public void appendStringAttribute(String name, String value){
		addAttribute(name, value);
	}

	@Override
	public void saveFile(String path){
		try{
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			DOMSource source = new DOMSource(xmlDocument);
			StreamResult result = new StreamResult(new File(path));
			transformer.transform(source, result);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	private Element getRootNode(){
		return xmlNode;
	}

	private Document getDocument(){
		return xmlDocument;
	}

	private void addNode(String name, String value){
		Element tempNode = xmlDocument.createElement(name);
		tempNode.setTextContent(value);
		xmlNode.appendChild(tempNode);
	}

	private void addAttribute(String name, String value){
		xmlNode.setAttribute(name, value);
	}
}
