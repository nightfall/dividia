package it.nightfall.dividia.bl.utility.playerMovements;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.gui_api.utility.IDirectionMovementManager;



public class EightDirectionMovementManager implements IDirectionMovementManager {
    
    public double speedFactor;
    private static double maxAngleAllowedBetweenViewAndMovement = IGameConstants.ROTATION_ANGLE_PER_FRAME;
    private Player player;    
    private IGamePoint playerFocus;
    private IGamePoint referenceSimpleFocus;
    private Movement movementStatus;
    private FocusStatus focusStatus;
	private boolean directionStopWillBeNotified;

    public EightDirectionMovementManager(Player player) {
        this.player = player;
        this.movementStatus = Movement.NOT_MOVING;        
		this.playerFocus = player.getDirection().oppositeDirection().getDistancedPointAlongDirection(player.getPosition(), IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION);
        this.referenceSimpleFocus = playerFocus.clone();
        this.focusStatus = FocusStatus.SIMPLE_ROTATION;
		this.speedFactor = IGameConstants.MOVEMENT_SPEED_CONSTANT;
		directionStopWillBeNotified = true;
    }
    

	@Override
    public void updateDirection(Movement movementStatus) {
		this.movementStatus = movementStatus;
    	if(movementStatus.isForwardLeftOrBackwardRight() && focusStatus!= FocusStatus.FORWARD_LEFT_BACKWARD_RIGHT) {
			focusRepositioning();
		}
        else if(movementStatus.isForwardRightOrBackwardLeft() && focusStatus!= FocusStatus.FORWARD_RIGHT_BACKWARD_LEFT) {
			focusRepositioning();
		}
        else if(movementStatus.isSimpleRotation() && focusStatus!= FocusStatus.SIMPLE_ROTATION) {
			focusRepositioning();
		}
        else if(movementStatus.isLinearMovement() && focusStatus!=FocusStatus.SIMPLE_ROTATION) {
			focusRepositioning();
		}
    }
    
    public void focusRepositioning() {
        if(movementStatus.isSimpleRotation() || movementStatus.isLinearMovement()) {
            playerFocus= referenceSimpleFocus.clone();
            focusStatus = FocusStatus.SIMPLE_ROTATION;
        }
        else if(movementStatus.isDiagonalMovement()){
            IDirection playerToReferenceFocusDirection = Direction.getDirectionBetweenPoints(player.getPosition(), referenceSimpleFocus);
            IDirection orthoDirection = playerToReferenceFocusDirection.clone();
            orthoDirection.rotateDirection(Math.PI/2);
            if(!movementStatus.isForwardLeftOrBackwardRight()) {
				orthoDirection = orthoDirection.oppositeDirection();
			}
            playerFocus = playerToReferenceFocusDirection.getDistancedPointAlongDirection(player.getPosition(), IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION);
            playerFocus = orthoDirection.getDistancedPointAlongDirection(playerFocus, IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION);
            if(movementStatus.isForwardLeftOrBackwardRight()) {
				focusStatus = FocusStatus.FORWARD_LEFT_BACKWARD_RIGHT;
			}
            else {
				focusStatus = FocusStatus.FORWARD_RIGHT_BACKWARD_LEFT;
			}
        }     
    }

	@Override
    public Movement getMovementStatus() {
        return movementStatus;
    }

    private boolean rotationNeededBeforeMovement(IDirection movementDirection, int tpf) {
        double angleBetweenViewDirAndMovementDir = player.getDirection().angleBetweenDirections(movementDirection);		
        if((SharedFunctions.pi2Modulus(angleBetweenViewDirAndMovementDir) > maxAngleAllowedBetweenViewAndMovement)  && (SharedFunctions.pi2Modulus(angleBetweenViewDirAndMovementDir) < Math.PI * 2  - maxAngleAllowedBetweenViewAndMovement)) {
            if(SharedFunctions.pi2Modulus(angleBetweenViewDirAndMovementDir) <= Math.PI) {
                player.getDirection().rotateDirection(maxAngleAllowedBetweenViewAndMovement);
			}
			else {
                player.getDirection().rotateDirection(-maxAngleAllowedBetweenViewAndMovement);
			}
            updatePlayerPosition(false, false, tpf);
            return true;
        }
		return false;
    }

    private void simpleRotationMovement(IDirection focusToPlayerDirection, double movementLenght, double focusToCharDistance, int tpf) {
        //angolo compreso tra la direzione player-focus e la direzione di movimento
        double angle = Math.acos(movementLenght / (2 * focusToCharDistance));
        //angolo di movimento
        IDirection movementDirection = new Direction(focusToPlayerDirection.oppositeDirection().getDirectionAngle());            
        if(movementStatus.isLeftMovement()) {
			movementDirection.rotateDirection(-angle);
		}
        else {
			movementDirection.rotateDirection(angle);
		}           
        if(!rotationNeededBeforeMovement(movementDirection, tpf)) {            
            player.setDirection(movementDirection);                
            updatePlayerPosition(true, false, tpf);
            if(movementStatus.isDiagonalMovement()) {
				updateReferenceFocus(angle);
			}
        }
    }
    
    private void updateReferenceFocus(double angle) {
        double rotationAngle = Math.PI-2*angle;
        IDirection focusToReferenceFocusDirection = Direction.getDirectionBetweenPoints(playerFocus, referenceSimpleFocus);
        IDirection nextDirection = focusToReferenceFocusDirection.clone();
        if(!movementStatus.isLeftMovement()) {
			rotationAngle *= -1;
		}
        nextDirection.rotateDirection(rotationAngle);
        referenceSimpleFocus = nextDirection.getDistancedPointAlongDirection(playerFocus, IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION);
		EventManager.getInstance().focusMoved(player.getId(), referenceSimpleFocus.getX(), referenceSimpleFocus.getY(), focusStatus.name());
    }
    
    private void linearMovement(IDirection focusToPlayerDirection, int tpf) {
        IDirection movementDirection = focusToPlayerDirection.clone();
        if(movementStatus == Movement.BACKWARD) {
			movementDirection = movementDirection.oppositeDirection();
		}
        if(!rotationNeededBeforeMovement(movementDirection, tpf)) {                
            player.setDirection(movementDirection);                
            updatePlayerPosition(true, true, tpf);
        }
    }   
    
    public void updatePlayerPosition(boolean needToMove, boolean linearFollowingFocus, int tpf){
		if(getMovementStatus() != Movement.NOT_MOVING){
			if(needToMove){
				double deltaX = player.getDirection().getVersorX() * getStep(tpf);
				double deltaY = player.getDirection().getVersorY() * getStep(tpf);
				player.translatePosition(deltaX, deltaY);
				if(linearFollowingFocus){
					playerFocus.translate(deltaX, deltaY);
					referenceSimpleFocus.translate(deltaX, deltaY);
					EventManager.getInstance().focusMoved(player.getId(), referenceSimpleFocus.getX(), referenceSimpleFocus.getY(), focusStatus.name());
				}
			}
		}
    }    
    
    public double getStep(int tpf) {
		
        return speedFactor * player.getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED) * 1.0 * (tpf * 1.0 / 1000000000) / 100;       
    }

	@Override
	public void tick(int tpf){
		if(!player.isDead() && movementStatus!= Movement.NOT_MOVING && player.getCurrentExecution()==null) {
			directionStopWillBeNotified = true;
			double focusToCharDistance = playerFocus.getDistanceFromAnotherPoint(player.getPosition());
			//lunghezza del passo in questo frame
			double movementLenght = getStep(tpf);
			IDirection focusToPlayerDirection = Direction.getDirectionBetweenPoints(playerFocus, player.getPosition());
			if(movementStatus.isSimpleRotation() || movementStatus.isDiagonalMovement()) {
				simpleRotationMovement(focusToPlayerDirection, movementLenght, focusToCharDistance, tpf);               
			}
			else if(movementStatus.isLinearMovement()) {
				linearMovement(focusToPlayerDirection, tpf);
			}	
			EventManager.getInstance().characterMoved(player);
		}
		else if(directionStopWillBeNotified) {
			EventManager.getInstance().characterStopped(player);
			directionStopWillBeNotified = false;
		}
	}
	
	@Override
	public void resetFocus() {
        this.referenceSimpleFocus = playerFocus.clone();
		this.referenceSimpleFocus = player.getDirection().oppositeDirection().getDistancedPointAlongDirection(player.getPosition(), IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION);
		this.playerFocus = referenceSimpleFocus.clone();
		/*if(movementStatus.isDiagonalMovement()) {
			this.playerFocus = player.getDirection().oppositeDirection().getDistancedPointAlongDirection(player.getPosition(), IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION);
		} else {
			IDirection playerFocusDirection = player.getDirection().clone().oppositeDirection();
			playerFocusDirection.rotateDirection(Math.PI/2);
			this.playerFocus = playerFocusDirection.getDistancedPointAlongDirection(player.getPosition(), IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION);
		}*/
			
		movementStatus = Movement.NOT_MOVING;
		EventManager.getInstance().focusReset(player, playerFocus.getX(), playerFocus.getY());
	}
}
