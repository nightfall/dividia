package it.nightfall.dividia.bl.utility.playerMovements;

public enum FocusStatus {
    SIMPLE_ROTATION,
    FORWARD_LEFT_BACKWARD_RIGHT,
    FORWARD_RIGHT_BACKWARD_LEFT;
	
}
