package it.nightfall.dividia.bl.utility.playerMovements;

import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.gui_api.utility.IDirectionMovementManager;

public class FourDirectionMovementManager implements IDirectionMovementManager {
	private Movement movementStatus;
	private Player player;
	public double speedFactor;
    public boolean directionChanged = false;
	private boolean directionStopWillBeNotified;

	public FourDirectionMovementManager(Player player){
		this.player = player;
		this.movementStatus = Movement.NOT_MOVING;
		this.speedFactor = IGameConstants.MOVEMENT_SPEED_CONSTANT;
		directionStopWillBeNotified = true;
	}

	private void updatePlayerPosition(float tpf){
		double deltaX = player.getDirection().getVersorX() * getStep(tpf);
		double deltaY = player.getDirection().getVersorY() * getStep(tpf);
		if (Math.abs(deltaX) > Math.abs(deltaY)){
			deltaY = 0;
		}else {
			deltaX = 0;
		}
        //System.out.println("BL > sposto di " + deltaX + " " + deltaY);
		player.translatePosition(deltaX, deltaY);
			
	}

	@Override
	public void updateDirection(Movement movementDirection){
		this.movementStatus = movementDirection;
        directionChanged = true;
	}

	@Override
	public Movement getMovementStatus(){
		return movementStatus;
	}

	private double getStep(){
		//TODO movement_speed inserito come campo e modificato dall'update characteristic dello status
		return speedFactor * player.getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED) * 1.0 * IGameConstants.BL_UPDATE_S / 100;
		
	}
	
	public double getStep(float tpf) {
		
        return speedFactor * player.getStatus().getCharacteristic(StatusBattleCharacteristic.MOVEMENT_SPEED) * 1.0 * (tpf * 1.0 / 1000000000) / 100;       
    }


	@Override
	public void tick(int tpf){
       if (directionChanged){
            if(!movementStatus.equals(Movement.NOT_MOVING)) {
                player.setDirection(SharedFunctions.getDirectionFromMovement2D(movementStatus));
            }
            directionChanged = false;
        }
        if (movementStatus != Movement.NOT_MOVING){			
			directionStopWillBeNotified = true;
            updatePlayerPosition(tpf);
            EventManager.getInstance().characterMoved(player);
        } 
		else if(directionStopWillBeNotified) {
			EventManager.getInstance().characterStopped(player);
			directionStopWillBeNotified = false;
		}
        
	}

	@Override
	public void resetFocus() {
	}
}
