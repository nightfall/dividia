package it.nightfall.dividia.bl.utility.playerMovements;

public enum Movement {
	NOT_MOVING("NOT_MOVING", 0),
	FORWARD("FORWARD", 0),
	BACKWARD("BACKWARD", Math.PI),
	LEFT("LEFT", Math.PI / 2),
	RIGHT("RIGHT", 3 * Math.PI / 2),
	FORWARD_RIGHT("FORWARD_RIGHT", 7 * Math.PI / 4),
	FORWARD_LEFT("FORWARD_LEFT", Math.PI / 4),
	BACKWARD_RIGHT("BACKWARD_RIGHT", 5 * Math.PI / 4),
	BACKWARD_LEFT("BACKWARD_LEFT", 3 * Math.PI / 4);
	private String name;
	private double generalAngle;

	private Movement(String name, double generalAngle){
		this.name = name;
		this.generalAngle = generalAngle;
	}

	public String getName(){
		return name;
	}

	public double getGeneralAngle(){
		return generalAngle;
	}

	public boolean isSimpleRotation(){
		return (this == LEFT || this == RIGHT);
	}

	public boolean isLinearMovement(){
		return (this == FORWARD || this == BACKWARD);
	}

	public boolean isDiagonalMovement(){
		return (isLeftDiagonal() || isRightDiagonal());
	}

	public boolean isLeftDiagonal(){
		return (this == FORWARD_LEFT || this == BACKWARD_LEFT);
	}

	public boolean isRightDiagonal(){
		return (this == FORWARD_RIGHT || this == BACKWARD_RIGHT);
	}

	public boolean isLeftMovement(){
		return (this == LEFT || this == FORWARD_LEFT || this == BACKWARD_LEFT);
	}

	public boolean isForwardLeftOrBackwardRight(){
		return (this == FORWARD_RIGHT || this == BACKWARD_LEFT);
	}

	public boolean isForwardRightOrBackwardLeft(){
		return (this == FORWARD_LEFT || this == BACKWARD_RIGHT);
	}
}
