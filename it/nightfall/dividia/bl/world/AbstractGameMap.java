package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.characters.IInteractionCharacter;
import it.nightfall.dividia.api.utility.ICallbackWaiting;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.api.world.summaries.IGeneralGameMapSummary;
import it.nightfall.dividia.bl.characters.AbstractCharacter;
import it.nightfall.dividia.bl.characters.NonPlayingCharacter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterDefinition;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.item.BagOfItems;
import it.nightfall.dividia.bl.utility.FutureCallback;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.bl.world.summaries.GeneralGameMapSummary;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractGameMap implements IGameMap {
	
	protected Map<String, BagOfItems> bagsOfItems;
    protected Map<String, Player> players;
	protected Map<String, Player> loadingPlayers;
	private IGameMapDefinition definition;
	private int bagDuration = 20;
	//todo move to definition
	protected Map<String,Player> deadPlayers = new HashMap<>();
	private Teleport toDefaultPositionTeleport;
	protected Map<String, NonPlayingCharacter> npcs = new HashMap<>();

	public AbstractGameMap(IGameMapDefinition definition) {
		this.definition = definition;
		toDefaultPositionTeleport = new Teleport(definition.getDefaultStartingPosition(), definition.getName());
		bagsOfItems = new HashMap<>();	
		players = new HashMap<>();
		loadingPlayers = new HashMap<>();
		List<NonPlayingCharacterDefinition> npcDefinitions = definition.getNpcDefinitions();
		for(NonPlayingCharacterDefinition npcDefinition:npcDefinitions) {
			NonPlayingCharacter nextNpc = npcDefinition.getNpcFromDefinition(this);
			npcs.put(nextNpc.getId(),nextNpc);
		}
		
	}

	@Override
	public void addPlayer(Player player){
		players.put(player.getId(), player);
	}

	@Override
	public void removePlayer(String player){
        players.remove(player);
    }
	
	@Override
	public void removeLoadingPlayer (String playingCharacter){
		loadingPlayers.remove(playingCharacter);
	}
	
	@Override
	public void addLoadingPlayer (Player player){
		loadingPlayers.put(player.getId(), player);
	}
	
	/**
	 * Tries to perform an action based on the interaction between a player and
	 * his current position.
	 *
	 * @param player : Player that wants to act.
	 */
	@Override
	public void findInteraction(Player player) {
		System.out.println("Finding interaction in : " + ((AbstractCharacter)player).getCollisionShape().getPosition().getX() + " " + ((AbstractCharacter)player).getCollisionShape().getPosition().getY());
		if (! bagInteraction(player)) {
			if(!playerAndNpcInteraction(player)) {
				portalInteraction(player);
			}
		}
	}
	
	protected boolean checkForPickUpInteraction(Player player, BagOfItems bag) {
		if(player.getPosition().getDistanceFromAnotherPoint(bag.getPosition()) <= IGameConstants.INTERACTION_RADIUS) {
			return true;
		}
		return false;
	}

	protected boolean bagInteraction(Player player) {		
		Collection <BagOfItems> Bags = this.bagsOfItems.values();
		if(Bags.isEmpty()) {
			return false;
		}
		BagOfItems tempBag = new BagOfItems();
		boolean interaction = false;
		float currentMinDistance = IGameConstants.INTERACTION_RADIUS + 1;
		for(BagOfItems b : Bags) {
			if(checkForPickUpInteraction(player,b) && currentMinDistance > player.getPosition().getDistanceFromAnotherPoint(b.getPosition())) {
				tempBag = b;
				interaction = true;
			}
		}
		if(!interaction) {
			return false;
		}
		else {
			this.removeBag(tempBag);
			tempBag.bagPickUp(player);
		}
		return true; //even if we could not collect the bag
	}
	
	protected boolean characterIsInInteractionAngle(Player player,IInteractionCharacter interactionCharacter) {
		if(SharedFunctions.gamePointIsInInteractionAngle(IGameConstants.PLAYER_HALF_INTERACTION_ANGLE, player.getPosition(), player.getDirection(), interactionCharacter.getPosition())) {
			return true;
		}
		return false;
				
	}
	
	protected boolean checkForCharacterInteraction(Player player, IInteractionCharacter interactionCharacter) {
		if(player == interactionCharacter) {
			return false;
		}
		if(player.getPosition().getDistanceFromAnotherPoint(interactionCharacter.getPosition()) <= IGameConstants.INTERACTION_RADIUS) {
			if(characterIsInInteractionAngle(player,interactionCharacter)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if a player is on a portal area, if yes player uses the teleport.
	 * @param player
	 * @return true if the player uses the teleport, false otherwhise.
	 */
	protected boolean portalInteraction (Player player){
		for (Portal currentPortal : definition.getPortals().values()){
			IGameArea portalArea = currentPortal.getArea();
			//portalArea.translate(currentPortal.getPortalLocation().getPointFromTranslation(-currentRadius));
			//System.out.println(currentPortal.getPortalLocation().getPointFromTranslation(-currentRadius));
			if (portalArea.contains(player.getPosition())){
				//TODO remove
				System.err.println("PORTAL USED");
				MapsManager.getInstance().useTeleportForPlayer(this, player, currentPortal);
				return true;
			}
		}
		return false;
	}
	
	
	protected boolean playerAndNpcInteraction(Player player) {
		List<IInteractionCharacter> playersAndNpcs = this.getAllNoMonsterCharacters();
		if(playersAndNpcs.isEmpty()) {
			return false;
		}
		boolean interaction = false;
		IInteractionCharacter interactionCharacter = null;
		int currentMinDistance = Integer.MAX_VALUE;
		for(IInteractionCharacter interCharacter : playersAndNpcs) {
			if(currentMinDistance > player.getPosition().getDistanceFromAnotherPoint(interCharacter.getPosition()) && checkForCharacterInteraction(player,interCharacter)) {
				interactionCharacter = interCharacter;
				interaction = true;
			}
		}
		if(!interaction) {
			return false;
		}
		interactionCharacter.speakToPlayer(player);
		
		return true;
	}
	//TODO DELETE!!
	/**
	 * This method creates the summary for the current map.
	 * @return MapSummary
	 */
	@Override
	public IGeneralGameMapSummary createMapSummary (){
		return new GeneralGameMapSummary(players.keySet(),bagsOfItems.keySet());
		
	}
	
	@Override
	public void addBag(final BagOfItems addingBag) {
		this.bagsOfItems.put(addingBag.getID(), addingBag);
		EventManager.getInstance().bagOfItemsDropped(addingBag, this);
		FutureCallback removeBagCallBack = new FutureCallback(new ICallbackWaiting() {
			@Override
			public boolean execute() {
				removeBag(addingBag);
				return true;
			}
		});
		removeBagCallBack.runAndWait(bagDuration*1000);
			
	}

	protected synchronized void removeBag(BagOfItems removingBag) {
		if(bagsOfItems.containsKey(removingBag.getID())) {
			this.bagsOfItems.remove(removingBag.getID());
			EventManager.getInstance().bagOfItemsRemoved(removingBag, this);
		}
	}
	

	@Override
	public String getName(){
		return definition.getName();
	}
        
	@Override
	public Map<String,Player> getPlayers(){
		return players;
	}

	@Override
	public String getDisplayName(){
		return definition.getDisplayName();
	}

	@Override
	public IGamePoint getDefaultStartingPosition() {
		return definition.getDefaultStartingPosition();
	}
	
	@Override
	public IDirection getDefaultStartingDirection() {
		return definition.getDefaultStartingDirection();
	}
	
	@Override
	public boolean is2D (){
		return definition.is2D();
	}
	
	@Override	
	public Map<String,Portal> getPortals() {
		return definition.getPortals();
	}
	
	@Override
	public Map<String,Player> getLoadingPlayers(){
		return loadingPlayers;
	}
	
    @Override
    public Map<String, Player> getActiveAndLoadingPlayers() {
        Map<String, Player> returningMap = new HashMap<>();
        returningMap.putAll(players);
        returningMap.putAll(loadingPlayers);
		returningMap.putAll(deadPlayers);
        return returningMap;
    }
    
	@Override
	public ISummary getSummary(){
		ISummary mapSummary = new Summary();
		ISummary playersSummary = new Summary();
		ISummary itemsSummary = new Summary();
		/*ISummary monstersSummary = new Summary();
		mapSummary.putData("monsters", monstersSummary);*/
		
		for (Map.Entry<String,Player> currentPlayerEntry : players.entrySet()){
			playersSummary.putData(currentPlayerEntry.getKey(), currentPlayerEntry.getValue().getStrictSummary());
		}
		
		for (Map.Entry<String,BagOfItems> currentItem : bagsOfItems.entrySet()){
			itemsSummary.putData(currentItem.getKey(), currentItem.getValue().getSummary());
		}

		mapSummary.putData("players", playersSummary);
		mapSummary.putData("items", itemsSummary);
		mapSummary.putData("name", definition.getName());
		
		
		ISummary npcsSummary = new Summary();			
		for (NonPlayingCharacter currentNpcs : npcs.values()){
			npcsSummary.putData(currentNpcs.getId(), currentNpcs.getSummary());
		}
		mapSummary.putData("npcs", npcsSummary);
		
		return mapSummary;
	}
	
	@Override
	public IGameMapDefinition getDefinition(){
		return definition;
	}
	
	@Override
	public boolean collisionDetected(Set<Integer> sectors, ICollisionShape playerShape){
		return definition.collisionDetected(sectors, playerShape);
	}
	
	@Override
	public double getHeight(){
		return definition.getHeight();
	}
	
	@Override
	public double getWidth(){
		return definition.getWidth();
	}
	
	@Override
	public boolean isPointInMap(IGamePoint point){
		return point.getX() <= definition.getWidth()&& point.getX() >= 0 &&
			   point.getY() <= definition.getHeight() && point.getY() >= 0;
	}
	
	@Override
    public void respawnPlayer(Player player) {
        MapsManager.getInstance().useTeleportForPlayer(this, player, toDefaultPositionTeleport);
		deadPlayers.remove(player.getId());
    }
	
	@Override
	public GuiStatus getGuiMode(){
		if (definition.is2D()){
			return GuiStatus.WORLD_2D;
		}
		return GuiStatus.WORLD_3D;
	}
	
	@Override
	public boolean doesPositionExist(IGamePoint position){
		return position.getX() >= 0 && position.getX() < definition.getWidth() && position.getY() >= 0 && position.getY() < definition.getHeight();
	}

}
