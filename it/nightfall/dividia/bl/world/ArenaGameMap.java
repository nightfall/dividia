package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.battle.IBattleManager;
import it.nightfall.dividia.api.characters.IInteractionCharacter;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.api.world.definitions.IArenaGameMapDefinition;
import it.nightfall.dividia.bl.battle.BattleManager;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArenaGameMap extends GeneralGameMap implements IBattleMap{
	
	private IBattleManager battleManager;
	private List<String> firstTeam = new LinkedList<>();
	private List<String> secondTeam = new LinkedList<>();
	private Teleport exitTeleport;
	private IArenaGameMapDefinition definition; //TODO derived definition

	public ArenaGameMap(IArenaGameMapDefinition definition) {
		super(definition);
		this.definition = definition;
		this.battleManager = new BattleManager(this);
	}

	public Teleport getExitTeleport() {
		return exitTeleport;
	}

	@Override
	public IBattleManager getBattleManager() {
		return battleManager;
	}
	

	public void leaveArena(Player player) {
		if(firstTeam.contains(player.getId())) {
			firstTeam.remove(player.getId());
		}
		else if(secondTeam.contains(player.getId())) {
			secondTeam.remove(player.getId());
		}

	}

	public void endBattle() {

	}

	@Override
	public List<PlayingCharacter> getPlayingAffectedCharactersInArea(IGameArea areaOfExecution, PlayingCharacter performer,	boolean isForEnemiesOnly) {
		List<PlayingCharacter> resultList = new ArrayList<>();
		List<String> targetPlayers = firstTeam;
		if(firstTeam.contains(performer.getId()) && isForEnemiesOnly || secondTeam.contains(performer.getId()) && !isForEnemiesOnly) {
			targetPlayers = secondTeam;
		}
		for (String playerId : targetPlayers) {
			if(players.containsKey(playerId)) {
				if (areaOfExecution.intersect(players.get(playerId).getCollisionShape().getArea())) {
					resultList.add(players.get(playerId));
				}
			}
		}
		return resultList;
	}
	
	@Override
	public List<IInteractionCharacter> getAllNoMonsterCharacters() {
		return super.getAllNoMonsterCharacters();
	}
	 
	@Override
	public void addPlayer(Player player){
		super.addPlayer(player);
		if(firstTeam.isEmpty()) {
			firstTeam.add(player.getId());
		}
		else {
			secondTeam.add(player.getId());
		}
	}

	@Override
	public void removePlayer(String player){
		if(firstTeam.contains(player)) {
			firstTeam.remove(player);
		}
		else {
			secondTeam.remove(player);
		}
		super.removePlayer(player);
    }
	
	@Override
	public void playerDied(Player player) {
		super.playerDied(player);
	}

	

}