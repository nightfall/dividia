package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.world.definitions.IDungeonGameMapDefinition;

public class DungeonGameMap extends MonsterGameMap {

	private IDungeonGameMapDefinition definition; // TODO type check
	
	public DungeonGameMap(IDungeonGameMapDefinition definition) {
		super(definition);
		this.definition = definition;
	}
}
