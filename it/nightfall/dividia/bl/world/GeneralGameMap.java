package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.characters.IInteractionCharacter;
import it.nightfall.dividia.api.world.definitions.IGeneralGameMapDefinition;
import it.nightfall.dividia.bl.characters.Player;
import java.util.LinkedList;
import java.util.List;

public class GeneralGameMap extends AbstractGameMap {

	private IGeneralGameMapDefinition definition;

	public GeneralGameMap(IGeneralGameMapDefinition definition){
		super(definition); //TODO type check
		this.definition = definition;
	}
	
	@Override
	public List<IInteractionCharacter> getAllNoMonsterCharacters() {
		List<IInteractionCharacter> list= new LinkedList<>();	
		list.addAll(players.values());
		list.addAll(npcs.values());
		return list;
	}

	@Override
	public void playerDied(Player player) {
		deadPlayers.put(player.getId(), player);
		players.remove(player.getId());
	}

   
}
