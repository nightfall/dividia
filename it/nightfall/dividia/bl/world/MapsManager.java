/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.IMapsManager;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.GameManager;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Andrea
 */
public class MapsManager implements IMapsManager {
	private Map<String, IGameMap> gameMaps;
	private static MapsManager instance = null;
	private GuiStatus GuiStatus;

	private MapsManager(){
		gameMaps = new HashMap<>();
	}

	public static MapsManager getInstance(){
		if(instance == null){
			instance = new MapsManager();
		}
		return instance;
	}

	@Override
	public void useTeleportForPlayer(IGameMap previousMap, Player player, Teleport teleport){
		IGameMapDefinition nextMapDefinition = GameStorage.getInstance().getMapDefinition(teleport.getNextMap());
		IGameMap currentMap = player.getCurrentMap();
		currentMap.removePlayer(player.getId());
		//vedere se le mappe sono uguali così andiamo avanti
		movePlayerToMap(player, teleport.getNextMap(), teleport.getNextLocation());
		if(currentMap.is2D() != nextMapDefinition.is2D()){
			//TODO delete print
			System.err.println("Changed direction movement manager");
			player.switchDirectionMovementManager(nextMapDefinition.is2D());
		}
		EventManager.getInstance().teleportUsed(previousMap, player, teleport);

	}
	
	@Override
	public void loadMap(String map, IGameMapDefinition definition){
		gameMaps.put(map, definition.getMapFromDefinition());
	}
	//chiamata da GUI quando finisce il caricamento totale

	@Override
	public void finishedLoadingMap(String playerName, String map){
		IGameMap gameMap = gameMaps.get(map);
		Player player = gameMap.getLoadingPlayers().get(playerName);
		player.setCurrentMap(gameMap);
	 
		if(GameManager.getInstance().getPlayer(playerName) == null) {
			GameManager.getInstance().addPlayer(player);
		}
		//old code : Player player = GameManager.getInstance().getPlayer(playerName);
		gameMap.addPlayer(player);
		gameMap.removeLoadingPlayer(playerName);
		EventManager.getInstance().playerEnteredMap(player.getSummary(), player.getCurrentMap());
	}

	public void buildSummary(String map, String player){
		Thread mapSummaryBuilder = new Thread(new MapSummaryBuilder(gameMaps.get(map), player), "map summary builder");
		mapSummaryBuilder.start();
	}

	public void movePlayerToMap(Player player, String nextMap, IGamePoint nextLocation){
		IGameMapDefinition nextMapDefinition = GameStorage.getInstance().getMapDefinition(nextMap);
		if(!existingMap(nextMap)){
			loadMap(nextMap, nextMapDefinition);
		}
		//collision shape selection
		GuiStatus guiStatus = GuiStatus.WORLD_3D;
		if (nextMapDefinition.is2D()){
			guiStatus = GuiStatus.WORLD_2D;
		}
		player.setCollisionShape(guiStatus);
		
		IGameMap map = gameMaps.get(nextMap);
		map.addLoadingPlayer(player);
		player.setCurrentMap(map);
		player.initPosition(nextLocation);
		buildSummary(nextMapDefinition.getName(), player.getId());

	}
	@Override
	public void summaryRequest(String playerName, String map){
		/*//Retrives current information on the map, by getting its summary
		 ISummary mapSummary = gameMaps.get(map).getSummary();
		 //Send the map summary
		 EventManager.getInstance().mapSummaryReady(playerName, mapSummary);
		 */
	}

	@Override
	public boolean existingMap(String mapId){
		return gameMaps.containsKey(mapId);
	}

	@Override
	public Map<String, IGameMap> getGameMaps(){
		return gameMaps;
	}

	@Override
	public IGameMap getMap(String mapId){
		return gameMaps.get(mapId);
	}

	private class MapSummaryBuilder implements Runnable {
		private IGameMap map;
		private String player;

		public MapSummaryBuilder(IGameMap map, String player){
			this.map = map;
			this.player = player;
		}

		private ISummary buildSummary(){
			return map.getSummary();
		}

		private void sendSummary(ISummary summary){
			EventManager.getInstance().mapSummaryReady(player, summary);
		}

		@Override
		public void run(){
			sendSummary(buildSummary());
		}
	}
}
