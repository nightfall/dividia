package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.battle.IBattleManager;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.world.IMonsterMap;
import it.nightfall.dividia.api.world.definitions.IMonsterGameMapDefinition;
import it.nightfall.dividia.api.world.summaries.IGeneralGameMapSummary;
import it.nightfall.dividia.bl.ai.AiManager;
import it.nightfall.dividia.bl.battle.BattleManager;
import it.nightfall.dividia.bl.characters.Monster;
import it.nightfall.dividia.bl.characters.NonPlayingCharacter;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.characters.PlayingCharacter;
import it.nightfall.dividia.bl.characters.definitions.MonsterDefinition;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.Summary;
import it.nightfall.dividia.bl.world.summaries.MonsterGameMapSummary;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MonsterGameMap extends GeneralGameMap implements IMonsterMap {

	private IBattleManager battleManager;
	private Map<String,Monster> monsters = new HashMap<>();
	private Map<String,Monster> deadMonsters = new HashMap<>();
	
	private IMonsterGameMapDefinition definition;
	private AiManager aiManager;

	public MonsterGameMap(IMonsterGameMapDefinition definition) {
		super(definition);
		this.definition = definition;
		this.battleManager = new BattleManager(this);
		this.aiManager = new AiManager();
		List<MonsterDefinition> monsterDefinitions = definition.getMonsterDefinitions();
		for(MonsterDefinition monsterDefinition:monsterDefinitions) {
			Monster nextMonster = monsterDefinition.getMonsterFromDefinition(this);
			monsters.put(nextMonster.getId(),nextMonster);
			nextMonster.inizialitePossibilities();
			aiManager.addMonster(nextMonster);
		}
		aiManager.activate();
			
	}

	
	
	@Override
	public IBattleManager getBattleManager() {
		return battleManager;

	}

	@Override
	public List<PlayingCharacter> getPlayingAffectedCharactersInArea(
			IGameArea areaOfExecution, PlayingCharacter performer,
			boolean isForEnemiesOnly) {
		List<PlayingCharacter> resultList = new ArrayList<>();

		boolean monsterFilter = false;
		if (performer instanceof Player && isForEnemiesOnly
				|| performer instanceof Monster && !isForEnemiesOnly) {
			monsterFilter = true;
		}
		
		for (NonPlayingCharacter n : npcs.values()){
			if (areaOfExecution.intersect(n.getCollisionShape().getArea())){
			}
		}
		
		Rectangle2D debugRectangle = areaOfExecution.getArea().getBounds2D();
		if (monsterFilter) {
			for (Monster m : monsters.values()) {
				if (areaOfExecution.intersect(m.getCollisionShape().getArea())) {
					resultList.add(m);
				}
			}
		} else {
			for (Player p : players.values()) {
				if (areaOfExecution.intersect(p.getCollisionShape().getArea())) {
					resultList.add(p);
				}
			}

		}
		return resultList;
	}

	public List<Player> getPlayersInPatrolArea(Monster inspector) {

		List<Player> playerList = new LinkedList<>();
		for (Player p : players.values()) {

			if (inspector.getPosition().getDistanceFromAnotherPoint(p.getPosition()) <= inspector.getUdibilityRadius()) {
				playerList.add(p);
			} else if (inspector.getPosition().getDistanceFromAnotherPoint(p.getPosition()) <= inspector.getVisibilityLenght()) {
				if (SharedFunctions.gamePointIsInInteractionAngle(inspector.getVisibilityAngle(), inspector.getPosition(), inspector.getDirection(), p.getPosition())) {
					playerList.add(p);
				}
			}
		}
		return playerList;
	}

	@Override
	public IGeneralGameMapSummary createMapSummary() {
		Set<String> monstersName = new HashSet<>();
		for (Monster currentMonster : monsters.values()) {
			monstersName.add(currentMonster.getId());
		}
		return new MonsterGameMapSummary(players.keySet(), bagsOfItems.keySet(), monstersName);
	}
	
	@Override
	public ISummary getSummary (){
		ISummary summary = super.getSummary();
		ISummary monstersSummary = new Summary();
			
		for (Monster currentMonster : monsters.values()){
			monstersSummary.putData(currentMonster.getId(), currentMonster.getSummary());
		}
		summary.putData("monsters", monstersSummary);
		return summary;
	}

	@Override
	public void monsterDied(Monster monster) {
		deadMonsters.put(monster.getId(), monster);
		monsters.remove(monster.getId());
		aiManager.removeMonster(monster);		
	}

	@Override
	public void monsterRespawn(Monster monster) {
		deadMonsters.remove(monster.getId());
		monsters.put(monster.getId(), monster);
		//TODO sincronizzare!
		aiManager.addMonster(monster);
		EventManager.getInstance().monsterEnteredMap(monster.getSummary(), this);
		
	}
	
	@Override
	public void playerDied(Player player) {
		super.playerDied(player);		
		for(Monster monster:monsters.values()) {
			if(monster.getTarget()!= null && monster.getTarget().equals(player)) {
				monster.removeAllDecisions();
				monster.setTarget(null);
			}
		}	
		
	}
	
	@Override
	public void removePlayer(String player){
        super.removePlayer(player);
		for(Monster monster:monsters.values()) {
			if(monster.getTarget()!= null && monster.getTarget().getId().equals(player)) {
				monster.removeAllDecisions();
				monster.setTarget(null);
			}
		}
    }
	
	
}
