package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.utility.GameArea;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.Summary;


/**
 * A portal is a physical representation of a teleport. 
 * @author falko91
 */
public class Portal extends Teleport {
	private IGamePoint location;
	private String relatedMap;
	private float radius;
	private IGameArea area;

	public Portal(IConfigReader configReader, String mapId) {
            super(configReader);
            this.location = new GamePoint(configReader);
            this.relatedMap = mapId;
            this.radius = configReader.getFloat("radious");
			//compute area of the teleport
			area = new GameArea(radius);
			//translate by radius, because java moves from edge
			area.translate(location.getPointFromTranslation(-radius));
			//TODO remove
			System.out.println(">> Loaded a portal in " + mapId + 
					" position :" + location.getX() +" "+ location.getY());
	}
	
	public IGamePoint getPortalLocation() {
            return location;
	}

	public String getPortalMap() {
            return relatedMap;
	}
	
	public float getRadius(){
            return radius;
	}

	public IGameArea getArea(){
		return area;
	}
	
	public void use(Player player) {
           
		
	}

	ISummary getSummary() {
		ISummary summary = new Summary();
		summary.putData("id", getId());
		summary.putData("radius", radius);
		return summary;
	}
}
