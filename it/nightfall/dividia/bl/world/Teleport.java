package it.nightfall.dividia.bl.world;

import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.GamePoint;

/**
 * A teleport is the mean used by players to change their game location.
 *
 * @author falko91
 */
public class Teleport {
	private IGamePoint nextLocation;
	private String nextMap;
	private String id;

	public Teleport(IConfigReader configReader){
		this.nextLocation = new GamePoint(configReader.getConfigReader("nextLocation"));
		this.nextMap = configReader.getString("nextMap");
		this.id = configReader.getString("id");
	}

	public Teleport(IGamePoint nextLocation, String nextMap) {
		this.nextLocation = nextLocation;
		this.nextMap = nextMap;
	}	

	public IGamePoint getNextLocation(){
		return nextLocation;
	}

	public void setNextLocation(GamePoint nextLocation){
		this.nextLocation = nextLocation;
	}

	public String getNextMap(){
		return nextMap;
	}

	public String getId(){
		return id;
	}
}
