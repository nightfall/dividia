/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.world.collisions;

import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.gui_api.world.IGraphicShape;

/**
 *
 * @author Andrea
 */
public abstract class AbstractCollisionShape implements ICollisionShape,Cloneable{

	protected IGameArea area;
	//position of the top-left corner of the shape or the rectangle that bounds the shape 
	protected IGamePoint position;
	
	public AbstractCollisionShape(IConfigReader conf){
		//top-left position
		double x = conf.getDoubleAttribute("x");
		double y = conf.getDoubleAttribute("y");
		position = new GamePoint(x, y);
		//System.out.println("Reading "+ conf.getStringAttribute("name") + "pos :" + position.getX() + " "+ position.getY());
	}
	
	public AbstractCollisionShape(double x, double y){
		position = new GamePoint(x, y);
	}
	
	@Override
	public IGameArea getArea() {
		return area;
	}

	@Override
	public IGamePoint getPosition() {
		return position;
	}
	//TODO replace with specifc-intersection methods of the subclasses
	@Override
	public boolean intersect(ICollisionShape otherShape) {
		return area.intersect(otherShape.getArea());
	}
	
	public void setArea(IGameArea area){
		this.area = area;
	}
	
	@Override
	public void translate(double deltaX, double deltaY){
		IGamePoint pos = new GamePoint(deltaX,deltaY);
		position.translate(pos);
		area.translate(pos);
	}
	
	@Override
	abstract public Object clone();
	
	/**
	 * Returns the sector id which contains a vertex.
	 * @param x x coordinate of the vertex.
	 * @param y y coordinate of the vertex.
	 * @param secWidth sector width.
	 * @param secHeight sector height.
	 * @param sectorsInColumns how many sectors are present in a column.
	 * @return 
	 */
	protected Integer getVertexSector(double x, double y, double secWidth, double secHeight, int sectorsInRow ){
		int row = (int) (y/secHeight);
		int col = (int) (x/secWidth);
		return row * sectorsInRow + col;
	}
	
}
