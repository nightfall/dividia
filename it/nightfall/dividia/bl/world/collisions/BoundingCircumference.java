/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.world.collisions;

import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.collisions.IBoundingCircumference;
import it.nightfall.dividia.api.world.collisions.IMapSector;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.utility.GameArea;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.gui.world.collisions.GraphicCircumferenceShape;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Andrea
 */
public class BoundingCircumference extends AbstractCollisionShape implements IBoundingCircumference {

	private double radius;

	public BoundingCircumference(IConfigReader conf) {
		super(conf);
		this.radius = conf.getDoubleAttribute("radius");
		super.setArea(new GameArea(new Area(new Ellipse2D.Double(position.getX(),position.getY(),radius*2,radius*2))));

	}
	
	public BoundingCircumference(double posX, double posY, double radius){
		super(posX, posY);
		this.radius = radius;
		super.setArea(new GameArea(new Area(new Ellipse2D.Double(posX,posY,radius*2,radius*2))));
	}
	
	
	@Override
	public Object clone() {
		return new BoundingCircumference(position.getX(),position.getY(),radius);
	}

	@Override
	public Set<Integer> findBelongingSectors(IGameMapDefinition map) {
		Set<Integer> result = new HashSet<>();
		double secWidth = map.getSectorWidth();
		double secHeight = map.getSectorHeight();
		int sectorInRows = map.getSectorsInRows();
		int mainBelongingSector = getVertexSector(position.getX(), position.getY(), secWidth, secHeight, sectorInRows);
		result.add(mainBelongingSector);
		
		//from sector number to row and cols
		int row = mainBelongingSector / sectorInRows;
		int col = mainBelongingSector % sectorInRows;
		
		//corresponding sector
		IMapSector mainSector = map.getSectors()[row][col];
		//bottom-left vertex
		double bottomLeftY = row * secHeight;
		double bottomLeftX = col * secWidth;
		//bottom-right vertex
		double bottomRightY = bottomLeftY;
		double bottomRightX = bottomLeftX + secWidth;
		//top-left vertex
		double topLeftY = bottomLeftY + secHeight;
		double topLeftX = bottomLeftX;
		//top-right vertex
		double topRightX = topLeftX + secWidth;
		double topRightY = topLeftY;

		//check if sector vertex are inside the circumference
		if(isPointInsideCircle(topLeftX,topLeftY)){
			//add left,upper and up-left sectors
			//left
			result.add(row * sectorInRows + col - 1);
			//upper
			result.add((row + 1) * sectorInRows + col);
			//left-upper
			result.add((row +1) * sectorInRows + col -1);
		}
		if(isPointInsideCircle(topRightX,topRightY)){
			//add right, upper and up-right sectors
			//right
			result.add(row * sectorInRows + col + 1);
			//upper
			result.add((row + 1) * sectorInRows + col);
			//right-upper
			result.add((row +1) * sectorInRows + col +1);
		}
		if(isPointInsideCircle(bottomLeftX,bottomLeftY)){
			//add left, bottom and bottom-left sectors
			//left
			result.add(row * sectorInRows + col - 1);
			//bottom
			result.add((row - 1) * sectorInRows + col);
			//bottom-left
			result.add((row -1) * sectorInRows + col -1);
		}
		if(isPointInsideCircle(bottomRightX,bottomRightY)){
			//add right, bottom and bottom-right sectors
			//right
			result.add(row * sectorInRows + col + 1);
			//bottom
			result.add((row - 1) * sectorInRows + col);
			//bottom-right
			result.add((row -1) * sectorInRows + col +1);
		
		}
		//check with edges of the sector
		
		//left edge
		if (isPointInsideCircle(bottomLeftX, bottomLeftY + secHeight/2)){
			//add left sec-thor
			result.add(row * sectorInRows + col -1);
		}
		//bottom edge
		if (isPointInsideCircle(bottomLeftX + secWidth/2, bottomLeftY)){
			//add bottom sec-thor
			result.add((row -1) * sectorInRows + col);
		}
		//top edge
		if (isPointInsideCircle(topLeftX , topLeftY + secHeight/2)){
			//add top sec-thor
			result.add((row+1) * sectorInRows + col);
		}
		//right edge
		if (isPointInsideCircle(topRightX, topRightY - secHeight/2 )){
			//add right sec-thor
			result.add((row) * sectorInRows + col +1);
		}
		
		return result;
	}
	
	private boolean isPointInsideCircle(double x, double y){
		return Math.pow(x - position.getX(), 2) + Math.pow(y - position.getY(),2) <= radius * radius;
	}

	@Override
	public void translateToFitCharacter(IGamePoint characterPosition) {
		this.position = new GamePoint(characterPosition.getX() - radius, characterPosition.getY() - radius);
		super.setArea(new GameArea(new Area(new Ellipse2D.Double(position.getX(),position.getY(),radius*2,radius*2))));
	}

	@Override
	public IGraphicShape createGraphicShape() {
		return new GraphicCircumferenceShape(this);
	}

	@Override
	public double getRadius() {
		return radius;
	}
	
}
