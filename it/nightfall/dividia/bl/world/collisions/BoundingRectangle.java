/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.world.collisions;

import it.nightfall.dividia.api.characters.ICharacter;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.collisions.IBoundingRectangle;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.utility.GameArea;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.gui.world.collisions.GraphicRectangularShape;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Set;
import sun.security.provider.SHA;

/**
 *
 * @author Andrea
 */
public class BoundingRectangle extends AbstractCollisionShape implements IBoundingRectangle{

	private double width;
	private double height;
	
	public BoundingRectangle (IConfigReader conf){
		super(conf);
		//bottom-left corner
		width = conf.getDoubleAttribute("width");
		height = conf.getDoubleAttribute("height");
		//set java area, from top-left origin
		//System.out.println(width + " " + height);
		super.setArea(new GameArea(new Area(new Rectangle2D.Double(position.getX(), position.getY(), width, height))));
	}
	
	public BoundingRectangle (double width, double height, double x, double y){
		super(x,y);
		this.width = width;
		this.height = height;
		super.setArea(new GameArea(new Area(new Rectangle2D.Double(x, y, width, height))));
	}
	
	@Override
	public double getHeight() {
		return height;
	}

	@Override
	public double getWidth() {
		return width;
	}

	/**
	 * Finds sectors which contain this shape.
	 * @param map Map where the check is performed
	 * @return 
	 */
	@Override
	public Set<Integer> findBelongingSectors(IGameMapDefinition map) {
		double secWidth = map.getSectorWidth();
		double secHeight = map.getSectorHeight();
		int sectorsInRow = map.getSectorsInRows();
		Set<Integer> result = new HashSet<>();
		//bottom-left vertex of the new position
		double x = position.getX();
		double y = position.getY();
		
		//top-left vertex
		result.add(getVertexSector(x, y, secWidth, secHeight, sectorsInRow));
		//top-right vertex
		result.add(getVertexSector(x+width, y, secWidth, secHeight, sectorsInRow));
		//bottom-left vertex
		result.add(getVertexSector(x, y+height, secWidth, secHeight, sectorsInRow));
		//bottom-right vertex
		result.add(getVertexSector(x+width, y+height, secWidth, secHeight, sectorsInRow));
		
		return result;
	}
	
	@Override
	public void translateToFitCharacter(IGamePoint characterPosition) {
		//because the character is a node, its position is in the center of the spatial. The shape should
		//cover the spatial, so a translation is performed : the bottom-left corner of the box has to be moved from the
		//center to the top-left "zone" of the character.
		position = new GamePoint(characterPosition.getX() - width/2, characterPosition.getY() - height/2);
		super.setArea(new GameArea(new Area(new Rectangle2D.Double(characterPosition.getX(), characterPosition.getY(), width, height))));
		//this.translate(characterPosition.getX() - width/2, characterPosition.getY() + height/2);
	}
	
	
	@Override 
	public Object clone(){
		return new BoundingRectangle(width, height, position.getX(), position.getY());
	}
	
	@Override
	public IGraphicShape createGraphicShape(){
		return new GraphicRectangularShape(this);
	}

}
