/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.world.collisions;

import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.api.world.collisions.IMapSector;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Andrea
 */
public class MapSector implements IMapSector{

	private Collection<ICollisionShape> collideableObjects;

	public MapSector(){
		this.collideableObjects = new ArrayList<>();
	}
	
	@Override
	public Collection<ICollisionShape> getCollideableObjects() {
		return collideableObjects;
	}

	@Override
	public void addCollisionObject(ICollisionShape obj) {
		collideableObjects.add(obj);
	}

	@Override
	public Collection<ICollisionShape> getCollisionObjects(ICollisionShape obj) {
		Collection<ICollisionShape> result = new ArrayList<>();
		for (ICollisionShape collisionShape : collideableObjects){
			if(collisionShape.intersect(obj)){
				result.add(obj);
			}
		}
		return result;
	}
}
