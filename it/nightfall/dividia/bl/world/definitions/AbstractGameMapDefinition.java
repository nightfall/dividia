
package it.nightfall.dividia.bl.world.definitions;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.api.world.collisions.IMapSector;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.characters.NonPlayingCharacter;
import it.nightfall.dividia.bl.characters.definitions.MonsterDefinition;
import it.nightfall.dividia.bl.characters.definitions.NonPlayingCharacterDefinition;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.world.collisions.BoundingRectangle;
import it.nightfall.dividia.bl.world.Portal;
import it.nightfall.dividia.bl.world.collisions.MapSector;
import it.nightfall.dividia.gui.utility.GuiStatus;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class AbstractGameMapDefinition implements IGameMapDefinition{
	protected double width;
	protected double height;
	protected String name;
	protected String displayName;
	protected IGamePoint defaultStartingPosition;
	protected IDirection defaultStartingDirection;
	protected List<NonPlayingCharacterDefinition> npcs;
	protected IMapSector[][] sectors;
	protected Set<ICollisionShape> collisionObjects;
	protected Map<String, Portal> portals;
	protected boolean is2D;
	protected int rowSectors;
	protected int colSectors;
	protected double sectorHeight;
	protected double sectorWidth;

	public AbstractGameMapDefinition(IConfigReader config) {
		this.width	= config.getInteger("width");
		this.height	= config.getInteger("height");
		this.name = config.getString("name");
		this.displayName = config.getString("displayName");
		this.rowSectors = config.getInteger("rowSectors");
		this.colSectors = config.getInteger("colSectors");
	    this.sectors = new IMapSector [rowSectors][colSectors];
		this.defaultStartingPosition = new GamePoint(config);
		this.defaultStartingDirection = new Direction(defaultStartingPosition);
		this.npcs = new ArrayList<>();
		this.portals = new HashMap<>();
		//portals
		Collection<IConfigReader> portalsConfig = config.getConfigReader("portals").getConfigReaderList("portal");
		for (IConfigReader currentPortal : portalsConfig){
			Portal toInsert = new Portal(currentPortal, name);
			portals.put(toInsert.getId(), toInsert);
		}
		this.is2D = config.getBoolean("dimension2D");
		//compute sector dimensions
		sectorWidth = width/colSectors;
		sectorHeight = height/rowSectors;
		collisionObjects = new HashSet<>();
		Collection<IConfigReader> objects = config.getConfigReader("objects").getConfigReaderList("object");
		//iterate over collision object elements in the XML
		for (IConfigReader currObject : objects){
			//get its class (its shape)
			String className = currObject.getStringAttribute("className");
			//TODO handle single exceptions
			try {
				//create a class object and instantiate the shape passing the config reader
				Class shapeClass = Class.forName(className);
				ICollisionShape obj = (ICollisionShape)shapeClass.getConstructors()[0].newInstance(currObject);
				
				//find all the belonging sectors for that collision object
				double posX = currObject.getDoubleAttribute("x");
				double posY = currObject.getDoubleAttribute("y");
				Set<Integer> belongingSectors = obj.findBelongingSectors(this);
				
				//for every sector, add that object
				for (Integer sector : belongingSectors){
					//row is the number of the current sector % the total sectors per col
					int row = (sector /	rowSectors);
					int col = (sector % rowSectors);
					//if is the first collidable object
					if(sectors[row][col] == null){
						sectors[row][col] = new MapSector();
					}
					//add the collideable object to the sector
					sectors[row][col].addCollisionObject(obj);
					//add the collideable object to the set of collision objects
					collisionObjects.add(obj);
				}
			} catch (Exception ex) {
				Logger.getLogger(AbstractGameMapDefinition.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		addNpcs(config);
	}
	public AbstractGameMapDefinition(){}
	
	@Override
	public String getName(){
		return name;
	}
	
	@Override
	public String getDisplayName(){
		return displayName;
	}

	@Override
	public IGamePoint getDefaultStartingPosition() {
		return defaultStartingPosition;
	}
	
	@Override
	public IDirection getDefaultStartingDirection() {
		return defaultStartingDirection;
	}
	
	@Override
	public boolean is2D (){
		return is2D;
	}
	
	@Override
	public Map<String, Portal> getPortals() {
		return portals;
	}
	
	@Override 
	public List<NonPlayingCharacterDefinition> getNpcDefinitions() {
		return npcs;
	}

	@Override
	abstract public IGameMap getMapFromDefinition();
	
	@Override
	public int getSectorsInColumns(){
		return colSectors;
	}
	
	@Override
	public double getSectorWidth(){
		return sectorWidth;
	}
	
	@Override
	public double  getSectorHeight(){
		return sectorHeight;
	}
	
	private void addNpcs(IConfigReader config) {
        Collection<IConfigReader> npcReaders = config.getConfigReader("npcs").getConfigReaderList("npc");
        for(IConfigReader reader:npcReaders) {
            npcs.add(new NonPlayingCharacterDefinition(reader));
        }
    }

	
	@Override
	public boolean collisionDetected(Set<Integer>playerSectors, ICollisionShape playerShape){
		//TODO create ad-hoc intersect function
		/*for (ICollisionShape currentObject : collisionObjects){
			if(playerShape.intersect(currentObject)){
				return true;
			}
		}
		return false;
		*/
		
		for (Integer sectorId : playerSectors){
			int rowIndex = sectorId / colSectors;
			int colIndex = sectorId % colSectors;
			if(sectors[rowIndex][colIndex] != null){
			Collection<ICollisionShape> collideableObj = sectors[rowIndex][colIndex].getCollideableObjects();
			if(collideableObj != null){
				for (ICollisionShape currentObject : collideableObj){
					if(playerShape.intersect(currentObject)){
						return true;
					}
				}
			}
			}
		}
		return false;
		
	
	}
	
	@Override
	public IMapSector[][] getSectors(){
		return sectors;
	}
	
	@Override
	public int getSectorsInRows(){
		return rowSectors;
	}
	
	@Override
	public Set<ICollisionShape> getCollisionObjects(){
		return collisionObjects;
	}
	
	@Override
	public double getHeight(){
		return height;
	}
	
	@Override
	public double getWidth(){
		return width;
	}
	
}
