package it.nightfall.dividia.bl.world.definitions;

import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.api.world.definitions.IArenaGameMapDefinition;
import it.nightfall.dividia.bl.world.ArenaGameMap;

public class ArenaGameMapDefinition extends AbstractGameMapDefinition implements IArenaGameMapDefinition {
	public ArenaGameMapDefinition(IConfigReader config){
		super(config);
	}

	@Override
	public IBattleMap getMapFromDefinition(){
		return new ArenaGameMap(this);
	}
}
