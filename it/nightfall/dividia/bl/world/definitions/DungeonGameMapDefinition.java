package it.nightfall.dividia.bl.world.definitions;

import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.api.world.definitions.IDungeonGameMapDefinition;
import it.nightfall.dividia.bl.world.DungeonGameMap;

public class DungeonGameMapDefinition extends MonsterGameMapDefinition implements IDungeonGameMapDefinition {
	public DungeonGameMapDefinition(IConfigReader config){
		super(config);
	}

	@Override
	public IBattleMap getMapFromDefinition(){
		return new DungeonGameMap(this);
	}
}
