package it.nightfall.dividia.bl.world.definitions;

import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.IGameMap;
import it.nightfall.dividia.api.world.definitions.IGeneralGameMapDefinition;
import it.nightfall.dividia.bl.world.GeneralGameMap;
import it.nightfall.dividia.gui.two_dimensional_gui.animations.AnimationData;

public class GeneralGameMapDefinition extends AbstractGameMapDefinition implements IGeneralGameMapDefinition {
	
	public GeneralGameMapDefinition(IConfigReader config){
		super(config);
	}
	
	@Override
	public IGameMap getMapFromDefinition(){
		return new GeneralGameMap(this);
	}
}
