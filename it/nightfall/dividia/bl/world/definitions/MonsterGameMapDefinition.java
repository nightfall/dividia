package it.nightfall.dividia.bl.world.definitions;

import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.IBattleMap;
import it.nightfall.dividia.api.world.definitions.IMonsterGameMapDefinition;
import it.nightfall.dividia.bl.characters.definitions.MonsterDefinition;
import it.nightfall.dividia.bl.world.MonsterGameMap;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class MonsterGameMapDefinition extends GeneralGameMapDefinition implements IMonsterGameMapDefinition {
	
	private List<MonsterDefinition> monsterDefinitions = new LinkedList<>();
	
	public MonsterGameMapDefinition(IConfigReader config){
		super(config);
		Collection<IConfigReader> monstersReader = config.getConfigReader("monsters").getConfigReaderList("monster");
		for(IConfigReader reader:monstersReader) {
			monsterDefinitions.add(new MonsterDefinition(reader));
		}
	}

	@Override
	public IBattleMap getMapFromDefinition(){
		return new MonsterGameMap(this);
	}

	@Override
	public List<MonsterDefinition> getMonsterDefinitions() {
		return monsterDefinitions;
	}
	
	
	
}
