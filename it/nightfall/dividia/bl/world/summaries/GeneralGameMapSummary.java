/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.world.summaries;

import it.nightfall.dividia.api.world.summaries.IGeneralGameMapSummary;
import it.nightfall.dividia.bl.events.EventManager;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui.world.SpatialsManager;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author falko91
 */
public class GeneralGameMapSummary implements IGeneralGameMapSummary{
	
	private Set<String> playerSet;
	private Set<String> itemSet;
	private String name;
	
	public GeneralGameMapSummary (Set<String> playerSet, Set<String> itemSet){
		this.itemSet = itemSet;
		this.playerSet = playerSet;
		this.name = name;
	}
	
	@Override
	public Set<String> getPlayerSet() {
		return Collections.unmodifiableSet(playerSet);
	}

	@Override
	public Set<String> getItemSet() {
		return Collections.unmodifiableSet(itemSet);
	}

	@Override
	public void receiveSummary() {
		//SpatialsManager.getInstance().loadSummary(this);
	}
	
}
