/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.bl.world.summaries;

import it.nightfall.dividia.api.world.summaries.IMonsterGameMapSummary;
import java.util.Collections;
import java.util.Set;

/**
 *
 * @author falko91
 */
public class MonsterGameMapSummary extends GeneralGameMapSummary implements IMonsterGameMapSummary{
	private Set<String> monsterSet;
	
	public MonsterGameMapSummary (Set<String> players, Set<String> items, Set<String> monsters){
		super(players,items);
		this.monsterSet = monsters;
	}
	
	@Override
	public Set<String> getMonsterSet() {
		return Collections.unmodifiableSet(monsterSet);
	}
}