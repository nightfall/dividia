/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui;

import com.jme3.asset.AssetManager;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.gui.utility.GuiStatus;
import it.nightfall.dividia.gui.world.AbstractPortal;
import it.nightfall.dividia.gui.world.Portal2D;
import it.nightfall.dividia.gui.world.Portal3D;

/**
 *
 */

public class EngineManager {
	private static EngineManager instance;
	private GuiStatus engine;
	private AssetManager assetManager;
	
	public static EngineManager getInstance(){
		if (instance == null){
			instance = new EngineManager();
		}
		return instance;
	}
	
	public void init(AssetManager aManager, GuiStatus e){
		engine = e;
		aManager = assetManager;
	}
	
	public AbstractPortal insertPortal(String name, float radius, IGamePoint position){
		if (engine == GuiStatus.WORLD_2D){
			return new Portal2D(name, assetManager, radius, position);
		}else{
			return new Portal3D(name, assetManager,radius, position);
		}
	}
}
