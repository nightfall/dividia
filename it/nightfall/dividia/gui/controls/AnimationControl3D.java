
package it.nightfall.dividia.gui.controls;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.gui.world.SpatialsManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class AnimationControl3D extends AbstractControl implements AnimEventListener {
	
	private AnimControl control;
	private AnimChannel channel;
	private List<Integer> idleCycle = new ArrayList<>();
	private int idleIndex = 0;
	private String movementAnimation;
	private String attackAnimation;
	private String deathAnimation;
	private boolean performingAnAttackAnimation = false;
	private boolean performingDeathAnimation = false;
	private float spatialSpeed;
	private float attacksPerSecond = 0;
	private Map<String, Float> animationSpeeds = new HashMap<>();

	public AnimationControl3D(float spatialSpeed) {
		this.spatialSpeed = spatialSpeed;
	}
	
	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
		control = spatial.getControl(AnimControl.class);
		channel = control.createChannel();
		control.addListener(this);
		channel.setAnim("Idle1");
	}
	
	@Override
	public synchronized void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
		if(animName.startsWith("Idle")) {
			idleIndex++;
			idleIndex = idleIndex % idleCycle.size();
			movementAnimation = "Idle"+idleCycle.get(idleIndex);			
		}
		if(performingAnAttackAnimation) {
			idleIndex = 0;
			if(movementAnimation == null) {
				movementAnimation = "Idle1";
			}
			performingAnAttackAnimation = false;
		}
		/*if(animName.equals("death")) {
			performingDeathAnimation = false;
			
			//SpatialsManager.getInstance().playingCharacterDeathCompleted(this.getSpatial());
		}*/
	}

	@Override
	public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
	}

	
	

	@Override
	protected synchronized void controlUpdate(float tpf) {
		//TODO all animation speed values must be read from file
		if(deathAnimation!=null && !performingDeathAnimation) {			
			attackAnimation = null;
			movementAnimation = null;
			channel.setAnim(deathAnimation,0.2f);
			Float relatedSpeed = animationSpeeds.get(deathAnimation);
			if(relatedSpeed!= null) {
				channel.setSpeed(relatedSpeed);				
			}
			else {
				channel.setSpeed(1);
			}
			channel.setLoopMode(LoopMode.DontLoop);
			deathAnimation = null;
			performingDeathAnimation = true;
			
		}
		else if(attackAnimation!=null && !performingDeathAnimation) {
			if(attackAnimation.startsWith("basicAttack")) {
				Random random = new Random();
				int randomInteger = 1 + random.nextInt(2);
				attackAnimation = attackAnimation + randomInteger;
			}
			channel.setAnim(attackAnimation,0.3f);
			Float relatedSpeed = animationSpeeds.get(attackAnimation);
			if(relatedSpeed!= null) {
				channel.setSpeed(relatedSpeed);				
			}
			if(attackAnimation.startsWith("basicAttack")) {
				channel.setSpeed(animationSpeeds.get("basicAttack") * attacksPerSecond);
			}
			channel.setLoopMode(LoopMode.DontLoop);
			performingAnAttackAnimation = true;
			attackAnimation = null;
			movementAnimation = null;
		}
		else if(movementAnimation!=null && !performingAnAttackAnimation && !performingDeathAnimation) {			
			channel.setAnim(movementAnimation,0.2f);
			if(movementAnimation.equals("Run")) {
				channel.setSpeed(animationSpeeds.get("Run") * spatialSpeed/100 );
			}
			else if(movementAnimation.startsWith("Idle")) {
				channel.setSpeed(animationSpeeds.get("Idle"));
			}
			channel.setLoopMode(LoopMode.Loop);
			movementAnimation = null;
		}	
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {
	}

	public synchronized void setSpatialSpeed(float spatialSpeed) {
		this.spatialSpeed = spatialSpeed;
        if(channel.getAnimationName().equals("Run")) {
            channel.setAnim("Run",0.2f);
            channel.setSpeed(animationSpeeds.get("Run") * spatialSpeed/100 );
        }
		
	}
	
	public synchronized void deathAnimationRequest(String otherAnimationId) {
		deathAnimation = otherAnimationId;
	}
	
	public synchronized void runRequest() {
		movementAnimation = "Run";		
	}

	public synchronized void idleRequest() {
		movementAnimation = "Idle1";
		idleIndex = 0;		
	}

	public synchronized void attackRequest(String animationId) {
		attackAnimation = animationId;
	}

	public synchronized void setAttackSpeed(int attackSpeed) {
		this.attacksPerSecond = attackSpeed/100.0f;
	}

	public synchronized void setAnimationSpeeds(Map<String, Float> animationSpeeds) {
		this.animationSpeeds = animationSpeeds;
	}

	public void setIdleCycle(List<Integer> idleCycle) {
		this.idleCycle = idleCycle;
	}
	
	@Override
	public Control cloneForSpatial(Spatial spatial) {
		return null;
	}
}
