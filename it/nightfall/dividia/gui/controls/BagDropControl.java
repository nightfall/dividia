/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.controls;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 *
 * @author bernardo
 */
public class BagDropControl extends Simple3DimensionalSpatialMovementControl {

	public BagDropControl(float spatialInitialSpeed) {
		super(spatialInitialSpeed);
	}
	
	@Override
    public void setSpatial(Spatial bag) {
        super.setSpatial(bag); 
		float droppingHeight = 0.7f;
		bag.getLocalTranslation().addLocal(0, 0, droppingHeight);
        destinationPoints.add(bag.getLocalTranslation().add(new Vector3f(0, 0, -droppingHeight)));
    }
}
