package it.nightfall.dividia.gui.controls;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.gui.sounds.SoundsManager;
import it.nightfall.dividia.gui_api.controls.ISpatialMovementControl;
import it.nightfall.dividia.gui_api.sounds.ICharacterSoundActionsListener;
import it.nightfall.dividia.gui_api.world.IGraphicShape;

	
public class CharacterMovementControl3D extends Simple3DimensionalSpatialMovementControl implements ISpatialMovementControl {
	public static int MAX_QUEUE_LENGTH = 10;
	public static int MAX_QUEUE_LENGTH_TO_MANTAIN_NORMAL_SPEED = 3;
	private boolean characterStopped = true;
	private AnimationControl3D animationControl;
	private boolean toDelete = false;
	
	
	public CharacterMovementControl3D(int playerInitialSpeed){ 
		super(playerInitialSpeed);
	} 
	
	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
		animationControl = new AnimationControl3D(spatialSpeed);
		spatial.addControl(animationControl);
	}
	
	private synchronized void jumpToCurrentLocation() {
		while(destinationPoints.size() != 1 ) {
			destinationPoints.poll();
		}
		Vector3f destination = destinationPoints.poll();
		spatial.setLocalTranslation(destination.clone());
	}

	@Override
	protected synchronized void controlUpdate(float tpf){
		if(!destinationPoints.isEmpty()){
			if(destinationPoints.size() > MAX_QUEUE_LENGTH_TO_MANTAIN_NORMAL_SPEED) {
				nextTranslationStep(tpf, 2);			
				System.out.println("DIMENSION REACHED");
			}			
			else {
				nextTranslationStep(tpf,1);
			}	
        }
	}
		
	@Override
	protected void controlRender(RenderManager rm, ViewPort vp){
	}
	
	@Override
	public synchronized void characterMoved(double x, double y) {
		destinationPoints.add(new Vector3f((float) x, (float)y, 0));
		if(characterStopped) {
			ICharacterSoundActionsListener characterSoundsManager = SoundsManager.getInstance().getCharacterSoundsManager(spatial.getName());
			if(characterSoundsManager!=null) {
				characterSoundsManager.characterStartedMoving();
			}
			animationControl.runRequest();
			characterStopped = false;
		}
	} 

	//TODO cambioVelocitÃƒÆ’Ã‚Â 
	@Override
	public Control cloneForSpatial(Spatial spatial){
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void characterStopped(){
		animationControl.idleRequest();
		characterStopped = true;
	}

	@Override
	public void setSpeed(int newValue) {
		super.spatialSpeed = newValue;
		animationControl.setSpatialSpeed(spatialSpeed);
	}

	public void setAnimationControl(AnimationControl3D animationControl) {
		this.animationControl = animationControl;
	}

}
