/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.controls;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import javax.xml.transform.Result;

/**
 *
 * @author Andrea
 */
public class CollisionShapeControl extends AbstractControl {

	private Spatial playerSpatial;
	private Vector3f lastPosition;
	
	public CollisionShapeControl(Spatial pSpatial){
		this.playerSpatial = pSpatial;
		lastPosition = playerSpatial.getLocalTranslation().clone();
	}
	
	@Override
	public void setSpatial(Spatial s){
		super.setSpatial(s);
	}
	@Override
	protected void controlUpdate(float tpf) {
	Vector3f newPosition = playerSpatial.getLocalTranslation().clone();
	spatial.move(newPosition.x - lastPosition.x, newPosition.y - lastPosition.y, 0);
	lastPosition = newPosition;
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {
	}
	
}
