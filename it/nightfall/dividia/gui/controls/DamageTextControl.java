/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.controls;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 *
 * @author bernardo
 */
public class DamageTextControl extends Simple3DimensionalSpatialMovementControl {
    
    public DamageTextControl(float spatialInitialSpeed) {
        super(spatialInitialSpeed);
        
    }
    
    @Override
    public void setSpatial(Spatial text) {
        super.setSpatial(text); 
        destinationPoints.add(text.getLocalTranslation().add(new Vector3f(0, 80, 0)));
    }
    
    @Override
	protected synchronized void controlUpdate(float tpf){		
        super.controlUpdate(tpf);
        if(destinationPoints.isEmpty()) {
            spatial.removeFromParent();
        }
	}
    
    
}
