
package it.nightfall.dividia.gui.controls;
import com.jme3.effect.ParticleEmitter;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import it.nightfall.dividia.gui.spellsAndEffects.SpeedFrame;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.controls.ISimple3DimensionalSpatialMovementControl;
import it.nightfall.dividia.gui_api.spells.IGraphicEffect;
import java.util.List;


public class EffectControl3D extends Simple3DimensionalSpatialMovementControlWithPause {

	protected IGraphicEffect effectNode;
	protected List<SpeedFrame> speedFrames;
	protected int frameIndex = 0;	
	private Node rootNode = SpatialsManager.getInstance().getRootNode();
	
	public EffectControl3D(float spatialInitialSpeed, IGraphicEffect effectNode) {		
		super(spatialInitialSpeed);
		this.effectNode = effectNode;
		this.speedFactor=1;
	}
	
	@Override
	public void setSpatial(Spatial effect) {
		super.setSpatial(effect);
		frameUpdate();
	}
	
	@Override
	protected void controlUpdate(float tpf) {	
		
		int leftWayPoints = destinationPoints.size();
		super.controlUpdate(tpf);
		for(ParticleEmitter emitter : effectNode.getEmitters()) {
			ISimple3DimensionalSpatialMovementControl emitterControl = emitter.getControl(ISimple3DimensionalSpatialMovementControl.class);
			emitterControl.getSpatial().setLocalTranslation(emitterControl.getSpatial().getLocalTranslation().add(lastTranslation));					
			if(!emitterControl.getDestinationPoints().isEmpty()) {				
				emitterControl.getDestinationPoints().peek().addLocal(lastTranslation);
				//emitterControl.addTranslationToNextPoint(lastTranslation);
			}
		}
		
		if(destinationPoints.isEmpty() && pauseTimes.isEmpty()) {
			//Node rootNode = SpatialsManager.getInstance().getRootNode();
			for(ParticleEmitter emitter : effectNode.getEmitters()) {
				emitter.setParticlesPerSec(0);
				if(rootNode!=null) {
					rootNode.detachChild(emitter);
				}
			}
			if(rootNode!=null) {
				rootNode.detachChild(spatial);
			}
		}
		else if(leftWayPoints-destinationPoints.size() != 0) {
			frameUpdate();
		}	
	}

	private void frameUpdate() {
		if(frameIndex < speedFrames.size()) {
			speedFrames.get(frameIndex).applyFrame(this);
			frameIndex++;
		}
	}

	public void setSpeedFrames(List<SpeedFrame> speedFrames) {
		this.speedFrames = speedFrames;
	}	

	public void setRootNode(Node rootNode) {
		this.rootNode = rootNode;
	}
	
	
	
}

