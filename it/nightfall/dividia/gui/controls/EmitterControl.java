
package it.nightfall.dividia.gui.controls;

import com.jme3.effect.ParticleEmitter;
import it.nightfall.dividia.gui.spellsAndEffects.NonPositionalEmitterFrame;
import java.util.List;


public class EmitterControl extends Simple3DimensionalSpatialMovementControlWithPause {

	private List<NonPositionalEmitterFrame> frames;
	private ParticleEmitter emitter;
	private NonPositionalEmitterFrame lastFrame;
	private int frameIndex =0;
	private Simple3DimensionalSpatialMovementControl spellControl;
	
	public EmitterControl(ParticleEmitter emitter) {
		super(0);
		this.emitter = emitter;			
		this.speedFactor=1;
	}
	
	public void applyTheFirstFrame() {		
		lastFrame = frames.get(frameIndex);
		frames.get(0).applyFrame(this);	
		frameIndex++;
	}
	
	@Override
	protected void controlUpdate(float tpf) {
		int leftFrames = nextFrameIsAPauseOne.size();
		super.controlUpdate(tpf);
		if(leftFrames-nextFrameIsAPauseOne.size() != 0) {
			applyFrame();
		}
		else if(frameIndex < frames.size()){
			frameUpdate();
		}		
	}

	private void applyFrame() {
		if(frameIndex< frames.size()) {
			if(!destinationPoints.isEmpty()) {
				destinationPoints.peek().addLocal(spellControl.getTotalTranslation());
			}
			lastFrame = frames.get(frameIndex);
			frames.get(frameIndex).applyFrame(this);
			frameIndex++;
			progress = 0;
		}
	}

	public void setFrames(List<NonPositionalEmitterFrame> frames) {
		this.frames = frames;
		this.lastFrame = frames.get(frameIndex);
	}

	private void frameUpdate() {
		frames.get(frameIndex).interpolateFrame(lastFrame,progress, this);
	}

	public void setSpellNode(Simple3DimensionalSpatialMovementControl spellControl) {
		this.spellControl = spellControl;
	}

	public ParticleEmitter getEmitter() {
		return emitter;
	}
	
}
