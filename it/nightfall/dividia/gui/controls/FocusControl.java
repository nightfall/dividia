package it.nightfall.dividia.gui.controls;

import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Spatial;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.bl.utility.playerMovements.FocusStatus;
import it.nightfall.dividia.gui.utility.input.InGameMapping;
import it.nightfall.dividia.gui_api.controls.IFocusControl;


public class FocusControl extends Simple3DimensionalSpatialMovementControl implements IFocusControl {
	
	private FocusStatus focusStatus;
	private Camera cam;
	private Vector3f camHeight = new Vector3f(0, 0, IGameConstants.CAMERA_DEFAULT_HEIGHT); 
	private Spatial player;
	private float playerSpeed;
	private boolean focusResetting = false;
	private boolean doubleSpeed = false;
	
	
	public FocusControl(int playerInitialSpeed, FocusStatus focusStatus, Camera cam, Spatial player) {
		super(playerInitialSpeed);
		this.focusStatus = focusStatus;
		playerSpeed = playerInitialSpeed;
		this.cam = cam;		
		this.player = player;
	}

	@Override
	public synchronized void focusMoved(double x, double y, String nextFocusStatus) {
		if(!focusResetting) {
			if(FocusStatus.valueOf(nextFocusStatus) == FocusStatus.SIMPLE_ROTATION && focusStatus!=FocusStatus.SIMPLE_ROTATION) {
				super.setSpatialSpeed(playerSpeed);
				focusStatus = FocusStatus.SIMPLE_ROTATION;
			}
			else if(FocusStatus.valueOf(nextFocusStatus) != FocusStatus.SIMPLE_ROTATION && focusStatus==FocusStatus.SIMPLE_ROTATION) {
				super.setSpatialSpeed(playerSpeed / IGameConstants.SQRT_OF_2f);
				focusStatus = FocusStatus.FORWARD_LEFT_BACKWARD_RIGHT;
			}
		}
		destinationPoints.add(new Vector3f((float) x, (float) y, 0));	
	}
	
	@Override
	protected synchronized void controlUpdate(float tpf) {
		if(focusResetting == true) {
			if(!doubleSpeed) {
				super.setSpatialSpeed(spatialSpeed*20);
				InGameMapping.getInstance().cameraIsAdjusting = true;
				doubleSpeed=true;
			}
			super.controlUpdate(tpf);	
			if(destinationPoints.size() ==0 ) {
				if(focusStatus == FocusStatus.SIMPLE_ROTATION) {
					super.setSpatialSpeed(playerSpeed);
				}
				else {
					super.setSpatialSpeed(playerSpeed / IGameConstants.SQRT_OF_2f);
				}
				doubleSpeed = false;
				focusResetting = false;
				InGameMapping.getInstance().cameraIsAdjusting = false;
			}
			
		}
		else {
			super.controlUpdate(tpf);	
		}
		cam.setLocation(spatial.getLocalTranslation());
		cam.getLocation().addLocal(camHeight);
		cam.lookAt(player.getLocalTranslation(), Vector3f.UNIT_Z);
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
	}	

	@Override
	public synchronized void focusReset(double x, double y) {	
		destinationPoints.add(new Vector3f((float) x,(float)  y,0));
		focusResetting = true;
	}
	
	@Override
	public void setSpatialSpeed(float spatialSpeed) {
		playerSpeed = spatialSpeed;
		doubleSpeed = false;
		if(focusStatus == FocusStatus.SIMPLE_ROTATION) {			
			super.setSpatialSpeed(spatialSpeed);			
		}
		else {			
			super.setSpatialSpeed(spatialSpeed / IGameConstants.SQRT_OF_2f);
		}
	}
	
    
    public void changeCameraHeight(float amount) {
        float sum = amount+camHeight.getZ();
        if(sum>=1 && sum<=10) {
            this.camHeight.addLocal(0, 0, amount);
        }
    }
    
 }
