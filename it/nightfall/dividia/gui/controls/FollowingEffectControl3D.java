package it.nightfall.dividia.gui.controls;

import com.jme3.effect.ParticleEmitter;
import com.jme3.scene.Node;
import it.nightfall.dividia.gui_api.controls.ISimple3DimensionalSpatialMovementControl;
import it.nightfall.dividia.gui_api.controls.ISpatialMovementControl;
import it.nightfall.dividia.gui_api.spells.IGraphicEffect;


public class FollowingEffectControl3D extends EffectControl3D {

    private ISpatialMovementControl toFollowControl;
    public FollowingEffectControl3D(float spatialInitialSpeed, IGraphicEffect spellNode, ISpatialMovementControl toFollowControl) {
        super(spatialInitialSpeed, spellNode);
        this.toFollowControl = toFollowControl;
        
    }

	@Override
	public void controlUpdate(float tpf) {
	   
		for(ParticleEmitter emitter : effectNode.getEmitters()) {
			ISimple3DimensionalSpatialMovementControl emitterControl = emitter.getControl(ISimple3DimensionalSpatialMovementControl.class);
			emitterControl.getSpatial().getLocalTranslation().x = toFollowControl.getSpatial().getLocalTranslation().x;
			emitterControl.getSpatial().getLocalTranslation().y = toFollowControl.getSpatial().getLocalTranslation().y;
		}
		super.controlUpdate(tpf);
       
	}

	public ISpatialMovementControl getToFollowControl() {
		return toFollowControl;
	}
    
    public void shutDownEffect(Node rootNode) {
		for(ParticleEmitter emitter : effectNode.getEmitters()) {
				emitter.setParticlesPerSec(0);
				if(rootNode!=null) {
					rootNode.detachChild(emitter);
				}
			}
			if(rootNode!=null) {
				rootNode.detachChild(spatial);
			}
	}
    
    
    
}
