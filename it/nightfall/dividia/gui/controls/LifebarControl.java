package it.nightfall.dividia.gui.controls;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import com.jme3.ui.Picture;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.gui.utility.CoordinatesManager;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.vecmath.Vector2f;

public class LifebarControl extends AbstractControl {
	private static final String CONFIG_FILE = SharedFunctions.getPath("gui|lifebar.xml");
	private static final float MIN_SPEED = 0.5f, MAX_SPEED = 2.5f, MINIMAP_RANGE_SCALING = 20;
	private static LifebarControl lifebarControl;
	private static Node lifebarNode = new Node();
	private final AssetManager assetManager;
	private final BitmapFont bitmapFont;
	private final CoordinatesManager coordinatesManager;
	private final Vector2f lifeBarPosition = new Vector2f(),
			pointerPosition = new Vector2f(),
			healthPosition = new Vector2f(),
			manaPosition = new Vector2f(),
			ragePosition = new Vector2f();
	private final Vector2f lifeBarDimensions = new Vector2f(),
			pointerDimension = new Vector2f(),
			healthDimension = new Vector2f(),
			manaDimension = new Vector2f(),
			rageDimension = new Vector2f();
	private final Picture lifeBarImage = new Picture("LifeBar"),
			lifeBar2Image = new Picture("LifeBar2"),
			healthImage = new Picture("Health"),
			manaImage = new Picture("Mana"),
			rageImage = new Picture("Rage"),
			healthDiffImage = new Picture("HealthDiff"),
			manaDiffImage = new Picture("ManaDiff"),
			rageDiffImage = new Picture("RageDiff"),
			pointerImage = new Picture("Arrow"),
			monsterImage = new Picture("Monster"),
			playerImage = new Picture("Player");
	private final Node pointerNode = new Node();
	private final Node minimapNode = new Node();
	private final BitmapText healthText, manaText;
	private int maxHealth, maxMana, maxRage = 100;
	private float shownHealth = 0, shownMana = 0, shownRage = 0;
	private float healthSpeed, manaSpeed, rageSpeed;
	private int currentHealth, currentMana, currentRage;
	private float shownHealthDiff = 0, shownManaDiff = 0, shownRageDiff = 0;
	private float wHealth, wMana, wRage;
	private float wHealthDiff, wManaDiff, wRageDiff;
	private float scaleFactor;
	private double pointerRotation = 0;
	private Vector2f playerPosition = new Vector2f(0, 0);
	private boolean barsValid = false;
	private boolean minimapValid = false;
	private Quaternion pointerQuaternion = new Quaternion();
	private Map<String, Vector2f> entityPositionsMap = new HashMap<>();
	private Map<String, Picture> entityPicturesMap = new HashMap<>();
	private Set<String> needToBeProcessed = new HashSet<>();
	private Set<String> attachedEntities = new HashSet<>();
	private Set<String> invisibleEntities = new HashSet<>();

	private boolean pointerValid;
	private boolean minimapCleared;
	
	public static void lifebarInit(AssetManager assetManager, Camera cam){
		lifebarControl = new LifebarControl(assetManager, cam);
		lifebarNode.addControl(lifebarControl);
	}

	public static Node getLifebar(){
		return lifebarNode;
	}

	public static LifebarControl getLifebarControl(){
		return lifebarControl;
	}

	protected LifebarControl(AssetManager assetManager, Camera cam){
		this.assetManager = assetManager;
		IConfigReader config = new ConfigReader(CONFIG_FILE);
		coordinatesManager = new CoordinatesManager(new Vector2f(cam.getWidth(), cam.getHeight()));
		bitmapFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		healthText = new BitmapText(bitmapFont, false);
		manaText = new BitmapText(bitmapFont, false);
		backgroundInitializer(config.getConfigReader("background2"), lifeBar2Image, lifeBarDimensions, lifeBarPosition, config.getFloat("scale"));
		pointerInitializer(config.getConfigReader("pointer"), pointerImage, pointerDimension, pointerPosition);
		backgroundInitializer(config.getConfigReader("background"), lifeBarImage, lifeBarDimensions, lifeBarPosition, config.getFloat("scale"));
		barsInitializer(config.getConfigReader("health"), healthImage, healthDiffImage, healthDimension, healthPosition);
		barsInitializer(config.getConfigReader("mana"), manaImage, manaDiffImage, manaDimension, manaPosition);
		barsInitializer(config.getConfigReader("rage"), rageImage, rageDiffImage, rageDimension, ragePosition);
		entityInitializer(config.getConfigReader("monsterPointer"), monsterImage, new Vector2f(), new Vector2f());
		entityInitializer(config.getConfigReader("playerPointer"), playerImage, new Vector2f(), new Vector2f());
		textInitializer(healthText, healthDimension, healthPosition);
		textInitializer(manaText, manaDimension, manaPosition);
	}

	public synchronized void setMaxHealth(int max){
		maxHealth = max;
		invalidateBars();
	}

	public synchronized void setMaxMana(int max){
		maxMana = max;
		invalidateBars();
	}

	public synchronized void setMaxRage(int max){
		maxRage = max;
		invalidateBars();
	}

	public synchronized void updateHealth(int delta){
		currentHealth += delta;
		healthSpeed = getSpeed(delta, maxHealth);
		invalidateBars();
	}

	public synchronized void updateMana(int delta){
		currentMana += delta;
		manaSpeed = getSpeed(delta, maxMana);
		invalidateBars();
	}

	public synchronized void updateRage(int delta){
		currentRage += delta;
		rageSpeed = getSpeed(delta, maxRage);
		invalidateBars();
	}

	public synchronized int getCurrentHealth(){
		return currentHealth;
	}

	public synchronized int getCurrentMana(){
		return currentMana;
	}

	public synchronized int getCurrentRage(){
		return currentRage;
	}

	public synchronized void setRotation(double angle){
		pointerRotation = angle;
		invalidatePointer();
	}

	public synchronized void updateCurrentPlayerPosition(float x, float y){
		playerPosition.x = x;
		playerPosition.y = y;
		invalidateMinimap();
	}

	public synchronized void updateEntityPosition(String id, float x, float y){
		entityPositionsMap.put(id, new Vector2f(x, y));
		needToBeProcessed.add(id);
	}

	public synchronized void addEntity(String id, boolean enemy){
		Picture tempPicture = (Picture) ((enemy) ? monsterImage.clone() : playerImage.clone());
		tempPicture.setName(id);
		entityPicturesMap.put(id, tempPicture);
		entityPositionsMap.put(id, new Vector2f());
	}

	private synchronized void processEntity(String id){
		if(!entityPicturesMap.containsKey(id)){
			return;
		}
		Picture entity = entityPicturesMap.get(id);
		Vector2f position = (Vector2f) entityPositionsMap.get(id).clone();
		position.sub(playerPosition);
		if(position.length() > MINIMAP_RANGE_SCALING || invisibleEntities.contains(id)){
			if(attachedEntities.contains(id)){
				entity.removeFromParent();
				attachedEntities.remove(id);
			}
			return;
		}else{
			if(!attachedEntities.contains(id)){
				minimapNode.attachChild(entity);
				attachedEntities.add(id);
			}
		}
		float scale = 100 / MINIMAP_RANGE_SCALING;
		position.scale(scaleFactor * scale);
		entityPicturesMap.get(id).setPosition(position.x, position.y);
	}

	public synchronized void removeEntity(String id){
		entityPicturesMap.remove(id);
		entityPositionsMap.remove(id);
	}

	public synchronized void showEntity(String id){
		invisibleEntities.remove(id);
	}

	public synchronized void hideEntity(String id){
		invisibleEntities.add(id);
	}

	public synchronized void clearEntities(){
		minimapCleared = true;
	}

	@Override
	protected synchronized void controlUpdate(float tpf){
		//TODO: Optimize
		if(!barsValid){
			shownHealth = calculateNextStep(shownHealth, currentHealth, maxHealth, tpf, healthSpeed);
			shownMana = calculateNextStep(shownMana, currentMana, maxMana, tpf, manaSpeed);
			shownRage = calculateNextStep(shownRage, currentRage, maxRage, tpf, rageSpeed);
			shownHealthDiff = calculateNextStep(shownHealthDiff, currentHealth, maxHealth, tpf, healthSpeed * 2);
			shownManaDiff = calculateNextStep(shownManaDiff, currentMana, maxMana, tpf, manaSpeed * 2);
			shownRageDiff = calculateNextStep(shownRageDiff, currentRage, maxRage, tpf, rageSpeed * 2);
			wHealth = updateBarWidth(healthImage, healthDimension, shownHealth, maxHealth);
			wMana = updateBarWidth(manaImage, manaDimension, shownMana, maxMana);
			wRage = updateBarWidth(rageImage, rageDimension, shownRage, maxRage);
			wHealthDiff = updateBarWidth(healthDiffImage, healthDimension, FastMath.abs(shownHealth - shownHealthDiff), maxHealth);
			wManaDiff = updateBarWidth(manaDiffImage, manaDimension, FastMath.abs(shownMana - shownManaDiff), maxMana);
			wRageDiff = updateBarWidth(rageDiffImage, rageDimension, FastMath.abs(shownRage - shownRageDiff), maxRage);
			placeBar(healthImage, healthDiffImage, healthPosition, healthDimension, wHealth, wHealthDiff, shownHealth, shownHealthDiff);
			placeBar(manaImage, manaDiffImage, manaPosition, manaDimension, wMana, wManaDiff, shownMana, shownManaDiff);
			placeBar(rageImage, rageDiffImage, ragePosition, rageDimension, wRage, wRageDiff, shownRage, shownRageDiff);
			healthText.setText(currentHealth + "/" + maxHealth);
			manaText.setText(currentMana + "/" + maxMana);
			if(shownHealthDiff == 0 && shownManaDiff == 0 && shownRageDiff == 0){
				barsValid = true;
			}
		}
		if(!pointerValid){
			pointerNode.setLocalRotation(pointerQuaternion.fromAngles(0, 0, (float) pointerRotation));
			pointerValid = true;
		}
		if(!minimapValid){
			needToBeProcessed = new HashSet<>(attachedEntities);
			minimapValid = true;
		}
		if(minimapCleared){
			minimapNode.detachAllChildren();
			entityPositionsMap.clear();
			entityPicturesMap.clear();
			needToBeProcessed.clear();
			attachedEntities.clear();
			minimapCleared = false;
		}
		for(String needToProcess : needToBeProcessed){
			processEntity(needToProcess);
		}
		needToBeProcessed.clear();
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp){
	}

	private void simpleInitializer(IConfigReader config, Picture image, Vector2f dimensions, Vector2f position){
		image.setImage(assetManager, config.getString("texture"), true);
		IConfigReader dim = config.getConfigReader("dimensions"), pos = config.getConfigReader("position");
		dimensions.set(dim.getFloat("width"), dim.getFloat("height"));
		position.set(pos.getFloat("x"), pos.getFloat("y"));
	}

	private void backgroundInitializer(IConfigReader config, Picture image, Vector2f dimensions, Vector2f position, float relativeScaleFactor){
		simpleInitializer(config, image, dimensions, position);
		scaleFactor = coordinatesManager.getScale(dimensions, relativeScaleFactor);
		dimensions.scale(scaleFactor);
		position.set(coordinatesManager.getTranslationFromEdge(position, dimensions, CoordinatesManager.TOP_RIGHT));
		image.setWidth(dimensions.x);
		image.setHeight(dimensions.y);
		image.setPosition(position.x, position.y);
		lifebarNode.attachChild(image);
	}

	private void barsInitializer(IConfigReader config, Picture image, Picture diffImage, Vector2f dimensions, Vector2f position){
		simpleInitializer(config, image, dimensions, position);
		diffImage.setImage(assetManager, config.getString("textureDiff"), true);
		dimensions.scale(scaleFactor);
		position.scale(scaleFactor);
		position.add(lifeBarPosition);
		image.setHeight(dimensions.y);
		diffImage.setHeight(dimensions.y);
		lifebarNode.attachChild(image);
		lifebarNode.attachChild(diffImage);
	}

	private void textInitializer(BitmapText text, Vector2f boxDimensions, Vector2f boxPosition){
		text.setSize(bitmapFont.getCharSet().getRenderedSize() * scaleFactor * 1.4f);
		float tempY = boxPosition.y + boxDimensions.y + (text.getLineHeight() / 2f);
		text.setBox(new Rectangle(boxPosition.x, tempY, boxDimensions.x, boxDimensions.y));
		text.setAlignment(BitmapFont.Align.Center);
		text.setVerticalAlignment(BitmapFont.VAlign.Center);
		lifebarNode.attachChild(text);
	}

	private void pointerInitializer(IConfigReader config, Picture image, Vector2f dimensions, Vector2f position){
		simpleInitializer(config, image, dimensions, position);
		dimensions.scale(scaleFactor);
		position.scale(scaleFactor);
		position.add(lifeBarPosition);
		image.setWidth(dimensions.x);
		image.setHeight(dimensions.y);
		image.center();
		pointerNode.setLocalTranslation(position.x, position.y, 0);
		minimapNode.setLocalTranslation(position.x, position.y, 0);
		pointerNode.attachChild(image);
		lifebarNode.attachChild(minimapNode);
		lifebarNode.attachChild(pointerNode);
		minimapNode.setLocalRotation(pointerQuaternion.fromAngles(0, 0, (float) 90));
	}

	private void entityInitializer(IConfigReader config, Picture image, Vector2f dimensions, Vector2f position){
		simpleInitializer(config, image, dimensions, position);
		dimensions.scale(scaleFactor);
		position.add(lifeBarPosition);
		image.setWidth(dimensions.x);
		image.setHeight(dimensions.y);
		image.center();
	}

	private float getSpeed(int delta, int max){
		float tempSpeed = delta * MAX_SPEED / max;
		if(tempSpeed < MIN_SPEED){
			tempSpeed = MIN_SPEED;
		}
		return tempSpeed;
	}

	private float calculateNextStep(float shown, float current, float max, float tpf, float speed){
		if(shown != current){
			return barLenght(shown, current, max, tpf, speed);
		}
		return shown;
	}

	private float barLenght(float initialValue, float endValue, float maxValue, float tpf, float speed){
		int sign = (endValue > initialValue) ? 1 : -1;
		float increment = speed * tpf * (maxValue / 3);
		if(increment > (endValue - initialValue) * sign){
			return endValue;
		}else{
			return initialValue + increment * sign;
		}
	}

	private float updateBarWidth(Picture image, Vector2f dimension, float shown, float max){
		float tempWidth = (dimension.x * shown) / max;
		image.setWidth(tempWidth);
		return tempWidth;
	}

	private void placeBar(Picture image, Picture diffImage, Vector2f position, Vector2f dimension, float width, float widthDiff, float shown, float shownDiff){
		float xStandard = position.x + (dimension.x - width);
		float xDiff = xStandard;
		if((shownDiff - shown) > 0){
			xDiff -= widthDiff;
		}
		image.setPosition(xStandard, position.y);
		diffImage.setPosition(xDiff, position.y);
	}

	private void invalidateBars(){
		barsValid = false;
	}

	private void invalidateMinimap(){
		minimapValid = false;
	}

	private void invalidatePointer(){
		pointerValid = false;
	}
}
