/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.controls;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author Nightfallt
 */
public class LoadingHourglassControl extends AbstractControl{
		
	public LoadingHourglassControl() {
	}

	@Override
	protected void controlUpdate(float tpf) {
		spatial.rotate(0, 0, tpf);
	}


	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

}
