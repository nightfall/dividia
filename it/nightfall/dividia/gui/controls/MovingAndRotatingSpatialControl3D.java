
package it.nightfall.dividia.gui.controls;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.gui_api.controls.ICharacterControl3D;
import it.nightfall.dividia.gui_api.controls.ISpatialMovementControl;
import it.nightfall.dividia.gui_api.controls.ISpatialRotationControl3D;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import java.io.IOException;


public class MovingAndRotatingSpatialControl3D implements ICharacterControl3D {
	private ISpatialMovementControl movementControl;
	private ISpatialRotationControl3D rotationControl;

	public MovingAndRotatingSpatialControl3D(int initialSpeed, double initialDirection, float offsetAngle) {
		movementControl = new CharacterMovementControl3D(initialSpeed);
		rotationControl = new SpatialRotationControl3D(initialDirection, offsetAngle);
		
	}

	@Override
	public synchronized void characterMoved(double x, double y, double direction) {
		movementControl.characterMoved(x, y);
		rotationControl.characterMoved(direction);
	}

	@Override
	public synchronized void characterStopped() {
		movementControl.characterStopped();
	}

	@Override
	public Control cloneForSpatial(Spatial spatial) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public synchronized void update(float tpf) {
		movementControl.update(tpf);
		rotationControl.update(tpf);
	}

	@Override
	public void render(RenderManager rm, ViewPort vp) {
	}

	@Override
	public void write(JmeExporter ex) throws IOException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void read(JmeImporter im) throws IOException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void setSpatial(Spatial spatial) {
		movementControl.setSpatial(spatial);
		rotationControl.setSpatial(spatial);
	}

	@Override
	public synchronized void setSpeed(int newValue) {
		movementControl.setSpeed(newValue);
		//System.out.println(movementControl.getSpatial().getName());
		//System.out.println("speed: "+newValue);
	}

    @Override
    public ISpatialMovementControl getMovementControl() {
        return movementControl;
    }
	
    
	
}
