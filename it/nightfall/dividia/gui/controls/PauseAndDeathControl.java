package it.nightfall.dividia.gui.controls;

public class PauseAndDeathControl extends Simple3DimensionalSpatialMovementControlWithPause {
	private float lifeTime;
	protected boolean pauseEnded = true;
	
	public PauseAndDeathControl(float lifeTime){
		super(0);
		this.lifeTime = lifeTime;
		super.addPauseTime(lifeTime);
		nextFrameIsAPauseOne.add(true);
	}

	@Override
	protected synchronized void controlUpdate(float tpf){
		super.controlUpdate(tpf);
		if(pauseTimes.isEmpty()){
			spatial.removeFromParent();
			pauseEnded = true;
		}
	}

	public synchronized void resetPause(){
		super.pauseTimes.clear();
		super.nextFrameIsAPauseOne.clear();
		timeProgress = 0;
		pauseEnded = false;
		super.addPauseTime(lifeTime);
		nextFrameIsAPauseOne.add(true);
	}

	public synchronized boolean isPauseEnded(){
		return pauseEnded;
	}
}
