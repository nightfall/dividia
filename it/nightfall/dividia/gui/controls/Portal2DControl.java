/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.controls;

import com.jme3.effect.ParticleEmitter;
import com.jme3.math.FastMath;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

/**
 *
 * @author Andrea
 */
public class Portal2DControl extends PortalControl {
	
	private float angle = 0;
	private ParticleEmitter particles;
	private float radius;
	
	public Portal2DControl(float minSize, float maxSize, ParticleEmitter particles){
		super(maxSize,minSize);
		this.particles = particles;
		radius = minSize;
	}
	
	//TODO: radius should be changed with the actual side of the portal (if the size is changing)
	@Override
    public void controlUpdate (float tpf) {
	   angle += tpf * 3.5f;
       angle %= FastMath.TWO_PI;
       float x = FastMath.cos(angle) * radius;
       float y = FastMath.sin(angle) * radius;
       particles.setLocalTranslation(x, y, 0);
	   spatial.rotate(0, 0, tpf);
    }

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {
	}
}
