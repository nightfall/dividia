package it.nightfall.dividia.gui.controls;

import com.jme3.export.Savable;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;

public class PortalControl extends AbstractControl implements Savable, Cloneable {

	private float minSize;
	private float maxSize;
	private boolean minReached = false;
	private Vector3f newScaling;
	
	public PortalControl (float minSize, float maxSize){
		this.minSize = minSize;
		this.maxSize = maxSize;
	}
	
	@Override
	public Control cloneForSpatial(Spatial spatial) {
		PortalControl portalControl = new PortalControl(minSize,maxSize);
		spatial.addControl(portalControl);
		return portalControl;
	}

	@Override
	protected void controlRender(RenderManager arg0, ViewPort arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void controlUpdate(float tpf) {
		 if (spatial.getLocalScale().x == minSize){
             minReached = true;
         } 
         if (spatial.getLocalScale().x == maxSize){
             minReached = false;
         }
         if (minReached){
            newScaling = spatial.getLocalScale().add(tpf*0.5f, 0, tpf*0.5f);
            if (newScaling.x > 1.5){
                newScaling.set(1.5f,0,1.5f);
            }
         }
         else{
        	 newScaling = spatial.getLocalScale().subtract(tpf*0.5f,0,tpf*0.5f);
            if (newScaling.x < 1){
                newScaling.set(1, 0, 1);
            }
         }
         spatial.setLocalScale(newScaling);
         spatial.rotate(0, tpf, 0);
		
	}

}
