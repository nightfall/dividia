
package it.nightfall.dividia.gui.controls;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.gui_api.controls.ISimple3DimensionalSpatialMovementControl;
import it.nightfall.dividia.gui_api.spells.IGraphicEffect;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;


public class Simple3DimensionalSpatialMovementControl extends AbstractControl implements ISimple3DimensionalSpatialMovementControl {
	
	protected Queue<Vector3f> destinationPoints;
	protected float spatialSpeed;
	protected double speedFactor;
	protected Vector3f lastWayPoint;
	protected Vector3f lastTranslation;
	protected Vector3f totalTranslation;
	protected Vector3f lastPointReached;
	protected float progress;
	protected float timeProgress = 0;
	
	public Simple3DimensionalSpatialMovementControl(float spatialInitialSpeed){ 
		destinationPoints = new LinkedList<>();		
		spatialSpeed = spatialInitialSpeed;
		speedFactor = IGameConstants.MOVEMENT_SPEED_CONSTANT;		
		lastTranslation = new Vector3f();
		totalTranslation = new Vector3f();
		progress = 0;
	} 
	
	
	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
		lastPointReached = spatial.getLocalTranslation();		
	}
	
	@Override
	protected synchronized void controlUpdate(float tpf){	
		
		lastTranslation.multLocal(0);
        if(!destinationPoints.isEmpty()) {			
			nextTranslationStep(tpf,1);			
        }
		totalTranslation.addLocal(lastTranslation);
	}	
	
	
	protected synchronized void nextTranslationStep(float tpf, float secondarySpeedFactor){
		Vector3f destination = destinationPoints.peek().clone();				
			
		if(lastWayPoint == null) {
			lastWayPoint = destinationPoints.peek().clone();
		}
		Vector3f actualVectorPosition = spatial.getLocalTranslation().clone();
		Vector3f movementDirection = destination.add(actualVectorPosition.mult(-1));
		double stepLength = speedFactor * spatialSpeed * 1.0 * tpf / 100;
		stepLength *= secondarySpeedFactor;
		Vector3f nextStepPoint = movementDirection.normalize().mult((float) stepLength).add(actualVectorPosition);
		double distance = movementDirection.length();

		if(stepLength >= distance){
			lastWayPoint = destinationPoints.poll();
			lastPointReached = lastWayPoint;
		}
		if(destinationPoints.isEmpty()){
			Vector3f finalPosition = destination.clone();
			lastTranslation = finalPosition.add(spatial.getLocalTranslation().mult(-1));
			spatial.setLocalTranslation(finalPosition);
		}else{

			Vector3f finalPosition = nextStepPoint.clone();
			lastTranslation = nextStepPoint.add(spatial.getLocalTranslation().mult(-1));
			spatial.setLocalTranslation(finalPosition);
			float distanceBetweenPoints = destination.add(lastWayPoint.mult(-1)).length();
			if(distanceBetweenPoints!=0) {
				progress += stepLength/distanceBetweenPoints;					
				if(progress >1) {
					progress = 1;
				}
			}
		}
		
	}	
	
	@Override
	protected void controlRender(RenderManager rm, ViewPort vp){
	}
	
	@Override
	public synchronized void addDestinationPoint(float x, float y, float z) {
		destinationPoints.add(new Vector3f(x, y, z));
	} 

	@Override
	public Control cloneForSpatial(Spatial spatial) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
	
	@Override
	public void setSpatialSpeed(float spatialSpeed) {
		this.spatialSpeed = spatialSpeed;
	}
	
	@Override
	public synchronized Queue<Vector3f> getDestinationPoints() {
		return destinationPoints;
	}

	public Vector3f getTotalTranslation() {
		return totalTranslation;
	}
	public void setSpeedFactor(double speedFactor) {
		this.speedFactor = speedFactor;
	}

	public void setLastWayPoint(Vector3f lastWayPoint) {
		this.lastWayPoint = lastWayPoint;
	}

    @Override
    public Vector3f getLastPointReached() {
        return lastPointReached;
    }

    @Override
    public float getSpatialSpeed() {
        return spatialSpeed;
    }	
}

