
package it.nightfall.dividia.gui.controls;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.gui_api.controls.ISimple3DimensionalSpatialMovementControl;
import java.util.LinkedList;
import java.util.Queue;


public class Simple3DimensionalSpatialMovementControlWithPause extends Simple3DimensionalSpatialMovementControl implements ISimple3DimensionalSpatialMovementControl {
	
	protected Queue<Float> pauseTimes = new LinkedList<>();
	protected Queue<Boolean> nextFrameIsAPauseOne = new LinkedList<>();
	
	public Simple3DimensionalSpatialMovementControlWithPause(float spatialInitialSpeed){ 
		super(spatialInitialSpeed);
	} 
	
	
	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);	
	}
	
	@Override
	protected synchronized void controlUpdate(float tpf){	
		if(nextFrameIsAPauseOne.isEmpty()) {
			return;
		}
		if(!nextFrameIsAPauseOne.peek()) {
			int leftWayPoints = destinationPoints.size();
			super.controlUpdate(tpf);
			if(leftWayPoints>destinationPoints.size()) {
				nextFrameIsAPauseOne.poll();
			}
		}
		else {
			lastTranslation = new Vector3f(Vector3f.ZERO);	
			if(!pauseTimes.isEmpty()){
				float nextTimeStep = pauseTimes.peek();
				timeProgress += tpf;
				if(timeProgress<nextTimeStep) {
					progress = timeProgress/nextTimeStep;	
					if(progress>1) {
						progress = 1;
					}
					
				}
				else {
					pauseTimes.poll();
					nextFrameIsAPauseOne.poll();
					progress = 1;
					timeProgress = 0;
				}		
			}
		}
	}	
	@Override
	protected void controlRender(RenderManager rm, ViewPort vp){
	}
	
	
	public synchronized void addPauseTime(float duration) {
		pauseTimes.add(duration);
	}
	
	public synchronized void addNextFrameIsAPauseOne(boolean value) {
		this.nextFrameIsAPauseOne.add(value);
	}
	

	@Override
	public Control cloneForSpatial(Spatial spatial) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
	
	@Override
	public void setSpatialSpeed(float spatialSpeed) {
		this.spatialSpeed = spatialSpeed;
	}
	
	public Queue<Float> getPauseTimes() {
		return pauseTimes;
	}
	
    
	
}

