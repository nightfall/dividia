/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.controls;

import it.nightfall.dividia.gui.world.SpatialsManager;

/**
 *
 * @author Andrea
 */
public class SpatialDeathControl extends PauseAndDeathControl{
	
	public SpatialDeathControl(float lifeTime){
		super(lifeTime);
	}
	

	@Override
	protected synchronized void controlUpdate(float tpf) {
		super.controlUpdate(tpf);
		if(pauseTimes.isEmpty()){
			spatial.removeFromParent();
			pauseEnded = true;
			SpatialsManager.getInstance().movingCharacterSpatialRemoved(spatial.getName());
		}
	}
}
