package it.nightfall.dividia.gui.controls;

import com.jme3.math.Quaternion;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.gui_api.controls.ISpatialRotationControl3D;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class SpatialRotationControl3D extends AbstractControl implements ISpatialRotationControl3D {
	
	private Queue<IDirection> destinationDirections;  
	public static int MAX_QUEUE_LENGTH = 10;
	public static int MAX_QUEUE_LENGTH_TO_MANTAIN_NORMAL_SPEED = 3;
	private IDirection currentSpatialDirection;
	private float offsetAngle;
 
	public SpatialRotationControl3D(double initialDirection, float offsetAngle){ 
		destinationDirections = new LinkedBlockingQueue<>();
		currentSpatialDirection = new Direction(initialDirection); //TODO inizializzazione corretta!
		this.offsetAngle = offsetAngle;
	} 
	
	private void jumpToCurrentRotation() {
		while(destinationDirections.size() != 1 ) {
			destinationDirections.poll();
		}		
		IDirection destinationDirection = destinationDirections.poll();
		spatial.setLocalRotation(new Quaternion().fromAngles(0, 0, (float) destinationDirection.getDirectionAngle() + offsetAngle));
	}		
	
	@Override
	protected void controlUpdate(float tpf){	
		if(!destinationDirections.isEmpty()) {
			if(destinationDirections.size() > MAX_QUEUE_LENGTH_TO_MANTAIN_NORMAL_SPEED) {
				nextRotationalStep(tpf, true);			
			}			
			else {
				nextRotationalStep(tpf,false);
			}
        }
		try{
			if(spatial.getName().equals(ClientTest.clientListener.getUsername())) {
				LifebarControl.getLifebarControl().setRotation(currentSpatialDirection.getDirectionAngle());
			}
		}catch(RemoteException ex){
		}
	}	
	
	public void nextRotationalStep(float tpf, boolean doubleSpeed) {
		
		IDirection destinationDirection = destinationDirections.peek().clone();
		IDirection currentDirection = currentSpatialDirection;			
		float rotationSpeed = (float) (IGameConstants.ROTATION_ANGLE_PER_FRAME * (1/IGameConstants.BL_UPDATE_S));	
		double rotationLength = tpf * rotationSpeed ;//TODO constant delete!
		double spanningAngle = currentDirection.angleBetweenDirections(destinationDirection);
		if(spanningAngle > Math.PI) {
			spanningAngle -= 2 * Math.PI;
		}
		
		if(doubleSpeed) {
			spanningAngle *=2;
		}
		if(rotationLength >= Math.abs(spanningAngle)) {
			spatial.rotate(0, 0, (float) spanningAngle);					
			currentSpatialDirection.rotateDirection((float) spanningAngle);	
			destinationDirections.poll();	
		}
		else {
			if(spanningAngle < 0) {
				spatial.rotate(0, 0, (float) -rotationLength);
				currentSpatialDirection.rotateDirection((float) -rotationLength);
			}
			else {
				spatial.rotate(0, 0, (float) rotationLength);
				currentSpatialDirection.rotateDirection((float) rotationLength);
			}
		}
	}
	
	@Override
	protected void controlRender(RenderManager rm, ViewPort vp){
	}

	

	@Override
	public void characterMoved(double direction) {
		destinationDirections.add(new Direction(direction));		
	} 
	
	//TODO cambioVelocitÃƒÂ 

	@Override
	public Control cloneForSpatial(Spatial spatial) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public IDirection getCurrentSpatialDirection() {
		return currentSpatialDirection;
	}
}
