package it.nightfall.dividia.gui.controls;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import com.jme3.ui.Picture;
import it.nightfall.dividia.api.utility.ICallbackWaiting;
import it.nightfall.dividia.api.utility.IFutureCallback;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.FutureCallback;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.gui.utility.CoordinatesManager;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.TextDisplayManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.vecmath.Vector2f;

public class SpellArcControl extends AbstractControl {
	private static final String CONFIG_FILE = SharedFunctions.getPath("gui|spellarc.xml");
	private static SpellArcControl spellArcControl;
	private static final Node spellArcNode = new Node();
	private final Picture spellArcBackgroundImage = new Picture("SpellArcBackground"),
			spellArcForegroundImage = new Picture("SpellArcForeground"),
			spellArcImage = new Picture("SpellArc"),
			expImage = new Picture("Exp"),
			vialsImage = new Picture("Vials");
	private final Picture[] spellImages = new Picture[4];
	private final Map<String, CooldownUpdater> spellCooldownIndicators = new HashMap<>();
	private final Map<String, Picture> spellImagesMap = new HashMap<>();
	private final Vector2f spellArcPosition = new Vector2f();
	private final Vector2f arcsPosition = new Vector2f();
	private final Vector2f spellArcDimensions = new Vector2f();
	private final AssetManager assetManager;
	private final CoordinatesManager coordinatesManager;
	private float scaleFactor;
	private int maxExperience;
	private int currentExperience;
	private int maxVials;
	private int currentVials;
	private boolean arcsValid = false;
	private Quaternion arcsQuaternion = new Quaternion();
	private float expRotation = 0;
	private float vialsRotation = 0;
	private DecimalFormat integerFormat = new DecimalFormat("#");
	private DecimalFormat decimalFormat = new DecimalFormat("#.#");

	public static void spellArcInit(AssetManager assetManager, Camera cam){
		spellArcControl = new SpellArcControl(assetManager, cam);
		spellArcNode.addControl(spellArcControl);
	}

	protected SpellArcControl(AssetManager assetManager, Camera cam){
		this.assetManager = assetManager;
		coordinatesManager = new CoordinatesManager(new Vector2f(cam.getWidth(), cam.getHeight()));
		initSpellArc();
	}

	public synchronized void setMaxExperience(int max){
		maxExperience = max;
		currentExperience = 0;
		arcsValid = false;
	}

	public synchronized void updateExperience(int currentExperience){
		this.currentExperience = currentExperience;
		arcsValid = false;
	}

	public synchronized void setMaxVials(int max){
		maxVials = max;
		currentVials = 0;
		arcsValid = false;
	}

	public synchronized void updateVials(int delta){
		currentVials += delta;
		arcsValid = false;
	}

	public synchronized void updateSpells(String... spells){
		if(spells.length > 4){
			return;
		}
		spellCooldownIndicators.clear();
		spellImagesMap.clear();
		int i = 0;
		for(String spell : spells){
			if(spell != null){
				setSpell(spell, i);
			}
			i++;
		}
	}

	public void spellOnCooldown(String spell){
		CooldownUpdater tempCDU = spellCooldownIndicators.get(spell);
		tempCDU.reset();
		IFutureCallback tempCallback = new FutureCallback(tempCDU);
		tempCallback.runAndWait(100);
		final Picture tempPicture = spellImagesMap.get(spell);
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				setOpacity(tempPicture, 0.3f);
			}
		});
	}

	private void setOpacity(Picture tempPicture, Float value){
		Material mat = tempPicture.getMaterial().clone();
		mat.setColor("Color", new ColorRGBA(1, 1, 1, value)); // Red with 50% transparency
		mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		tempPicture.setMaterial(mat);
	}

	public static Node getSpellArc(){
		return spellArcNode;
	}

	public static SpellArcControl getSpellArcControl(){
		return spellArcControl;
	}

	@Override
	protected void controlUpdate(float tpf){
		if(!arcsValid){
			synchronized(this){
				expImage.setLocalRotation(arcsQuaternion.fromAngles(0, 0, getRotation(currentExperience, maxExperience)));
				vialsImage.setLocalRotation(arcsQuaternion.fromAngles(0, 0, FastMath.HALF_PI));
				//vialsImage.setLocalRotation(arcsQuaternion.fromAngles(0, 0, getRotation(currentVials, maxVials)));
				arcsValid = true;
			}
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp){
	}

	private void simpleInitializer(String texture, Picture image, Vector2f dimensions, Vector2f position){
		image.setImage(assetManager, texture, true);
		image.setWidth(dimensions.x);
		image.setHeight(dimensions.y);
		image.setPosition(position.x, position.y);
		spellArcNode.attachChild(image);
	}

	private void initSpellArc(){
		IConfigReader config = new ConfigReader(CONFIG_FILE);
		IConfigReader dim = config.getConfigReader("dimensions"), pos = config.getConfigReader("position");
		Vector2f tempSpellArcPosition = new Vector2f(pos.getFloat("x"), pos.getFloat("y"));
		spellArcDimensions.set(dim.getFloat("width"), dim.getFloat("height"));
		scaleFactor = coordinatesManager.getScale(spellArcDimensions, config.getInteger("scale"));
		spellArcDimensions.scale(scaleFactor);
		spellArcPosition.set(coordinatesManager.getTranslationFromEdge(tempSpellArcPosition, spellArcDimensions, CoordinatesManager.BOTTOM_RIGHT));
		arcsPosition.set(coordinatesManager.getTranslationFromEdge(tempSpellArcPosition, spellArcDimensions, CoordinatesManager.BOTTOM_LEFT));
		simpleInitializer(config.getString("backgroundTexture"), spellArcBackgroundImage, spellArcDimensions, spellArcPosition);
		initSpells(config.getConfigReader("spells"));
		simpleInitializer(config.getString("spellArcTexture"), spellArcImage, spellArcDimensions, spellArcPosition);
		simpleInitializer(config.getString("expTexture"), expImage, spellArcDimensions, arcsPosition);
		simpleInitializer(config.getString("vialsTexture"), vialsImage, spellArcDimensions, arcsPosition);
		simpleInitializer(config.getString("foregroundTexture"), spellArcForegroundImage, spellArcDimensions, spellArcPosition);

	}

	private void initSpells(IConfigReader config){
		Collection<IConfigReader> spellsInitData = config.getConfigReaderList("spell");
		int i = 0;
		for(IConfigReader configData : spellsInitData){
			Picture image = new Picture(Integer.toString(i));
			spellImages[i] = image;
			IConfigReader dim = configData.getConfigReader("dimensions"), pos = configData.getConfigReader("position");
			Vector2f dimensions = new Vector2f(dim.getFloat("width"), dim.getFloat("height"));
			Vector2f position = new Vector2f(pos.getFloat("x"), pos.getFloat("y"));
			dimensions.scale(scaleFactor);
			position.scale(scaleFactor);
			position.add(spellArcPosition);
			image.setImage(assetManager, "Textures/spellMiniatures/empty.png", true);
			image.setWidth(dimensions.x);
			image.setHeight(dimensions.y);
			image.setPosition(position.x, position.y);
			spellArcNode.attachChild(image);
			TextDisplayManager.getInstance().createHandler("spellArc" + i, position, dimensions, 1.01f);
			i++;
		}
	}

	private float getRotation(int current, int max){
		if(max == 0){
			//System.out.println("Max == 0");
			return 0;
			//TODO: Exception?
		}
		return FastMath.PI - ((float) (current * FastMath.HALF_PI) / (float) max);
	}

	private void setSpell(String spellName, int position){
		spellImages[position].setImage(assetManager, "Textures/spellMiniatures/" + spellName + ".png", true);
		float tempCD = GameStorage.getInstance().getSpellDefinition(spellName).getCooldown();
		spellCooldownIndicators.put(spellName, new CooldownUpdater(tempCD, "spellArc" + position, spellName));
		spellImagesMap.put(spellName, spellImages[position]);
	}

	public class CooldownUpdater implements ICallbackWaiting {
		private float cooldownMax;
		private float cooldownRemaining;
		private String id;
		private String spellName;
		public CooldownUpdater(float cd, String id, String spell){
			cooldownMax = cd;
			this.id = id;
			spellName = spell;
		}

		public void reset(){
			cooldownRemaining = cooldownMax;
		}

		@Override
		public boolean execute(){
			cooldownRemaining -= 0.1f;
			if(cooldownRemaining <= 0){
				TextDisplayManager.getInstance().hideText(id);
				final Picture tempPicture = spellImagesMap.get(spellName);
				GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
					@Override
					public void execute(){
						setOpacity(tempPicture, 1f);
					}
				});
				return true;
			}else if(cooldownRemaining < 1.0f){
				TextDisplayManager.getInstance().updateText(id, decimalFormat.format(cooldownRemaining));
			}else{
				TextDisplayManager.getInstance().updateText(id, integerFormat.format(cooldownRemaining));
			}
			return false;
		}
	}
}
