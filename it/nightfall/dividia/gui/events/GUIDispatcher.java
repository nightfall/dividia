package it.nightfall.dividia.gui.events;

import it.nightfall.dividia.api.network.IGUIDispatcher;
import it.nightfall.dividia.api.network.IReceivedMessage;
import it.nightfall.dividia.bl.network.AbstractDispatcher;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class GUIDispatcher extends AbstractDispatcher implements IGUIDispatcher {
	private static IGUIDispatcher dispatcher;
	private final BlockingQueue<IReceivedMessage> messages = new LinkedBlockingQueue<>();

	private GUIDispatcher(){
	}

	public static void initDispatcher(){
		dispatcher = new GUIDispatcher();
		Thread dispatcherThread = new Thread(dispatcher);
		dispatcherThread.setName("GUI Dispatcher");
		dispatcherThread.start();
	}
	
	public static IGUIDispatcher getInstance(){
		return dispatcher;
	}

	@Override
	public void run(){
		while(super.isRunning()){
			try{
				IReceivedMessage message = getNextMessage();
				super.checkPause();
				message.perform();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void addMessage(IReceivedMessage message){
		try{
			messages.put(message);
		}catch(InterruptedException ex){
			ex.printStackTrace();
		}
	}

	private IReceivedMessage getNextMessage(){
		try{
			return messages.take();
		}catch(InterruptedException ex){
			ex.printStackTrace();
			return null;
		}
	}
}
