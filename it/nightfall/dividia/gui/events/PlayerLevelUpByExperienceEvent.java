/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.events;

import it.nightfall.dividia.gui.spellsAndEffects.EffectsManager;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;

/**
 *
 * @author bernardo
 */
public class PlayerLevelUpByExperienceEvent implements IGUIEvent{
	
	private String playerId;

	public PlayerLevelUpByExperienceEvent(String id) {
		this.playerId = id;
				
	}

	@Override
	public void execute() {
		EffectsManager.getInstance().launchFollowingEffect(playerId,"levelUp");
	}
	
}
