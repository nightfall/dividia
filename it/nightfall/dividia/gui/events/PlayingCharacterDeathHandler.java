
package it.nightfall.dividia.gui.events;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import it.nightfall.dividia.gui.controls.FollowingEffectControl3D;
import it.nightfall.dividia.gui.sounds.SoundsManager;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;


public class PlayingCharacterDeathHandler implements IGUIEvent{

	private String playingCharacterId;

	public PlayingCharacterDeathHandler(String playingCharacterId) {
		this.playingCharacterId = playingCharacterId;
	}
	
	@Override
	public void execute() {
		SpatialsManager.getInstance().performDeath(playingCharacterId);
		Node rootNode = SpatialsManager.getInstance().getRootNode();
		for(Spatial spatial: rootNode.getChildren()) {
			if(spatial.getControl(FollowingEffectControl3D.class)!=null) {
				FollowingEffectControl3D removingControl = spatial.getControl((FollowingEffectControl3D.class));
				if(removingControl.getToFollowControl().getSpatial().getName().equals(playingCharacterId)) {
					removingControl.shutDownEffect(rootNode);
				}
			}
		}
		SoundsManager.getInstance().getCharacterSoundsManager(playingCharacterId).charactedDeath();
			
		
	}

}
