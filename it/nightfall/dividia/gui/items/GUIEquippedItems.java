package it.nightfall.dividia.gui.items;

import it.nightfall.dividia.api.item.IEquippedItems;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.EquippedItems;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;

public class GUIEquippedItems {
	private static GUIEquippedItems instance = new GUIEquippedItems();
	private IEquippedItems equippedItems = new EquippedItems();

	public static GUIEquippedItems getInstance(){
		return instance;
	}

	public void initFromSummary(ISummary summary){
		equippedItems.buildFromSummary(summary);
	}

	public IEquippedItems getEquippedItems(){
		return equippedItems;
	}

	public IEquipmentItem getEquippedItemFromType(String type){
		switch(type){
			case "weapon":
				return equippedItems.getWeapon();
			case "helmet":
				return equippedItems.getHelmet();
			case "necklace":
				return equippedItems.getNecklace();
			case "armor":
				return equippedItems.getArmor();
			case "ring":
				return equippedItems.getRing();
			case "gloves":
				return equippedItems.getGloves();
			case "shield":
				return equippedItems.getShield();
			case "boots":
				return equippedItems.getBoots();
			default:
				return null;
		}
	}

	public void unequipItemFromType(String type){
		try{
			switch(type){
				case "helmet":
					ClientTest.intentionManager.unequipHelmet();
					break;
				case "necklace":
					ClientTest.intentionManager.unequipNecklace();
					break;
				case "armor":
					ClientTest.intentionManager.unequipArmor();
					break;
				case "ring":
					ClientTest.intentionManager.unequipRing();
					break;
				case "gloves":
					ClientTest.intentionManager.unequipGloves();
					break;
				case "shield":
					ClientTest.intentionManager.unequipShield();
					break;
				case "boots":
					ClientTest.intentionManager.unequipBoots();
					break;
			}
		}catch(RemoteException ex){
			ex.printStackTrace();
		}
	}
}
