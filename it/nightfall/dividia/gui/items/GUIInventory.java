package it.nightfall.dividia.gui.items;

import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.IGenericInventory;
import it.nightfall.dividia.api.item.IItem;
import it.nightfall.dividia.api.item.IKeyItem;
import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.IUtilityItem;
import it.nightfall.dividia.api.item.IVial;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.storages.IDStorage;
import it.nightfall.dividia.gui.items.storages.OrderedDevaOrbStorage;
import it.nightfall.dividia.gui.items.storages.OrderedEquipmentStorage;
import it.nightfall.dividia.gui.items.storages.OrderedKeyStorage;
import it.nightfall.dividia.gui.items.storages.OrderedMysticalSphereStorage;
import it.nightfall.dividia.gui.items.storages.OrderedTropoGemStorage;
import it.nightfall.dividia.gui.items.storages.OrderedUtilityStorage;
import it.nightfall.dividia.gui.items.storages.OrderedVialStorage;

public class GUIInventory implements IGenericInventory{
	private static GUIInventory guiInventory;
	private int currentWeight = 0;
	private IDStorage idStorage;
	private OrderedDevaOrbStorage devaOrbStorage;
	private OrderedEquipmentStorage equipmentStorage;
	private OrderedKeyStorage keyStorage;
	private OrderedMysticalSphereStorage mysticalSphereStorage;
	private OrderedTropoGemStorage tropoGemStorage;
	private OrderedUtilityStorage utilityStorage;
	private OrderedVialStorage vialStorage;

	public static void initInventory(ISummary inventorySummary){
		guiInventory = new GUIInventory(inventorySummary);
	}

	public static GUIInventory getInstance(){
		return guiInventory;
	}

	private GUIInventory(ISummary inventorySummary){
		idStorage = new IDStorage(inventorySummary.getSummary("idStorage"));
		devaOrbStorage = new OrderedDevaOrbStorage(inventorySummary.getSummary("devaOrbStorage"));
		equipmentStorage = new OrderedEquipmentStorage(inventorySummary.getSummary("equipmentStorage"));
		keyStorage = new OrderedKeyStorage(inventorySummary.getSummary("keyStorage"));
		mysticalSphereStorage = new OrderedMysticalSphereStorage(inventorySummary.getSummary("mysticalSphereStorage"));
		tropoGemStorage = new OrderedTropoGemStorage(inventorySummary.getSummary("tropoGemStorage"));
		utilityStorage = new OrderedUtilityStorage(inventorySummary.getSummary("utilityStorage"));
		vialStorage = new OrderedVialStorage(inventorySummary.getSummary("vialStorage"));
	}

	public IDStorage getIdStorage(){
		return idStorage;
	}

	public OrderedDevaOrbStorage getDevaOrbStorage(){
		return devaOrbStorage;
	}

	public OrderedEquipmentStorage getEquipmentStorage(){
		return equipmentStorage;
	}

	public OrderedKeyStorage getKeyStorage(){
		return keyStorage;
	}

	public OrderedMysticalSphereStorage getMysticalSphereStorage(){
		return mysticalSphereStorage;
	}

	public OrderedTropoGemStorage getTropoGemStorage(){
		return tropoGemStorage;
	}

	public OrderedUtilityStorage getUtilityStorage(){
		return utilityStorage;
	}

	public OrderedVialStorage getVialStorage(){
		return vialStorage;
	}

	@Override
	public void addItem(IEquipmentItem item){
		registerToInventory(item);
		equipmentStorage.add(item);
	}

	@Override
	public void addItem(IKeyItem item){
		registerToInventory(item);
		keyStorage.add(item);
	}

	@Override
	public void addItem(IUtilityItem item){
		registerToInventory(item);
		utilityStorage.add(item);
	}

	@Override
	public void addItem(IMysticalSphere item){
		registerToInventory(item);
		mysticalSphereStorage.add(item);
	}

	@Override
	public void addItem(IVial item){
		registerToInventory(item);
		vialStorage.add(item);
	}

	@Override
	public void addItem(ITropoGem item){
		registerToInventory(item);
		tropoGemStorage.add(item);
	}

	@Override
	public void addItem(IDevaOrb item){
		registerToInventory(item);
		devaOrbStorage.add(item);
	}

	@Override
	public void registerToInventory(IItem item){
		idStorage.add(item);
		currentWeight += item.getItemDefinition().getWeight();
	}

	@Override
	public void removeItem(IEquipmentItem item){
		removeFromInventory(item);
		equipmentStorage.remove(item);
	}

	@Override
	public void removeItem(IKeyItem item){
		removeFromInventory(item);
		keyStorage.remove(item);
	}

	@Override
	public void removeItem(IUtilityItem item){
		removeFromInventory(item);
		utilityStorage.remove(item);
	}

	@Override
	public void removeItem(IMysticalSphere item){
		removeFromInventory(item);
		mysticalSphereStorage.remove(item);
	}

	@Override
	public void removeItem(IVial item){
		removeFromInventory(item);
		vialStorage.remove(item);
	}

	@Override
	public void removeItem(ITropoGem item){
		removeFromInventory(item);
		tropoGemStorage.remove(item);
	}

	@Override
	public void removeItem(IDevaOrb item){
		removeFromInventory(item);
		devaOrbStorage.remove(item);
	}

	@Override
	public void removeFromInventory(IItem item){
		idStorage.remove(item);
		currentWeight -= item.getItemDefinition().getWeight();
	}
}
