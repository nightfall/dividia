package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.IDevaOrb;
import it.nightfall.dividia.api.item.IDevaOrbsBag;
import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.definitions.IDevaOrbDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.storages.DevaOrbStorage;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

public class OrderedDevaOrbStorage extends DevaOrbStorage {
	private SortedStorage<String, IDevaOrbsBag> byName = new SortedStorage<>();
	private SortedStorage<Integer, IDevaOrbsBag> byPrice = new SortedStorage<>();

	public OrderedDevaOrbStorage(ISummary summary){
		Map<String, Serializable> summaryMap = summary.getInternalMap();
		for(Entry<String, Serializable> data : summaryMap.entrySet()){
			ISummary bagSummary = (ISummary) data.getValue();
			IDevaOrbDefinition definition = (IDevaOrbDefinition) GameStorage.getInstance().getItemDefinition(bagSummary.getString("info"));
			Map<String, Serializable> elementSummaryMap = bagSummary.getInternalMap();
			for(Entry<String, Serializable> elementData : elementSummaryMap.entrySet()){
				IDevaOrb devaOrb = definition.getItemFromDefinition();
				devaOrb.buildFromSummary((ISummary) elementData.getValue());
				this.add(devaOrb);
			}
		}
	}

	@Override
	public final void add(IDevaOrb item){
		super.add(item);
		IDevaOrbDefinition devaOrbDefinition = item.getItemDefinition();
		IDevaOrbsBag bag = getBag(devaOrbDefinition.getName());
		byName.add(item.getItemDefinition().getDisplayName(), bag);
		byPrice.add(item.getItemDefinition().getPrice(), bag);
	}

	@Override
	public void remove(IDevaOrb item){
		IDevaOrbDefinition devaOrbDefinition = item.getItemDefinition();
		IDevaOrbsBag bag = getBag(devaOrbDefinition.getName());
		byName.remove(item.getItemDefinition().getDisplayName(), bag);
		byPrice.remove(item.getItemDefinition().getPrice(), bag);
		super.remove(item);
	}

	public Collection<Map<String, ISortableItem>> getByName(){
		return byName.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByPrice(){
		return byPrice.getSortedValues();
	}
}
