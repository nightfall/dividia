package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.item.equipment_items.definitions.IEquipmentItemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.storages.EquipmentStorage;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class OrderedEquipmentStorage extends EquipmentStorage {
	private SortedStorage<String, IEquipmentItem> byName = new SortedStorage<>();
	private SortedStorage<Integer, IEquipmentItem> byPrice = new SortedStorage<>();
	private SortedStorage<Integer, IEquipmentItem> byWeight = new SortedStorage<>();

	public OrderedEquipmentStorage(ISummary summary){
		Map<String, Serializable> summaryMap = summary.getInternalMap();
		for(Map.Entry<String, Serializable> data : summaryMap.entrySet()){
			ISummary itemSummary = (ISummary) data.getValue();
			IEquipmentItemDefinition definition = (IEquipmentItemDefinition) GameStorage.getInstance().getItemDefinition(itemSummary.getString("name"));
			IEquipmentItem item = definition.getItemFromDefinition();
			item.buildFromSummary(itemSummary);
			add(item);
		}
	}

	@Override
	public final void add(IEquipmentItem item){
		super.add(item);
		byPrice.add(item.getItemDefinition().getPrice(), item);
		byWeight.add(item.getItemDefinition().getWeight(), item);
		byName.add(item.getItemDefinition().getDisplayName(), item);
	}

	@Override
	public void remove(IEquipmentItem item){
		super.remove(item);
		byPrice.remove(item.getItemDefinition().getPrice(), item);
		byWeight.remove(item.getItemDefinition().getWeight(), item);
		byName.remove(item.getItemDefinition().getDisplayName(), item);
	}

	public Collection<Map<String, ISortableItem>> getByPrice(){
		return byPrice.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByWeight(){
		return byWeight.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByName(){
		return byName.getSortedValues();
	}
}