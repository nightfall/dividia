package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.IKeyItem;
import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.definitions.IKeyItemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.storages.KeyStorage;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class OrderedKeyStorage extends KeyStorage {
	private SortedStorage<String, IKeyItem> byName = new SortedStorage<>();
	private SortedStorage<Integer, IKeyItem> byWeight = new SortedStorage<>();

	public OrderedKeyStorage(ISummary summary){
		Map<String, Serializable> summaryMap = summary.getInternalMap();
		for(Map.Entry<String, Serializable> data : summaryMap.entrySet()){
			ISummary itemSummary = (ISummary) data.getValue();
			IKeyItemDefinition definition = (IKeyItemDefinition) GameStorage.getInstance().getItemDefinition(itemSummary.getString("name"));
			IKeyItem item = definition.getItemFromDefinition();
			item.buildFromSummary(itemSummary);
			add(item);
		}
	}

	@Override
	public final void add(IKeyItem item){
		super.add(item);
		byWeight.add(item.getItemDefinition().getWeight(), item);
		byName.add(item.getItemDefinition().getDisplayName(), item);
	}

	@Override
	public void remove(IKeyItem item){
		super.remove(item);
		byWeight.remove(item.getItemDefinition().getWeight(), item);
		byName.remove(item.getItemDefinition().getDisplayName(), item);
	}

	public Collection<Map<String, ISortableItem>> getByWeight(){
		return byWeight.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByName(){
		return byName.getSortedValues();
	}
}
