package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.IMysticalSphere;
import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.equipment_items.definitions.IMysticalSphereDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.storages.MysticalSphereStorage;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class OrderedMysticalSphereStorage extends MysticalSphereStorage {
	private SortedStorage<String, IMysticalSphere> byName = new SortedStorage<>();
	private SortedStorage<Integer, IMysticalSphere> byWeight = new SortedStorage<>();

	public OrderedMysticalSphereStorage(ISummary summary){
		Map<String, Serializable> summaryMap = summary.getInternalMap();
		for(Map.Entry<String, Serializable> data : summaryMap.entrySet()){
			ISummary itemSummary = (ISummary) data.getValue();
			IMysticalSphereDefinition definition = (IMysticalSphereDefinition) GameStorage.getInstance().getItemDefinition(itemSummary.getString("name"));
			IMysticalSphere item = definition.getItemFromDefinition();
			item.buildFromSummary(itemSummary);
			add(item);
		}
	}

	@Override
	public final void add(IMysticalSphere item){
		super.add(item);
		byWeight.add(item.getItemDefinition().getWeight(), item);
		byName.add(item.getItemDefinition().getDisplayName(), item);
	}

	@Override
	public void remove(IMysticalSphere item){
		super.remove(item);
		byWeight.remove(item.getItemDefinition().getWeight(), item);
		byName.remove(item.getItemDefinition().getDisplayName(), item);
	}

	public Collection<Map<String, ISortableItem>> getByWeight(){
		return byWeight.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByName(){
		return byName.getSortedValues();
	}
}