package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.ITropoGem;
import it.nightfall.dividia.api.item.ITropoGemsBag;
import it.nightfall.dividia.api.item.definitions.ITropoGemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.equipment_items.EquipmentItemType;
import it.nightfall.dividia.bl.item.storages.TropoGemStorage;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class OrderedTropoGemStorage extends TropoGemStorage {
	private SortedStorage<String, ITropoGemsBag> byName = new SortedStorage<>();
	private SortedStorage<Integer, ITropoGemsBag> byPrice = new SortedStorage<>();
	private SortedStorage<EquipmentItemType, ITropoGemsBag> byType = new SortedStorage<>();

	public OrderedTropoGemStorage(ISummary summary){
		Map<String, Serializable> summaryMap = summary.getInternalMap();
		for(Map.Entry<String, Serializable> data : summaryMap.entrySet()){
			ISummary bagSummary = (ISummary) data.getValue();
			ITropoGemDefinition definition = (ITropoGemDefinition) GameStorage.getInstance().getItemDefinition(bagSummary.getString("info"));
			Map<String, Serializable> dataSummaryMap = bagSummary.getInternalMap();
			for(Map.Entry<String, Serializable> elementData : dataSummaryMap.entrySet()){
				ITropoGem tropogem = definition.getItemFromDefinition();
				tropogem.buildFromSummary((ISummary) elementData.getValue());
				this.add(tropogem);
			}
		}
	}

	@Override
	public final void add(ITropoGem item){
		super.add(item);
		ITropoGemDefinition tropoGemDefinition = item.getItemDefinition();
		ITropoGemsBag bag = getBag(tropoGemDefinition.getName());
		byName.add(item.getItemDefinition().getDisplayName(), bag);
		byPrice.add(item.getItemDefinition().getPrice(), bag);
		byType.add(item.getItemDefinition().getType(), bag);
	}

	@Override
	public void remove(ITropoGem item){
		ITropoGemDefinition tropoGemDefinition = item.getItemDefinition();
		ITropoGemsBag bag = getBag(tropoGemDefinition.getName());
		byName.remove(item.getItemDefinition().getDisplayName(), bag);
		byPrice.remove(item.getItemDefinition().getPrice(), bag);
		byType.remove(item.getItemDefinition().getType(), bag);
		super.remove(item);
	}

	public Collection<Map<String, ISortableItem>> getByName(){
		return byName.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByPrice(){
		return byPrice.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByType(){
		return byType.getSortedValues();
	}
}
