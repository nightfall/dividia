package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.IUtilityItem;
import it.nightfall.dividia.api.item.definitions.IUtilityItemDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.storages.UtilityStorage;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class OrderedUtilityStorage extends UtilityStorage {
	private SortedStorage<String, IUtilityItem> byName = new SortedStorage<>();
	private SortedStorage<Integer, IUtilityItem> byWeight = new SortedStorage<>();

	public OrderedUtilityStorage(ISummary summary){
		Map<String, Serializable> summaryMap = summary.getInternalMap();
		for(Map.Entry<String, Serializable> data : summaryMap.entrySet()){
			ISummary itemSummary = (ISummary) data.getValue();
			IUtilityItemDefinition definition = (IUtilityItemDefinition) GameStorage.getInstance().getItemDefinition(itemSummary.getString("name"));
			IUtilityItem item = definition.getItemFromDefinition();
			item.buildFromSummary(itemSummary);
			add(item);
		}
	}

	@Override
	public final void add(IUtilityItem item){
		super.add(item);
		byWeight.add(item.getItemDefinition().getWeight(), item);
		byName.add(item.getItemDefinition().getDisplayName(), item);
	}

	@Override
	public void remove(IUtilityItem item){
		super.remove(item);
		byWeight.remove(item.getItemDefinition().getWeight(), item);
		byName.remove(item.getItemDefinition().getDisplayName(), item);
	}

	public Collection<Map<String, ISortableItem>> getByWeight(){
		return byWeight.getSortedValues();
	}

	public Collection<Map<String, ISortableItem>> getByName(){
		return byName.getSortedValues();
	}
}