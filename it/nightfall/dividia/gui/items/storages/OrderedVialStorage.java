package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.api.item.IVial;
import it.nightfall.dividia.api.item.IVialsBag;
import it.nightfall.dividia.api.item.definitions.IVialDefinition;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.bl.item.storages.VialStorage;
import it.nightfall.dividia.bl.utility.GameStorage;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class OrderedVialStorage extends VialStorage {
	private SortedStorage<String, IVialsBag> byName = new SortedStorage<>();

	public OrderedVialStorage(ISummary summary){
		Map<String, Serializable> summaryMap = summary.getInternalMap();
		for(Map.Entry<String, Serializable> data : summaryMap.entrySet()){
			ISummary bagSummary = (ISummary) data.getValue();
			IVialDefinition definition = (IVialDefinition) GameStorage.getInstance().getItemDefinition(bagSummary.getString("info"));
			Map<String, Serializable> dataSummaryMap = bagSummary.getInternalMap();
			for(Map.Entry<String, Serializable> elementData : dataSummaryMap.entrySet()){
				IVial vial = definition.getItemFromDefinition();
				vial.buildFromSummary((ISummary) elementData.getValue());
				this.add(vial);
			}
		}
	}

	@Override
	public final void add(IVial item){
		super.add(item);
		IVialDefinition vialDefinition = item.getItemDefinition();
		IVialsBag bag = getBag(vialDefinition.getName());
		byName.add(item.getItemDefinition().getDisplayName(), bag);
	}

	@Override
	public void remove(IVial item){
		IVialDefinition vialDefinition = item.getItemDefinition();
		IVialsBag bag = getBag(vialDefinition.getName());
		byName.remove(item.getItemDefinition().getDisplayName(), bag);
		super.remove(item);
	}

	public Collection<Map<String, ISortableItem>> getByName(){
		return byName.getSortedValues();
	}
}
