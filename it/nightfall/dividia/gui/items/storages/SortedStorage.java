package it.nightfall.dividia.gui.items.storages;

import it.nightfall.dividia.api.item.ISortableItem;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class SortedStorage<KeyType, ValueType extends ISortableItem> {
	Map<KeyType, Map<String, ISortableItem>> orderedStorage = new TreeMap<>();

	Set<KeyType> getKeySet(){
		return orderedStorage.keySet();
	}

	Map<String, ISortableItem> getMap(KeyType key){
		return orderedStorage.get(key);
	}

	Collection<Map<String, ISortableItem>> getSortedValues(){
		return orderedStorage.values();
	}
	
	void add(KeyType key, ValueType value){
		if(orderedStorage.containsKey(key)){
			orderedStorage.get(key).put(value.getId(), value);
		}else{
			Map<String, ISortableItem> tempList = new HashMap<>();
			tempList.put(value.getId(), value);
			orderedStorage.put(key, tempList);
		}
	}

	void remove(KeyType key, ValueType value){
		if(orderedStorage.containsKey(key)){
			Map<String, ISortableItem> tempMap = orderedStorage.get(key);
			tempMap.remove(value.getId());
			if(tempMap.isEmpty()){
				orderedStorage.remove(key);
			}
		}
	}
}
