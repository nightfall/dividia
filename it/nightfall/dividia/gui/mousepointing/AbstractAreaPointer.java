package it.nightfall.dividia.gui.mousepointing;

import com.jme3.input.InputManager;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseAxisTrigger;
import java.util.Map;
import javax.vecmath.Vector2d;

public abstract class AbstractAreaPointer implements AnalogListener {
	protected static final int X_AXIS = 0, Y_AXIS = 1;
	private InputManager inputManager;
	protected Vector2d cursor = new Vector2d(0, 0);
	protected Vector2d indicator = new Vector2d(0, 0);
	protected boolean hasTargets;
	protected final float maxRadius;
	protected final float activationRadius;
	protected final Map<String, Vector2d> elementsInRange;

	public AbstractAreaPointer(InputManager iM, float maxRadius, float activationRadius, Map<String, Vector2d> elements){
		this.elementsInRange = elements;
		this.activationRadius = activationRadius;
		this.maxRadius = maxRadius;
		this.inputManager = iM;
	}

	public void enablePointer(){
		inputManager.addMapping("AreaPointer_UP", new MouseAxisTrigger(MouseInput.AXIS_Y, false));
		inputManager.addMapping("AreaPointer_DOWN", new MouseAxisTrigger(MouseInput.AXIS_Y, true));
		inputManager.addMapping("AreaPointer_RIGHT", new MouseAxisTrigger(MouseInput.AXIS_X, false));
		inputManager.addMapping("AreaPointer_LEFT", new MouseAxisTrigger(MouseInput.AXIS_X, true));
		inputManager.addListener(this, "AreaPointer_UP", "AreaPointer_DOWN", "AreaPointer_RIGHT", "AreaPointer_LEFT");
	}

	@Override
	public void onAnalog(String name, float value, float tpf){
		float movement = value / tpf * 10;
		switch(name){
			case "AreaPointer_LEFT":
				movement *= -1;
			case "AreaPointer_RIGHT":
				moveInternal(X_AXIS, movement);
				break;
			case "AreaPointer_DOWN":
				movement *= -1;
			case "AreaPointer_UP":
				moveInternal(Y_AXIS, movement);
				break;
		}
		checkMaxRange();
	}

	private void moveInternal(int axis, float delta){
		if(axis == X_AXIS){
			cursor.x += delta;
		}else{
			cursor.y += delta;
		}
		cursor.clamp(-maxRadius - 50, maxRadius + 50);
	}

	synchronized private void checkMaxRange(){
		if(cursor.length() > maxRadius){
			double scaleFactor = maxRadius / cursor.length();
			Vector2d tempVector = new Vector2d(cursor);
			tempVector.scale(scaleFactor);
			indicator.set(tempVector);
		}else{
			indicator.set(cursor);
		}
		updateIndicator();
	}

	public abstract void updateIndicator();

	public Vector2d getCursor(){
		return indicator;
	}

	public Vector2d getInternalCursor(){
		return cursor;
	}

	public boolean hasTargetsIn(){
		return hasTargets;
	}
}
