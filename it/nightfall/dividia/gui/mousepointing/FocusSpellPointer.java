package it.nightfall.dividia.gui.mousepointing;

import com.jme3.input.InputManager;
import java.util.Map;
import javax.vecmath.Vector2d;

public class FocusSpellPointer extends AbstractAreaPointer {
	public FocusSpellPointer(InputManager iM, float radius, float activationRadius, Map<String, Vector2d> elements){
		super(iM, radius, activationRadius, elements);
	}

	@Override
	public void updateIndicator(){
		Vector2d distance;
		for(Vector2d elementPosition : elementsInRange.values()){
			distance = new Vector2d(elementPosition);
			distance.sub(indicator);
			if(distance.length() <= activationRadius){
				hasTargets = true;
				return;
			}
		}
		hasTargets = false;
	}
}
