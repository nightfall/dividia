package it.nightfall.dividia.gui.mousepointing;

import com.jme3.input.InputManager;
import java.util.Map;
import javax.vecmath.Vector2d;

public class TargetSpellPointer extends AbstractAreaPointer {
	float overrangedPointer;

	public TargetSpellPointer(InputManager iM, float maxRadius, float activationRadius, Map<String, Vector2d> elements){
		super(iM, maxRadius, activationRadius, elements);
		overrangedPointer = activationRadius * 3f;
	}

	@Override
	public void updateIndicator(){
		Vector2d distance = new Vector2d();
		double minDistance = Double.POSITIVE_INFINITY;
		double tempDistance;
		Vector2d selectedPosition = new Vector2d();
		for(Vector2d elementPosition : elementsInRange.values()){
			distance.set(elementPosition);
			distance.sub(cursor);
			tempDistance = distance.length();
			if(tempDistance <= overrangedPointer){
				if(minDistance > tempDistance){
					minDistance = tempDistance;
					selectedPosition.set(elementPosition);
				}
			}
		}
		if(minDistance < Double.POSITIVE_INFINITY){
			indicator.set(selectedPosition);
			hasTargets = true;
		}else{
			hasTargets = false;
		}
	}
}