
package it.nightfall.dividia.gui.sounds;

import com.jme3.audio.AudioNode;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;


public class BackgroundsManager {
	private static BackgroundsManager instance;
	
	private AudioNode currentBackground;
	
	public static BackgroundsManager getInstance() {
		if(instance == null) {
			instance = new BackgroundsManager();
		}
		return instance;
	}

	public static void init() {
		instance = new BackgroundsManager();
	}
	
	public BackgroundsManager() {
	}
	
	public void changeMap(final String mapId) {
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				AudioNode oldBackground = currentBackground;
				currentBackground = SoundsLoader.getInstance().getBackgoundsNode(mapId);
				if(currentBackground!=null && oldBackground!=null)  {
					if(!oldBackground.equals(currentBackground)) {
						oldBackground.stop();
					}
				}
				if(currentBackground == null && oldBackground!=null) {
					oldBackground.stop();
				}
				if(currentBackground!=null) {
					currentBackground.setReverbEnabled(false);
					currentBackground.setVolume(0.45f);
					currentBackground.setLooping(true);
					currentBackground.play();
				}
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);
	}
	
}
