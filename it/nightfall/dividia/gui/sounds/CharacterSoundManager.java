package it.nightfall.dividia.gui.sounds;

import com.jme3.audio.AudioNode;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui_api.sounds.ICharacterSoundActionsListener;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;

public class CharacterSoundManager implements ICharacterSoundActionsListener {
	
	protected static String movementQuotesDir = "movement quotes";
	protected static String dyingQuotesDir = "dying quotes";
	protected static String attackSoundsDir = "attack sounds";
	protected static String basickAttackHitSoundsDir = "basic attack hit";
	protected static String attackQuotesDir = "attack quotes";
	protected static String spellCastSoundsDir = "spell cast sounds";
	protected String race;
	protected AudioNode basickAttackHitSound;
	protected AudioNode quotes = new AudioNode();

	public CharacterSoundManager(String race) {
		this.race = race;
		basickAttackHitSound = SoundsLoader.getInstance().getCharacterAudioNode(race, basickAttackHitSoundsDir, null);
	}
	
	
	@Override
	public void characterStartedMoving() {
	}

	@Override
	public void spellCast(String spellId) {
	}

	@Override
	public void basicAttackCast() {
	}

	@Override
	public void charactedDeath() {
		quotes = SoundsLoader.getInstance().getCharacterAudioNode(race, dyingQuotesDir, null);
		quotes.setReverbEnabled(false);
		quotes.setVolume(0.2f);
		quotes.play();
	}

	@Override
	public void characterStopped() {
	}

	@Override
	public void basicAttackHit() {
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				basickAttackHitSound = SoundsLoader.getInstance().getCharacterAudioNode(race, basickAttackHitSoundsDir, null);
				basickAttackHitSound.setReverbEnabled(false);
				basickAttackHitSound.setVolume(0.05f);
				basickAttackHitSound.play();
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);
	}
	
}
