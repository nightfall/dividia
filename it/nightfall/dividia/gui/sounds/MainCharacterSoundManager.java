package it.nightfall.dividia.gui.sounds;

import com.jme3.audio.AudioNode;
import static it.nightfall.dividia.gui.sounds.CharacterSoundManager.attackSoundsDir;
import static it.nightfall.dividia.gui.sounds.CharacterSoundManager.movementQuotesDir;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;

public class MainCharacterSoundManager extends CharacterSoundManager {
	
	//private AudioNode movementQuotes = SoundsLoader.getInstance().getCharacterAudioNode(race, movementQuotesDir, null);
	//private AudioNode attackQuotes = SoundsLoader.getInstance().getCharacterAudioNode(race, attackQuotesDir, null);
	//private AudioNode attackSounds = SoundsLoader.getInstance().getCharacterAudioNode(race, attackSoundsDir, null);
	private boolean characterStopped = true;
	
	public MainCharacterSoundManager(String race) {
		super(race);
	}
	
	
	
	@Override
	public void characterStartedMoving() {
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				if(characterStopped && notPlayingVoiceSound()) {
					if(Math.random()>=0.88) {
						
						quotes = SoundsLoader.getInstance().getCharacterAudioNode(race, movementQuotesDir, null);
						quotes.setReverbEnabled(false);
						quotes.setVolume(0.4f);
						quotes.play();
					}
					characterStopped = false;
				}		
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);
		
		
	}
	
	
	@Override
	public void characterStopped() {
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				characterStopped = true;
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);
	}
	
	
	@Override
	public void basicAttackCast() {
		if(Math.random()>=0.6) {
			IGUIEvent event = new IGUIEvent() {

				@Override
				public void execute() {
					if(notPlayingVoiceSound()) {
						quotes = SoundsLoader.getInstance().getCharacterAudioNode(race, attackSoundsDir, null);
						quotes.setReverbEnabled(false);
						quotes.setVolume(0.2f);
						quotes.play();
					}
				}
			};
			GUIEventsHandler.getInstance().putEvent(event);
		}
	}
	
	@Override
	public void basicAttackHit() {
		super.basicAttackHit();
		if(notPlayingVoiceSound() && Math.random()>= 0.88) {
			IGUIEvent event = new IGUIEvent() {

				@Override
				public void execute() {
					quotes = SoundsLoader.getInstance().getCharacterAudioNode(race, attackQuotesDir, null);
					quotes.setReverbEnabled(false);
					quotes.setVolume(0.4f);
					quotes.play();
				}
			};
			GUIEventsHandler.getInstance().putEvent(event);
		}
		
	}
	
	@Override
	public void spellCast(final String spellId) {
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				if(notPlayingVoiceSound()) {
					quotes = SoundsLoader.getInstance().getCharacterAudioNode(race, spellCastSoundsDir, spellId);
					quotes.setReverbEnabled(false);
					quotes.setVolume(0.2f);
					quotes.play();
				}
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);

	}
	
	private boolean notPlayingVoiceSound() {
		return quotes.getStatus() == AudioNode.Status.Stopped;
	}
}
