package it.nightfall.dividia.gui.sounds;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class SoundsLoader {
	
	private static SoundsLoader instance;
	private String soundsExtension = ".wav";
	private String charactersSoundsLocationWithinAssets = "Sounds/Races/";
	private String charactersSoundsLocation = "assets/"+charactersSoundsLocationWithinAssets;
	private String backgroundsLocationWithinAssets = "Sounds/Backgrounds/";
	private String backgroundsSoundsLocation = "assets/"+backgroundsLocationWithinAssets;
	private String spellSoundsLocationWithinAssets = "Sounds/Spells/";
	private String spellSoundsSoundsLocation = "assets/"+spellSoundsLocationWithinAssets;
	private HashMap<String, HashMap<String, HashMap<String,AudioNode>>> charactersAudioNodes = new HashMap<>();
	private HashMap<String, HashMap<String,AudioNode>> spellsAudioNodes = new HashMap<>();
	private Map<String, AudioNode> backgounds = new HashMap<>();
	
	public static SoundsLoader getInstance() {
		if(instance == null) {
			instance = new SoundsLoader();
		}
		return instance;
	}

	public static void init() {
		instance = new SoundsLoader();
	}
	
	public SoundsLoader() {
	}

	public void soundsLoaderInit(AssetManager assetManager) {
		loadRaceSounds(assetManager);
		loadBackgrounds(assetManager);
		loadSpellSounds(assetManager);
		
	}
	
	public AudioNode getCharacterAudioNode(String race, String subDirectory, String fileName) {
		HashMap<String, AudioNode> actionRelatedNodes = this.charactersAudioNodes.get(race).get(subDirectory);
		if(fileName!= null) {
			return  actionRelatedNodes.get(fileName+soundsExtension);
		}
		int size = actionRelatedNodes.size();
		return actionRelatedNodes.get((String)actionRelatedNodes.keySet().toArray()[SharedFunctions.getRandomInt(size)]);
	}
	
	public AudioNode getBackgoundsNode(String backgroundId) {
		return backgounds.get(backgroundId+soundsExtension);
	}

	private void loadRaceSounds(AssetManager assetManager) {
		File characterSoundsDirectory = new File(charactersSoundsLocation);
		for(File raceDirectory : characterSoundsDirectory.listFiles()) {
			HashMap<String,HashMap<String, AudioNode>> currentRaceMap = new HashMap<>();
			for(File subDirectoryFile : raceDirectory.listFiles()) {
				HashMap<String, AudioNode> currentSoundsMap = new HashMap<>();
				for(File soundFile : subDirectoryFile.listFiles()) {
					AudioNode audioNode = new AudioNode(assetManager, charactersSoundsLocationWithinAssets+raceDirectory.getName()+"/"+subDirectoryFile.getName()+"/"+soundFile.getName());
					currentSoundsMap.put(soundFile.getName(), audioNode);
				}
				currentRaceMap.put(subDirectoryFile.getName(), currentSoundsMap);
			}
			charactersAudioNodes.put(raceDirectory.getName(), currentRaceMap);
		}
	}

	private void loadBackgrounds(AssetManager assetManager) {
		File backgroundsDirectory = new File(backgroundsSoundsLocation);
		for(File soundFile : backgroundsDirectory.listFiles()) {
			AudioNode audioNode = new AudioNode(assetManager, backgroundsLocationWithinAssets+soundFile.getName());
			backgounds.put(soundFile.getName(), audioNode);
		}
	}

	private void loadSpellSounds(AssetManager assetManager) {
		File spellsDirectory = new File(spellSoundsSoundsLocation);
		for(File folders : spellsDirectory.listFiles()) {
			HashMap<String, AudioNode> currentSoundsMap = new HashMap<>();
			for(File soundFile : folders.listFiles()) {
				AudioNode audioNode = new AudioNode(assetManager, spellSoundsLocationWithinAssets+folders.getName()+"/"+soundFile.getName());
				currentSoundsMap.put(soundFile.getName(), audioNode);
			}
			spellsAudioNodes.put(folders.getName(), currentSoundsMap);
		}
	}
	
	public AudioNode getSpellCastNode(String spellId) {
		return spellsAudioNodes.get("Cast").get(spellId+soundsExtension);
	}
	
	public AudioNode getSpellHitNode(String spellId) {
		return spellsAudioNodes.get("Hit").get(spellId+soundsExtension);
	}
	
}
