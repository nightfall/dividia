package it.nightfall.dividia.gui.sounds;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import it.nightfall.dividia.gui_api.sounds.ICharacterSoundActionsListener;
import java.util.HashMap;
import java.util.Map;

public class SoundsManager {
	
	private static SoundsManager instance;
	private Map<String, ICharacterSoundActionsListener> charactersSoundsManagers = new HashMap<>();
	private AssetManager assetManager;
	private Node rootNode;
	
	public static SoundsManager getInstance() {
		if(instance == null) {
			instance = new SoundsManager();
		}
		return instance;
	}

	public static void init() {
		instance = new SoundsManager();
	}
	
	public SoundsManager() {
	}

	public void addCharactersSoundsManagers(String characterId, String race, boolean mainCharacter) {
		if(mainCharacter) {
			charactersSoundsManagers.put(characterId, new MainCharacterSoundManager(race));
		}
		else {
			charactersSoundsManagers.put(characterId, new CharacterSoundManager(race));
		}
	}
	
	public void soundsManagerInit(AssetManager assetManager, Node rootNode) {
		this.assetManager = assetManager;
		this.rootNode = rootNode;
		SoundsLoader.getInstance().soundsLoaderInit(assetManager);
		
	}
	
	public ICharacterSoundActionsListener getCharacterSoundsManager(String characterId) {
		return charactersSoundsManagers.get(characterId);
	}
	
}
