package it.nightfall.dividia.gui.sounds;

import com.jme3.audio.AudioNode;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;

public class SpellSoundsManager {
	
	private static SpellSoundsManager instance;
	
	
	public static SpellSoundsManager getInstance() {
		if(instance == null) {
			instance = new SpellSoundsManager();
		}
		return instance;
	}

	public static void init() {
		instance = new SpellSoundsManager();
	}
	
	public SpellSoundsManager() {
	}
	
	public void spellHitSound(final String spellId) {
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				AudioNode spellHitNode = SoundsLoader.getInstance().getSpellHitNode(spellId);
				if(spellHitNode!=null) {
					spellHitNode.setReverbEnabled(false);
					spellHitNode.setVolume(0.15f);
					spellHitNode.playInstance();
				}
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);
	}
	
	public void spellCastSound(final String spellId) {
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				AudioNode spellCastNode = SoundsLoader.getInstance().getSpellCastNode(spellId);
				if(spellCastNode!=null) {
					spellCastNode.setReverbEnabled(false);
					spellCastNode.setVolume(0.15f);
					spellCastNode.playInstance();
				}
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);
	}
}
