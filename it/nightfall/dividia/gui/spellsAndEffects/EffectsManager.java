package it.nightfall.dividia.gui.spellsAndEffects;

import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.util.BufferUtils;
import it.nightfall.dividia.api.battle.definitions.IAreaSpellDefinition;
import it.nightfall.dividia.api.battle.definitions.ISpellDefinition;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGameArea;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.gui.controls.LifebarControl;
import it.nightfall.dividia.gui.controls.PauseAndDeathControl;
import it.nightfall.dividia.gui.controls.Simple3DimensionalSpatialMovementControlWithPause;
import it.nightfall.dividia.gui.controls.SpellArcControl;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.spells.IGraphicEffect;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EffectsManager{
	private static EffectsManager spellManager = null;
	private final SpellArcControl spellArcControl;
	private final LifebarControl lifeBarControl;
	private ArrayList<String[]> pages = new ArrayList<>();
	private int selectedPage = 0;
	private final Map<String, Long> spellsCooldown = new HashMap<>();
	private Map<String, ISpellDefinition> knownSpells = new HashMap<>();
	private ArrayList<IGraphicEffect> waitingSpells = new ArrayList<>();

	public static void initSpellManager(SpellArcControl spellArc){
		spellManager = new EffectsManager(spellArc, LifebarControl.getLifebarControl());
	}

	public static EffectsManager getInstance(){
		return spellManager;
	}

	private EffectsManager(SpellArcControl spellArc, LifebarControl lifeBar){
		this.spellArcControl = spellArc;
		this.lifeBarControl = lifeBar;
		this.pages.add(new String[4]);
		GUISpellCaster.initGUISpellCaster();
		//TODO remove test

		
		/*addSpell("simpleSpell");
		addSpell("selfSpell");
		addSpell("bersek");
		addSpell("blueExplosion");*/
	}

	public void nextPage(){
		if(selectedPage == pages.size() - 1){
			selectedPage = 0;
		}else{
			selectedPage++;
		}
		spellArcControl.updateSpells(pages.get(selectedPage));
	}

	public void previousPage(){
		if(selectedPage == 0){
			selectedPage = pages.size() - 1;
		}else{
			selectedPage--;
		}
		spellArcControl.updateSpells(pages.get(selectedPage));
	}

	public void executeSpell(int position){
		String[] page = pages.get(selectedPage);
		if(position <= page.length){
			String spellName = page[position - 1];
			if(spellName!=null) {
				if(canCast(spellName)){
					knownSpells.get(spellName).cast(GUISpellCaster.getInstance());
				}
			}
		}
	}

	public synchronized void addSpell(String spellName){

		for(String[] page : pages){
			for(int i = 0; i < 4; i++){
				if(page[i] == null){
					page[i] = spellName;
					knownSpells.put(spellName, GameStorage.getInstance().getSpellDefinition(spellName));
					spellArcControl.updateSpells(pages.get(selectedPage));
					return;
				}
			}
		}
		pages.add(new String[4]);
		pages.get(pages.size() - 1)[0] = spellName;
		knownSpells.put(spellName, GameStorage.getInstance().getSpellDefinition(spellName));
		spellArcControl.updateSpells(pages.get(selectedPage));
	}

	public synchronized boolean canCast(String spellName){
		ISpellDefinition spell = knownSpells.get(spellName);
		if(spellsCooldown.containsKey(spell.getName())){
			Long tempTime = spellsCooldown.get(spell.getName());
			if(System.currentTimeMillis() - tempTime <= spell.getCooldown()){
				return false;
			}
		}
		if(lifeBarControl.getCurrentMana() < spell.getMpCost()){
			return false;
		}
		return true;
	}

	public void goOnCooldown(String spell){
		synchronized(spellsCooldown){
			spellsCooldown.put(spell, System.currentTimeMillis());

		}

	}

	public void launchAreaEffect(String spellName, double directionX, double directionY, double castingX, double castingY, String casterId, boolean showArea){
		if(EffectsStorage.getInstance().getEffectDefinition(spellName)!=null) {
			IGraphicEffect spell = EffectsStorage.getInstance().getEffectDefinition(spellName).getGraphicEffectFromDefinition(new Direction(directionX, directionY), new GamePoint(castingX, castingY), casterId);	
			synchronized(waitingSpells){
				waitingSpells.add(spell);
			}
			
		}
		if(showArea) {
			showAffectedArea(spellName, directionX,  directionY,  castingX,  castingY);
		}
	}


	public void update(){
		synchronized(waitingSpells){
			for(IGraphicEffect spell : waitingSpells){
				spell.launchEffect();
			}
			waitingSpells.clear();
		}
	}

	public void launchFollowingEffect(String affectedId, String alteredStatusName) {
		if(EffectsStorage.getInstance().getEffectDefinition(alteredStatusName)!=null) {
			Vector3f affectedSpatialLocation = SpatialsManager.getInstance().getCharacterSpatial(affectedId).getLocalTranslation();
			IGraphicEffect spell = EffectsStorage.getInstance().getEffectDefinition(alteredStatusName).getGraphicEffectFromDefinition(new Direction(0, 0), new GamePoint(affectedSpatialLocation.getX(), affectedSpatialLocation.getY()), affectedId);
			synchronized(waitingSpells){
				waitingSpells.add(spell);
			}
		}
	}

	private void showAffectedArea(String spellName, double directionX, double directionY, double castingX, double castingY) {
		IAreaSpellDefinition spellDefinition = (IAreaSpellDefinition)GameStorage.getInstance().getSpellDefinition(spellName);
		IGameArea spellArea = spellDefinition.getAreaOfEffect();
		ArrayList<IGamePoint> perimeter = spellArea.getPerimeter();
		
		Vector3f [] vertices = new Vector3f[perimeter.size()];
		for(int i = 0;i<vertices.length;i++) {
			vertices[i] = new Vector3f((float)perimeter.get(i).getX(),(float) perimeter.get(i).getY(), 0f);
		}
		
		
		ArrayList<Integer> tringleIndexes = new ArrayList<>();
		for(int i=0;i<vertices.length-2;i++) {
			tringleIndexes.add(0);
			tringleIndexes.add(i+1);
			tringleIndexes.add(i+2);
		}
		int [] indexes = new int[tringleIndexes.size()];
		for(int i = 0;i<tringleIndexes.size();i++) {
			indexes[i] = tringleIndexes.get(i);
		}
		Mesh customMesh = new Mesh();
		customMesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
		customMesh.setBuffer(VertexBuffer.Type.Index,    3, BufferUtils.createIntBuffer(indexes));
		customMesh.updateBound();
		final Geometry customGeometry = new Geometry("custom", customMesh);
		Material mat = new Material(ClientTest.getInstance().getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", new ColorRGBA(0, 1, 1, 0.3f));
		mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);		
		mat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
		customGeometry.setQueueBucket(RenderQueue.Bucket.Transparent);
		customGeometry.setMaterial(mat);
		customGeometry.setLocalTranslation((float)castingX,(float)castingY,0.01f);
		IDirection direction = new Direction(directionX, directionY);
		
		Quaternion q = new Quaternion();
		q.fromAngles(0, 0, (float) direction.getDirectionAngle());
		customGeometry.setLocalRotation(q);
		PauseAndDeathControl control = new PauseAndDeathControl(0.5f);
		customGeometry.addControl(control);
			
		
		IGUIEvent event = new IGUIEvent() {

			@Override
			public void execute() {
				ClientTest.getInstance().getRootNode().attachChild(customGeometry);
			}
		};
		GUIEventsHandler.getInstance().putEvent(event);
	}
}
