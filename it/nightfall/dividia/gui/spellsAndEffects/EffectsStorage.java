
package it.nightfall.dividia.gui.spellsAndEffects;

import com.jme3.asset.AssetManager;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.gui_api.spells.IGraphicEffectDefinition3D;
import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class EffectsStorage {
	
	private AssetManager assetManager;
	private final String CONFIG_GRAPHIC_SPELL_PATH = SharedFunctions.getPath("config|spells|graphics|");
	private final String CONFIG_ALTERED_STATUS_PATH = SharedFunctions.getPath("config|alteredstatuses|graphics|");
	private final String CONFIG_OTHER_EFFECTS_PATH = SharedFunctions.getPath("config|generalGraphics|");
	private static EffectsStorage storage;
	private Map<String, IGraphicEffectDefinition3D> effects = new HashMap<>();

	private EffectsStorage(AssetManager assetManager) {
		this.assetManager = assetManager;
	}
	
	public IGraphicEffectDefinition3D getEffectDefinition(String spellId) {
		return effects.get(spellId);
	}
	
	public static void initStorage(AssetManager assetManager){
		storage = new EffectsStorage(assetManager);
		storage.loadStorage();
		//TODO decommentare e risolvere
	}
	public static EffectsStorage getInstance(){
		return storage;
	}

	private void loadStorage() {
		
		try{
			loadFolder(CONFIG_GRAPHIC_SPELL_PATH);
			loadFolder(CONFIG_ALTERED_STATUS_PATH);
			loadFolder(CONFIG_OTHER_EFFECTS_PATH);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

	public void setAssetManager(AssetManager assetManager) {
		this.assetManager = assetManager;
	}

	private void loadFolder(String path) {
		IConfigReader config;	
		File areaSpellsDefinitionsDirectory = new File(path);
		File[] areaSpellsFiles = areaSpellsDefinitionsDirectory.listFiles();
		if(areaSpellsFiles!=null && areaSpellsFiles.length > 0){
			for(File tempFile : areaSpellsFiles){
				if(tempFile.isFile()){
					config = new ConfigReader(tempFile.getAbsolutePath());
					effects.put(config.getString("name"), new GraphicEffectDefinition3D(
							config));
				}
			}
		}
	}
	
	
	
}
