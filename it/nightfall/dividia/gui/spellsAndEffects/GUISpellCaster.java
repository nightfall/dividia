package it.nightfall.dividia.gui.spellsAndEffects;

import it.nightfall.dividia.api.battle.ISpellCaster;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;

public class GUISpellCaster implements ISpellCaster {
	private static GUISpellCaster guiSpellCaster = null;

	public static void initGUISpellCaster(){
		guiSpellCaster = new GUISpellCaster();
	}

	public static GUISpellCaster getInstance(){
		return guiSpellCaster;
	}

	private GUISpellCaster(){
	}

	@Override
	public void castAreaSpell(String spellName){
		try{
			ClientTest.intentionManager.castAreaSpell(spellName);
		}catch(RemoteException ex){
			
		}
	}

	@Override
	public void castFocusedSpell(String spellName){
	}

	@Override
	public void castTargetAreaSpell(String spellName){
	}

	@Override
	public void castTargetSpell(String spellName){
	}
}
