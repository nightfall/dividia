
package it.nightfall.dividia.gui.spellsAndEffects;




import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.gui.controls.EmitterControl;
import it.nightfall.dividia.gui.controls.EffectControl3D;
import it.nightfall.dividia.gui.spellsAndEffects.emitterShapes.BoxShape;
import it.nightfall.dividia.gui.spellsAndEffects.emitterShapes.PointShape;
import it.nightfall.dividia.gui.spellsAndEffects.emitterShapes.SphereShape;
import it.nightfall.dividia.gui_api.spells.IGameEmitterDefinition3D;
import it.nightfall.dividia.gui_api.spells.emitterShapes.IEmitterShape;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


public class GameEmitterDefinition3D extends TrackedElement implements IGameEmitterDefinition3D {
	
    private ParticleEmitter prototype;
	private List<NonPositionalEmitterFrame> frames = new LinkedList<>();;
	private NonPositionalEmitterFrame lastFrame;	
	private IEmitterShape shape;
	
	public GameEmitterDefinition3D(IConfigReader config) {
		super();
		prototype = new ParticleEmitter("", ParticleMesh.Type.Triangle, 0);
		
		IConfigReader wayPointReader = config.getConfigReader("spellLocalPositions");
		Collection<IConfigReader> pointReaders = wayPointReader.getConfigReaderList("point");
		
		for(IConfigReader pointReader: pointReaders) {			
			NonPositionalEmitterFrame previousFrame = null;
			if(!frames.isEmpty()) {
				previousFrame = frames.get(frames.size()-1);
			}			
			NonPositionalEmitterFrame frame = new NonPositionalEmitterFrame();
			super.addPoint(pointReader, frame);
			if(previousFrame!=null) {
				previousFrame.setSpeed(nextSpeed);
			}	
			
			if(pointReader.doesElementExist("sColor")) {
				ColorRGBA startColor = new ColorRGBA();
				int hexStartColor = pointReader.getHexadecimal("sColor");
				startColor.fromIntRGBA(hexStartColor);
				frame.setStartColor(startColor);
			}
			else {
				frame.setStartColor(lastFrame.getStartColor().clone());
			}
			
			if(pointReader.doesElementExist("eColor")) {
				ColorRGBA endColor = new ColorRGBA(); 
				int hexEndColor = pointReader.getHexadecimal("eColor");
				endColor.fromIntRGBA(hexEndColor);
				frame.setEndColor(endColor);
			}else {
				frame.setEndColor(lastFrame.getEndColor().clone());
			}
			
			if(pointReader.doesElementExist("pps")) {
				frame.setParticlePerSecond(pointReader.getFloat("pps"));
			}
			else {
				frame.setParticlePerSecond(lastFrame.getParticlePerSecond());
			}
			
			if(pointReader.doesElementExist("sSize")) {
				frame.setStartSize(pointReader.getFloat("sSize"));
			}
			else {
				frame.setStartSize(lastFrame.getStartSize());
			}
				
			if(pointReader.doesElementExist("eSize")) {
				frame.setEndSize(pointReader.getFloat("eSize"));
			}
			else {
				frame.setEndSize(lastFrame.getEndSize());
			}
			
			if(pointReader.doesElementExist("lLife")) {
				frame.setLowLife(pointReader.getFloat("lLife"));
			}
			else {
				frame.setLowLife(lastFrame.getLowLife());
			}
			
			if(pointReader.doesElementExist("hLife")) {
				frame.setHighLife(pointReader.getFloat("hLife"));
			}
			else {
				frame.setHighLife(lastFrame.getHighLife());
			}
			
			frames.add(frame);
			lastFrame = frame;
			
		}
		
		prototype.setNumParticles(config.getInteger("particlesNumber"));
		prototype.setRandomAngle(config.getBoolean("randomAngle"));
		prototype.setImagesX(config.getInteger("imagesX"));
		prototype.setImagesY(config.getInteger("imagesY"));
		prototype.setRotateSpeed(config.getFloat("rotateSpeed"));
		prototype.setInWorldSpace(config.getBoolean("inWorldSpace"));
		IConfigReader gravityReader = config.getConfigReader("gravity");
		prototype.setGravity(gravityReader.getFloat("x"), gravityReader.getFloat("y"), gravityReader.getFloat("z"));
		//TODO aggiustare i separators
		Material mat = new Material(EffectsStorage.getInstance().getAssetManager(), SharedFunctions.getPath(config.getString("material")));
        mat.setTexture("Texture", EffectsStorage.getInstance().getAssetManager().loadTexture(SharedFunctions.getPath(config.getString("texture"))));
		prototype.setMaterial(mat);
		
		prototype.getParticleInfluencer().setInitialVelocity(Vector3f.ZERO);
		
		IConfigReader shapeReader = config.getConfigReader("shape");
		if(shapeReader.doesElementExist("sphere")) {			
			IConfigReader sphereReader = shapeReader.getConfigReader("sphere");
			IConfigReader centerReader = sphereReader.getConfigReader("center");
			Vector3f center = new Vector3f(centerReader.getFloat("x"), centerReader.getFloat("y"), centerReader.getFloat("z"));			
			float radius = shapeReader.getFloat("radius");
			shape =new SphereShape(center, radius);
		}
		else if(shapeReader.doesElementExist("box")){
			IConfigReader box = shapeReader.getConfigReader("box");
			IConfigReader minReader = box.getConfigReader("min");
			Vector3f minVector = new Vector3f(minReader.getFloat("x"), minReader.getFloat("y"), minReader.getFloat("z"));			
			IConfigReader maxReader = box.getConfigReader("max");
			Vector3f maxVector = new Vector3f(maxReader.getFloat("x"), maxReader.getFloat("y"), maxReader.getFloat("z"));
			shape = new BoxShape(minVector, maxVector);
		}
		else {
			shape = new PointShape(Vector3f.ZERO);
		}
		
		
		prototype.setRandomAngle(true);
		
		
	}
	
	@Override
	public ParticleEmitter getEmitterFromDefinition(IDirection castingDirection, Vector3f nodeInitialPosition, EffectControl3D spellControl) {
		ParticleEmitter p = prototype.clone();
		EmitterControl control = new EmitterControl(p);		
		shape.rotate(castingDirection);
		p.setShape(shape.getEmitterShape());
		for(Vector3f wayPoint: wayPoints) {
			Vector3f clonePoint = wayPoint.clone();
			IGamePoint planePoint = new GamePoint(clonePoint.x, clonePoint.y);
			planePoint.rotatePoint(castingDirection);
			clonePoint.setX((float) planePoint.getX());
			clonePoint.setY((float) planePoint.getY());
			clonePoint.addLocal(nodeInitialPosition);
			control.addDestinationPoint(clonePoint.getX(), clonePoint.getY(), clonePoint.getZ());
		} 
		if(!wayPoints.isEmpty()) {
			p.setLocalTranslation(control.getDestinationPoints().peek());
			control.setLastWayPoint(control.getDestinationPoints().peek());
			control.getDestinationPoints().poll();
		}
		else {
			p.setLocalTranslation(nodeInitialPosition.clone());
		}
		control.setFrames(frames);
		control.setSpellNode(spellControl);
		for(Float pauseTime: pauseTimes) {
			control.addPauseTime(pauseTime);
		}
		for(Boolean value: nextFrameIsAPauseOne) {
			control.addNextFrameIsAPauseOne(value);
		}
		p.addControl(control);		
		control.applyTheFirstFrame();
		return p;
	}	
}
