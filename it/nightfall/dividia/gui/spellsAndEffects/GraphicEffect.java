
package it.nightfall.dividia.gui.spellsAndEffects;

import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import it.nightfall.dividia.gui.controls.EffectControl3D;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.spells.IGraphicEffect;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


public class GraphicEffect extends Node implements IGraphicEffect {

	private List<ParticleEmitter> emitters;
	
	public GraphicEffect() {
		super();
		emitters = new LinkedList<>();
		
	}	
	
	@Override
	public void launchEffect() {
		for(ParticleEmitter emitter : emitters) {
			emitter.setEnabled(true);
			SpatialsManager.getInstance().getRootNode().attachChild(emitter);
		}
		SpatialsManager.getInstance().getRootNode().attachChild(this);
		
	}

	@Override
	public void addEmitter(ParticleEmitter emitter) {
		emitters.add(emitter);
	}

	@Override
	public Collection<ParticleEmitter> getEmitters() {
		return emitters;
	}

	@Override
	public void setControl(EffectControl3D control) {
		super.addControl(control);
	}
	
	@Override
	public void setLocalTranslation(Vector3f position) {
		super.setLocalTranslation(position);
	}

	@Override
	public Node getSpellNode() {
		return this;
	}
	
}
