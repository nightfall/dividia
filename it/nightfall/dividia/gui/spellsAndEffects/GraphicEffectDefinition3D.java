
package it.nightfall.dividia.gui.spellsAndEffects;

import it.nightfall.dividia.gui_api.spells.IGraphicEffectDefinition3D;
import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.gui.controls.FollowingEffectControl3D;
import it.nightfall.dividia.gui.controls.MovingAndRotatingSpatialControl3D;
import it.nightfall.dividia.gui.controls.Simple3DimensionalSpatialMovementControl;
import it.nightfall.dividia.gui.controls.EffectControl3D;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.controls.ISimple3DimensionalSpatialMovementControl;
import it.nightfall.dividia.gui_api.spells.IGameEmitterDefinition3D;
import it.nightfall.dividia.gui_api.spells.IGraphicEffect;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


public class GraphicEffectDefinition3D extends TrackedElement implements IGraphicEffectDefinition3D { 
	
	private List<IGameEmitterDefinition3D> emitters = new ArrayList<>();;
	private List<SpeedFrame> speedFrames = new LinkedList<>();
    private boolean followCaster;

	public GraphicEffectDefinition3D() {
	}
	
	public GraphicEffectDefinition3D(IConfigReader configReader) {
		super();
		Collection<IConfigReader> wayPointReaders = configReader.getConfigReader("playerLocalPositions").getConfigReaderList("point");
		for(IConfigReader pointReader : wayPointReaders) {
			SpeedFrame speedFrame = new SpeedFrame();
			super.addPoint(pointReader, speedFrame);			
			if(wayPoints.size() != 1) {		
				speedFrames.add(speedFrame);
			}
		}
		Collection <IConfigReader> configs = configReader.getConfigReader("emitters").getConfigReaderList("emitter");
		for (IConfigReader currentConfigReader : configs) {
			emitters.add(new GameEmitterDefinition3D(currentConfigReader));
		}
        followCaster = configReader.getBoolean("followCaster");
		
	}
	
	@Override
	public IGraphicEffect getGraphicEffectFromDefinition(IDirection castingDirection,IGamePoint castingPoint, String executorId) {
		IGraphicEffect spell = new GraphicEffect();	
        EffectControl3D control;
        if(followCaster) {
            control = new FollowingEffectControl3D(0, spell, SpatialsManager.getInstance().getCharacterSpatial(executorId).getControl(MovingAndRotatingSpatialControl3D.class).getMovementControl());
        }
        else {
            control = new EffectControl3D(0,spell);
        }
		control.setSpeedFrames(speedFrames);
		for(Vector3f wayPoint: wayPoints) {
			Vector3f pointClone = wayPoint.clone();
			IGamePoint planePoint = new GamePoint(pointClone.x, pointClone.y);
			planePoint.rotatePoint(castingDirection);
			pointClone.setX((float) planePoint.getX());
			pointClone.setY((float) planePoint.getY());
			pointClone.addLocal(new Vector3f((float) castingPoint.getX(), (float) castingPoint.getY(),0));
			control.addDestinationPoint(pointClone.getX(), pointClone.getY(), pointClone.getZ());
		}			
		Vector3f initialPosition = new Vector3f((float) castingPoint.getX(), (float) castingPoint.getY(),0);
		if(!wayPoints.isEmpty()) {
			initialPosition = control.getDestinationPoints().peek();
			control.getDestinationPoints().poll();
		}
		for(Float pauseTime: pauseTimes) {
			control.addPauseTime(pauseTime);
		}
		for(Boolean value: nextFrameIsAPauseOne) {
			control.addNextFrameIsAPauseOne(value);
		}
		spell.setLocalTranslation(initialPosition);
		for(IGameEmitterDefinition3D emitter: emitters) {
			spell.addEmitter(emitter.getEmitterFromDefinition(castingDirection, initialPosition, control));
		}
		spell.setControl(control);
		return spell;		
	}	

	@Override
	public List<IGameEmitterDefinition3D> getEmitters() {
		return emitters;
	}

	@Override
	public void setEmitters(List<IGameEmitterDefinition3D> emitters) {
		this.emitters = emitters;
	}

	@Override
	public List<SpeedFrame> getSpeedFrames() {
		return speedFrames;
	}

	@Override
	public void setSpeedFrames(List<SpeedFrame> speedFrames) {
		this.speedFrames = speedFrames;
	}
	
	
}

