
package it.nightfall.dividia.gui.spellsAndEffects;

import com.jme3.effect.ParticleEmitter;
import com.jme3.math.ColorRGBA;
import it.nightfall.dividia.gui.controls.EmitterControl;


public class NonPositionalEmitterFrame extends SpeedFrame {	
	
	private ColorRGBA startColor;
	private ColorRGBA endColor;
	private float particlePerSecond;
	private float startSize;
	private float endSize;
	private float lowLife;
	private float highLife;
	
	

	public NonPositionalEmitterFrame() {
	}
	
	public void applyFrame(EmitterControl control) {
		super.applyFrame(control);
		
		ParticleEmitter emitter = control.getEmitter();
		emitter.setStartColor(startColor.clone());
		emitter.setEndColor(endColor.clone());
		
		emitter.setParticlesPerSec(particlePerSecond);
		
		emitter.setStartSize(startSize);
		emitter.setEndSize(endSize);
		
		emitter.setLowLife(lowLife);
		emitter.setHighLife(highLife);
		
		
	}

	public void setStartColor(ColorRGBA frameColor) {
		this.startColor = frameColor;
	}

	public ColorRGBA getStartColor() {
		return startColor;
	}

	public ColorRGBA getEndColor() {
		return endColor;
	}

	public void setEndColor(ColorRGBA endColor) {
		this.endColor = endColor;
	}

	public float getParticlePerSecond() {
		return particlePerSecond;
	}

	public void setParticlePerSecond(float particlePerSecond) {
		this.particlePerSecond = particlePerSecond;
	}
	
	public void interpolateFrame(NonPositionalEmitterFrame lastFrame, float progress, EmitterControl control) {
		//super.interpolateFrame(lastFrame, progress, control);
		interpolateStartColor(lastFrame.getStartColor(),progress, control);
		interpolateEndColor(lastFrame.getEndColor(),progress, control);
		interpolateSizes(lastFrame.getStartSize(), lastFrame.getEndSize() ,progress, control);
		interpolateLives(lastFrame.getLowLife(), lastFrame.getHighLife() ,progress, control);
	}

	private void interpolateStartColor(ColorRGBA lastStartColor, float progress, EmitterControl control) {
		ColorRGBA interpolationColor = lastStartColor.clone();
		interpolationColor.interpolate(this.startColor, progress);
		control.getEmitter().setStartColor(interpolationColor.clone());
		//System.out.println("start" + interpolationColor.clone()+  " " + progress);
	}
	
	private void interpolateEndColor(ColorRGBA lastEndColor, float progress, EmitterControl control) {
		ColorRGBA interpolationColor = lastEndColor.clone();
		interpolationColor.interpolate(this.endColor, progress);
		control.getEmitter().setEndColor(interpolationColor.clone());
		
		//System.out.println("end" + interpolationColor.clone() + " " + progress);
	}

	public float getStartSize() {
		return startSize;
	}

	public void setStartSize(float startSize) {
		this.startSize = startSize;
	}

	public float getEndSize() {
		return endSize;
	}

	public void setEndSize(float endSize) {
		this.endSize = endSize;
	}

	private void interpolateSizes(float startSize, float endSize, float progress, EmitterControl control) {
		control.getEmitter().setStartSize(startSize + progress * (this.startSize - startSize));
		control.getEmitter().setEndSize(endSize + progress * (this.endSize - endSize));		
	}
	
	private void interpolateLives(float lowLife, float highLife, float progress, EmitterControl control) {
		control.getEmitter().setLowLife(lowLife + progress * (this.lowLife - lowLife));
		control.getEmitter().setHighLife(highLife + progress * (this.highLife - highLife));
	}

	public float getLowLife() {
		return lowLife;
	}

	public void setLowLife(float lowLife) {
		this.lowLife = lowLife;
	}

	public float getHighLife() {
		return highLife;
	}

	public void setHighLife(float highLife) {
		this.highLife = highLife;
	}

	
	
	
	
	
	
}
