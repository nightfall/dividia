
package it.nightfall.dividia.gui.spellsAndEffects;

import it.nightfall.dividia.gui.controls.EmitterControl;
import it.nightfall.dividia.gui_api.controls.ISimple3DimensionalSpatialMovementControl;


public class SpeedFrame {
	private float speed;
	
	public void applyFrame(ISimple3DimensionalSpatialMovementControl spatialControl) {
		spatialControl.setSpatialSpeed(speed);
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void interpolateFrame(SpeedFrame lastFrame, float progress, EmitterControl control) {
		control.setSpatialSpeed(lastFrame.speed + progress * (speed - lastFrame.speed));
	}

	public float getSpeed() {
		return speed;
	}
	
	
	
	
}
