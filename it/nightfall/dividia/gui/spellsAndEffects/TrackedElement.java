
package it.nightfall.dividia.gui.spellsAndEffects;

import it.nightfall.dividia.gui_api.spells.ITrakedElement;
import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import java.util.LinkedList;
import java.util.List;


public abstract class TrackedElement implements ITrakedElement {
	
	protected List<Vector3f> wayPoints = new LinkedList<>();		
	protected List<Float> pauseTimes = new LinkedList<>();
	protected List<Boolean> nextFrameIsAPauseOne = new LinkedList<>();
	private float time = 0;
	protected float nextSpeed = 0;
	
	public TrackedElement() {
	}
	
	@Override
	public void addPoint(IConfigReader pointReader, SpeedFrame speedFrame){		
		Vector3f nextWayPoint = new Vector3f(pointReader.getFloat("x"), pointReader.getFloat("y"), pointReader.getFloat("z"));				
		float nextTime = pointReader.getFloat("t");
		if(wayPoints.isEmpty()) {
			wayPoints.add(nextWayPoint);
			
		}
		else if(wayPoints.size()>0) {
			Vector3f previousPoint  = wayPoints.get(wayPoints.size()-1);
			Vector3f difference = nextWayPoint.add(previousPoint.mult(-1));
			nextSpeed = difference.length()/(nextTime-time) * 100;
			if(nextWayPoint.equals(previousPoint)) {
				pauseTimes.add(nextTime-time);
				nextFrameIsAPauseOne.add(Boolean.TRUE);
			}
			else {
				nextFrameIsAPauseOne.add(Boolean.FALSE);
				wayPoints.add(nextWayPoint);
			}
			time=nextTime;			
		}
		if(wayPoints.size() != 1) {
			speedFrame.setSpeed(nextSpeed);
		}
	}

	@Override
	public List<Vector3f> getWayPoints() {
		return wayPoints;
	}

	@Override
	public void setWayPoints(List<Vector3f> wayPoints) {
		this.wayPoints = wayPoints;
	}

	@Override
	public List<Float> getPauseTimes() {
		return pauseTimes;
	}

	@Override
	public void setPauseTimes(List<Float> pauseTimes) {
		this.pauseTimes = pauseTimes;
	}

	@Override
	public float getNextSpeed() {
		return nextSpeed;
	}

	@Override
	public void setNextSpeed(float nextSpeed) {
		this.nextSpeed = nextSpeed;
	}
	
	
}
