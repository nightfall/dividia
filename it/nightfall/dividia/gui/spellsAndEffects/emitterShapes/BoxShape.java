
package it.nightfall.dividia.gui.spellsAndEffects.emitterShapes;

import com.jme3.effect.shapes.EmitterBoxShape;
import com.jme3.effect.shapes.EmitterShape;
import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.gui_api.spells.emitterShapes.IEmitterShape;


public class BoxShape extends EmitterBoxShape implements IEmitterShape {

	public BoxShape(Vector3f min, Vector3f max) {
		super(min, max);
	}

	@Override
	public void rotate(IDirection direction) {
		Vector3f boxMin = super.getMin();
		Vector3f boxLen = super.getLen();
		Vector3f boxMax = boxMin.add(boxLen);			
		IGamePoint minPoint = new GamePoint(boxMin.getX(), boxMin.getY());
			float minZ = boxMin.getZ();
			IGamePoint maxPoint = new GamePoint(boxMax.getX(), boxMax.getY());
			float maxZ = boxMax.getZ();
			minPoint.rotatePoint(direction);
			maxPoint.rotatePoint(direction);			
			Vector3f newMin = new Vector3f((float)minPoint.getX(), (float)minPoint.getY(), minZ);
			Vector3f newMax = new Vector3f((float)maxPoint.getX(), (float)maxPoint.getY(), maxZ);
			super.setMin(newMin);
			super.setLen(newMax.add(newMin.mult(-1)));
	}
	
	@Override
	public EmitterShape getEmitterShape() {
		return deepClone();
	}
	
}
