
package it.nightfall.dividia.gui.spellsAndEffects.emitterShapes;

import com.jme3.effect.shapes.EmitterPointShape;
import com.jme3.effect.shapes.EmitterShape;
import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.gui_api.spells.emitterShapes.IEmitterShape;


public class PointShape extends EmitterPointShape implements IEmitterShape{

	public PointShape(Vector3f point) {
		super(point);
	}
	
	@Override
	public void rotate(IDirection direction) {
	}

	@Override
	public EmitterShape getEmitterShape() {
		return deepClone();
	}

}
