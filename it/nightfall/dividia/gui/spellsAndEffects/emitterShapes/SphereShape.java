
package it.nightfall.dividia.gui.spellsAndEffects.emitterShapes;

import com.jme3.effect.shapes.EmitterShape;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.gui_api.spells.emitterShapes.IEmitterShape;


public class SphereShape extends EmitterSphereShape implements IEmitterShape{

	public SphereShape(Vector3f center, float radius) {
		super(center, radius);
	}	
	
	@Override
	public void rotate(IDirection direction) {
	}

	@Override
	public EmitterShape getEmitterShape() {
		return deepClone();
	}

}
