package it.nightfall.dividia.gui.two_dimensional_gui.animations;


import com.jme3.asset.AssetManager;
import com.jme3.texture.Texture;
import it.nightfall.dividia.api.utility.io.IConfigReader;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andrea
 */
public class AnimationData {
    
    private Texture animationSheet;
    private float size;
    private int spritePerRow;
    private int spriteperCol;
    private float speed;
	private String name;
    
    public AnimationData (IConfigReader configReader, AssetManager assetManager){
        this.animationSheet = assetManager.loadTexture(configReader.getString("texture"));
        this.size = configReader.getFloat("size");
        this.spritePerRow = configReader.getInteger("spritePerRow");
        this.spriteperCol = configReader.getInteger("spritePerCol");
        this.speed = configReader.getFloat("speed");
		this.name = configReader.getString("name");
        
    }

    public Texture getAnimationSheet() {
        return animationSheet;
    }

    public float getSpeed() {
        return speed;
    }

    public int getSpritePerRow() {
        return spritePerRow;
    }

    public int getSpriteperCol() {
        return spriteperCol;
    }
    
    public float getSize(){
        return size;
    }
    
	public String getName(){
		return name;
	}
}
