package it.nightfall.dividia.gui.two_dimensional_gui.animations;

import com.jme3.asset.AssetManager;
import com.jme3.texture.Texture;
import it.nightfall.dividia.api.utility.io.IConfigReader;
/*
 * DRAFT
 */
public class TimedAnimationData extends AnimationData{

	private float duration;
	
	public TimedAnimationData(IConfigReader configReader, AssetManager assetManager) {
		super(configReader,assetManager);
		this.duration = configReader.getFloat("duration");
	}
	
	public float getDuration(){
		return duration;
	}

}
