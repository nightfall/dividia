package it.nightfall.dividia.gui.two_dimensional_gui.controls;

import com.jme3.export.Savable;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.playerMovements.Movement;
import it.nightfall.dividia.gui.controls.LifebarControl;
import it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.characters.PlayingCharacter2D;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.controls.ICharacterAnimationControl2D;
import it.nightfall.dividia.gui_api.controls.ISpatialMovementControl;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CharacterMovementControl2D extends AbstractControl implements Savable, Cloneable, ISpatialMovementControl {

	private LinkedList <IGamePoint> destinations;
	private Queue <Movement> directions;
	private Movement lastDirection;
	public static int MAX_QUEUE_LENGTH = 10;
	public static int MAX_QUEUE_LENGTH_TO_MANTAIN_NORMAL_SPEED = 3;
	public int initialMovementSpeed;
	private boolean characterStopped;
	private boolean followedByCamera;
	private Camera appCamera;
	private IGraphicShape characterShape;
	
	public CharacterMovementControl2D(int initialMovementSpeed, Camera appCamera, IGraphicShape shape, boolean followedByCamera){
		this.destinations = new LinkedList<>();
		this.directions = new LinkedList<>();
		this.lastDirection = Movement.BACKWARD;
		this.initialMovementSpeed = initialMovementSpeed;
		this.characterStopped = false;
		this.appCamera = appCamera;
		this.characterShape = shape;
		this.followedByCamera = followedByCamera;
	} 
	
	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
    }
	
	
	/**
	 * Updates spatial position and direction, according to the next direction and destination to be processed.
	 */
	@Override
	protected synchronized void controlUpdate(float tpf){
		
		if(spatial != null && !destinations.isEmpty()) {
			if (destinations.size() > MAX_QUEUE_LENGTH_TO_MANTAIN_NORMAL_SPEED){
				interpolateTranslation(tpf,true);
                System.out.println("Max queue length reached for normal speed");
                
			} else {
				interpolateTranslation(tpf,false);
            }
			
			if (!directions.isEmpty()){
				spatial.getControl(ICharacterAnimationControl2D.class).playAnimation(directions.peek());	
            } 
			
		} if (directions.isEmpty() && characterStopped){
			((PlayingCharacter2D)spatial).stand(lastDirection);
            spatial.getControl(ICharacterAnimationControl2D.class).animationFinished();
			characterStopped = false;
		}
		try {
					if(spatial.getName().equals(ClientTest.clientListener.getUsername())) {
						LifebarControl.getLifebarControl().setRotation(lastDirection.getGeneralAngle());
					}
				} catch (RemoteException ex) {
				Logger.getLogger(CharacterMovementControl2D.class.getName()).log(Level.SEVERE, null, ex);
				}
	}
	
	private void interpolateTranslation(float tpf, boolean doubleSpeed){
		IGamePoint destination = destinations.peek();
		Movement currentMovement = directions.peek();
		IGamePoint currentPosition = new GamePoint (spatial.getLocalTranslation().x, spatial.getLocalTranslation().y);
		double stepLength = IGameConstants.MOVEMENT_SPEED_CONSTANT * initialMovementSpeed * 1.0 * tpf / 100 * 1.05f;
		IGamePoint nextStepPoint;// = movementDirection.getDistancedPointAlongDirection(currentPosition, stepLength);
		//TODO fix it
		if(doubleSpeed){
			stepLength *= 2;
		} 
		switch (currentMovement){
			case FORWARD :
				nextStepPoint = new GamePoint(currentPosition.getX(),stepLength + currentPosition.getY());
				break;
			case BACKWARD :
				nextStepPoint = new GamePoint(currentPosition.getX(),currentPosition.getY() - stepLength);
				break;
			case LEFT :
				nextStepPoint = new GamePoint(currentPosition.getX() - stepLength,currentPosition.getY());
				break;
			case RIGHT :
				nextStepPoint = new GamePoint (currentPosition.getX() + stepLength,currentPosition.getY());
				break;
			default :
				//should not entere here
				nextStepPoint = new GamePoint(currentPosition.getX(),currentPosition.getY());
				break;
		}
		//System.out.println("-- PASSO DI " + stepLength + " DISTANZA " + currentPosition.getDistanceFromAnotherPoint(destination));
		if (stepLength >= currentPosition.getDistanceFromAnotherPoint(destination)){
			characterShape.translate((float) (destination.getX() - currentPosition.getX()),(float)(destination.getY() - currentPosition.getY()),0);
			spatial.setLocalTranslation((float)destination.getX(),(float) destination.getY(),0.5f);
			destinations.poll();
			lastDirection = directions.poll();
			//System.out.println("!DEL >> " + destinations.size() + " " + directions.size());
		} else {
			characterShape.translate((float) (nextStepPoint.getX() - currentPosition.getX()),(float)(nextStepPoint.getY() - currentPosition.getY()),0);
			spatial.setLocalTranslation((float) nextStepPoint.getX(),(float)nextStepPoint.getY(),0.5f);
		}
		if(followedByCamera) {
			appCamera.setLocation(new Vector3f(spatial.getLocalTranslation().x, spatial.getLocalTranslation().y,appCamera.getLocation().z));
		}
	}
	
	private synchronized void jumpToCurrentLocation(){
		while (directions.size() != 1){
			directions.remove();
		}
		IGamePoint destination = destinations.poll();
		characterShape.translate((float) (destination.getX() - spatial.getLocalTranslation().x),(float)(destination.getY() - spatial.getLocalTranslation().y),0);
		spatial.setLocalTranslation(new Vector3f((float) destination.getX(), (float) destination.getY(), 0.5f));
		if(followedByCamera) {
			appCamera.setLocation(new Vector3f(spatial.getLocalTranslation().x, spatial.getLocalTranslation().y,appCamera.getLocation().z));
		}
		directions.poll();
	}
		 
	@Override
	public Control cloneForSpatial(Spatial spatial){
		return null;
	}

	/**
	 * Handles the character's movement, putting the next direction and the next point to be reached.
	 */
	@Override
	public synchronized void characterMoved(double x, double y) {
		double deltaX, deltaY;
		IGamePoint lastPoint;
		if (!destinations.isEmpty()){
			lastPoint = destinations.getLast();
        }
        else{
			lastPoint = new GamePoint (spatial.getLocalTranslation().x,spatial.getLocalTranslation().y);
        }
		deltaX = x - lastPoint.getX();
		deltaY = y - lastPoint.getY();
		
		if (Math.abs(deltaX) > Math.abs(deltaY)){
			deltaY = 0;
			y = lastPoint.getY();
		} else {
			deltaX = 0;
			x = lastPoint.getX();
		}
		
		Movement nextDirection = Movement.NOT_MOVING;

		if (deltaX < 0 && deltaY == 0){
			nextDirection = Movement.LEFT;
		} else if (deltaX == 0 && deltaY < 0){
			nextDirection = Movement.BACKWARD;
		} else if (deltaX > 0 && deltaY == 0){
			nextDirection = Movement.RIGHT;
		} else if (deltaX == 0 && deltaY > 0){
			nextDirection = Movement.FORWARD;
		}
		
		destinations.add(new GamePoint(x, y));
		directions.add(nextDirection);
		//System.out.println("!ADD >>>" + destinations.size() + " " + directions.size());
	}
	@Override
	protected void controlRender(RenderManager arg0, ViewPort arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public synchronized void characterStopped() {
		characterStopped = true;
	}

	@Override
	public void setSpeed(int newValue) {
		//TODO generare
	}

    
    //TODO it may implement a different interface
    @Override
    public Vector3f getLastPointReached() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addDestinationPoint(float x, float y, float z) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSpatialSpeed(float spatialSpeed) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Queue<Vector3f> getDestinationPoints() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float getSpatialSpeed() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }	 
}
