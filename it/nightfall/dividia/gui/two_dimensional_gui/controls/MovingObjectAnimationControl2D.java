package it.nightfall.dividia.gui.two_dimensional_gui.controls;

import it.nightfall.dividia.bl.utility.playerMovements.Movement;
import it.nightfall.dividia.gui.two_dimensional_gui.animations.AnimationData;
import it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.characters.PlayingCharacter2D;
import it.nightfall.dividia.gui_api.controls.ICharacterAnimationControl2D;

import java.util.Map;

import com.jme3.export.Savable;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import it.nightfall.dividia.gui.controls.MovingAndRotatingSpatialControl3D;
import it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.FourDirectionMovingObject2D;

public class MovingObjectAnimationControl2D extends AbstractControl implements ICharacterAnimationControl2D, Savable, Cloneable {

	private Movement currentDirection;
	private Map <String, AnimationData> animations;
	private boolean animationChanged = false;
	
	public MovingObjectAnimationControl2D(Map <String, AnimationData> animations){
		this.currentDirection = Movement.NOT_MOVING;
		this.animations = animations;
	}
	
	@Override
	public Control cloneForSpatial(Spatial spatial) {
		MovingObjectAnimationControl2D charAnimationControl = new MovingObjectAnimationControl2D(this.animations);
		spatial.addControl(charAnimationControl);
		return charAnimationControl;
	}
	
	@Override
	public void playAnimation(Movement nextDirection) {
		if (nextDirection != currentDirection){
			currentDirection = nextDirection;
			animationChanged = true;
		}
	}
    
	@Override
    public void animationFinished(){
        currentDirection = Movement.NOT_MOVING;
    }

	@Override
	protected void controlRender(RenderManager arg0, ViewPort arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void controlUpdate(float arg0) {
		if (animationChanged){
			if(currentDirection != Movement.NOT_MOVING){
				((FourDirectionMovingObject2D) spatial).playAnimation(animations.get(currentDirection.toString()));
				animationChanged = false;
			}
		}
		
	}

}
