package it.nightfall.dividia.gui.two_dimensional_gui.moving_objects;


import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.gui.two_dimensional_gui.animations.AnimationData;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.IAnimable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andrea
 */
public abstract class FourDirectionMovingObject2D extends ParticleEmitter implements IAnimable{
    
    private String displayName;
	private Map<String,AnimationData> animations;
	private Material material;
    
    public FourDirectionMovingObject2D (IConfigReader reader){
        super(reader.getString("name"), Type.Triangle, 1);
        this.setImagesX(1);
        this.setImagesY(1);
        this.setGravity(Vector3f.ZERO);
        this.setInWorldSpace(false);
		Material mat = new Material(SpatialsManager.getInstance().getAssetManager(), reader.getString("material"));
        this.setMaterial(mat);
        //TODO caricarle in modo concorde a tutte le animazioni
        this.setLowLife(reader.getFloat("lowLife"));
        this.setHighLife(reader.getFloat("highLife"));
        this.setSize(reader.getFloat("size"));
		animations = new HashMap<>();
		Collection<IConfigReader> animationReader = reader.getConfigReader("animations").getConfigReaderList("animation");
		for (IConfigReader currReder : animationReader){
			AnimationData tempAnimation = new AnimationData(currReder, SpatialsManager.getInstance().getAssetManager());
			animations.put(tempAnimation.getName(), tempAnimation);
		}
		this.setName(name);
        
       mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
       this.setQueueBucket(Bucket.Transparent);
         
    }
    
    protected void setSize (float size){
        this.setStartSize(size);
        this.setEndSize(size);
    }
	
	@Override
    public void playAnimation(AnimationData animation){
        this.getMaterial().setTexture("ColorMap", animation.getAnimationSheet());
        this.setImagesX(animation.getSpriteperCol());
        this.setImagesY(animation.getSpritePerRow());
        this.setSize(animation.getSize());
        this.setHighLife(animation.getSpeed());
        this.setLowLife(animation.getSpeed());        
    }
    
    
    public String getDisplayName(){
    	return displayName;
    }
	
	public Map<String,AnimationData> getAnimations(){
		return animations;
	}
    
}
