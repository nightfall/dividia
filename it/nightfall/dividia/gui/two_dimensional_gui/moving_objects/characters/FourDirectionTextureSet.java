package it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.characters;


import com.jme3.asset.AssetManager;
import com.jme3.texture.Texture;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.playerMovements.Movement;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andrea
 */
public class FourDirectionTextureSet {
    
    private Texture leftTexture;
    private Texture rightTexture;
    private Texture frontTexture;
    private Texture backTexture;
    
    public FourDirectionTextureSet (IConfigReader textureSetReader,AssetManager assetManager){
        this.leftTexture = assetManager.loadTexture(textureSetReader.getString("left")) ;
        this.rightTexture = assetManager.loadTexture(textureSetReader.getString("right"));
        this.frontTexture = assetManager.loadTexture(textureSetReader.getString("front"));
        this.backTexture = assetManager.loadTexture(textureSetReader.getString("back"));
    }
    
    public Texture getTexture (Movement direction){
        switch (direction){
            case LEFT :
                return leftTexture;
            case RIGHT :
                return rightTexture;
            case FORWARD :
                return frontTexture;
            case BACKWARD :
                return backTexture;
            default:
                return null;
        }
    }

    
}
