/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.characters;

import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.playerMovements.Movement;
import it.nightfall.dividia.gui.two_dimensional_gui.animations.AnimationData;
import it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.characters.FourDirectionTextureSet;
import it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.FourDirectionMovingObject2D;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.IAnimable;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andrea
 */
public class PlayingCharacter2D extends FourDirectionMovingObject2D{
    
    private FourDirectionTextureSet standTextures;
    
    public PlayingCharacter2D (IConfigReader reader){
        super(reader);
        this.standTextures = new FourDirectionTextureSet(reader.getConfigReader("standTextures"), SpatialsManager.getInstance().getAssetManager());
		stand(Movement.valueOf(reader.getString("defaultStandTexture")));
    }
	
    
    public void stand (Movement standPosition){
        this.getMaterial().setTexture("ColorMap", standTextures.getTexture(standPosition));
        this.setImagesX(1);
        this.setImagesY(1);
    }
}
