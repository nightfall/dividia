/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.two_dimensional_gui.utility;

import it.nightfall.dividia.bl.utility.playerMovements.Movement;
import it.nightfall.dividia.gui_api.utility.IClientDirectionManager;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * This class represent the Client Direction Manager for the 2D world.
 */
public class ClientDirectionManager2D implements IClientDirectionManager {

	private Queue<String> keyQueue;
	private boolean movementChanged;

	public ClientDirectionManager2D() {
		this.keyQueue = new LinkedList<>();
		this.movementChanged = false;
	}

	/**
	 * Manages a key action by adding or removing a key in the key queue.
	 *
	 * @param binding
	 * @param isPressed
	 */
	@Override
	public void keyAction(String binding, boolean isPressed) {
		if (isPressed) {
			if (keyQueue.isEmpty()){
				movementChanged = true;
			}
			keyQueue.add(binding);
			
		} else {
			if(!keyQueue.isEmpty()) {
				if (keyQueue.peek().equals(binding)){
					movementChanged = true;
				}
				keyQueue.remove(binding);
			}
		}
		if (movementChanged){
			updateDirection();
			movementChanged = false;
		}
		
	}

	public boolean isEmpty() {
		return keyQueue.isEmpty();
	}

	public boolean contains(Movement dir) {
		return keyQueue.contains(dir.getName());
	}
	
	public boolean isMoving (){
		return !keyQueue.isEmpty();
	}

	private void updateDirection() {
		String nextDirection = Movement.NOT_MOVING.getName();
		try {
			if (isMoving()) {
				switch (keyQueue.peek()){
					case "Up":
						nextDirection = Movement.FORWARD.getName();
						break;
					case "Down":
						nextDirection = Movement.BACKWARD.getName();
						break;
					case "Left":
						nextDirection = Movement.LEFT.getName();
						break;
					case "Right":
						nextDirection = Movement.RIGHT.getName();
						break;
				}
			}
				ClientTest.intentionManager.movePlayer(nextDirection);
			 
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void cameraIsAdjusting(boolean b) {
	}
}
