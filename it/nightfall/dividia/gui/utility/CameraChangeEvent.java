/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.utility;

import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;

/**
 *
 * @author bernardo
 */
public class CameraChangeEvent implements IGUIEvent {

    private float amount;
    
    public CameraChangeEvent(float amount) {
        this.amount = amount;
                
    }

    @Override
    public void execute() {
        SpatialsManager.getInstance().changeCameraHeight(amount);
    }
    
}
