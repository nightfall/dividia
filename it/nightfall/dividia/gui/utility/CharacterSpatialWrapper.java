
package it.nightfall.dividia.gui.utility;

import com.jme3.math.Quaternion;
import com.jme3.scene.Spatial;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CharacterSpatialWrapper {
	
	private Spatial model;
	private float offsetAngle;
	private Map <String,Float> animationDefaultSpeeds = new HashMap<>();
	private Map <String,String> animationMappings = new HashMap<>();
	private List<Integer> cycleIndices;

	public CharacterSpatialWrapper(Spatial model) {
		this.model = model;
	}

	public void setOffsetAngle(float offsetAngle) {
		this.offsetAngle = offsetAngle;
		model.setLocalRotation(new Quaternion().fromAngles(0, 0, offsetAngle));
	}
	
	public Spatial getModel() {		
		return model;
	}

	public float getOffsetAngle() {
		return offsetAngle;
	}
	
	public void putAnimationDefaultSpeed(String animation, Float speed) {
		animationDefaultSpeeds.put(animation, speed);		
	}

	public Map<String, Float> getAnimationDefaultSpeeds() {
		return animationDefaultSpeeds;
	}

	public List<Integer> getCycleIndices() {
		return cycleIndices;
	}

	public void setCycleIndices(List<Integer> cycleIndices) {
		this.cycleIndices = cycleIndices;
	}

	public void putAnimationMapping(String actionName, String animationName) {
		animationMappings.put(actionName, animationName);
	}
	
	public String getActionAnimation(String actionName) {
		return animationMappings.get(actionName);
	}
	
}
