package it.nightfall.dividia.gui.utility;

import it.nightfall.dividia.bl.utility.playerMovements.Movement;
import it.nightfall.dividia.gui_api.utility.IClientDirectionManager;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;

public class ClientDirectionManager implements IClientDirectionManager {
	private boolean forward = false;
	private boolean left = false;
	private boolean right = false;
	private boolean backward = false;
	private boolean cameraIsAdjusting = false;
	private Movement movementStatus = Movement.NOT_MOVING;

	@Override
	public void keyAction(String binding, boolean pressed){
		switch(binding){
			case "Up":
				forward = pressed;
				break;
			case "Down":
				backward = pressed;
				break;
			case "Right":
				right = pressed;
				break;
			case "Left":
				left = pressed;
				break;
		}
		if(!cameraIsAdjusting) {
			updateDirection();
		}
		if(binding.equals("ResetCamera") && !pressed) {
			try{ 
			ClientTest.intentionManager.resetFocus();
			}catch(RemoteException ex){
				//TODO gestire l'eccezione
			}
		}
	}

	private void updateDirection(){
		if((forward && backward) || (left && right)){
			movementStatus = Movement.NOT_MOVING;
		}else if(forward && left){
			movementStatus = Movement.FORWARD_LEFT;
		}else if(forward && right){
			movementStatus = Movement.FORWARD_RIGHT;
		}else if(backward && left){
			movementStatus = Movement.BACKWARD_LEFT;
		}else if(backward && right){
			movementStatus = Movement.BACKWARD_RIGHT;
		}else if(right){
			movementStatus = Movement.RIGHT;
		}else if(left){
			movementStatus = Movement.LEFT;
		}else if(forward){
			movementStatus = Movement.FORWARD;
		}else if(backward){
			movementStatus = Movement.BACKWARD;
		}else{
			movementStatus = Movement.NOT_MOVING;
		}
		try{
			ClientTest.intentionManager.movePlayer(movementStatus.getName());
		}catch(RemoteException ex){
			//TODO gestire l'eccezione
		}
	}

	@Override
	public void cameraIsAdjusting(boolean b) {
		cameraIsAdjusting = b;
	}
}
