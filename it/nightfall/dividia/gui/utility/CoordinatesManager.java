package it.nightfall.dividia.gui.utility;

import javax.vecmath.Vector2f;

public class CoordinatesManager {
	private static final int UNITS = 1000;
	public static final int TOP_LEFT = 0, TOP_RIGHT = 1, BOTTOM_LEFT = 2, BOTTOM_RIGHT = 3, CENTER = 4;
	private final float singleUnit;
	private final Vector2f fakeResolution;

	public CoordinatesManager(Vector2f screenResolution){
		singleUnit = screenResolution.x / UNITS;
		fakeResolution = new Vector2f(UNITS, screenResolution.y / singleUnit);
	}

	public float getScale(Vector2f realDimension, float widthUnits){
		return widthUnits / (realDimension.x / singleUnit);
	}

	public Vector2f getTranslation(Vector2f fakePosition){
		float tempX = ((fakePosition.x + 1) / 2) * fakeResolution.x;
		float tempY = ((fakePosition.y + 1) / 2) * fakeResolution.y;
		return new Vector2f(tempX * singleUnit, tempY * singleUnit);
	}

	public Vector2f getTranslationFromEdge(Vector2f fakePosition, Vector2f objectDimension, int edge){
		Vector2f tempTranslation = getTranslation(fakePosition);
		float tempX = tempTranslation.x;
		float tempY = tempTranslation.y;
		switch(edge){
			case TOP_LEFT:
				tempY -= objectDimension.y;
				break;
			case TOP_RIGHT:
				tempX -= objectDimension.x;
				tempY -= objectDimension.y;
				break;
			case BOTTOM_RIGHT:
				tempX -= objectDimension.x;
				break;
			case CENTER:
				tempX -= objectDimension.x / 2;
				tempY -= objectDimension.y / 2;
				break;
		}
		return new Vector2f(tempX, tempY);
	}
}
