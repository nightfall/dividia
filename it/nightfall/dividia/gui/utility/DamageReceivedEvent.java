/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.utility;

import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;

/**
 *
 * @author bernardo
 */
public class DamageReceivedEvent implements IGUIEvent {

    private int damage;
    private boolean damageToUser;
	private IDamageType damageType;
    
    public DamageReceivedEvent(int damage, boolean damageToUser, IDamageType damageType) {
        this.damage = damage;
        this.damageToUser = damageToUser;
		this.damageType = damageType;
    }

    @Override
    public void execute() {
        SpatialsManager.getInstance().showDamageText(damage, damageToUser, damageType);
    }
    
}
