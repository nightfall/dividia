package it.nightfall.dividia.gui.utility;

import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GUIEventsHandler {
	private static GUIEventsHandler instance = new GUIEventsHandler();
	private final List<IGUIEvent> pendingEvents = new LinkedList<>();

	private GUIEventsHandler(){
	}

	public static GUIEventsHandler getInstance(){
		return instance;
	}

	public synchronized void putEvent(IGUIEvent event){
		pendingEvents.add(event);
	}

	public synchronized void executePendingEvents(){
		for(IGUIEvent event : pendingEvents){
			event.execute();
		}
		pendingEvents.clear();
	}
}
