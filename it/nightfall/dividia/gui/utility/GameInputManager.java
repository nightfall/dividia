package it.nightfall.dividia.gui.utility;

import com.jme3.input.InputManager;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.gui.utility.input.EquippedItemsMapping;
import it.nightfall.dividia.gui.utility.input.InGameMapping;
import it.nightfall.dividia.gui.utility.input.InventoryMapping;
import it.nightfall.dividia.gui_api.utility.input.IKeyMapping;
import java.util.Collection;

/**
 * This class defines the inputs and their actions.
 *
 * @author Nightfall
 */
public class GameInputManager {
	private InputManager inputManager;
	private static GameInputManager instance = null;
	private String remappingAction;
	private IKeyMapping activeMapping;

	private GameInputManager(){
	}

	public static GameInputManager getInstance(){
		if(instance == null){
			instance = new GameInputManager();
		}
		return instance;
	}

	public void init(InputManager inputManager){
		this.inputManager = inputManager;
		String path = IGameConstants.CONFIG_PATH + SharedFunctions.getPath("keysMap.xml");
		IConfigReader config = new ConfigReader(path);
		Collection<IConfigReader> mappingConfigs = config.getConfigReaderList("mapping");
		for(IConfigReader mappingEntry : mappingConfigs){
			String mapName = mappingEntry.getString("name");
			int mapValue = mappingEntry.getInteger("value");
			Trigger trigger = null;
			switch(mappingEntry.getString("type")){
				case "keyboard":
					trigger = new KeyTrigger(mapValue);
					break;
				case "mouse":
					trigger = new MouseButtonTrigger(mapValue);
					break;
			}
			if(trigger != null){
				inputManager.addMapping(mapName, trigger);
			}
		}
	}

	public void setInputState(InputState state){
		inputManager.removeListener(activeMapping);
		switch(state){
			case IN_GAME:
				activeMapping = InGameMapping.getInstance();
				break;
			case EQUIPPED_ITEMS:
				activeMapping = new EquippedItemsMapping();
				break;
			case INVENTORY:
				activeMapping = new InventoryMapping();
				break;
		}
		inputManager.addListener(activeMapping, activeMapping.getMappingIds());
	}

	public void remappedKey(String type, int mapValue){
		inputManager.deleteMapping(remappingAction);
		Trigger trigger = null;
		switch(type){
			case "keyboard":
				trigger = new KeyTrigger(mapValue);
				break;
			case "mouse":
				trigger = new MouseButtonTrigger(mapValue);
				break;
		}
		if(trigger != null){
			inputManager.addMapping(type, trigger);
		}
	}

	public enum InputState {
		IN_GAME, EQUIPPED_ITEMS, INVENTORY
	}
}
