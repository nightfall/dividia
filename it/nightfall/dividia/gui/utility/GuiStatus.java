package it.nightfall.dividia.gui.utility;

public enum GuiStatus {
	WORLD_3D, WORLD_2D;
}
