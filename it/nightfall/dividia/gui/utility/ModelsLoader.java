
package it.nightfall.dividia.gui.utility;

import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;
import com.jme3.scene.Spatial;
import com.jme3.ui.Picture;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class ModelsLoader {
	
	private AssetManager assetManager;
	private Map<String,CharacterSpatialWrapper> models = new HashMap<>();
	private Map<String, Spatial> staticModels = new HashMap<>();
	private Map<String, Picture> mapIcons = new HashMap<>();
	
	static ModelsLoader instance;	
	private String modelsLocationWithinAssets = "Models/Races/";
	private String modelsLocation = "assets/"+modelsLocationWithinAssets;
	private String staticModelsLocationWithinAssets = "Models/Objects/StaticObjects";
	private String staticModelsLocation = "assets/"+staticModelsLocationWithinAssets;
	private String mapIconsLocationWithinAssets= "Textures/mapIcons";
	private String mapIconsLocation = "assets/"+mapIconsLocationWithinAssets;
	
	public static ModelsLoader getInstance() {
		if(instance == null) {
			instance = new ModelsLoader();					
		}
		return instance;		
	}

	public ModelsLoader() {			
	}
	
	public void loadModels() {
		File modelsDirectory = new File(modelsLocation);
		for(File subDirectory : modelsDirectory.listFiles()) {
			String raceId = subDirectory.getName();
			Spatial model = assetManager.loadModel(modelsLocationWithinAssets+raceId+"/"+raceId+".mesh.xml");
			CharacterSpatialWrapper spatialWrapper = new CharacterSpatialWrapper(model);
			applyConfigs(raceId, spatialWrapper);
			models.put(raceId, spatialWrapper);	
		}	
		
		File staticModelsDirectory = new File(staticModelsLocation);
		for(File subDirectory : staticModelsDirectory.listFiles()) {
			String objectId = subDirectory.getName();
			Spatial model = assetManager.loadModel(staticModelsLocationWithinAssets+"/"+objectId+"/"+objectId+".mesh.xml");
			staticModels.put(objectId, model);	
		}	
		
		File mapIconsDirectory = new File(mapIconsLocation);
		for(File iconFile : mapIconsDirectory.listFiles()) {
			if(iconFile.getName().endsWith(".png")) {
				Picture icon = new Picture(iconFile.getName());
				icon.setImage(assetManager, mapIconsLocationWithinAssets+"/"+iconFile.getName(), true);
				icon.setWidth(400);
				icon.setHeight(200);
				icon.setPosition(5, 5);
				mapIcons.put(iconFile.getName(), icon);	
			}
		}	
	}

	public CharacterSpatialWrapper getModel(String modelId) {
		return models.get(modelId);
	}
	
	public Picture getMapIcon(String mapId) {
		return  mapIcons.get(mapId+".png");
	}
	
	public Spatial getStaticModel(String modelId) {
		return staticModels.get(modelId);
	}

	public void init(AssetManager assetManager) {
		this.assetManager = assetManager;
	}

	private void applyConfigs(String raceId, CharacterSpatialWrapper spatialWrapper) {
		IConfigReader configReader = new ConfigReader(modelsLocation+"/"+raceId +"/" + raceId + ".configs.xml");
		float scaleFactor = configReader.getFloat("scaleFactor");		
		String idleCycle  = configReader.getString("idleCycle");		
		String [] stringIndices =  idleCycle.split(",");
		ArrayList<Integer>  indices = new ArrayList<>();
		for(String index:stringIndices) {
			indices.add(Integer.parseInt(index));
		}
		spatialWrapper.setCycleIndices(indices);
		spatialWrapper.getModel().setLocalScale(scaleFactor);		
		float offsetAngle = configReader.getFloat("offsetAngle");		
		offsetAngle = FastMath.DEG_TO_RAD * offsetAngle;
		
		Collection<IConfigReader> animationSpeedsReader = configReader.getConfigReader("animationSpeeds").getConfigReaderList("animationSpeed");
		for(IConfigReader singleReader : animationSpeedsReader) {
			spatialWrapper.putAnimationDefaultSpeed(singleReader.getString("name"), singleReader.getFloat("value"));
		}
		
		spatialWrapper.setOffsetAngle(offsetAngle);	
		
		
		Collection<IConfigReader> animationMappingsReaders = configReader.getConfigReader("animationMappings").getConfigReaderList("animationMapping");
		for(IConfigReader singleReader : animationMappingsReaders) {
			spatialWrapper.putAnimationMapping(singleReader.getString("actionName"), singleReader.getString("animationName"));
		}
	}

	
	
	
}
