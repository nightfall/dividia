package it.nightfall.dividia.gui.utility;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.scene.Node;
import it.nightfall.dividia.gui.controls.PauseAndDeathControl;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import java.util.HashMap;
import java.util.Map;
import javax.vecmath.Vector2f;

public class TextDisplayManager {
	private static TextDisplayManager instance = new TextDisplayManager();
	private final BitmapFont bitmapFont;
	private final Node textDisplayNode = new Node("textDisplayMainNode");
	private Map<String, PauseAndDeathControl> controls = new HashMap<>();
	private Map<String, BitmapText> spatials = new HashMap<>();

	private TextDisplayManager(){
		bitmapFont = ClientTest.getInstance().getAssetManager().loadFont("Interface/Fonts/Default.fnt");
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getGuiNode().attachChild(textDisplayNode);
			}
		});
	}

	public static TextDisplayManager getInstance(){
		return instance;
	}

	public void createHandler(final String name, final Vector2f boxPosition, final Vector2f boxDimensions, final float timeShown){
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				BitmapText text = new BitmapText(bitmapFont);
				text.setLocalTranslation(0, 0, 9999);
				text.setSize(bitmapFont.getCharSet().getRenderedSize() * 1.4f);
				float tempY = boxPosition.y + boxDimensions.y + (text.getLineHeight() / 2f);
				text.setBox(new Rectangle(boxPosition.x, tempY, boxDimensions.x, boxDimensions.y));
				text.setAlignment(BitmapFont.Align.Center);
				text.setVerticalAlignment(BitmapFont.VAlign.Center);
				text.setAlignment(BitmapFont.Align.Center);
				PauseAndDeathControl tempControl = new PauseAndDeathControl(timeShown);
				controls.put(name, tempControl);
				spatials.put(name, text);
				text.addControl(tempControl);
			}
		});
	}

	public void updateText(String handler, final String text){
		final PauseAndDeathControl tempControl = controls.get(handler);
		final BitmapText tempText = spatials.get(handler);
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				if(tempControl.isPauseEnded()){
					textDisplayNode.attachChild(tempText);
				}
				tempControl.resetPause();
				tempText.setText(text);
			}
		});
	}

	public void hideText(String handler){
		final BitmapText tempText = spatials.get(handler);
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				tempText.setText("");
			}
		});
	}
}
