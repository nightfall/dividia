package it.nightfall.dividia.gui.utility.input;

import it.nightfall.dividia.gui_api.utility.input.IKeyMapping;
import it.nightfall.dividia.testing.screencontrollers.EquipmentItemsController;

public class EquippedItemsMapping implements IKeyMapping {
	@Override
	public String[] getMappingIds(){
		return new String[]{"Exit"};
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf){
		if(isPressed){
			return;
		}
		switch(name){
			case "Exit":
				EquipmentItemsController.getInstance().closeEquipment();
				break;
		}
	}
}
