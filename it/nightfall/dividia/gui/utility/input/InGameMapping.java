package it.nightfall.dividia.gui.utility.input;

import it.nightfall.dividia.gui.spellsAndEffects.EffectsManager;
import it.nightfall.dividia.gui.two_dimensional_gui.utility.ClientDirectionManager2D;
import it.nightfall.dividia.gui.utility.CameraChangeEvent;
import it.nightfall.dividia.gui.utility.ClientDirectionManager;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.GuiStatus;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IClientDirectionManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.gui_api.utility.input.IKeyMapping;
import it.nightfall.dividia.testing.ClientTest;
import it.nightfall.dividia.testing.screencontrollers.EquipmentItemsController;
import it.nightfall.dividia.testing.screencontrollers.InventoryController;
import it.nightfall.dividia.testing.screencontrollers.OptionsScreenController;
import java.rmi.RemoteException;
import java.util.HashSet;

public class InGameMapping implements IKeyMapping {
	private static InGameMapping instance;
	private IClientDirectionManager clientDirectionManager;
	private GuiStatus guiStatus = null;
	private static HashSet<String> movementStrings = movementStringInit();
	public boolean cameraIsAdjusting = false;

	public static InGameMapping getInstance(){
		if(instance == null){
			instance = new InGameMapping();
		}
		return instance;
	}

	public void init(){
		if(guiStatus == null){
			this.guiStatus = GuiStatus.WORLD_3D;
		}
		clientDirectionManager = new ClientDirectionManager();
	}

	private static HashSet<String> movementStringInit(){
		HashSet<String> resultSet = new HashSet<>();
		resultSet.add("Up");
		resultSet.add("Down");
		resultSet.add("Left");
		resultSet.add("Right");
		return resultSet;
	}

	public synchronized GuiStatus getGuiStatus(){
		return guiStatus;
	}

	public synchronized void setGuiStatus(GuiStatus guiStatus){
		this.guiStatus = guiStatus;
	}

	public void switchClientDirectionManager(){
		if(guiStatus == GuiStatus.WORLD_3D){
			clientDirectionManager = new ClientDirectionManager();
		}else{
			clientDirectionManager = new ClientDirectionManager2D();
		}
	}

	public void cameraIsAdjusting(boolean b){
		cameraIsAdjusting = b;
		clientDirectionManager.cameraIsAdjusting(true);
	}

	@Override
	public String[] getMappingIds(){
		return new String[]{
			"Left", "Right", "Down", "Up", "ResetCamera", "Interaction", "StopEvents", "ResumeEvents",
			"BasicAttack", "Spell1", "Spell2", "Spell3", "Spell4", "Exit", "EquippedItems", "Inventory", "CameraUp", "CameraDown", "DebugSwitch"
		};
	}

	@Override
	public void onAction(String binding, boolean isPressed, float tpf){
		try{
			if(movementStrings.contains(binding)){
				clientDirectionManager.keyAction(binding, isPressed);
			}
			if(binding.equals("ResetCamera")){
				clientDirectionManager.keyAction(binding, isPressed);
			}
			if(isPressed){//TODO fix || !canCast(name)){
				return;
			}
			switch(binding){
				case "Interaction":
					ClientTest.intentionManager.interact();
					break;
				case "StopEvents":
					ClientTest.intentionManager.pauseEvents();
					break;
				case "ResumeEvents":
					ClientTest.intentionManager.resumeEvents();
					break;
				case "BasicAttack":
					ClientTest.intentionManager.basicAttack();
					break;
				case "Exit":
					OptionsScreenController.getInstance().showOptions();
					break;
				case "Spell1":
					EffectsManager.getInstance().executeSpell(1);
					break;
				case "Spell2":
					EffectsManager.getInstance().executeSpell(2);
					break;
				case "Spell3":
					EffectsManager.getInstance().executeSpell(3);
					break;
				case "Spell4":
					EffectsManager.getInstance().executeSpell(4);
					break;
				case "EquippedItems":
					EquipmentItemsController.getInstance().openEquipment();
					break;
				case "Inventory":
					InventoryController.getInstance().openInventory();
					break;
				case "CameraUp":
					GUIEventsHandler.getInstance().putEvent(new CameraChangeEvent(+1));
					break;
				case "CameraDown":
					GUIEventsHandler.getInstance().putEvent(new CameraChangeEvent(-1));
					break;
				case "DebugSwitch":
					GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
						@Override
						public void execute(){
							SpatialsManager.getInstance().debugSwitched();
						}
					});
			}
		}catch(RemoteException ex){
			ex.printStackTrace();
		}
	}
}
