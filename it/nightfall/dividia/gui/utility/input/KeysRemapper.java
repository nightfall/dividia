package it.nightfall.dividia.gui.utility.input;

import com.jme3.input.InputManager;
import com.jme3.input.RawInputListener;
import com.jme3.input.event.JoyAxisEvent;
import com.jme3.input.event.JoyButtonEvent;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.input.event.TouchEvent;
import it.nightfall.dividia.gui.utility.GameInputManager;

public class KeysRemapper implements RawInputListener{
	private static KeysRemapper instance;
	private InputManager inputManager;
	private GameInputManager gameInputManager;
	
	public static KeysRemapper getInstance(){
		return instance;
	}
	
	public static void init(InputManager inputManager, GameInputManager gameInputManager){
		instance = new KeysRemapper(inputManager, gameInputManager);
	}
	
	private KeysRemapper(InputManager inputManager, GameInputManager gameInputManager){
		this.inputManager = inputManager;
		this.gameInputManager = gameInputManager;
	}

	
	public void getNextInput(){
		inputManager.addRawInputListener(this);
	}
	
	private void endInputListening(){
		inputManager.removeRawInputListener(this);
	}
	
	@Override
	public void beginInput(){
		
	}

	@Override
	public void endInput(){
		
	}

	@Override
	public void onJoyAxisEvent(JoyAxisEvent evt){
		
	}

	@Override
	public void onJoyButtonEvent(JoyButtonEvent evt){
		
	}

	@Override
	public void onMouseMotionEvent(MouseMotionEvent evt){
		
	}

	@Override
	public void onMouseButtonEvent(MouseButtonEvent evt){
		if(evt.isReleased()){
			gameInputManager.remappedKey("mouse", evt.getButtonIndex());
		}
		evt.setConsumed();
	}

	@Override
	public void onKeyEvent(KeyInputEvent evt){
		if(evt.isReleased()){
			gameInputManager.remappedKey("keyboard", evt.getKeyCode());
		}
		evt.setConsumed();
	}

	@Override
	public void onTouchEvent(TouchEvent evt){
		
	}
}
