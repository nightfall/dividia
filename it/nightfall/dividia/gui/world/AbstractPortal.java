package it.nightfall.dividia.gui.world;

import com.jme3.asset.AssetManager;

import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
/*
 * 
 */
public abstract class AbstractPortal extends Node {

    protected ParticleEmitter particles;
    protected Geometry portalGeometry;
    protected AudioNode portalAudio;
    
    public AbstractPortal (String name, Mesh portalShape,AudioNode audio,float radius, AssetManager assetManager,String path){
    	super(name + " Node");
    	this.portalGeometry = new Geometry(name, portalShape);
    	this.portalAudio = audio;
    	/*
    	audio.setLooping(true);
    	audio.play();
    	audio.setPositional(true);
    	audio.setLocalTranslation(this.getLocalTranslation());
    	this.attachChild(audio);
    	*/
		
		IConfigReader conf = new ConfigReader(SharedFunctions.getPath("config|maps|"+path+".xml"));
		//rotation
		IConfigReader supportReader = conf.getConfigReader("rotation");
		this.rotate(supportReader.getFloat("x") * FastMath.DEG_TO_RAD, supportReader.getFloat("y"), supportReader.getFloat("z"));
		//location
		this.setLocalTranslation(0, 0, IGameConstants.DISTANCE_TO_FLOOR);
		//Particle Emitter init
		initParticles(conf,assetManager,radius);
		//portal material
		Material unshadedMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		unshadedMaterial.setTexture("ColorMap", assetManager.loadTexture(conf.getString("texture")));
		unshadedMaterial.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
		portalGeometry.setMaterial(unshadedMaterial);
		portalGeometry.setQueueBucket(RenderQueue.Bucket.Transparent);
		//attach nodes
		this.attachChild(portalGeometry);
		this.attachChild(particles);
    }
	
	private void initParticles(IConfigReader conf, AssetManager assetManager,float radius){
		IConfigReader supportReader;
		
		particles = new ParticleEmitter("Lights", ParticleMesh.Type.Triangle, conf.getInteger("numberOfParticle"));
		
		//particles material
		Material particleMaterial = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
		particleMaterial.setTexture("Texture", assetManager.loadTexture(conf.getString("particleTexture")));
		particles.setMaterial(particleMaterial);
		//particles color
		supportReader = conf.getConfigReader("startColor");
		particles.setEndColor(new ColorRGBA(supportReader.getFloat("r"),supportReader.getFloat("g"),
				supportReader.getFloat("b"),supportReader.getFloat("a")));
		supportReader = conf.getConfigReader("endColor");
		particles.setStartColor(new ColorRGBA(supportReader.getFloat("r"),supportReader.getFloat("g"),
				supportReader.getFloat("b"),supportReader.getFloat("a")));
		particles.getStartColor().r = 0;
		//particles settings
		particles.setHighLife(conf.getFloat("life"));
		particles.setLowLife(conf.getFloat("life"));
		particles.setStartSize(conf.getFloat("startSize"));
		particles.setEndSize(conf.getFloat("endSize"));
		particles.setImagesX(conf.getInteger("imagesX"));
		particles.setImagesY(conf.getInteger("imagesX"));
		particles.setParticlesPerSec(conf.getInteger("pps"));
		supportReader = conf.getConfigReader("gravity");
		particles.setGravity(supportReader.getFloat("x"),supportReader.getFloat("y"),supportReader.getFloat("z"));

	}
    
    public void attachToNode(Node node){
    	node.attachChild(this);
    }
    
    public void detachFromNode (Node node){
    	node.detachChild(this);
    }
    
}

