/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.world;

/**
 *
 * @author Andrea
 */
public class LoadingState {
	private LoadingSteps step;
	private float loadedValue = 0;
	
	public LoadingState(){
		step = LoadingSteps.NONE;
	}
	
	public synchronized void setStep (LoadingSteps step){
		loadedValue += this.step.getWeight();
		this.step = step;
	}
	
	public float getLoadedQuantity(){
		return loadedValue;
	}
	
	public String getCurrentLoadingString(){
		return step.getName();
	}
	
	public void reset(){
		this.step = LoadingSteps.NONE;
		this.loadedValue = 0;
	}
	
	public void resetToValue (float def){
		this.step = LoadingSteps.NONE;
		this.loadedValue = def;
	}
	
}
