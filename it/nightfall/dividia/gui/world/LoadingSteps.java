/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.world;

/**
 *
 * @author Andrea
 */
public enum LoadingSteps {
	PREPARING("preparing scene",0f),PORTALS("loading portals",0.1f),NPCS("loading npcs",0.2f),
	MODELS("loading models",0.5f), SCENE("loading scene",0.2f), SUMMARY("waiting for summary",0f),
	COMPLETED("ready",0f),NONE("",0f);
	
	private String name;
	private float relativeWeight;
	
	private LoadingSteps(String string, float w){
		this.name = string;
		this.relativeWeight = w;
	}
	
	public String getName(){
		return name;
	}
	
	public float getWeight(){
		return relativeWeight;
	}
}
