/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.world;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.shape.Box;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.gui.controls.Portal2DControl;

/**
 *
 * @author Andrea
 */
public class Portal2D extends AbstractPortal{
	
	
	public Portal2D (String name,AssetManager assetManager, float radius, IGamePoint position ){
		super(name, new Box(radius,radius, 0),new AudioNode(),radius, assetManager,"portal_specification2D");
		this.setLocalTranslation((float)position.getX(), (float)position.getY(),0);
		portalGeometry.addControl(new Portal2DControl(radius,radius,particles));
	}
}
