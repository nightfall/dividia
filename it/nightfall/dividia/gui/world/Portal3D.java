package it.nightfall.dividia.gui.world;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.math.Vector3f;
import com.jme3.scene.shape.Box;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.gui.controls.PortalControl;

/**
 *
 * @author Andrea
 */
public class Portal3D extends AbstractPortal {
	public Portal3D(String name, AssetManager assetManager, float radius, IGamePoint position){
		super(name, new Box(radius,0,radius), new AudioNode(),radius,assetManager,"portal_specification");//assetManager,"Assets/Sound/World/portal.ogg",false));
		particles.setShape(new EmitterSphereShape(Vector3f.ZERO, radius));
		this.setLocalTranslation((float)position.getX(), (float)position.getY(),0);
		this.addControl(new PortalControl(radius, radius));
	}
}
