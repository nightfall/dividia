package it.nightfall.dividia.gui.world;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture;
import com.jme3.ui.Picture;
import com.jme3.util.SkyFactory;
import it.nightfall.dividia.api.battle.IDamageType;
import it.nightfall.dividia.api.characters.definitions.IRaceDefinition;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.api.utility.ISummary;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.api.world.collisions.IMapSector;
import it.nightfall.dividia.api.world.definitions.IGameMapDefinition;
import it.nightfall.dividia.bl.battle.DamageType;
import it.nightfall.dividia.bl.utility.Direction;
import it.nightfall.dividia.bl.utility.GamePoint;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.bl.utility.playerMovements.FocusStatus;
import it.nightfall.dividia.bl.world.Portal;
import it.nightfall.dividia.gui.controls.AnimationControl3D;
import it.nightfall.dividia.gui.controls.BagDropControl;
import it.nightfall.dividia.gui.controls.CollisionShapeControl;
import it.nightfall.dividia.gui.controls.DamageTextControl;
import it.nightfall.dividia.gui.controls.FocusControl;
import it.nightfall.dividia.gui.controls.LifebarControl;
import it.nightfall.dividia.gui.controls.MovingAndRotatingSpatialControl3D;
import it.nightfall.dividia.gui.controls.PauseAndDeathControl;
import it.nightfall.dividia.gui.controls.SpatialDeathControl;
import it.nightfall.dividia.gui.sounds.SoundsManager;
import it.nightfall.dividia.gui.two_dimensional_gui.animations.AnimationData;
import it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.characters.FourDirectionTextureSet;
import it.nightfall.dividia.gui.two_dimensional_gui.moving_objects.characters.PlayingCharacter2D;
import it.nightfall.dividia.gui.two_dimensional_gui.controls.MovingObjectAnimationControl2D;
import it.nightfall.dividia.gui.two_dimensional_gui.controls.CharacterMovementControl2D;
import it.nightfall.dividia.gui.utility.CharacterSpatialWrapper;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.GuiStatus;
import it.nightfall.dividia.gui.utility.ModelsLoader;
import it.nightfall.dividia.gui.utility.input.InGameMapping;
import it.nightfall.dividia.gui.world.collisions.AbstractJMEGraphicShape;
import it.nightfall.dividia.gui_api.controls.ICharacterControl3D;
import it.nightfall.dividia.gui_api.controls.ISpatialMovementControl;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import it.nightfall.dividia.testing.ClientTest;
import it.nightfall.dividia.testing.appstates.InGameAppState;
import it.nightfall.dividia.testing.appstates.LoadingAppState;
import java.io.File;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SpatialsManager {
	private Map<String, Spatial> characters = new HashMap<>();
	private BlockingQueue <String> toDelete = new LinkedBlockingQueue<>();
	private BlockingQueue <Spatial> toAdd = new LinkedBlockingQueue<>();
	private Spatial clientFocus;
	private AssetManager assetManager;
	private InputManager inputManager;
	private Node rootNode;
	private Node guiNode;
	private BitmapFont guiFont;
	private Camera appCamera;
	private Camera camera3D;
	private Camera camera2D;
	private static SpatialsManager instance;
	private Lock summaryLock = new ReentrantLock();
	private ISummary summaryToLoad = null;
	private boolean finishedLoadScene = false;
	private String currMapName;
	private LoadingAppState currentLoadingState = LoadingAppState.getInstance();
    private float defaultTextSpeed = 8000;
	private final LoadingState loadingState = new LoadingState();
	private boolean gameStarted = false;
	private boolean debugMode = false;
	private Node graphicCollisionTree = new Node("collision rootNode");
	private Map<String,AbstractJMEGraphicShape> collisionShapes = new HashMap<>();
	
	public static SpatialsManager getInstance() {
		if(instance==null) {
			instance = new SpatialsManager();
		}
		return instance;
	}
    
	
	public void SpatialsManagerInit(AssetManager assetManager, Node rootNode, Camera appCamera, InputManager inputManager, Node guiNode, BitmapFont guiFont){
		loadingState.setStep(LoadingSteps.PREPARING);
		ModelsLoader.getInstance().init(assetManager);
		//start the loading scene
		ClientTest.getInstance().attachAppState(currentLoadingState);
		//init spatial manager
		instance.assetManager = assetManager;
		instance.rootNode = rootNode;
		instance.appCamera = appCamera;
		instance.camera3D = appCamera.clone();
		instance.camera2D = appCamera.clone();
		camera2D.setParallelProjection(true);
		float aspect = (float) appCamera.getWidth() / appCamera.getHeight();
		float size   = 3f;
		camera2D.setFrustum(-1000, 1000, -aspect * size, aspect * size, size , -size);
		instance.inputManager = inputManager;
		instance.guiNode = guiNode;
		instance.guiFont = guiFont;
		
	}

	public void characterMoved(String id, double x, double y, double direction){
		if(characters.containsKey(id)){
			if(InGameMapping.getInstance().getGuiStatus() == GuiStatus.WORLD_3D){
				characters.get(id).getControl(ICharacterControl3D.class).characterMoved(x, y, direction);
			}else{
				characters.get(id).getControl(ISpatialMovementControl.class).characterMoved(x, y);
			}
		}
	}

	public void focusMoved(String id, double x, double y, String focusStatus){
		if(InGameMapping.getInstance().getGuiStatus() == GuiStatus.WORLD_3D){
			clientFocus.getControl(FocusControl.class).focusMoved(x, y, focusStatus);
		}
	}

	public void characterStopped(String id){
		if(characters.containsKey(id)){
			if(InGameMapping.getInstance().getGuiStatus() == GuiStatus.WORLD_3D){
				characters.get(id).getControl(ICharacterControl3D.class).characterStopped();
			}else{
				characters.get(id).getControl(ISpatialMovementControl.class).characterStopped();
			}
		}
	}

	public void removePlayingCharacterSpatial(String playingCharacter){
		characters.remove(playingCharacter);
		//TODO handle exception in a good way
		try {
			//if we are deleting our character and we are in 3D, detach its camera node
			if (playingCharacter.equals(ClientTest.login) && InGameMapping.getInstance().getGuiStatus() == GuiStatus.WORLD_3D){
				toDelete.put((playingCharacter + " focus"));
			}
			toDelete.put(playingCharacter);
		} catch (InterruptedException ex) {
			Logger.getLogger(SpatialsManager.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void addFocusNode(String player, double x, double y, int characterSpeed, String focusStatus, Spatial geom, double direction){
		clientFocus = new Node(player + " focus");
		Vector3f playerLocalTranslation = new Vector3f((float) x, (float) y, 0);
		IGamePoint distancePoint = new GamePoint(-IGameConstants.PLAYER_TO_FOCUS_DISTANCE_ON_SIMPLE_ROTATION, 0);
		distancePoint.rotatePoint(new Direction(direction));
		playerLocalTranslation.addLocal((float) distancePoint.getX(),(float) distancePoint.getY(), 0);
		clientFocus.setLocalTranslation(playerLocalTranslation);
		clientFocus.addControl(new FocusControl(characterSpeed, FocusStatus.valueOf(focusStatus), appCamera, geom));
		toAdd.add(clientFocus);	
		
	}

	public void addMovingCharacterSpatial(String playingCharacter, double x, double y, double direction, int characterSpeed, int attackSpeed, boolean spatialWithCameraControl, String race){
		CharacterSpatialWrapper spatialWrapper = ModelsLoader.getInstance().getModel(race);
		Spatial model = spatialWrapper.getModel().clone();
		model.setName(playingCharacter);
		model.setShadowMode(RenderQueue.ShadowMode.Cast);
		model.rotate(new Quaternion().fromAngles(0, 0, (float) direction));
		model.setLocalTranslation(new Vector3f((float) x, (float) y, 0));
		model.addControl(new MovingAndRotatingSpatialControl3D(characterSpeed, direction, 0));
		
		if(spatialWithCameraControl){
			addFocusNode(playingCharacter, x, y, characterSpeed, FocusStatus.SIMPLE_ROTATION.name(), model, direction);
		}		
		model.getControl(AnimationControl3D.class).setIdleCycle(spatialWrapper.getCycleIndices());
		model.getControl(AnimationControl3D.class).setAnimationSpeeds(spatialWrapper.getAnimationDefaultSpeeds());
		model.getControl(AnimationControl3D.class).setAttackSpeed(attackSpeed);
		try {
			toAdd.put(model);
		} catch (InterruptedException ex) {
			Logger.getLogger(SpatialsManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		//Collision shape
		final IGraphicShape shape = getGraphicCollisionShape(race, x, y);
		((AbstractJMEGraphicShape) shape).getSpatial().addControl(new CollisionShapeControl(model));
		//green
		shape.setColor(new Vector4f(0, 1, 0, 0.5f));
		//add shape to collision shape map
		collisionShapes.put(playingCharacter, ((AbstractJMEGraphicShape)shape));
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {

			@Override
			public void execute() {
				graphicCollisionTree.attachChild((((AbstractJMEGraphicShape) shape).getSpatial()));
			}
		});
		
		characters.put(playingCharacter, model);
		
	}
	/*
	 * 2D TEST ZONE
	 */

	public void addMovingCharacterSpatial2D(String playerID, double x, double y, int characterSpeed,String race,  String username){
		//Load player
		PlayingCharacter2D player = new PlayingCharacter2D(new ConfigReader(SharedFunctions.getPath("config|character2D|riku.xml")));
		player.setName(playerID);
		//Collision shape
		final IGraphicShape characterCollisionShape = getGraphicCollisionShape(race,x,y);
		//green
		characterCollisionShape.setColor(new Vector4f(0,1,0,0.5f));
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {

			@Override
			public void execute() {
				graphicCollisionTree.attachChild(((AbstractJMEGraphicShape)characterCollisionShape).getSpatial());
			}
		});
		//add to collision shapes
		collisionShapes.put(playerID, ((AbstractJMEGraphicShape)characterCollisionShape));
		//Controls
		CharacterMovementControl2D characterMovementControl2D = (new CharacterMovementControl2D(characterSpeed, appCamera, characterCollisionShape, username.equals(playerID)));
		player.addControl(characterMovementControl2D);
		player.addControl(new MovingObjectAnimationControl2D(player.getAnimations()));
		//player.addControl(new MovevingObjectAnimationControl2D(initAnimations()));
		player.setLocalTranslation(new Vector3f((float) x, (float) y, 1));
		//re-check if we are in the correct graphic engine
		if(playerID.equals(username)) {
			appCamera.setLocation(new Vector3f((float)x,(float)y,5));
			appCamera.lookAt(new Vector3f((float)x,(float)y,0), Vector3f.UNIT_Y);
		}
		
		//TEST
		
		characters.put(playerID, player);
		toAdd.add(player);
	}

	private FourDirectionTextureSet initTexture(){
		IConfigReader textureConfigReader = new ConfigReader("config/character_idle_textures/riku.xml");
		FourDirectionTextureSet textures = new FourDirectionTextureSet(textureConfigReader, assetManager);
		return textures;
	}

	private Map<String, AnimationData> initAnimations(AnimationData animationData){
		Map<String, AnimationData> animations = new HashMap<>();
		IConfigReader animationConfigReader = new ConfigReader(SharedFunctions.getPath("config|animations|riku.xml"));
		Collection<IConfigReader> animConfigReaders = animationConfigReader.getConfigReaderList("animation");
		System.out.println(animConfigReaders.size());
	/*
		for(String ){
			animations.put(tempConfigReader.getString("name"), new AnimationData(tempConfigReader, assetManager));
		}*/
		return animations;
	}

	private void focusToGeometryTestFunction(String player){
		Box b = new Box(Vector3f.ZERO, 0.2f, 0.2f, 0.2f);
		clientFocus = new Geometry(player, b);
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", ColorRGBA.Red);
		clientFocus.setMaterial(mat);
		toAdd.add(clientFocus);
	}

	/**
	 * Loads the next map scene.
	 * @param nextMap 
	 */
	public void loadMapScene(String nextMap){
		//LifebarControl.getLifebarControl().clearEntities();
		//If the game is already started the (items,players,monsters) models are already in memory
		//so start from 0.5 and begin loading scene
		currMapName = nextMap;
		if (gameStarted){
			//TODO delete this dummy debug line
			System.out.println("Game is started. I'm loading "+ nextMap);
			loadingState.setStep(LoadingSteps.PREPARING);
			loadingState.resetToValue(0.5f);
			ClientTest.getInstance().attachAppState(currentLoadingState);
			InGameAppState.getInstance().setRemoveScene(true);
			characters.clear();
		}
		//if the game is not started, just continue
		currMapName = nextMap;
		//TODO read from config reader if a map is 2D or not (or send it in the function)
		
		//load scene
		boolean isMap2D = GameStorage.getInstance().getMapDefinition(nextMap).is2D();
		loadingState.setStep(LoadingSteps.SCENE);
		toAdd.add(loadMap(nextMap, isMap2D));
		
		//load portals
		loadingState.setStep(LoadingSteps.PORTALS);
		loadPortals(nextMap);
		
		//load npcs
		loadingState.setStep(LoadingSteps.NPCS);
		//loadNPCs(nextMap);
		
		//load/wait summary
		loadingState.setStep(LoadingSteps.SUMMARY);
		
		summaryLock.lock();
			if(summaryToLoad != null){
				loadSummary();
				summaryToLoad = null;
			}else{
				finishedLoadScene = true;
			}
		summaryLock.unlock();
	}
	
	public void setSummary(ISummary mapSummary){
		summaryLock.lock();
			summaryToLoad = mapSummary;
			if (finishedLoadScene){
				loadSummary();
				summaryToLoad = null;
				finishedLoadScene  = false;
			}
		summaryLock.unlock();
	}

	public void loadSummary(){
		final ISummary items    = summaryToLoad.getSummary("items");
		final ISummary players  = summaryToLoad.getSummary("players");
		final ISummary monsters = summaryToLoad.getSummary("monsters");
		final ISummary npcs = summaryToLoad.getSummary("npcs");
		
		//load items
		loadItems(items);
		//load players
		loadPlayers(players);
		//load monsters
		loadMonsters(monsters);
		//load nonPlayingCharacters
		loadNpcs(npcs);
		//TODO handle in a meaningful way
		ClientTest.getInstance().detachAppState(currentLoadingState);
		try {
			
			ClientTest.intentionManager.loadingMapCompleted(currMapName);
		} catch (RemoteException ex) {
			Logger.getLogger(SpatialsManager.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public Node getRootNode() {
		return rootNode;
	}

	
	public AssetManager getAssetManager() {
		return assetManager;
	}

	public void showDamageText(int damage, boolean damageReceived, IDamageType damageType) {
		guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
		BitmapText damageText = new BitmapText(guiFont,false);
		damageText.setSize(guiFont.getCharSet().getRenderedSize()*2f);
        damageText.setText(""+damage);
		damageText.setLocalTranslation(appCamera.getWidth()/2-20, damageText.getLineHeight()+ appCamera.getHeight()/2 + 100, 0);
		if(damageReceived) {
			damageText.setColor(ColorRGBA.Red);
		}
		else {
			if(damageType.equals(DamageType.MAGICAL)) {
				damageText.setColor(ColorRGBA.Blue);			
			}
			else if(damageType.equals(DamageType.PHYSICAL)) {
				damageText.setColor(ColorRGBA.Orange);			
			}
			else {
				damageText.setColor(ColorRGBA.White);			
			}
			
		}
		damageText.addControl(new DamageTextControl(defaultTextSpeed));
		guiNode.attachChild(damageText);
		
	}

	public void focusReset(double x, double y) {
		if(InGameMapping.getInstance().getGuiStatus() == GuiStatus.WORLD_3D){
			clientFocus.getControl(FocusControl.class).focusReset(x,y);
		}
	}

	public void focusSpeedChanged(int newValue) {
		clientFocus.getControl(FocusControl.class).setSpatialSpeed(newValue);
	}
	
	public void characterSpeedChanged(String playingCharacter, int newValue) {
		if(InGameMapping.getInstance().getGuiStatus().equals(GuiStatus.WORLD_3D)) {
			characters.get(playingCharacter).getControl(ICharacterControl3D.class).setSpeed(newValue);
		} else {
			characters.get(playingCharacter).getControl(ISpatialMovementControl.class).setSpeed(newValue);
		}
	}

	public void startCharacterSpellAnimation(String spellName, String playerId, String raceId) {
		String relatedAnimationName = ModelsLoader.getInstance().getModel(raceId).getActionAnimation(spellName);
		if(characters.get(playerId).getControl(AnimationControl3D.class)!=null) {
			characters.get(playerId).getControl(AnimationControl3D.class).attackRequest(relatedAnimationName);		
		}
	}

	public void performBasicAttack(String executor) {
		characters.get(executor).getControl(AnimationControl3D.class).attackRequest("basicAttack");
	}
	
	public void performDeath(String executor) {
		characters.get(executor).getControl(AnimationControl3D.class).deathAnimationRequest("death");
		characters.get(executor).addControl(new SpatialDeathControl(7));
	}

	public void characterAttackSpeedChanged(String playingCharacter, int newValue) {
		if(InGameMapping.getInstance().getGuiStatus().equals(GuiStatus.WORLD_3D)) {
			characters.get(playingCharacter).getControl(AnimationControl3D.class).setAttackSpeed(newValue);
		}
	}
	
	private void loadPortals(String nextMap) {
		IGameMapDefinition mapDef = GameStorage.getInstance().getMapDefinition(nextMap);
		for (Map.Entry<String,Portal> currentPortalEntry : mapDef.getPortals().entrySet()){
			AbstractPortal portal;
			if(InGameMapping.getInstance().getGuiStatus().equals(GuiStatus.WORLD_3D)){
				portal = new Portal3D(currentPortalEntry.getKey(), assetManager, currentPortalEntry.getValue().getRadius(), 
												currentPortalEntry.getValue().getPortalLocation());
			}else{
				portal = new Portal2D(currentPortalEntry.getKey(), assetManager, currentPortalEntry.getValue().getRadius(), 
												currentPortalEntry.getValue().getPortalLocation());
			}
			toAdd.add(portal);
		}
	}

	
	public void loadSingleCharacter(ISummary singleSumary) {
		String name = singleSumary.getString("id");
		double positionX = singleSumary.getDouble("positionX");
		double positionY = singleSumary.getDouble("positionY");
		double direction = singleSumary.getDouble("direction");
		int attackSpeed = singleSumary.getInteger("attackSpeed");
		int movementSpeed = singleSumary.getInteger("movementSpeed");
		String race = singleSumary.getString("race");
		
		GuiStatus guiStatus = InGameMapping.getInstance().getGuiStatus();
		if(guiStatus.equals(GuiStatus.WORLD_3D)) {
			addMovingCharacterSpatial(name, positionX, positionY, direction, movementSpeed, attackSpeed, false, race);
		}
		else {
			addMovingCharacterSpatial2D(name, positionX, positionY, movementSpeed, race, "");
		}
		
		SoundsManager.getInstance().addCharactersSoundsManagers(name, race, false);
		LifebarControl.getLifebarControl().addEntity(singleSumary.getString("id"), true);
		LifebarControl.getLifebarControl().updateEntityPosition(singleSumary.getString("id"), (float) positionX, (float) positionY);
	}

	private void loadMonsters(ISummary monstersSummary) {
		if(monstersSummary == null) {
			return;
		}
		Map<String, Serializable> internalMap = monstersSummary.getInternalMap();
		for(String key:internalMap.keySet()) {
			loadSingleCharacter(monstersSummary.getSummary(key));
		}
	}

	private void loadPlayers(ISummary playersSummary) {
		if(playersSummary == null) {
			return;
		}
		Map<String, Serializable> internalMap = playersSummary.getInternalMap();
		for(String key:internalMap.keySet()) {
			loadSingleCharacter(playersSummary.getSummary(key));
		}
	}

	private void loadItems(ISummary itemsSummary) {
	}
	
	/**
	 * Clean the scene by deleting nodes to be discarded from the scene.
	 */
	public void cleanup(){
		for (String currentString : toDelete){
			rootNode.detachChildNamed(currentString);
			toDelete.remove(currentString);
		}
	}

	public void updateScene() {
		for (Spatial currentSpatial : toAdd){
			rootNode.attachChild(currentSpatial);
			toAdd.remove(currentSpatial);
		}
	}

	public boolean isThereSomethingToDelete() {
		return toDelete.size() != 0;
	}

	public boolean isThereSomethingToAdd() {
		return toAdd.size() != 0;
	}
	
	public LoadingState getLoadingState(){
		synchronized (loadingState){
			return loadingState;
		}
	}
	
	private Spatial loadMap(String nextMap, boolean is2D){
		Spatial map = null;
		Node result = new Node();
		graphicCollisionTree = new Node("collision rootNode");
		if (is2D){
			appCamera.copyFrom(camera2D);
			Material unshaded = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
			unshaded.setTexture("ColorMap", assetManager.loadTexture(SharedFunctions.getPath("Textures|maps|" + nextMap + ".png")));
			int height = unshaded.getTextureParam("ColorMap").getTextureValue().getImage().getHeight();
			int width = unshaded.getTextureParam("ColorMap").getTextureValue().getImage().getWidth();
			map = new Geometry(nextMap, new Quad((float)SharedFunctions.getJMESizeFromPixel(width),(float)SharedFunctions.getJMESizeFromPixel(height)));
			//set the 0,0 in the top-left corner of the image
			//result.setLocalTranslation(0, -15, 0);
			//TEST 
			drawMapSectorsAndShapes(nextMap);
			map.setMaterial(unshaded);
			if (InGameMapping.getInstance().getGuiStatus() != GuiStatus.WORLD_2D){
				//TODO delete : dummy debug line
				System.out.println("Switching from 3D to 2D");
				InGameMapping.getInstance().setGuiStatus(GuiStatus.WORLD_2D);
				InGameMapping.getInstance().switchClientDirectionManager();
			}
		}else{
			appCamera.copyFrom(camera3D);
			map = assetManager.loadModel(SharedFunctions.getPath("Scenes|" + nextMap + "|" + nextMap +".j3o"));
			map.setShadowMode(RenderQueue.ShadowMode.Receive);
			//*** TEMPORAL ***//
			IGameMapDefinition mapDefinition = GameStorage.getInstance().getMapDefinition(nextMap);
			double height = mapDefinition.getHeight();
			double width = mapDefinition.getWidth();
			map.rotate(FastMath.HALF_PI,0,0);
			map.setLocalTranslation((float)height/2, (float)width/2, -0.1f);
			//*** END TEMPORAL ***//
			if (InGameMapping.getInstance().getGuiStatus() != GuiStatus.WORLD_3D){
				InGameMapping.getInstance().setGuiStatus(GuiStatus.WORLD_3D);
				InGameMapping.getInstance().switchClientDirectionManager();
			}
		}
		File skyboxDirectory = new File (SharedFunctions.getPath("assets|Textures|skybox"));
		File[] skyboxes = skyboxDirectory.listFiles();
		File selectedSkybox = null;
		
		for (int i=0 ; i < skyboxes.length;i++){
			String fileName = skyboxes[i].getName();
			int pos = fileName.lastIndexOf(".");
			if(pos > 0){
				System.out.println(nextMap + " " + fileName.substring(0,pos));
				if (nextMap.equals(fileName.substring(0,pos))){
					selectedSkybox = skyboxes[i];
					break;
				}
			}
		}
		if (selectedSkybox != null){
			//Skybox
			Spatial skybox = SkyFactory.createSky(assetManager, SharedFunctions.getPath("Textures|skybox|"+selectedSkybox.getName()),false);
			skybox.rotate(-FastMath.HALF_PI, 0, 0);
			result.attachChild(skybox);
		}
		result.attachChild(map);
		return result;
	}

	public void playingCharacterDeathCompleted(Spatial spatial) {
		characters.remove(spatial.getName());
		rootNode.detachChild(spatial);
	}

    public void changeCameraHeight(float changeAmount) {
        clientFocus.getControl(FocusControl.class).changeCameraHeight(changeAmount);
    }
	
	public boolean isTheGameStarted(){
		return gameStarted;
	}
	
	public void setLoadingStep (LoadingSteps step){
		synchronized(loadingState){
			loadingState.setStep(step);
		}
	}

	public void setGameStarted() {
		gameStarted = true;
	}
	
	 public Spatial getCharacterSpatial(String spatialId) {
        return characters.get(spatialId);
    }

	public void addDroppingBag(IGamePoint position,String bagId) {
		Spatial bagOfItems = ModelsLoader.getInstance().getStaticModel("bagOfItems").clone();
		bagOfItems.setLocalTranslation(new Vector3f((float)position.getX(), (float)position.getY(), 0));
		bagOfItems.setLocalScale(0.01f);
		bagOfItems.setLocalRotation(new Quaternion(new float[] {3.14f/2,0,(float)Math.random()*3.14f*2}));
		bagOfItems.addControl(new BagDropControl(50));
		bagOfItems.setName(bagId);
		rootNode.attachChild(bagOfItems);
				
	}

	public void removeBag(String bagId) {
		rootNode.detachChildNamed(bagId);
	}
	/**
	 * Draws character collision shape 2D in the current map.
	 * Character shape is represented as a green rectangle, bounding the player sprite.
	 * @param race
	 * @param positionX
	 * @param positionY
	 * @return 
	 */
	private IGraphicShape getGraphicCollisionShape(String race,double positionX, double positionY){
		IRaceDefinition playerRace = GameStorage.getInstance().getRaceDefinition(race);
		//TODO estendere collision shape con una classe GUI, togliere il get spatial dalla classe attuale
		// che rimarrà solo BL
		ICollisionShape playerShape = playerRace.getBoundingShape(InGameMapping.getInstance().getGuiStatus());
		//TODO puo' diventare translate to fit model, così da settare tutti gli offset automaticamente
		playerShape.translateToFitCharacter(new GamePoint(positionX, positionY));
		return playerShape.createGraphicShape();
	}
	/**
	 * Draws map sectors and collision object in the map.
	 * Sectors are drown as blue rectangles while collision object in red. 
	 * @param nextMap
	 * @param node 
	 */
	private void drawMapSectorsAndShapes(String nextMap) {
		IGameMapDefinition mapDef= GameStorage.getInstance().getMapDefinition(nextMap);
		IMapSector[][] sectors = mapDef.getSectors();
		int maxRowSectors = mapDef.getSectorsInRows();
		int maxColsSectors = mapDef.getSectorsInColumns();
		double sectorWidth = mapDef.getSectorWidth();
		double sectorHeight = mapDef.getSectorHeight();
		/*
		for (int i = 0; i < maxRowSectors; i++){
			for (int j = 0; j < maxColsSectors; j++){
				//draw sector
				Spatial sector = new Geometry("sector"+i + " " + j,new Quad((float) sectorWidth,(float) sectorHeight));
				sector.setLocalTranslation((float)(i * sectorWidth),(float) (j * sectorHeight), 0.1f);
				Material sectorMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
				//blue
				sectorMaterial.setColor("Color", new ColorRGBA(0,0,1,0.4f));
				sector.setMaterial(sectorMaterial);
				graphicCollisionTree.attachChild(sector);
				sectorMaterial.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
				sector.setQueueBucket(Bucket.Transparent);
			}
		}
		* */
		//draw collision objects in the sector
		for (ICollisionShape currentCollisionShape : mapDef.getCollisionObjects()){
			IGraphicShape graphicShape = currentCollisionShape.createGraphicShape();
			graphicCollisionTree.attachChild(((AbstractJMEGraphicShape)graphicShape).getSpatial());
		}
	}

	private void loadSingleNpcs(ISummary singleSumary) {
		String name = singleSumary.getString("id");
		double positionX = singleSumary.getDouble("positionX");
		double positionY = singleSumary.getDouble("positionY");
		double direction = singleSumary.getDouble("direction");
		String race = singleSumary.getString("race");
		addNonMovingCharacterSpatial(name, positionX, positionY, direction, race);
	}

	private void addNonMovingCharacterSpatial(String name, double x, double y, double direction, String race) {
		CharacterSpatialWrapper spatialWrapper = ModelsLoader.getInstance().getModel(race);
		Spatial model = spatialWrapper.getModel().clone();
		model.setName(name);
		model.setShadowMode(RenderQueue.ShadowMode.Cast);
		model.rotate(new Quaternion().fromAngles(0, 0, (float) direction));
		model.setLocalTranslation(new Vector3f((float) x, (float) y, 0));
		model.addControl(new AnimationControl3D(0));
		model.getControl(AnimationControl3D.class).setIdleCycle(spatialWrapper.getCycleIndices());
		model.getControl(AnimationControl3D.class).setAnimationSpeeds(spatialWrapper.getAnimationDefaultSpeeds());
		try {
			toAdd.put(model);
		} catch (InterruptedException ex) {
			Logger.getLogger(SpatialsManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		GuiStatus guiStatus = InGameMapping.getInstance().getGuiStatus();
		ICollisionShape collisionShape =  GameStorage.getInstance().getNonPlayingCharacterRaceDefinition(race).getBoundingShape(guiStatus);
		collisionShape.translateToFitCharacter(new GamePoint(x, y));
		IGraphicShape graphicShape = collisionShape.createGraphicShape();
		graphicCollisionTree.attachChild(((AbstractJMEGraphicShape)graphicShape).getSpatial());
	}
	
	private void loadNpcs(ISummary npcsSummary) {
        Map<String, Serializable> internalMap = npcsSummary.getInternalMap();
        for(String key:internalMap.keySet()) {
			if(InGameMapping.getInstance().getGuiStatus().equals(GuiStatus.WORLD_3D)){
				loadSingleNpcs(npcsSummary.getSummary(key));
			}else{
				loadSingleNpc2D(npcsSummary.getSummary(key));
			}
		}
	}

	public void debugSwitched() {
		if(!debugMode){
			rootNode.attachChild(graphicCollisionTree);
		}else{
			rootNode.detachChild(graphicCollisionTree);
		}
		debugMode = !debugMode;
	}

	public void movingCharacterSpatialRemoved(final String name) {
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute() {
				collisionShapes.get(name).getSpatial().removeFromParent();
				collisionShapes.remove(name); 
			}
		});
	}

	private void loadSingleNpc2D(ISummary summary) {
		String race = summary.getString("race");
		String name = summary.getString("name");
		double positionX = summary.getDouble("positionX");
		double positionY = summary.getDouble("positionY");
		double direction = summary.getDouble("direction");
		//TODO load as game starts
		Texture t = assetManager.loadTexture("SpriteSheets/"+ race + "/" + race + ".png");
		//TODO set dimensions
		final Geometry npc = new Geometry(name, new Quad(1, 1));
		Material mat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		mat.setTexture("ColorMap", t);
		npc.setMaterial(mat);
		npc.scale(0.5f);
		npc.setLocalTranslation((float)positionX -0.5f, (float)positionY - 0.5f, 0.1f);
		
        mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        npc.setQueueBucket(Bucket.Transparent);
		
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {

			@Override
			public void execute() {
				rootNode.attachChild(npc);
			}
		});
	}

	public void addMapIcon(String mapId) {
		Picture icon = ModelsLoader.getInstance().getMapIcon(mapId);
		if(icon!=null) {
			Picture clone = (Picture) icon.clone();
			clone.setPosition(3, camera3D.getHeight()-203);
			clone.addControl(new PauseAndDeathControl(3));
			guiNode.attachChild(clone);
		}
	}
}
