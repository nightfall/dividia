/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.world.collisions;

import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector4f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.gui_api.world.IGraphicShape;

/**
 *
 * @author Andrea
 */
public abstract class AbstractJMEGraphicShape implements IGraphicShape{
	
	protected Geometry graphicShape;
	boolean alreadyDrawn = false;
	
	@Override
	public void draw() {
		if (!alreadyDrawn){
			GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
				//TODO root node aggiunge + di una volta nodi uguali?
				@Override
				public void execute() {
					SpatialsManager.getInstance().getRootNode().attachChild(graphicShape);
				}
			});
			alreadyDrawn = true;
		}
	}

	@Override
	public void drawAt(double x, double y, double z) {
		graphicShape.setLocalTranslation((float)x, (float)y, (float)z);
		if (!alreadyDrawn){
			draw();
		}
	}
	
	@Override
	public void setColor(Vector4f color) {
		graphicShape.getMaterial().setColor("Color", new ColorRGBA(color.x, color.y, color.z, color.w));
	}

	@Override
	public void erase() {
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {

			@Override
			public void execute() {
				SpatialsManager.getInstance().getRootNode().detachChild(graphicShape);
			}
		});
		alreadyDrawn = false;
	}

	@Override
	public void translate(double dx, double dy, double dz) {
		graphicShape.move((float)dx, (float)dy, (float)dz);
	}

	@Override
	public void setLocation(double x, double y, double z) {
		graphicShape.setLocalTranslation((float)x, (float)y, (float)z);
	}
	
	public Spatial getSpatial(){
		return graphicShape;
	}
}
