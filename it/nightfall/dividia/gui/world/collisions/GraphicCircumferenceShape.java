/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.world.collisions;

import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Curve;
import it.nightfall.dividia.api.world.collisions.IBoundingCircumference;
import it.nightfall.dividia.gui.custom_meshes.Circle;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.world.IGraphicShape;

/**
 *
 * @author Andrea
 */
public class GraphicCircumferenceShape extends AbstractJMEGraphicShape implements IGraphicShape{

	public GraphicCircumferenceShape (IBoundingCircumference shape){
		createGeometry(shape);
	}
	
	public GraphicCircumferenceShape (IBoundingCircumference shape, Vector4f color){
		createGeometry(shape);
		setColor(color);
	}

	private void createGeometry(IBoundingCircumference shape) {
		Vector3f shapePosition = new Vector3f((float)shape.getPosition().getX(),(float) shape.getPosition().getY(), 0);
		graphicShape = new Geometry("shape", new Circle((float)shape.getRadius(),64));
		graphicShape.rotateUpTo(Vector3f.UNIT_Z);
		graphicShape.setLocalTranslation((float)(shapePosition.getX() + shape.getRadius()),(float) (shapePosition.getY() + shape.getRadius()), 0);
		Material mat = new Material (SpatialsManager.getInstance().getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", new ColorRGBA(1, 1, 1, 0.5f));
		graphicShape.setMaterial(mat);
		
		mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
		graphicShape.setQueueBucket(RenderQueue.Bucket.Transparent);
	}
	
}
