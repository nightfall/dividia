/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui.world.collisions;

import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Quad;
import it.nightfall.dividia.api.network.IReceivedMessage;
import it.nightfall.dividia.api.world.collisions.IBoundingRectangle;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;
import it.nightfall.dividia.bl.world.collisions.BoundingRectangle;
import it.nightfall.dividia.gui.events.GUIDispatcher;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.gui_api.world.IGraphicShape;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;

/**
 * JME3 rectangular shape.
 * @author Andrea
 */
public class GraphicRectangularShape extends AbstractJMEGraphicShape implements IGraphicShape {

	
	public GraphicRectangularShape(IBoundingRectangle shape){
		createGeometry(shape);
	}
	
	public GraphicRectangularShape (IBoundingRectangle shape, Vector4f color){
		createGeometry(shape);
		setColor(color);
	}
	

	private void createGeometry (IBoundingRectangle shape){
		graphicShape = new Geometry("shape", new Quad((float)shape.getWidth(),(float)shape.getHeight()));
		graphicShape.setLocalTranslation((float)(shape.getPosition().getX()), (float)(shape.getPosition().getY()), 0);
		Material mat = new Material (SpatialsManager.getInstance().getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", new ColorRGBA(1, 1, 1, 0.5f));
		graphicShape.setMaterial(mat);
		
		mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
		graphicShape.setQueueBucket(RenderQueue.Bucket.Transparent);
	}	

	
}
