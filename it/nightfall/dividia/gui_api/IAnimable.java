package it.nightfall.dividia.gui_api;


import it.nightfall.dividia.gui.two_dimensional_gui.animations.AnimationData;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andrea
 */
public interface IAnimable {
    
    public void playAnimation(AnimationData animation);
}
