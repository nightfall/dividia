package it.nightfall.dividia.gui_api.controls;

import com.jme3.scene.control.Control;
import it.nightfall.dividia.bl.utility.playerMovements.Movement;

public interface ICameraControl extends Control {
	
	public void setMovementStatus(Movement movementStatus);

	public void spatialHasMoved(boolean b);
}
