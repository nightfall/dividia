package it.nightfall.dividia.gui_api.controls;

import it.nightfall.dividia.bl.utility.playerMovements.Movement;

import com.jme3.scene.control.Control;

public interface ICharacterAnimationControl2D extends Control{

	public void playAnimation (Movement direction);
    public void animationFinished ();
}
