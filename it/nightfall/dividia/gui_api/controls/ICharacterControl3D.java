package it.nightfall.dividia.gui_api.controls;

import com.jme3.scene.control.Control;

public interface ICharacterControl3D extends Control{
	
	public void characterMoved( double x, double y, double direction);
	
	public void characterStopped();

	public void setSpeed(int newValue);
    
   public ISpatialMovementControl getMovementControl();
	
}
