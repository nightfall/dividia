package it.nightfall.dividia.gui_api.controls;

import com.jme3.scene.control.Control;

public interface IFocusControl extends Control {
	
	public void focusMoved(double x, double y, String nextFocusStatus);
	
	public void focusReset(double x, double y);
	
}
