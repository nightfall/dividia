package it.nightfall.dividia.gui_api.controls;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import java.util.Queue;

public interface ISimple3DimensionalSpatialMovementControl  extends Control {
	
	public void addDestinationPoint(float x, float y, float z);
	
	public void setSpatialSpeed(float spatialSpeed);
	
	public Queue<Vector3f> getDestinationPoints();
	
	public Spatial getSpatial();

    public Vector3f getLastPointReached();
    
    public float getSpatialSpeed();
}