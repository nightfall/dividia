package it.nightfall.dividia.gui_api.controls;

import com.jme3.scene.control.Control;

public interface ISimpleSpatialControl extends Control {

	public void addDestinationPoint( double x, double y);
}
