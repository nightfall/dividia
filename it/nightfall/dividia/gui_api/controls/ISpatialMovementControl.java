package it.nightfall.dividia.gui_api.controls;


public interface ISpatialMovementControl extends ISimple3DimensionalSpatialMovementControl {
	public void characterMoved( double x, double y );
	
	public void characterStopped();

	public void setSpeed(int newValue);
	
}
