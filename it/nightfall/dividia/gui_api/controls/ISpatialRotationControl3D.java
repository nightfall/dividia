package it.nightfall.dividia.gui_api.controls;

import com.jme3.scene.control.Control;

public interface ISpatialRotationControl3D extends Control {
	public void characterMoved(double direction);
}
