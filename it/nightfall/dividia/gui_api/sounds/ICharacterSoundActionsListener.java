package it.nightfall.dividia.gui_api.sounds;

public interface ICharacterSoundActionsListener {
	void characterStartedMoving();
	
	void spellCast(String spellId);
	
	void basicAttackCast();
	
	void charactedDeath();
	
	void characterStopped();

	void basicAttackHit();
}
