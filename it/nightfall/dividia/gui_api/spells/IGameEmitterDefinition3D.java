
package it.nightfall.dividia.gui_api.spells;

import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.gui.controls.EffectControl3D;


public interface IGameEmitterDefinition3D {
	public ParticleEmitter getEmitterFromDefinition(IDirection castingDirection,  Vector3f castingPosition, EffectControl3D spellControl);
}
