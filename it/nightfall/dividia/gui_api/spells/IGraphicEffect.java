package it.nightfall.dividia.gui_api.spells;

import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import it.nightfall.dividia.gui.controls.EffectControl3D;
import java.util.Collection;

public interface IGraphicEffect {
	public void launchEffect();
	
	public void addEmitter(ParticleEmitter emitter);

	public Collection<ParticleEmitter> getEmitters();

	public void setControl(EffectControl3D control);
	
	public void setLocalTranslation(Vector3f position);
	
	public Node getSpellNode();
}
