package it.nightfall.dividia.gui_api.spells;

import it.nightfall.dividia.api.utility.IDirection;
import it.nightfall.dividia.api.utility.IGamePoint;
import it.nightfall.dividia.gui.spellsAndEffects.SpeedFrame;
import java.util.List;

public interface IGraphicEffectDefinition3D {

	List<IGameEmitterDefinition3D> getEmitters();

	IGraphicEffect getGraphicEffectFromDefinition(IDirection castingDirection, IGamePoint castingPoint, String executorId);

	List<SpeedFrame> getSpeedFrames();

	void setEmitters(List<IGameEmitterDefinition3D> emitters);

	void setSpeedFrames(List<SpeedFrame> speedFrames);

}
