package it.nightfall.dividia.gui_api.spells;

import com.jme3.math.Vector3f;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.gui.spellsAndEffects.SpeedFrame;
import java.util.List;

public interface ITrakedElement {

	void addPoint(IConfigReader pointReader, SpeedFrame speedFrame);

	float getNextSpeed();

	List<Float> getPauseTimes();

	List<Vector3f> getWayPoints();

	void setNextSpeed(float nextSpeed);

	void setPauseTimes(List<Float> pauseTimes);

	void setWayPoints(List<Vector3f> wayPoints);

}
