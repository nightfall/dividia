package it.nightfall.dividia.gui_api.spells.emitterShapes;

import com.jme3.effect.shapes.EmitterShape;
import it.nightfall.dividia.api.utility.IDirection;

public interface IEmitterShape {
	
	void rotate(IDirection direction);

	public EmitterShape getEmitterShape();
}
