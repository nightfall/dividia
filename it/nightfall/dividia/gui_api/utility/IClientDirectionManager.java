package it.nightfall.dividia.gui_api.utility;

public interface IClientDirectionManager {

	public void keyAction (String binding, boolean isPressed);

	public void cameraIsAdjusting(boolean b);
}
