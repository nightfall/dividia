package it.nightfall.dividia.gui_api.utility;

import it.nightfall.dividia.api.utility.IClockListener;
import it.nightfall.dividia.bl.utility.playerMovements.Movement;

public interface IDirectionMovementManager extends IClockListener {
	public void updateDirection(Movement movementDirection);

	public Movement getMovementStatus();

	public void resetFocus();
}
