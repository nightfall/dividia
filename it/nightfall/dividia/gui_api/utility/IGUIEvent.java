package it.nightfall.dividia.gui_api.utility;

public interface IGUIEvent {
	void execute();
}
