package it.nightfall.dividia.gui_api.utility.input;

import com.jme3.input.controls.ActionListener;

public interface IKeyMapping extends ActionListener {
	String[] getMappingIds();
}
