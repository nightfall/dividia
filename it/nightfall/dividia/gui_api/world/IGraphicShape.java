/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.gui_api.world;

import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import it.nightfall.dividia.api.world.collisions.ICollisionShape;

/**
 * Graphic representation of a shape.
 * @author Andrea
 */
public interface IGraphicShape {
	
	/**
	 * Provides a way to draw a shape in its current position.
	 */
	void draw();
	
	/**
	 * Draws @param shape in the given position. 
	 * 
	 * @param x x position offset
	 * @param y y position offset
	 * @param z z position offset
	 */
	void drawAt(double x, double y, double z);
	
	/**
	 * Sets the mesh of the shape to the specified color.
	 * @param color 
	 */
	void setColor (Vector4f color);
	
	void translate (double dx, double dy, double dz);
	
	void setLocation (double x, double y, double z);
	
	void erase ();
}
