package it.nightfall.dividia.testing;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.Camera;
import de.lessvoid.nifty.screen.ScreenController;
import it.nightfall.dividia.api.network.IClientEventListener;
import it.nightfall.dividia.api.network.IIntentionManager;
import it.nightfall.dividia.bl.utility.GameStorage;
import it.nightfall.dividia.gui.spellsAndEffects.EffectsStorage;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.GameInputManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.appstates.InGameAppState;
import it.nightfall.dividia.testing.appstates.LoadingAppState;
import it.nightfall.dividia.testing.appstates.MainMenu;
import it.nightfall.dividia.testing.screencontrollers.DialogScreenController;
import it.nightfall.dividia.testing.screencontrollers.EquipmentItemsController;
import it.nightfall.dividia.testing.screencontrollers.InventoryController;
import it.nightfall.dividia.testing.screencontrollers.OptionsScreenController;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientTest extends SimpleApplication {
	public static IIntentionManager intentionManager;
	public static String login;
	private static GameInputManager gameInputManager;
	public static IClientEventListener clientListener;
	private static ClientTest instance = new ClientTest();
	private AbstractAppState currentState = new MainMenu();
	private Map<String, NiftyJmeDisplay> niftyProcessors = new HashMap<>();
	private NiftyJmeDisplay activeNiftyProcessor = null;

	public static ClientTest getInstance(){
		return instance;
	}

	public static void main(String[] args) throws RemoteException{
		Logger.getLogger("").setLevel(Level.SEVERE);
		GameStorage.initStorage();
		instance.start();
		
	}

	@Override
	public void simpleInitApp(){
		
		setDisplayStatView(false);
		setPauseOnLostFocus(false);
		inputManager.clearMappings();
		EffectsStorage.initStorage(assetManager);
		gameInputManager = GameInputManager.getInstance();
		gameInputManager.init(inputManager);
		flyCam.setEnabled(false);
		loadNifty();
		activeNiftyProcessor = niftyProcessors.get("mainmenu");
		guiViewPort.addProcessor(activeNiftyProcessor);
		stateManager.attach(currentState);
	}

	public static void setClientEventListener(IClientEventListener listener){
		clientListener = listener;
	}

	public static void setIntentionManager(IIntentionManager im){
		intentionManager = im;
	}

	public static GameInputManager getGameInputManager(){
		return gameInputManager;
	}

	public void enableSP() throws RemoteException{
		stateManager.detach(currentState);
		currentState = InGameAppState.getInstance();
		stateManager.attach(currentState);
	}

	public void attachAppState(AbstractAppState appState){
		stateManager.attach(appState);
	}

	public void detachAppState(AbstractAppState appState){
		stateManager.detach(appState);
	}

	@Override
	public void simpleUpdate(float tpf){
		GUIEventsHandler.getInstance().executePendingEvents();
		//listener.setLocation(cam.getLocation());
		//listener.setRotation(cam.getRotation());
	}

	public void changeNiftyProcessor(final String processorId){
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				guiViewPort.removeProcessor(activeNiftyProcessor);
				activeNiftyProcessor.getNifty().gotoScreen("empty");
				activeNiftyProcessor = niftyProcessors.get(processorId);
				activeNiftyProcessor.getNifty().gotoScreen("empty");
				guiViewPort.addProcessor(activeNiftyProcessor);
			}
		});
	}

	public NiftyJmeDisplay getActiveNiftyDisplay(){
		return activeNiftyProcessor;
	}

	private void loadNifty(){
		loadNiftyStep("mainmenu", "Interface/mainmenu.xml");
		loadNiftyStep("multiplayerLogin", "Interface/multiplayerLogin.xml");
		loadNiftyStep("multiplayerRegistration", "Interface/multiplayerReg.xml");
		loadNiftyStepWithController("dialog", "Interface/dialogues.xml", DialogScreenController.getInstance());
		loadNiftyStepWithController("equipments", "Interface/equipments.xml", EquipmentItemsController.getInstance());
		loadNiftyStepWithController("inventory", "Interface/inventory.xml", InventoryController.getInstance());
		loadNiftyStepWithController("options", "Interface/options.xml", OptionsScreenController.getInstance());
		LoadingAppState.getInstance().initNifty();
	}
	
	private void loadNiftyStep(String id, String file){
		niftyProcessors.put(id, new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort));
		niftyProcessors.get(id).getNifty().fromXml(file, "empty");
	}

	private void loadNiftyStepWithController(String id, String file, ScreenController controller){
		niftyProcessors.put(id, new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort));
		niftyProcessors.get(id).getNifty().fromXml(file, "empty", controller);
	}
	
	public Camera getCam() {
		return cam;
	}
	
	public String getLoginName(){
		return login;
	}
	
	public void setCamera(Camera camera){
		cam = camera;
		cam.update();
	}
}
