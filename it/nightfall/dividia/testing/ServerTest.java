package it.nightfall.dividia.testing;

import it.nightfall.dividia.bl.utility.GameManager;
import it.nightfall.dividia.bl.utility.GameStorage;

public class ServerTest {
	public static void main(String[] args){	
		GameStorage.initStorage();
		GameManager.getInstance().startEngine();
		GameManager.getInstance().startServer();
	}
}
