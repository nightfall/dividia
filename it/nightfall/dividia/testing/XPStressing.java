package it.nightfall.dividia.testing;

import it.nightfall.dividia.bl.characters.Player;
import it.nightfall.dividia.bl.utility.GameManager;
import it.nightfall.dividia.bl.utility.SharedFunctions;

public class XPStressing implements Runnable {
	@Override
	public void run(){
		Player a = GameManager.getInstance().getPlayer("pippo");
		while(true){
			try{
				Thread.sleep(100);
				a.gainExperience(50);
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
}
