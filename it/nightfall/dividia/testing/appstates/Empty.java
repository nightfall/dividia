package it.nightfall.dividia.testing.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;

public class Empty extends AbstractAppState {
	private SimpleApplication app;

	@Override
	public void initialize(AppStateManager stateManager, Application app){
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;
	}

	@Override
	public void update(float tpf){
	}
}
