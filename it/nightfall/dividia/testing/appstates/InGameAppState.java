package it.nightfall.dividia.testing.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.shadow.PssmShadowRenderer;
import it.nightfall.dividia.api.network.IReceivedMessage;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.gui.events.GUIDispatcher;
import it.nightfall.dividia.gui.sounds.BackgroundsManager;
import it.nightfall.dividia.gui.spellsAndEffects.EffectsManager;
import it.nightfall.dividia.gui.utility.GameInputManager;
import it.nightfall.dividia.gui.utility.input.InGameMapping;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;

public class InGameAppState extends AbstractAppState {
	private SimpleApplication app;
	private PssmShadowRenderer pssmRenderer;
	private static InGameAppState instance = new InGameAppState();
	private boolean removeScene = false;

	public static InGameAppState getInstance(){
		return instance;
	}

	@Override
	public void initialize(AppStateManager stateManager, Application application){
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) application;
		InGameMapping.getInstance().init();
		GameInputManager.getInstance().setInputState(GameInputManager.InputState.IN_GAME);
		//start loading the scene, in another thread 
		GUIDispatcher.getInstance().addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException {
				String loginName = ClientTest.getInstance().getLoginName();
				//TODO just one file in the flolder?
				String pathToSaveGame = SharedFunctions.getPath("savegames|" + loginName + "|" + loginName + ".xml");
				IConfigReader reader = new ConfigReader(pathToSaveGame);
				String mapName = reader.getString("map");
				SpatialsManager.getInstance().loadMapScene(mapName);
				BackgroundsManager.getInstance().changeMap(mapName);
				SpatialsManager.getInstance().setGameStarted();
			}
		});
	}

	@Override
	public void update(float tpf){
		EffectsManager.getInstance().update();
		if(removeScene == true){
			removeScene();
			removeScene = false;
		}
		//if there is some node to delete, clean the scene
		if(SpatialsManager.getInstance().isThereSomethingToDelete()){
			SpatialsManager.getInstance().cleanup();
		}
		//if there is some node to add, add it
		if(SpatialsManager.getInstance().isThereSomethingToAdd()){
			SpatialsManager.getInstance().updateScene();
		}
	}

	private void removeScene(){
		app.getRootNode().detachAllChildren();
	}

	public void setRemoveScene(boolean b){
		removeScene = true;
	}
}
