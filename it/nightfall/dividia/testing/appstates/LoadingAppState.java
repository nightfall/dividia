/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.nightfall.dividia.testing.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Line;
import com.jme3.ui.Picture;
import com.jme3.util.SkyFactory;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.SizeValue;
import it.nightfall.dividia.api.network.IReceivedMessage;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.gui.controls.LifebarControl;
import it.nightfall.dividia.gui.controls.LoadingHourglassControl;
import it.nightfall.dividia.gui.controls.SpellArcControl;
import it.nightfall.dividia.gui.events.GUIDispatcher;
import it.nightfall.dividia.gui.spellsAndEffects.EffectsManager;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.ModelsLoader;
import it.nightfall.dividia.gui.world.LoadingState;
import it.nightfall.dividia.gui.world.LoadingSteps;
import it.nightfall.dividia.gui.world.SpatialsManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;
//TODO fix 3D elements OVER nifty gui, for now only nifty is visible.

public class LoadingAppState extends AbstractAppState implements ScreenController {
	private SimpleApplication app;
	private Node guiNode;
	private NiftyJmeDisplay niftyDisplay;
	private Nifty nifty;
	private Element progressBar;
	private Screen screen;
	private ViewPort niftyViewPort;
	private BitmapFont customFont;
	private BitmapText loadingDetailsText;
	private BitmapText loadingText;
	private int progressBarUnit;
	private int frames = 0;
	private Node loadingGuiNode;
	private static LoadingAppState instance = null;
	private boolean gameStarted;
	private boolean firstFrame = true;
	private Label niftyLoadingTextLabel;

	public static LoadingAppState getInstance(){
		if(instance == null){
			instance = new LoadingAppState();
		}
		return instance;
	}

	private LoadingAppState(){
		this.app = ClientTest.getInstance();
	}

	@Override
	public void initialize(AppStateManager stateManager, Application app){
		super.initialize(stateManager, app);
		this.app = (SimpleApplication) app;
		this.guiNode = this.app.getGuiNode();
		this.gameStarted = SpatialsManager.getInstance().isTheGameStarted();
		this.loadingGuiNode = new Node("LoadingNode");
		//this.initNifty();
		nifty.gotoScreen("start");
		this.initText();
		//initGuiNode();
	}

	@Override
	public void stateAttached(AppStateManager stateManager){
		super.stateAttached(stateManager);
	}

	@Override
	public void stateDetached(AppStateManager stateManager){
		super.stateDetached(stateManager);
	}

	@Override
	public void cleanup(){
		super.cleanup();
		niftyDisplay.getNifty().gotoScreen("empty");
		guiNode.detachChildNamed("LoadingNode");
		niftyViewPort.clearProcessors();
	}

	private void initGuiNode(){
		//take screen dimensions
		int screenWidth = app.getCamera().getWidth();
		int screenHeigh = app.getCamera().getHeight();
		//set up black screen
		Picture bpic = new Picture("Black screen");
		bpic.setImage(app.getAssetManager(), SharedFunctions.getPath("Textures|black_screen.png"), true);
		//the black img starts from the bottom-left corner of the screen
		bpic.setPosition(0, 0);
		//covers the whole screen
		bpic.setWidth(screenWidth);
		bpic.setHeight(screenHeigh);

		//set up logo
		Picture pic = new Picture("Logo");
		pic.setImage(app.getAssetManager(), SharedFunctions.getPath("Textures|DividiaLogo.png"), true);
		pic.setWidth(647);
		pic.setHeight(647);
		pic.setPosition((screenWidth - 647) / 2, (screenHeigh - 647) / 2);

		Spatial hourglass = app.getAssetManager().loadModel("Models/Objects/Hourglass/hourglass.mesh.xml");
		hourglass.scale(10);
		//pivot node used for rotating the hourglass from its center
		Node pivot = new Node("Hourglass");
		pivot.setLocalTranslation(screenWidth - 100, 100, 0);
		pivot.attachChild(hourglass);
		pivot.addControl(new LoadingHourglassControl());
		hourglass.center();
		setUpLoadingLight();

		//attach children to gui node
		loadingGuiNode.attachChild(bpic);
		loadingGuiNode.attachChild(pic);
		loadingGuiNode.attachChild(pivot);
		//put it forward
		loadingGuiNode.move(0, 0, 3);
	}

	private void setUpLoadingLight(){
		DirectionalLight light = new DirectionalLight();
		light.setColor(ColorRGBA.White);
		light.setDirection(new Vector3f(0, 0, -0.5f));
		loadingGuiNode.addLight(light);
	}

	public void initNifty(){
		//create a new view port and put it forward
		niftyViewPort = app.getRenderManager().createPostView("Loading viewport", app.getCamera());
		//create a nifty display using asset manager, input manager, audio renderer and the new view port
		this.niftyDisplay = new NiftyJmeDisplay(app.getAssetManager(), app.getInputManager(), app.getAudioRenderer(), niftyViewPort);
		nifty = niftyDisplay.getNifty();
		//load the nifty gui from the defined xml
		nifty.fromXml(SharedFunctions.getPath("Interface|Loading|scene_loading_bar.xml"), "empty", this);
		//attach the nifty display to the processor of the new created view port
		niftyViewPort.addProcessor(niftyDisplay);
	}

	@Override
	public void update(float tpf){
		super.update(tpf);
		//if the game is not started
		if(firstFrame && !gameStarted){
			initGame();
		}
		LoadingState currentLoadingStep = SpatialsManager.getInstance().getLoadingState();
		//TODO optimize with events called by SpatialManager
		setProgress(currentLoadingStep.getLoadedQuantity(), currentLoadingStep.getCurrentLoadingString());
		if(frames % 60 == 0){
			loadingText.setText(loadingText.getText() + ".");
		}
		if(frames % 240 == 0){
			loadingText.setText("L O A D I N G ");
		}
		frames++;
	}

	public void initText(){
		//defaultFont = app.getAssetManager().loadFont("Interface/Fonts/Default.fnt");
		customFont = app.getAssetManager().loadFont("Interface/Fonts/Wilderness.fnt");
		/*
		 loadingDetailsText = new BitmapText(defaultFont,false);
		 loadingDetailsText.setName("Loading info");
		 loadingDetailsText.setText("Preparing map");
		 loadingDetailsText.setSize(defaultFont.getCharSet().getRenderedSize());
		 * */
		loadingText = new BitmapText(customFont, false);
		loadingText.setText("Loading ");
		loadingText.setSize(customFont.getCharSet().getRenderedSize() * 2);
		loadingText.setName("Loading Text");
		//dispose text correctly basing on the position of nifty elements
		//put the loading text visible
		//loadingText.move(0, 0, 2);

		//loadingGuiNode.attachChild(loadingText);
		//alignText();
	}

	public void alignText(){
		//Loading detailed text are now rendered in nifty. De-comment this to use java instead
		/*
		 int h = nifty.getCurrentScreen().findElementByName("loadingpanel").getHeight();
		 int w = nifty.getCurrentScreen().findElementByName("loadingpanel").getWidth();
		 int x = nifty.getCurrentScreen().findElementByName("loadingpanel").getX();
		 int y = nifty.getCurrentScreen().findElementByName("loadingpanel").getY();
		
		 //representation of the nifty gui panel, with some operation for size porting
		 //note that camera heigh is added because of the different reference systems of nifty and jme
		 //note that char rendered size is added for aligning text with the bottom-left edge (top-left is the default)
		 Rectangle rec = new Rectangle(x, app.getCamera().getHeight() - y + defaultFont.getCharSet().getRenderedSize(), w, h);
		 loadingDetailsText.setBox(rec);
		 loadingDetailsText.setAlignment(BitmapFont.Align.Center);
		 loadingDetailsText.setVerticalAlignment(BitmapFont.VAlign.Bottom);
		 //needed for making the text visible (we have depth = 1, not 0)
		 loadingDetailsText.setLocalTranslation(loadingDetailsText.getLocalTranslation().x, loadingDetailsText.getLocalTranslation().y, 2);
		
		 //put the loading text visible
		 loadingText.setLocalTranslation(x, y, 2);
		
		 loadingGuiNode.attachChild(loadingText);
		 loadingGuiNode.attachChild(loadingDetailsText);
		 */
	}

	@Override
	public void bind(Nifty nifty, Screen screen){
		this.nifty = nifty;
		this.screen = screen;

	}

	@Override
	public void onStartScreen(){
		this.screen = nifty.getCurrentScreen();
		if(screen.getScreenId().equals("start")){
			//initialize elements
			this.progressBar = screen.findElementByName("progressbar");
			this.progressBarUnit = progressBar.getConstraintWidth().getValueAsInt(100);
			this.niftyLoadingTextLabel = screen.findNiftyControl("loadingtext", Label.class);
		}
	}

	@Override
	public void onEndScreen(){
	}

	public void setProgress(float progress, String text){
		int pixelWidth = (int) (progressBarUnit + (progressBar.getParent().getWidth() - progressBarUnit) * progress);
		progressBar.setConstraintWidth(new SizeValue(pixelWidth + "px"));
		progressBar.getParent().layoutElements();

		niftyLoadingTextLabel.setText(text);
	}

	private void initGame(){
		//Tell to gui dispatcher to load models, in another thread from render
		GUIDispatcher.getInstance().addMessage(new IReceivedMessage() {
			@Override
			public void perform() throws RemoteException{
				//load models
				SpatialsManager.getInstance().setLoadingStep(LoadingSteps.MODELS);
				ModelsLoader.getInstance().loadModels();

				//init HUD
				LifebarControl.lifebarInit(app.getAssetManager(), app.getCamera());
				SpellArcControl.spellArcInit(app.getAssetManager(), app.getCamera());
				EffectsManager.initSpellManager(SpellArcControl.getSpellArcControl());

				//load player in BL
				try{
					ClientTest.intentionManager.loadGameSave(ClientTest.login);
				}catch(RemoteException ex){
					ex.printStackTrace();
				}
				// add common map stuff, modify scene so must be done in a safe place :
				// Renderer thread is used
				GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
					@Override
					public void execute(){
						//set background
						app.getViewPort().setBackgroundColor(ColorRGBA.Black);
						app.getGuiNode().attachChild(LifebarControl.getLifebar());
						app.getGuiNode().attachChild(SpellArcControl.getSpellArc());

						//drawAxes();
						drawSky();
						setUpLight();
						//drawAxes();
						//enable in game state
						try{
							ClientTest.getInstance().enableSP();
						}catch(RemoteException ex){
						}
					}
				});
			}
		});
		firstFrame = false;
		ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("empty");
	}

	private void drawAxes(){
		Line xLine = new Line(new Vector3f(60, 60, 0), new Vector3f(160, 60, 0));
		Line yLine = new Line(new Vector3f(60, 60, 0), new Vector3f(60, 160, 0));
		Line zLine = new Line(new Vector3f(60, 60, 0), new Vector3f(60, 60, 100));
		Geometry xLineGeo = new Geometry("xLine", xLine);
		Geometry yLineGeo = new Geometry("yLine", yLine);
		Geometry zLineGeo = new Geometry("zLine", zLine);
		Material mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", ColorRGBA.Red);
		xLineGeo.setMaterial(mat);
		yLineGeo.setMaterial(mat);
		zLineGeo.setMaterial(mat);
		app.getRootNode().attachChild(xLineGeo);
		app.getRootNode().attachChild(yLineGeo);
		app.getRootNode().attachChild(zLineGeo);
	}

	private void drawSky(){
		Spatial sky = SkyFactory.createSky(app.getAssetManager(), "Textures/Sky/Bright/BrightSky.dds", false);
		sky.rotate(-FastMath.HALF_PI, 0, 0);
		app.getRootNode().attachChild(sky);
	}

	public void setUpLight(){
		/*
		 pssmRenderer = new PssmShadowRenderer(app.getAssetManager(), 2048, 2);
		 pssmRenderer.setShadowIntensity(0.5f);
		 pssmRenderer.setDirection(new Vector3f(-3f, 2f, -5f).normalizeLocal()); // light direction
		 app.getViewPort().addProcessor(pssmRenderer);
		 app.getRootNode().setShadowMode(RenderQueue.ShadowMode.Off);
		 */

		AmbientLight al = new AmbientLight();
		al.setColor(ColorRGBA.White.mult(1.5f));
		app.getRootNode().addLight(al);

		DirectionalLight dl = new DirectionalLight();
		dl.setColor(ColorRGBA.White.mult(0.8f));
		dl.setDirection(new Vector3f(-3f, 2f, -5f).normalizeLocal());
		app.getRootNode().addLight(dl);
	}
}
