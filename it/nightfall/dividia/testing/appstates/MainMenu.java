package it.nightfall.dividia.testing.appstates;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import it.nightfall.dividia.api.network.IDCPSession;
import it.nightfall.dividia.api.utility.IGameConstants;
import it.nightfall.dividia.api.utility.io.IConfigReader;
import it.nightfall.dividia.bl.network.ClientEventListener;
import it.nightfall.dividia.bl.network.EventDispatcher;
import it.nightfall.dividia.bl.network.IntentionManager;
import it.nightfall.dividia.bl.network.dcp.ClientSession;
import it.nightfall.dividia.bl.network.dcp.ClientStates;
import it.nightfall.dividia.bl.utility.GameManager;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.bl.utility.io.ConfigReader;
import it.nightfall.dividia.gui.events.GUIDispatcher;
import it.nightfall.dividia.gui.sounds.BackgroundsManager;
import it.nightfall.dividia.gui.sounds.SoundsManager;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import static it.nightfall.dividia.testing.ClientTest.clientListener;
import java.net.Socket;
import java.rmi.RemoteException;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class MainMenu extends AbstractAppState implements ScreenController {
	private Nifty nifty;
	private Screen screen;
	private SimpleApplication app;
	private Element loginErrorP, loginExceptionP, loginVersionP, regUsernameP, regPasswordP, regNicknameP, regExceptionP;
	private Element activePopup;
	private String serverIpPath = SharedFunctions.getPath("client|serverAddress.xml");
	

	@Override
	public void bind(Nifty nifty, Screen screen){
		this.nifty = nifty;
		this.screen = screen;
	}

	@Override
	public void onStartScreen(){
	}

	@Override
	public void onEndScreen(){
	}

	@Override
	public void initialize(AppStateManager stateManager, Application application){
		super.initialize(stateManager, application);
		this.app = (SimpleApplication) application;
		ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("start");
		SoundsManager.getInstance().soundsManagerInit(ClientTest.getInstance().getAssetManager(), ClientTest.getInstance().getRootNode());
		BackgroundsManager.getInstance().changeMap("title_screen");
	}

	public void startSP() throws RemoteException{
		ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("empty");
		GameManager.getInstance().startEngine();
		System.out.println(System.currentTimeMillis());
		GUIDispatcher.initDispatcher();
		ClientTest.login = "pippo";
		ClientTest.clientListener = new ClientEventListener("pippo");
		ClientTest.clientListener.sessionAuthenticated(new IntentionManager("pippo"), GameManager.getInstance().getSaveGamesSummary("pippo"));
		EventDispatcher.getInstance().addClient("pippo", clientListener);
	}

	public void login() throws RemoteException{
		GUIDispatcher.initDispatcher();
		String login = screen.findNiftyControl("username", TextField.class).getRealText();
		String password = screen.findNiftyControl("password", TextField.class).getRealText();
		Security.addProvider(new BouncyCastleProvider());
		ClientTest.clientListener = new ClientEventListener(login);
		ClientTest.login = login;
		try{			
			IConfigReader serverIpReader = new ConfigReader(serverIpPath);
			Socket socket = new Socket(serverIpReader.getString("ip"), IGameConstants.SOCKET_PORT);
			IDCPSession logSession = new ClientSession(socket, ClientSession.LOGIN_INTENTION, login, "", password, ClientTest.clientListener, this);
			new Thread(logSession).start();
		}catch(Exception ex){
			if(loginExceptionP == null){
				loginExceptionP = nifty.createPopup("popupException");
			}
			showErrorPopup(loginExceptionP);
		}
	}

	public void register() throws RemoteException{
		GUIDispatcher.initDispatcher();
		String login = screen.findNiftyControl("username", TextField.class).getRealText();
		String nickname = screen.findNiftyControl("nickname", TextField.class).getRealText();
		String password = screen.findNiftyControl("password", TextField.class).getRealText();
		Security.addProvider(new BouncyCastleProvider());
		ClientTest.clientListener = new ClientEventListener(login);
		ClientTest.login = login;
		try{
			IConfigReader serverIpReader = new ConfigReader(serverIpPath);
			Socket socket = new Socket(serverIpReader.getString("ip"), IGameConstants.SOCKET_PORT);
			IDCPSession regSession = new ClientSession(socket, ClientSession.REGISTRATION_INTENTION, login, nickname, password, ClientTest.clientListener, this);
			new Thread(regSession).start();
		}catch(Exception ex){
			if(regExceptionP == null){
				regExceptionP = nifty.createPopup("popupException");
			}
			showErrorPopup(regExceptionP);
		}
	}

	public void goMultiplayerLogin(){
		ClientTest.getInstance().changeNiftyProcessor("multiplayerLogin");
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("multiplayerLogin");
			}
		});
	}

	public void goMultiplayerRegistration(){
		ClientTest.getInstance().changeNiftyProcessor("multiplayerRegistration");
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("multiplayerRegistration");
			}
		});
	}

	public void returnToMain(){
		ClientTest.getInstance().changeNiftyProcessor("mainmenu");
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("start");
			}
		});
	}

	public void quitGame(){
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().stop();
			}
		});
	}

	public void removePopup(){
		if(activePopup == null){
			return;
		}
		nifty.closePopup(activePopup.getId());
		activePopup = null;
	}

	private void showErrorPopup(Element popup){
		if(popup == null){
			return;
		}
		activePopup = popup;
		nifty.showPopup(screen, popup.getId(), null);
	}

	public void showError(ClientStates state){
		Element tempElement = null;
		switch(state){
			case VERSION:
				if(loginVersionP == null){
					loginVersionP = nifty.createPopup("popupVersion");
				}
				tempElement = loginVersionP;
				break;
			case LOGIN_USER:
			case LOGIN_PASSWORD:
				if(loginVersionP == null){
					loginErrorP = nifty.createPopup("popupError");
				}
				tempElement = loginErrorP;
				break;
			case REGISTRATION_USER:
				if(regUsernameP == null){
					regUsernameP = nifty.createPopup("popupErrorLogin");
				}
				tempElement = regUsernameP;
				break;
			case REGISTRATION_PASSWORD:
				if(regPasswordP == null){
					regPasswordP = nifty.createPopup("popupErrorPassword");
				}
				tempElement = regPasswordP;
				break;
			case REGISTRATION_NICK:
				if(regNicknameP == null){
					regNicknameP = nifty.createPopup("popupErrorNick");
				}
				tempElement = regNicknameP;
				break;
		}
		showErrorPopup(tempElement);
	}
}
