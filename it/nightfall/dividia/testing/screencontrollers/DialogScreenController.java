package it.nightfall.dividia.testing.screencontrollers;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import it.nightfall.dividia.bl.utility.dialogues.IDialog;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DialogScreenController implements ScreenController {
	private static DialogScreenController instance = new DialogScreenController();
	private Screen screen;
	private Nifty nifty;
	private IDialog dialog;
	private Collection<IDialog> dialogs = new ArrayList<>();

	private DialogScreenController(){
	}

	public static DialogScreenController getInstance(){
		return instance;
	}

	@Override
	public void bind(Nifty nifty, Screen screen){
		this.nifty = nifty;
	}

	@Override
	public synchronized void onStartScreen(){
		screen = nifty.getCurrentScreen();
		switch(screen.getScreenId()){
			case "dialog":
				Label label = screen.findNiftyControl("dialogText", Label.class);
				label.setText(dialog.getText());
				Button button = screen.findNiftyControl("exitDialogButton", Button.class);
				button.setText(dialog.getExitComment());
				break;
			case "choiceDialog":
				Element fillingElement = screen.findElementByName("choicePanel");
				List<Element> clearingElements = fillingElement.getElements();
				for(Element tempElement : clearingElements){
					nifty.removeElement(screen, tempElement);
				}
				for(final IDialog tempDialog : dialogs){
					ButtonBuilder tempButton = new ButtonBuilder("selectDialog_" + tempDialog.getId(), tempDialog.getEntryComment()) {
						{
							alignCenter();
							width("80%");
							height("30px");
							marginBottom("5px");
							interactOnClick("selectDialog(" + tempDialog.getId() + ")");
						}
					};
					tempButton.build(nifty, screen, fillingElement);
				}
				break;
		}
	}

	@Override
	public void onEndScreen(){
	}

	public void setDialog(IDialog dialog){
		this.dialog = dialog;
	}

	public synchronized void setDialogList(Collection<IDialog> dialogs){
		this.dialogs.clear();
		this.dialogs.addAll(dialogs);
	}

	public void exitDialog(){
		nifty.gotoScreen("empty");
		try{
			ClientTest.intentionManager.dialogCompleted(dialog.getId());
		}catch(RemoteException ex){
		}
	}

	public void selectDialog(String dialogId){
		nifty.gotoScreen("empty");
		try{
			ClientTest.intentionManager.dialogChoosen(dialogId);
		}catch(RemoteException ex){
		}
	}
}
