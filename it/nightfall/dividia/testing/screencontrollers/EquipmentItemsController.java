package it.nightfall.dividia.testing.screencontrollers;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import it.nightfall.dividia.api.battle.IElement;
import it.nightfall.dividia.api.battle.IStatusBattleCharacteristic;
import it.nightfall.dividia.api.item.equipment_items.IEquipmentItem;
import it.nightfall.dividia.api.utility.IModifiableInteger;
import it.nightfall.dividia.bl.battle.StatusBattleCharacteristic;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.gui.items.GUIEquippedItems;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.GameInputManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import java.util.Map;
import java.util.Map.Entry;

public class EquipmentItemsController implements ScreenController {
	private static EquipmentItemsController instance = new EquipmentItemsController();
	private Screen screen;
	private Nifty nifty;

	public static EquipmentItemsController getInstance(){
		return instance;
	}

	private EquipmentItemsController(){
	}

	@Override
	public void bind(Nifty nifty, Screen screen){
		this.nifty = nifty;
	}

	@Override
	public void onStartScreen(){
		screen = nifty.getCurrentScreen();
		drawScreen();
	}

	@Override
	public void onEndScreen(){
	}

	public void redraw(){
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				drawScreen();
			}
		});
	}

	private void drawScreen(){
		if(screen != null && screen.getScreenId().equals("equipments")){
			setElementText(screen, "helmetText", getEquipmentDisplayName("helmet"));
			setElementText(screen, "necklaceText", getEquipmentDisplayName("necklace"));
			setElementText(screen, "armorText", getEquipmentDisplayName("armor"));
			setElementText(screen, "glovesText", getEquipmentDisplayName("gloves"));
			setElementText(screen, "ringText", getEquipmentDisplayName("ring"));
			setElementText(screen, "shieldText", getEquipmentDisplayName("shield"));
			setElementText(screen, "mysticalSphereText", getEquipmentDisplayName("mysticalSphere"));
			setElementText(screen, "bootsText", getEquipmentDisplayName("boots"));
			setElementText(screen, "weaponText", getEquipmentDisplayName("weapon"));
			setElementImage(screen, "helmetIcon", getEquipmentName("helmet"));
			setElementImage(screen, "necklaceIcon", getEquipmentName("necklace"));
			setElementImage(screen, "armorIcon", getEquipmentName("armor"));
			setElementImage(screen, "glovesIcon", getEquipmentName("gloves"));
			setElementImage(screen, "ringIcon", getEquipmentName("ring"));
			setElementImage(screen, "shieldIcon", getEquipmentName("shield"));
			setElementImage(screen, "mysticalSphereIcon", getEquipmentName("mysticalSphere"));
			setElementImage(screen, "bootsIcon", getEquipmentName("boots"));
			setElementImage(screen, "weaponIcon", getEquipmentName("weapon"));
		}
	}

	public String getEquipmentDisplayName(String name){
		IEquipmentItem toRead = GUIEquippedItems.getInstance().getEquippedItemFromType(name);
		if(toRead != null){
			return toRead.getItemDefinition().getDisplayName();
		}
		return "No item equipped";
	}

	public String getEquipmentName(String name){
		IEquipmentItem toRead = GUIEquippedItems.getInstance().getEquippedItemFromType(name);
		if(toRead != null){
			return toRead.getItemDefinition().getName();
		}
		return "noItem";
	}

	public void showEquipmentInfo(final String type){
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				if(screen.getScreenId().equals("equipments")){
					screen.findElementByName("equipmentInfo").setVisible(true);
					IEquipmentItem toRead = GUIEquippedItems.getInstance().getEquippedItemFromType(type);
					if(toRead != null){
						for(IStatusBattleCharacteristic data : StatusBattleCharacteristic.getStatusAlterationsStats()){
							setElementText(screen, "F_" + data.getName(), "---");
							setElementText(screen, "S_" + data.getName(), "---");
						}
						for(IElement data : it.nightfall.dividia.bl.battle.Element.values()){
							setElementText(screen, data.getName(), "---");
						}
						Map<IStatusBattleCharacteristic, IModifiableInteger> tempFAlt = toRead.getFinalStatsAlterations();
						for(Entry<IStatusBattleCharacteristic, IModifiableInteger> data : tempFAlt.entrySet()){
							setElementText(screen, "F_" + data.getKey().getName(), "" + data.getValue().getValue());
						}
						Map<IStatusBattleCharacteristic, IModifiableInteger> tempSAlt = toRead.getItemDefinition().getStatsAlterations();
						for(Entry<IStatusBattleCharacteristic, IModifiableInteger> data : tempSAlt.entrySet()){
							int real = data.getValue().getValue();
							int diff = tempFAlt.get(data.getKey()).getValue() - real;
							setElementText(screen, "S_" + data.getKey().getName(), "(" + real + " + " + diff + ")");
						}
						Map<IElement, IModifiableInteger> resistances = toRead.getItemDefinition().getElementResistancesAlteration();
						for(Entry<IElement, IModifiableInteger> data : resistances.entrySet()){
							setElementText(screen, data.getKey().getName(), "" + data.getValue().getValue());
						}
					}
				}
			}
		});
	}

	public void unequip(String type){
		GUIEquippedItems.getInstance().unequipItemFromType(type);
	}

	private void setElementText(Screen screen, String element, String text){
		Label label = screen.findNiftyControl(element, Label.class);
		if(label != null){
			label.setText(text);
		}
	}

	private void setElementImage(Screen screen, String element, String img){
		NiftyImage newImage = nifty.getRenderEngine().createImage(SharedFunctions.getPath("Textures|items|" + img + ".png"), false);
		Element accountAvatar = screen.findElementByName(element);
		accountAvatar.getRenderer(ImageRenderer.class).setImage(newImage);
	}

	public void closeEquipment(){
		nifty.gotoScreen("empty");
		GameInputManager.getInstance().setInputState(GameInputManager.InputState.IN_GAME);
	}

	public void closeInfo(){
		screen.findElementByName("equipmentInfo").setVisible(false);
	}
	
	public void openEquipment(){
		ClientTest.getInstance().changeNiftyProcessor("equipments");
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("equipments");
				screen.findElementByName("equipmentInfo").setVisible(false);
				GameInputManager.getInstance().setInputState(GameInputManager.InputState.EQUIPPED_ITEMS);
			}
		});
	}
}
