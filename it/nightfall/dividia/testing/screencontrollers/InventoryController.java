package it.nightfall.dividia.testing.screencontrollers;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.RadioButtonGroupStateChangedEvent;
import de.lessvoid.nifty.controls.Scrollbar;
import de.lessvoid.nifty.controls.ScrollbarChangedEvent;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import it.nightfall.dividia.api.item.ISortableItem;
import it.nightfall.dividia.bl.utility.SharedFunctions;
import it.nightfall.dividia.gui.items.GUIInventory;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui.utility.GameInputManager;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

public class InventoryController implements ScreenController {
	private static InventoryController instance = new InventoryController();
	private Screen screen;
	private Nifty nifty;
	private String selectedStorage = "equipment", selectedOrder = "name";
	private String calledFunction = "", buttonText = "";
	Collection<Map<String, ISortableItem>> sortedItems = new ArrayList<>();
	ArrayList<ISortableItem> shownItems = new ArrayList<>();
	private int scrollBarPosition = 0;
	Scrollbar scrollBar;

	private InventoryController(){
	}

	public static InventoryController getInstance(){
		return instance;
	}

	@Override
	public void bind(final Nifty nifty, Screen screen){
		this.nifty = nifty;
	}

	@Override
	public void onStartScreen(){
		screen = nifty.getCurrentScreen();
		scrollBar = screen.findNiftyControl("itemsScrollbar", Scrollbar.class);
		viewStorage(selectedStorage);
	}

	@Override
	public void onEndScreen(){
	}

	public void redraw(){
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				if(screen != null && screen.getScreenId().equals("inventory")){
					viewStorage(selectedStorage);
				}
			}
		});
	}

	public void viewStorage(final String type){
		scrollBarPosition = 0;
		selectedStorage = type;
		orderStorage(selectedOrder);
	}

	@NiftyEventSubscriber(id = "OrderGroup")
	public void sortingChanged(final String id, final RadioButtonGroupStateChangedEvent event){
		scrollBarPosition = 0;
		switch(event.getSelectedId()){
			case "nameOrder":
				selectedOrder = "name";
				break;
			case "priceOrder":
				selectedOrder = "price";
				break;
			case "weightOrder":
				selectedOrder = "weight";
				break;
			case "quantityOrder":
				selectedOrder = "quantity";
				break;
		}
		orderStorage(selectedOrder);
	}

	@NiftyEventSubscriber(id = "itemsScrollbar")
	public void scrollBarMoved(String id, final ScrollbarChangedEvent event){
		scrollBarPosition = (int) event.getValue();
		updateElements();
	}

	public void orderStorage(final String orderBy){
		scrollBar = screen.findNiftyControl("itemsScrollbar", Scrollbar.class);
		switch(selectedStorage){
			case "equipment":
				calledFunction = "equipItem";
				buttonText = "Equip";
				switch(orderBy){
					case "name":
						sortedItems = GUIInventory.getInstance().getEquipmentStorage().getByName();
						break;
					case "price":
						sortedItems = GUIInventory.getInstance().getEquipmentStorage().getByPrice();
						break;
					case "weight":
						sortedItems = GUIInventory.getInstance().getEquipmentStorage().getByWeight();
						break;
				}
				break;
			case "devaorb":
				calledFunction = "useDevaorb";
				buttonText = "Use";
				switch(orderBy){
					case "name":
						sortedItems = GUIInventory.getInstance().getDevaOrbStorage().getByName();
						break;
					case "price":
						sortedItems = GUIInventory.getInstance().getDevaOrbStorage().getByPrice();
						break;
				}
				break;
			case "tropoGem":
				calledFunction = "useTropogem";
				buttonText = "Use";
				switch(orderBy){
					case "name":
						sortedItems = GUIInventory.getInstance().getTropoGemStorage().getByName();
						break;
					case "price":
						sortedItems = GUIInventory.getInstance().getTropoGemStorage().getByPrice();
						break;
					case "weight":
						sortedItems = GUIInventory.getInstance().getTropoGemStorage().getByType();
						break;
				}
				break;
			case "utility":
				calledFunction = "useUtility";
				buttonText = "Use";
				switch(orderBy){
					case "name":
						sortedItems = GUIInventory.getInstance().getUtilityStorage().getByName();
						break;
					case "weight":
						sortedItems = GUIInventory.getInstance().getUtilityStorage().getByWeight();
						break;
				}
				break;
			case "key":
				calledFunction = "useKey";
				buttonText = "Use";
				switch(orderBy){
					case "name":
						sortedItems = GUIInventory.getInstance().getKeyStorage().getByName();
						break;
					case "weight":
						sortedItems = GUIInventory.getInstance().getKeyStorage().getByWeight();
						break;
				}
				break;
			case "vial":
				calledFunction = "useVial";
				buttonText = "Use";
				switch(orderBy){
					case "name":
						sortedItems = GUIInventory.getInstance().getVialStorage().getByName();
						break;
				}
				break;
			case "mysticalSphere":
				calledFunction = "equipItem";
				buttonText = "Equip";
				switch(orderBy){
					case "name":
						sortedItems = GUIInventory.getInstance().getMysticalSphereStorage().getByName();
						break;
					case "weight":
						sortedItems = GUIInventory.getInstance().getMysticalSphereStorage().getByWeight();
						break;
				}
				break;
		}
		scrollBar.setButtonStepSize(1);
		scrollBar.setPageStepSize(1);
		scrollBar.setValue(0);
		int totalItems = 0;
		for(Map<String, ISortableItem> item : sortedItems){
			totalItems += item.size();
		}
		if(totalItems > 5){
			scrollBar.setWorldMax(totalItems);
			scrollBar.setWorldPageSize(5);
		}else{
			scrollBar.setWorldMax(1);
			scrollBar.setWorldPageSize(1);
		}
		updateElements();
	}

	public void updateElements(){
		int count = 0;
		int remaining = 5;
		shownItems.clear();
		for(Map<String, ISortableItem> collidedItems : sortedItems){
			for(Entry<String, ISortableItem> entry : collidedItems.entrySet()){
				if(count >= scrollBarPosition && remaining > 0){
					shownItems.add(entry.getValue());
					remaining--;
				}
				count++;
			}
		}
		for(int i = 0; i < 5; i++){
			Element tempElement = screen.findElementByName("item_" + (i + 1));
			if(shownItems.size() >= i + 1){
				tempElement.setVisible(true);
				ISortableItem item = shownItems.get(i);
				setElementText(screen, "itemLabel_" + (i + 1), item.getItemDefinition().getDisplayName());
				setElementImage(screen, "itemImage_" + (i + 1), item.getItemDefinition().getName());
			}else{
				tempElement.setVisible(false);
			}
		}
	}

	public void useItem(String id){
		try{
			ClientTest.intentionManager.equipItem(shownItems.get(Integer.parseInt(id)).getId());
		}catch(RemoteException ex){
			ex.printStackTrace();
		}
	}

	private void setElementImage(Screen screen, String element, String img){
		NiftyImage newImage = nifty.getRenderEngine().createImage(SharedFunctions.getPath("Textures|items|" + img + ".png"), false);
		Element accountAvatar = screen.findElementByName(element);
		accountAvatar.getRenderer(ImageRenderer.class).setImage(newImage);
	}

	private void setElementText(Screen screen, String element, String text){
		Label label = screen.findNiftyControl(element, Label.class);
		label.setText(text);
	}

	public void closeInventory(){
		
		nifty.gotoScreen("empty");
		GameInputManager.getInstance().setInputState(GameInputManager.InputState.IN_GAME);
	}

	public void openInventory(){
		ClientTest.getInstance().changeNiftyProcessor("inventory");
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("inventory");
				GameInputManager.getInstance().setInputState(GameInputManager.InputState.INVENTORY);
			}
		});
	}
}
