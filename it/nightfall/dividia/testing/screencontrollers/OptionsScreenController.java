package it.nightfall.dividia.testing.screencontrollers;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import it.nightfall.dividia.gui.utility.GUIEventsHandler;
import it.nightfall.dividia.gui_api.utility.IGUIEvent;
import it.nightfall.dividia.testing.ClientTest;
import java.rmi.RemoteException;

public class OptionsScreenController implements ScreenController {
	private static OptionsScreenController instance = new OptionsScreenController();
	private Screen screen;
	private Nifty nifty;

	private OptionsScreenController(){
	}

	public static OptionsScreenController getInstance(){
		return instance;
	}

	@Override
	public void bind(Nifty nifty, Screen screen){
		this.nifty = nifty;
	}

	@Override
	public synchronized void onStartScreen(){
		screen = nifty.getCurrentScreen();
	}

	@Override
	public void onEndScreen(){
	}

	public void showOptions(){
		ClientTest.getInstance().changeNiftyProcessor("options");
		GUIEventsHandler.getInstance().putEvent(new IGUIEvent() {
			@Override
			public void execute(){
				ClientTest.getInstance().getActiveNiftyDisplay().getNifty().gotoScreen("options");
			}
		});
	}
	
	public void exitOptions(){
		nifty.gotoScreen("empty");
	}
	
	public void chengeKeyBindings(){
		
	}
	
	public void saveAndQuit(){
		try{
			ClientTest.intentionManager.disconnect();
		}catch(RemoteException ex){
			ex.printStackTrace();
		}
	}
}
